/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller.old;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Hidden;

/**
 * This part of REST API provides healthcheck of Naming application.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/rest/healthcheck")
@EnableAutoConfiguration
public class HealthcheckControllerV0 {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /healthcheck - healthcheck()
     */

    /**
     * Perform healthcheck of Naming application in general and healtcheck endpoint in particular.
     * To be used mainly for checking HTTP response code, in particular HTTP STATUS OK - 200.
     *
     * @return server timestamp
     */
    @GetMapping
    public String healthcheck() {
        // return healthcheck as server timestamp
        //     datetime - dateStyle, timeStyle - full

        return SimpleDateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date());
    }

}
