/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.rest.api.v1.INames;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.service.NamesService;
import org.openepics.names.util.ExceptionUtil;
import org.openepics.names.util.LogUtil;
import org.openepics.names.util.ServiceHttpStatusException;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * This part of REST API provides name data for Naming application.
 *
 * @author Lars Johansson
 */
@RestController
@EnableAutoConfiguration
public class NamesController implements INames {

    // note
    //     global exception handler available

    private static final Logger LOGGER = Logger.getLogger(NamesController.class.getName());

    private NamesService namesService;

    @Autowired
    public NamesController(
            NamesService namesService) {
        this.namesService = namesService;
    }

    @Override
    public List<NameElement> createNames(List<NameElement> nameElements) {
        // validate authority
        //     naming user & admin
        // validate
        // do

        try {
            namesService.validateNamesCreate(nameElements);
            return namesService.createNames(nameElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<NameElement> readNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return namesService.readNames(
                    deleted, queryFields, queryValues,
                    orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<NameElement> readNames(
            String name,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return namesService.readNames(name, orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<NameElement> readNamesSystemStructure(
            String mnemonicpath,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return namesService.readNamesSystemStructure(mnemonicpath, orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<NameElement> readNamesDeviceStructure(
            String mnemonicpath,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return namesService.readNamesDeviceStructure(mnemonicpath, orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<NameElement> readNamesHistory(
            String uuid,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return namesService.readNamesHistory(uuid, orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public String equivalenceName(String name) {
        try {
            return namesService.equivalenceName(name);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> existsName(String name) {
        try {
            return new ResponseEntity<>(new ResponseBoolean(namesService.existsName(name)), Response.HEADER_JSON, HttpStatus.OK);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()), Response.HEADER_JSON, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.HEADER_JSON, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> isLegacyName(String name) {
        try {
            return new ResponseEntity<>(new ResponseBoolean(namesService.isLegacyName(name)), Response.HEADER_JSON, HttpStatus.OK);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()), Response.HEADER_JSON, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.HEADER_JSON, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> isValidToCreateName(String name) {
        try {
            return new ResponseEntity<>(new ResponseBoolean(namesService.isValidToCreateName(name)), Response.HEADER_JSON, HttpStatus.OK);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()), Response.HEADER_JSON, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.HEADER_JSON, HttpStatus.OK);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<ResponseBooleanList> validateNamesCreate(List<NameElement> nameElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (NameElement nameElement : nameElements) {
            try {
                namesService.validateNamesCreate(nameElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateNamesUpdate(List<NameElement> nameElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (NameElement nameElement : nameElements) {
            try {
                namesService.validateNamesUpdate(nameElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateNamesDelete(List<NameElement> nameElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (NameElement nameElement : nameElements) {
            try {
                namesService.validateNamesDelete(nameElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<NameElement> updateNames(List<NameElement> nameElements) {
        // validate authority
        //     naming user & admin
        // validate
        // do

        try {
            namesService.validateNamesUpdate(nameElements);
            return namesService.updateNames(nameElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<NameElement> deleteNames(List<NameElement> nameElements) {
        // validate authority
        //     naming user & admin
        // validate
        // do

        try {
          namesService.validateNamesDelete(nameElements);
          return namesService.deleteNames(nameElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

}