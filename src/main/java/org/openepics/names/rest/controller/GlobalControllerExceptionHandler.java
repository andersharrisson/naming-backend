/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.util.ExceptionUtil;
import org.openepics.names.util.ServiceHttpStatusException;
import org.openepics.names.util.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Global exception handler to ensure exceptions are communicated to request origin in a uniform way.
 *
 * @author Lars Johansson
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

    // note
    //     no logging here

    private static final Logger LOGGER = Logger.getLogger(GlobalControllerExceptionHandler.class.getName());

    @ExceptionHandler
    protected ResponseEntity<Response> handleConflict(RuntimeException ex, WebRequest request) {
        LOGGER.log(Level.INFO, "handleConflict, ex.getMessage: " + ex.getMessage());

        Response response = new Response("", "");
        response.setMessage(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED);

        HttpStatus resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (ex instanceof ServiceHttpStatusException) {
            response.setMessage(StringUtils.trimToEmpty(ex.getMessage()));
            response.setDetails(StringUtils.trimToEmpty(((ServiceHttpStatusException) ex).getDetails()));
            resultStatus = ((ServiceHttpStatusException) ex).getHttpStatus();
        }

        return new ResponseEntity<>(response, Response.HEADER_JSON, resultStatus);
    }

}
