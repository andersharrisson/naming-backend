/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller.old;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.old.PartElement;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.NamingConventionUtil;
import org.openepics.names.util.old.PartElementUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.Hidden;

/**
 * This part of REST API provides name part data for Naming application.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/rest/parts")
@EnableAutoConfiguration
public class PartControllerV0 {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /parts/mnemonic/{mnemonic}                - findPartsByMnemonic(String)
           read      GET /parts/mnemonic/search/{mnemonic}         - findPartsByMnemonicSearch(String)
           read      GET /parts/mnemonicPath/search/{mnemonicPath} - findPartsByMnemonicPathSearch(String)
     */

    private static final Logger LOGGER = Logger.getLogger(PartControllerV0.class.getName());

    private HolderIRepositories holderIRepositories;

    @Autowired
    public PartControllerV0(
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository) {

        holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository);
    }

    /**
     * Find name parts by mnemonic.
     * Note mnemonic (exact match, case sensitive).
     *
     * @param mnemonic mnemonic to look for
     * @return a list of name parts
     */
    @GetMapping("/mnemonic/{mnemonic}")
    public List<PartElement> findPartsByMnemonic(@PathVariable("mnemonic") String mnemonic) {
        // note
        //     exact match
        //     case sensitive

        final List<PartElement> partElements = Lists.newArrayList();

        // find in system structure

        List<SystemGroup> systemGroups = holderIRepositories.getSystemGroupRepository().findLatestByMnemonic(mnemonic);
        List<System>      systems      = holderIRepositories.getSystemRepository().findLatestByMnemonic(mnemonic);
        List<Subsystem>   subsystems   = holderIRepositories.getSubsystemRepository().findLatestByMnemonic(mnemonic);

        for (SystemGroup systemGroup : systemGroups) {
            partElements.add(PartElementUtil.getPartElement(systemGroup));
        }
        for (System system : systems) {
            partElements.add(PartElementUtil.getPartElement(system, holderIRepositories));
        }
        for (Subsystem subsystem : subsystems) {
            partElements.add(PartElementUtil.getPartElement(subsystem, holderIRepositories));
        }

        // find in system structure

        List<Discipline> disciplines = holderIRepositories.getDisciplineRepository().findLatestByMnemonic(mnemonic);
        List<DeviceType> deviceTypes = holderIRepositories.getDeviceTypeRepository().findLatestByMnemonic(mnemonic);

        for (Discipline discipline : disciplines) {
            partElements.add(PartElementUtil.getPartElement(discipline));
        }
        for (DeviceType deviceType : deviceTypes) {
            partElements.add(PartElementUtil.getPartElement(deviceType, holderIRepositories));
        }

        LOGGER.log(Level.INFO, "findPartsByMnemonic, mnemonic:          " + mnemonic);
        LOGGER.log(Level.INFO, "findPartsByMnemonic, partElements.size: " + partElements.size());

        return partElements;
    }

    /**
     * Find name parts by mnemonic search.
     * Note mnemonic (search, case sensitive, regex).
     *
     * @param mnemonic mnemonic to search for
     * @return a list of name parts
     */
    @GetMapping("/mnemonic/search/{mnemonic}")
    public List<PartElement> findPartsByMnemonicSearch(@PathVariable("mnemonic") String mnemonic) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<PartElement> partElements = Lists.newArrayList();

        Pattern pattern = null;
        try {
            pattern = Pattern.compile(mnemonic);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }

        // find in system structure

        List<SystemGroup> systemGroups = holderIRepositories.getSystemGroupRepository().findLatest();
        List<System>      systems      = holderIRepositories.getSystemRepository().findLatest();
        List<Subsystem>   subsystems   = holderIRepositories.getSubsystemRepository().findLatest();

        for (SystemGroup systemGroup : systemGroups) {
            String sub = systemGroup.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(systemGroup));
            }
        }
        for (System system : systems) {
            String sub = system.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(system, holderIRepositories));
            }
        }
        for (Subsystem subsystem : subsystems) {
            String sub = subsystem.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(subsystem, holderIRepositories));
            }
        }

        // find in device structure

        List<Discipline> disciplines = holderIRepositories.getDisciplineRepository().findLatest();
        List<DeviceType> deviceTypes = holderIRepositories.getDeviceTypeRepository().findLatest();

        for (Discipline discipline : disciplines) {
            String sub = discipline.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(discipline));
            }
        }
        for (DeviceType deviceType : deviceTypes) {
            String sub = deviceType.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(deviceType, holderIRepositories));
            }
        }

        LOGGER.log(Level.INFO, "getAllPartsByMnemonicSearch, mnemonic:          " + mnemonic);
        LOGGER.log(Level.INFO, "getAllPartsByMnemonicSearch, partElements.size: " + partElements.size());

        return partElements;
    }

    /**
     * Find name parts by mnemonic path search.
     * Note mnemonic path (search, case sensitive, regex).
     *
     * @param mnemonicPath mnemonic path to search for
     * @return a list of name parts
     */
    @GetMapping("/mnemonicPath/search/{mnemonicPath}")
    public List<PartElement> findPartsByMnemonicPathSearch(@PathVariable("mnemonicPath") String mnemonicPath) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<PartElement> partElements = Lists.newArrayList();

        Pattern pattern = null;
        try {
            pattern = Pattern.compile(mnemonicPath);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }

        // find in system structure

        List<SystemGroup> systemGroups = holderIRepositories.getSystemGroupRepository().findLatest();
        List<System>      systems      = holderIRepositories.getSystemRepository().findLatest();
        List<Subsystem>   subsystems   = holderIRepositories.getSubsystemRepository().findLatest();

        for (SystemGroup systemGroup : systemGroups) {
            String sub = systemGroup.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(systemGroup));
            }
        }
        for (System system : systems) {
            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            String sub = NamingConventionUtil.mnemonicPath2String(systemGroup.getName(), system.getName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(system, holderIRepositories));
            }
        }
        for (Subsystem subsystem : subsystems) {
            System      system      = holderIRepositories.getSystemRepository().findLatestByUuid(subsystem.getParentUuid().toString());
            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            String sub = NamingConventionUtil.mnemonicPath2String(systemGroup.getMnemonic(), system.getMnemonic(), subsystem.getMnemonic());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(subsystem, holderIRepositories));
            }
        }

        // find in device structure

        List<Discipline> disciplines = holderIRepositories.getDisciplineRepository().findLatest();
        List<DeviceType> deviceTypes = holderIRepositories.getDeviceTypeRepository().findLatest();

        for (Discipline discipline : disciplines) {
            String sub = discipline.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(discipline));
            }
        }
        for (DeviceType deviceType : deviceTypes) {
            DeviceGroup deviceGroup = holderIRepositories.getDeviceGroupRepository().findLatestByUuid(deviceType.getParentUuid().toString());
            Discipline  discipline  = holderIRepositories.getDisciplineRepository().findLatestByUuid(deviceGroup.getParentUuid().toString());

            String sub = NamingConventionUtil.mnemonicPath2String(discipline.getMnemonic(), deviceType.getMnemonic());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(deviceType, holderIRepositories));
            }
        }

        LOGGER.log(Level.INFO, "findPartsByMnemonicPathSearch, mnemonicPath:      " + mnemonicPath);
        LOGGER.log(Level.INFO, "findPartsByMnemonicPathSearch, partElements.size: " + partElements.size());

        return partElements;
    }

}
