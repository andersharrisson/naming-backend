/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.old.model.DeviceRevision;
import org.openepics.names.old.model.NamePartRevision;
import org.openepics.names.old.model.NamePartRevisionStatus;
import org.openepics.names.old.nameviews.NameViewProvider;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.repository.old.IDeviceRevisionRepository;
import org.openepics.names.repository.old.INamePartRevisionRepository;
import org.openepics.names.util.HolderIRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Hidden;

/**
 * This part of REST API provides verification of data migration for Naming application.
 *
 * <p>Prerequisite(s)
 * <ul>
 * <li>both old and migrated database available</li>
 * </ul>
 *
 * <p>
 * Ideally, some knowledge of database tables and object structures acquired to dig into this class.
 * It can be done in any case and there is documentation available.
 * It is recommended to have database tables and object structures available.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/verification")
@EnableAutoConfiguration
public class VerificationController {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /verification/migration_devicerevision   - readMigrationDeviceRevision()
           read      GET /verification/migration_namepartrevision - readMigrationNamePartRevision
           read      GET /verification/data_reachable             - readDataReachable()
           read      GET /verification/restapi_oldvsnew           - readRestApiOldVsNew()
     */

    private static final Logger LOGGER = Logger.getLogger(VerificationController.class.getName());

    private static final String NEW_LINE_BR = "<br/>";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private HolderIRepositories holderIRepositories;

    @Autowired
    IDeviceRevisionRepository deviceRevisionRepository;
    @Autowired
    INamePartRevisionRepository namePartRevisionRepository;

    @Autowired
    public VerificationController(
            INameRepository nameRepository,
            ISystemGroupRepository systemGroupRepository,
            ISystemRepository systemRepository,
            ISubsystemRepository subsystemRepository,
            IDisciplineRepository disciplineRepository,
            IDeviceGroupRepository deviceGroupRepository,
            IDeviceTypeRepository deviceTypeRepository) {

        holderIRepositories = new HolderIRepositories(
                nameRepository,
                systemGroupRepository,
                systemRepository,
                subsystemRepository,
                disciplineRepository,
                deviceGroupRepository,
                deviceTypeRepository);
    }

    /**
     * Perform verification of data migration with focus on device revision.
     * Ok if all entries ok and no entry nok.
     *
     * @return report of data migration
     */
    @GetMapping("/migration_devicerevision")
    public String readMigrationDeviceRevision() {
        // verification of
        //     name vs. devicerevision, device
        //     with help of namepartrevision, namepart

        // note
        //     check entry by entry
        //     ----------
        //     to check  1st, 2nd, 3rd parent to determine if systemgroup, system or subsystem
        //     otherwise not clear how to make sure if it is supposed to be systemgroup and not system, subsystem and vice versa
        //     ----------
        //     date may be in different format for different objects, to be formatted before being compared
        //     ----------
        //     name.id                                = devicerevision.id
        //     name.version                           = devicerevision.version
        //     name.uuid                              = devicerevision.device_id             (device.id      --> device.uuid)
        //     name.namepartrevision_systemgroup_uuid = devicerevision.section_id            (namepart.id    --> namepart.uuid
        //     name.namepartrevision_system_uuid      = devicerevision.section_id            (namepart.id    --> namepart.uuid
        //     name.namepartrevision_subsystem_uuid   = devicerevision.section_id            (namepart.id    --> namepart.uuid
        //     name.namepartrevision_devicetype_uuid  = devicerevision.devicetype_id         (namepart.id    --> namepart.uuid
        //     name.instance_index                    = devicerevision.instanceindex
        //     name.convention_name                   = devicerevision.conventionname
        //     name.convention_name_equivalence       = devicerevision.conventionnameeqclass
        //     name.description                       = devicerevision.additionalinfo
        //     name.status                            = null
        //     name.latest                            = true if id = get max id for uuid     (not consider status)
        //     name.deleted                           = devicerevision.deleted
        //     name.requested                         = devicerevision.requestdate
        //     name.requested_by                      = devicerevision.requestedby_id        (useraccount.id --> useraccount.username)
        //     name.requested_comment                 = null
        //     name.processed                         = null
        //     name.processed_by                      = null
        //     name.processed_comment                 = devicerevision.processorcomment

        StringBuilder reportHtml = new StringBuilder();

        // find data
        //     for verification
        //         names
        //     to support
        //         device names
        //         name part revisions

        List<Name>             names             = holderIRepositories.getNameRepository().findAll();
        List<DeviceRevision>   deviceNames       = deviceRevisionRepository.findAll();
        List<NamePartRevision> namePartRevisions = namePartRevisionRepository.findAll();

        prepareLogReport("readMigrationDeviceRevision, find data,     names.size:                       " + names.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, find data,     deviceNames.size:                 " + deviceNames.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, find data,     namePartRevisions.size:           " + namePartRevisions.size(), reportHtml);

        // utility
        //     interpret lists into hashmaps for faster retrieval in for loop below
        //     used to help check name entries
        //     ----------
        //     mapIdDeviceRevision     - find corresponding (old) device revision for given id
        //     mapUuidMaxIdName        - find out latest name id for given uuid
        //     mapUuidNamePartRevision - find out name part revision for given uuid

        HashMap<Long, DeviceRevision> mapIdDeviceRevision = new HashMap<>((int)(deviceNames.size()/0.75 + 2));
        for (DeviceRevision deviceRevision : deviceNames) {
            mapIdDeviceRevision.put(deviceRevision.getId(), deviceRevision);
        }
        HashMap<UUID, Long> mapUuidMaxIdName = new HashMap<>();
        for (Name name : names) {
            if (mapUuidMaxIdName.get(name.getUuid()) == null
                    ||  name.getId() > mapUuidMaxIdName.get(name.getUuid())) {
                mapUuidMaxIdName.put(name.getUuid(), name.getId());
            }
        }
        HashMap<UUID, NamePartRevision> mapUuidNamePartRevision = new HashMap<>((int)(namePartRevisions.size()/0.75 + 2));
        for (NamePartRevision namePartRevision : namePartRevisions) {
            mapUuidNamePartRevision.put(namePartRevision.getNamePart().getUuid(), namePartRevision);
        }

        prepareLogReport("readMigrationDeviceRevision, utility,       mapIdDeviceRevision.size:         " + mapIdDeviceRevision.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, utility,       mapUuidMaxIdName.size:            " + mapUuidMaxIdName.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, utility,       mapUuidNamePartRevision.size:     " + mapUuidNamePartRevision.size(), reportHtml);

        // keep track of id
        //     ok
        //     nok
        SortedSet<Long> id_ok_deviceRevisionDevice  = new TreeSet<>();
        SortedSet<Long> id_nok_deviceRevisionDevice = new TreeSet<>();

        prepareLogReport("readMigrationDeviceRevision, check, before, id_ok_deviceRevisionDevice.size:  " + id_ok_deviceRevisionDevice.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, check, before, id_nok_deviceRevisionDevice.size: " + id_nok_deviceRevisionDevice.size(), reportHtml);

        // name
        // check entry by entry
        //     each attribute as expected
        boolean check = false;
        DeviceRevision deviceRevision = null;
        for (Name name : names) {
            check = true;
            deviceRevision = mapIdDeviceRevision.get(name.getId());

            check = deviceRevision != null;

            check = check && name.getId().equals(deviceRevision.getId());
            check = check && name.getVersion().equals(deviceRevision.getVersion());
            check = check && name.getUuid().equals(deviceRevision.getDevice().getUuid());

            // to check  1st, 2nd, 3rd parent to determine if systemgroup, system or subsystem
            // otherwise not clear how to make sure if it is supposed to be systemgroup and not system, subsystem and vice versa
            NamePartRevision parent1 = deviceRevision.getSection() != null
                    ? mapUuidNamePartRevision.get(deviceRevision.getSection().getUuid())
                    : null;
            NamePartRevision parent2 = parent1 != null && parent1.getParent() != null
                    ? mapUuidNamePartRevision.get(parent1.getParent().getUuid())
                    : null;
            NamePartRevision parent3 = parent2 != null && parent2.getParent() != null
                    ? mapUuidNamePartRevision.get(parent2.getParent().getUuid())
                    : null;
            // parent 1    but not parent 2, 3 - system group
            // parent 1, 2 but not parent 3    - system
            // parent 1, 2, 3                  - subsystem
            // else nok
            UUID systemGroupUuid = name.getSystemgroupUuid();
            UUID systemUuid = name.getSystemUuid();
            UUID subsystemUuid = name.getSubsystemUuid();
            UUID sectionUuid = deviceRevision.getSection().getUuid();
            if (parent1 != null && parent2 == null && parent3 == null) {
                check = check && sectionUuid.equals(systemGroupUuid) && systemUuid == null && subsystemUuid == null;
            } else if (parent1 != null && parent2 != null && parent3 == null) {
                check = check && sectionUuid.equals(systemUuid) && systemGroupUuid == null && subsystemUuid == null;
            } else if (parent1 != null && parent2 != null && parent3 != null) {
                check = check && sectionUuid.equals(subsystemUuid) && systemGroupUuid == null && systemUuid == null;
            } else {
                check = false;
            }

            check = check && ((name.getDevicetypeUuid() == null && deviceRevision.getDeviceType() == null)
                                || (name.getDevicetypeUuid().equals(deviceRevision.getDeviceType().getUuid())));
            check = check && StringUtils.equals(name.getInstanceIndex(), deviceRevision.getInstanceIndex());
            check = check && StringUtils.equals(name.getConventionName(), deviceRevision.getConventionName());
            check = check && StringUtils.equals(name.getConventionNameEquivalence(), deviceRevision.getConventionNameEqClass());
            check = check && StringUtils.equals(name.getDescription(), deviceRevision.getAdditionalInfo());
            check = check && name.getStatus() == null;

            // latest
            //     true if id = get max id for uuid
            check = check && name.isLatest() == name.getId().equals(mapUuidMaxIdName.get(name.getUuid()));
            check = check && name.isDeleted() == deviceRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((name.getRequested() == null && deviceRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(name.getRequested()), SDF.format(deviceRevision.getRequestDate())));
            check = check && StringUtils.equals(name.getRequestedBy(), deviceRevision.getRequestedBy().getUsername());
            check = check && name.getRequestedComment() == null;
            check = check && name.getProcessed() == null;
            check = check && name.getProcessedBy() == null;
            check = check && StringUtils.equals(name.getProcessedComment(), deviceRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_deviceRevisionDevice.add(name.getId());
            } else {
                id_nok_deviceRevisionDevice.add(name.getId());
            }
        }

        // ok if
        //     all entries ok
        //     no entry nok
        boolean ok = id_ok_deviceRevisionDevice.size() == names.size()
                && id_nok_deviceRevisionDevice.size() == 0;

        prepareLogReport("readMigrationDeviceRevision, check, after,  id_ok_deviceRevisionDevice.size:  " + id_ok_deviceRevisionDevice.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, check, after,  id_nok_deviceRevisionDevice.size: " + id_nok_deviceRevisionDevice.size(), reportHtml);
        prepareLogReport("readMigrationDeviceRevision, ok: " + ok, reportHtml);

        return reportHtml.toString();
    }

    /**
     * Perform verification of data migration with focus on name part revision.
     * Ok if all entries ok and no entry nok.
     *
     * @return report of data migration
     */
    @GetMapping("/migration_namepartrevision")
    public String readMigrationNamePartRevision() {
        // verification of
        //     systemgroup
        //     system
        //     subsystem
        //     discipline
        //     devicegroup
        //     devicetype
        //         vs.
        //     namepartrevision
        //     with help of namepartrevision, namepart

        // note
        //     check entry by entry
        //     ----------
        //     to check parent uuid
        //     ----------
        //     date may be in different format for different objects, to be formatted before being compared
        //     ----------
        //     e.g.
        //     system.id                  = namepartrevision.id
        //     system.version             = namepartrevision.version
        //     system.uuid                = namepartrevision.namepart_id      (namepart.id    --> namepart.uuid)
        //     ( system.parent_uuid       = namepartrevision.parent_id        (namepart.id    --> namepart.uuid) )
        //     system.name                = namepartrevision.name
        //     system.mnemonic            = namepartrevision.mnemonic
        //     system.mnemonicequivalence = namepartrevision.mnemoniceqclass
        //     system.description         = namepartrevision.description
        //     system.status              = namepartrevision.status
        //     system.latest              = true if id = get max id for uuid  (consider status, but not PENDING)
        //     system.deleted             = namepartrevision.deleted
        //     system.requested           = namepartrevision.requestdate
        //     system.requested_by        = namepartrevision.requestedby_id   (useraccount.id --> useraccount.username)
        //     system.requested_comment   = namepartrevision.requestercomment
        //     system.processed           = namepartrevision.processdate
        //     system.processed_by        = namepartrevision.processedby_id   (useraccount.id --> useraccount.username)
        //     system.processed_comment   = namepartrevision.processorcomment

        StringBuilder reportHtml = new StringBuilder();

        // find data
        //     for verification
        //         system groups
        //         systems
        //         subsystems
        //         disciplines
        //         device groups
        //         device types
        //     to support
        //         name part revisions

        List<SystemGroup>      systemGroups      = holderIRepositories.getSystemGroupRepository().findAll();
        List<System>           systems           = holderIRepositories.getSystemRepository().findAll();
        List<Subsystem>        subsystems        = holderIRepositories.getSubsystemRepository().findAll();
        List<Discipline>       disciplines       = holderIRepositories.getDisciplineRepository().findAll();
        List<DeviceGroup>      deviceGroups      = holderIRepositories.getDeviceGroupRepository().findAll();
        List<DeviceType>       deviceTypes       = holderIRepositories.getDeviceTypeRepository().findAll();
        List<NamePartRevision> namePartRevisions = namePartRevisionRepository.findAll();

        prepareLogReport("readMigrationNamePartRevision, find data,                systemGroups.size:            " + systemGroups.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, find data,                systems.size:                 " + systems.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, find data,                subsystems.size:              " + subsystems.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, find data,                disciplines.size:             " + disciplines.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, find data,                deviceGroups.size:            " + deviceGroups.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, find data,                deviceTypes.size:             " + deviceTypes.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, find data,                namePartRevisions.size:       " + namePartRevisions.size(), reportHtml);

        // utility
        //     interpret lists into hashmaps for faster retrieval in for loop below
        //     used to help check name entries
        //     ----------
        //     mapIdNamePartRevision   - find corresponding (old) name part revision for given id
        //     mapUuidMaxIdSystemGroup - find out latest system group id for given uuid
        //     mapUuidMaxIdSystem      - find out latest system id for given uuid
        //     mapUuidMaxIdSubsystem   - find out latest subsystem id for given uuid
        //     mapUuidMaxIdDiscipline  - find out latest discipline id for given uuid
        //     mapUuidMaxIdDeviceGroup - find out latest device group id for given uuid
        //     mapUuidMaxIdDeviceType  - find out latest device type id for given uuid

        HashMap<Long, NamePartRevision> mapIdNamePartRevision = new HashMap<>((int)(namePartRevisions.size()/0.75 + 2));
        for (NamePartRevision namePartRevision : namePartRevisions) {
            mapIdNamePartRevision.put(namePartRevision.getId(), namePartRevision);
        }
        HashMap<UUID, Long> mapUuidMaxIdSystemGroup = new HashMap<>();
        for (SystemGroup systemGroup : systemGroups) {
            if (		(mapUuidMaxIdSystemGroup.get(systemGroup.getUuid()) == null
                        ||  systemGroup.getId() > mapUuidMaxIdSystemGroup.get(systemGroup.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(systemGroup.getStatus().name())) {
                mapUuidMaxIdSystemGroup.put(systemGroup.getUuid(), systemGroup.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdSystem = new HashMap<>();
        for (System system : systems) {
            if ((		mapUuidMaxIdSystem.get(system.getUuid()) == null
                        ||  system.getId() > mapUuidMaxIdSystem.get(system.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(system.getStatus().name())) {
                mapUuidMaxIdSystem.put(system.getUuid(), system.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdSubsystem = new HashMap<>();
        for (Subsystem subsystem : subsystems) {
            if (		(mapUuidMaxIdSubsystem.get(subsystem.getUuid()) == null
                        ||  subsystem.getId() > mapUuidMaxIdSubsystem.get(subsystem.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(subsystem.getStatus().name())) {
                mapUuidMaxIdSubsystem.put(subsystem.getUuid(), subsystem.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdDiscipline = new HashMap<>();
        for (Discipline discipline : disciplines) {
            if ((mapUuidMaxIdDiscipline.get(discipline.getUuid()) == null
                    ||  discipline.getId() > mapUuidMaxIdDiscipline.get(discipline.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(discipline.getStatus().name())) {
                mapUuidMaxIdDiscipline.put(discipline.getUuid(), discipline.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdDeviceGroup = new HashMap<>();
        for (DeviceGroup deviceGroup : deviceGroups) {
            if ((mapUuidMaxIdDeviceGroup.get(deviceGroup.getUuid()) == null
                    ||  deviceGroup.getId() > mapUuidMaxIdDeviceGroup.get(deviceGroup.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(deviceGroup.getStatus().name())) {
                mapUuidMaxIdDeviceGroup.put(deviceGroup.getUuid(), deviceGroup.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdDeviceType = new HashMap<>();
        for (DeviceType deviceType : deviceTypes) {
            if ((mapUuidMaxIdDeviceType.get(deviceType.getUuid()) == null
                    ||  deviceType.getId() > mapUuidMaxIdDeviceType.get(deviceType.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(deviceType.getStatus().name())) {
                mapUuidMaxIdDeviceType.put(deviceType.getUuid(), deviceType.getId());
            }
        }

        prepareLogReport("readMigrationNamePartRevision, utility,                  mapIdNamePartRevision.size:   " + mapIdNamePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, utility,                  mapUuidMaxIdSystemGroup.size: " + mapUuidMaxIdSystemGroup.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, utility,                  mapUuidMaxIdSystem.size:      " + mapUuidMaxIdSystem.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, utility,                  mapUuidMaxIdSubsystem.size:   " + mapUuidMaxIdSubsystem.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, utility,                  mapUuidMaxIdDiscipline.size:  " + mapUuidMaxIdDiscipline.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, utility,                  mapUuidMaxIdDeviceGroup.size: " + mapUuidMaxIdDeviceGroup.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, utility,                  mapUuidMaxIdDeviceType.size:  " + mapUuidMaxIdDeviceType.size(), reportHtml);

        // keep track of id
        //     ok
        //     nok
        SortedSet<Long> id_ok_namePartRevision  = new TreeSet<>();
        SortedSet<Long> id_nok_namePartRevision = new TreeSet<>();

        prepareLogReport("readMigrationNamePartRevision, check, before,            id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, before,            id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // system group
        // check entry by entry
        //         each attribute as expected
        boolean check = false;
        NamePartRevision namePartRevision = null;
        for (SystemGroup systemGroup : systemGroups) {
            check = true;
            namePartRevision = mapIdNamePartRevision.get(systemGroup.getId());

            check = namePartRevision != null;

            check = check && systemGroup.getId().equals(namePartRevision.getId());
            check = check && systemGroup.getVersion().equals(namePartRevision.getVersion());
            check = check && systemGroup.getUuid().equals(namePartRevision.getNamePart().getUuid());

            // no parent uuid for system group
            check = check && StringUtils.equals(systemGroup.getName(), namePartRevision.getName());
            check = check && StringUtils.equals(systemGroup.getMnemonic(), namePartRevision.getMnemonic());
            check = check && StringUtils.equals(systemGroup.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
            check = check && StringUtils.equals(systemGroup.getDescription(), namePartRevision.getDescription());
            check = check && ((systemGroup.getStatus() == null && namePartRevision.getStatus() == null)
                                || (systemGroup.getStatus().name().equals(namePartRevision.getStatus().name())));

            // latest
            //     true if id = get max id for uuid
            //     special rules for pending, not consider pending
            check = check && systemGroup.isLatest() == systemGroup.getId().equals(mapUuidMaxIdSystemGroup.get(systemGroup.getUuid()));
            check = check && systemGroup.isDeleted() == namePartRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((systemGroup.getRequested() == null && namePartRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(systemGroup.getRequested()), SDF.format(namePartRevision.getRequestDate())));
            check = check && (systemGroup.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                                || 	StringUtils.equals(systemGroup.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
            check = check && StringUtils.equals(systemGroup.getRequestedComment(), namePartRevision.getRequesterComment());
            check = check && ((systemGroup.getProcessed() == null && namePartRevision.getProcessDate() == null)
                                || StringUtils.equals(SDF.format(systemGroup.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
            check = check && (systemGroup.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                                || 	StringUtils.equals(systemGroup.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
            check = check && StringUtils.equals(systemGroup.getProcessedComment(), namePartRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_namePartRevision.add(systemGroup.getId());
            } else {
                id_nok_namePartRevision.add(systemGroup.getId());
            }
        }
        prepareLogReport("readMigrationNamePartRevision, check, after systemgroup, id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after systemgroup, id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // system
        // check entry by entry
        //         each attribute as expected
        for (System system : systems) {
            check = true;
            namePartRevision = mapIdNamePartRevision.get(system.getId());

            check = namePartRevision != null;

            check = check && system.getId().equals(namePartRevision.getId());
            check = check && system.getVersion().equals(namePartRevision.getVersion());
            check = check && system.getUuid().equals(namePartRevision.getNamePart().getUuid());

            // parent uuid
            check = check && system.getParentUuid().equals(namePartRevision.getParent().getUuid());
            check = check && StringUtils.equals(system.getName(), namePartRevision.getName());
            check = check && StringUtils.equals(system.getMnemonic(), namePartRevision.getMnemonic());
            check = check && StringUtils.equals(system.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
            check = check && StringUtils.equals(system.getDescription(), namePartRevision.getDescription());
            check = check && ((system.getStatus() == null && namePartRevision.getStatus() == null)
                                || (system.getStatus().name().equals(namePartRevision.getStatus().name())));

            // latest
            //     true if id = get max id for uuid
            //     special rules for pending, not consider pending
            check = check && system.isLatest() == system.getId().equals(mapUuidMaxIdSystem.get(system.getUuid()));
            check = check && system.isDeleted() == namePartRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((system.getRequested() == null && namePartRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(system.getRequested()), SDF.format(namePartRevision.getRequestDate())));
            check = check && (system.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                                || 	StringUtils.equals(system.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
            check = check && StringUtils.equals(system.getRequestedComment(), namePartRevision.getRequesterComment());
            check = check && ((system.getProcessed() == null && namePartRevision.getProcessDate() == null)
                                || StringUtils.equals(SDF.format(system.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
            check = check && (system.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                                || 	StringUtils.equals(system.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
            check = check && StringUtils.equals(system.getProcessedComment(), namePartRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_namePartRevision.add(system.getId());
            } else {
                id_nok_namePartRevision.add(system.getId());
            }
        }
        prepareLogReport("readMigrationNamePartRevision, check, after system,      id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after system,      id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // subsystem
        // check entry by entry
        //         each attribute as expected
        for (Subsystem subsystem : subsystems) {
            check = true;
            namePartRevision = mapIdNamePartRevision.get(subsystem.getId());

            check = namePartRevision != null;

            check = check && subsystem.getId().equals(namePartRevision.getId());
            check = check && subsystem.getVersion().equals(namePartRevision.getVersion());
            check = check && subsystem.getUuid().equals(namePartRevision.getNamePart().getUuid());

            // parent uuid
            check = check && subsystem.getParentUuid().equals(namePartRevision.getParent().getUuid());
            check = check && StringUtils.equals(subsystem.getName(), namePartRevision.getName());
            check = check && StringUtils.equals(subsystem.getMnemonic(), namePartRevision.getMnemonic());
            check = check && StringUtils.equals(subsystem.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
            check = check && StringUtils.equals(subsystem.getDescription(), namePartRevision.getDescription());
            check = check && ((subsystem.getStatus() == null && namePartRevision.getStatus() == null)
                                || (subsystem.getStatus().name().equals(namePartRevision.getStatus().name())));

            // latest
            //     true if id = get max id for uuid
            //     special rules for pending, not consider pending
            check = check && subsystem.isLatest() == subsystem.getId().equals(mapUuidMaxIdSubsystem.get(subsystem.getUuid()));
            check = check && subsystem.isDeleted() == namePartRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((subsystem.getRequested() == null && namePartRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(subsystem.getRequested()), SDF.format(namePartRevision.getRequestDate())));
            check = check && (subsystem.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                                || 	StringUtils.equals(subsystem.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
            check = check && StringUtils.equals(subsystem.getRequestedComment(), namePartRevision.getRequesterComment());
            check = check && ((subsystem.getProcessed() == null && namePartRevision.getProcessDate() == null)
                                || StringUtils.equals(SDF.format(subsystem.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
            check = check && (subsystem.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                                || 	StringUtils.equals(subsystem.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
            check = check && StringUtils.equals(subsystem.getProcessedComment(), namePartRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_namePartRevision.add(subsystem.getId());
            } else {
                id_nok_namePartRevision.add(subsystem.getId());
            }
        }
        prepareLogReport("readMigrationNamePartRevision, check, after subsystem,   id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after subsystem,   id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // discipline
        // check entry by entry
        //         each attribute as expected
        for (Discipline discipline : disciplines) {
            check = true;
            namePartRevision = mapIdNamePartRevision.get(discipline.getId());

            check = namePartRevision != null;

            check = check && discipline.getId().equals(namePartRevision.getId());
            check = check && discipline.getVersion().equals(namePartRevision.getVersion());
            check = check && discipline.getUuid().equals(namePartRevision.getNamePart().getUuid());

            // no parent uuid for discipline
            check = check && StringUtils.equals(discipline.getName(), namePartRevision.getName());
            check = check && StringUtils.equals(discipline.getMnemonic(), namePartRevision.getMnemonic());
            check = check && StringUtils.equals(discipline.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
            check = check && StringUtils.equals(discipline.getDescription(), namePartRevision.getDescription());
            check = check && ((discipline.getStatus() == null && namePartRevision.getStatus() == null)
                                || (discipline.getStatus().name().equals(namePartRevision.getStatus().name())));

            // latest
            //     true if id = get max id for uuid
            //     special rules for pending, not consider pending
            check = check && discipline.isLatest() == discipline.getId().equals(mapUuidMaxIdDiscipline.get(discipline.getUuid()));
            check = check && discipline.isDeleted() == namePartRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((discipline.getRequested() == null && namePartRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(discipline.getRequested()), SDF.format(namePartRevision.getRequestDate())));
            check = check && (discipline.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                                || 	StringUtils.equals(discipline.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
            check = check && StringUtils.equals(discipline.getRequestedComment(), namePartRevision.getRequesterComment());
            check = check && ((discipline.getProcessed() == null && namePartRevision.getProcessDate() == null)
                                || StringUtils.equals(SDF.format(discipline.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
            check = check && (discipline.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                                || 	StringUtils.equals(discipline.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
            check = check && StringUtils.equals(discipline.getProcessedComment(), namePartRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_namePartRevision.add(discipline.getId());
            } else {
                id_nok_namePartRevision.add(discipline.getId());
            }
        }
        prepareLogReport("readMigrationNamePartRevision, check, after discipline,  id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after discipline,  id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // device group
        // check entry by entry
        //         each attribute as expected
        for (DeviceGroup deviceGroup : deviceGroups) {
            check = true;
            namePartRevision = mapIdNamePartRevision.get(deviceGroup.getId());

            check = namePartRevision != null;

            check = check && deviceGroup.getId().equals(namePartRevision.getId());
            check = check && deviceGroup.getVersion().equals(namePartRevision.getVersion());
            check = check && deviceGroup.getUuid().equals(namePartRevision.getNamePart().getUuid());

            // parent uuid
            check = check && deviceGroup.getParentUuid().equals(namePartRevision.getParent().getUuid());
            check = check && StringUtils.equals(deviceGroup.getName(), namePartRevision.getName());
            check = check && StringUtils.equals(deviceGroup.getMnemonic(), namePartRevision.getMnemonic());
            check = check && StringUtils.equals(deviceGroup.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
            check = check && StringUtils.equals(deviceGroup.getDescription(), namePartRevision.getDescription());
            check = check && ((deviceGroup.getStatus() == null && namePartRevision.getStatus() == null)
                                || (deviceGroup.getStatus().name().equals(namePartRevision.getStatus().name())));

            // latest
            //     true if id = get max id for uuid
            //     special rules for pending, not consider pending
            check = check && deviceGroup.isLatest() == deviceGroup.getId().equals(mapUuidMaxIdDeviceGroup.get(deviceGroup.getUuid()));
            check = check && deviceGroup.isDeleted() == namePartRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((deviceGroup.getRequested() == null && namePartRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(deviceGroup.getRequested()), SDF.format(namePartRevision.getRequestDate())));
            check = check && (deviceGroup.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                                || 	StringUtils.equals(deviceGroup.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
            check = check && StringUtils.equals(deviceGroup.getRequestedComment(), namePartRevision.getRequesterComment());
            check = check && ((deviceGroup.getProcessed() == null && namePartRevision.getProcessDate() == null)
                                || StringUtils.equals(SDF.format(deviceGroup.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
            check = check && (deviceGroup.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                                || 	StringUtils.equals(deviceGroup.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
            check = check && StringUtils.equals(deviceGroup.getProcessedComment(), namePartRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_namePartRevision.add(deviceGroup.getId());
            } else {
                id_nok_namePartRevision.add(deviceGroup.getId());
            }
        }
        prepareLogReport("readMigrationNamePartRevision, check, after devicegroup, id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after devicegroup, id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // device type
        // check entry by entry
        //         each attribute as expected
        for (DeviceType deviceType : deviceTypes) {
            check = true;
            namePartRevision = mapIdNamePartRevision.get(deviceType.getId());

            check = namePartRevision != null;

            check = check && deviceType.getId().equals(namePartRevision.getId());
            check = check && deviceType.getVersion().equals(namePartRevision.getVersion());
            check = check && deviceType.getUuid().equals(namePartRevision.getNamePart().getUuid());

            // parent uuid
            check = check && deviceType.getParentUuid().equals(namePartRevision.getParent().getUuid());
            check = check && StringUtils.equals(deviceType.getName(), namePartRevision.getName());
            check = check && StringUtils.equals(deviceType.getMnemonic(), namePartRevision.getMnemonic());
            check = check && StringUtils.equals(deviceType.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
            check = check && StringUtils.equals(deviceType.getDescription(), namePartRevision.getDescription());
            check = check && ((deviceType.getStatus() == null && namePartRevision.getStatus() == null)
                                || (deviceType.getStatus().name().equals(namePartRevision.getStatus().name())));

            // latest
            //     true if id = get max id for uuid
            //     special rules for pending, not consider pending
            check = check && deviceType.isLatest() == deviceType.getId().equals(mapUuidMaxIdDeviceType.get(deviceType.getUuid()));
            check = check && deviceType.isDeleted() == namePartRevision.isDeleted();

            // date may be in different format for different objects, to be formatted before being compared
            check = check && ((deviceType.getRequested() == null && namePartRevision.getRequestDate() == null)
                                || StringUtils.equals(SDF.format(deviceType.getRequested()), SDF.format(namePartRevision.getRequestDate())));
            check = check && (deviceType.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                                || 	StringUtils.equals(deviceType.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
            check = check && StringUtils.equals(deviceType.getRequestedComment(), namePartRevision.getRequesterComment());
            check = check && ((deviceType.getProcessed() == null && namePartRevision.getProcessDate() == null)
                                || StringUtils.equals(SDF.format(deviceType.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
            check = check && (deviceType.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                                || 	StringUtils.equals(deviceType.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
            check = check && StringUtils.equals(deviceType.getProcessedComment(), namePartRevision.getProcessorComment());

            // add to count
            if (check) {
                id_ok_namePartRevision.add(deviceType.getId());
            } else {
                id_nok_namePartRevision.add(deviceType.getId());
            }
        }
        prepareLogReport("readMigrationNamePartRevision, check, after devicetype,  id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after devicetype,  id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);

        // ok if
        //     all entries ok
        //     no entry nok
        boolean ok = id_ok_namePartRevision.size() == namePartRevisions.size()
                && id_nok_namePartRevision.size() == 0;

        prepareLogReport("readMigrationNamePartRevision, check, after,             id_ok_namePartRevision.size:  " + id_ok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, check, after,             id_nok_namePartRevision.size: " + id_nok_namePartRevision.size(), reportHtml);
        prepareLogReport("readMigrationNamePartRevision, ok:  " + ok, reportHtml);

        return reportHtml.toString();
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Perform verification of data in sense that all data can be reached. Current data is one thing but suggested data (pending) and old data (obsolete) is another thing.
     * All data reached verification can be done like selecting all entries from tables (names, system group, system, subsystem, discipline, device group, device type)
     * and then take uuid and retrieve history for uuid. By that, all entries should be reached. In a sense, select distinct uuid, then retrieve history by uuid,
     * all ids should be encompassed by looking at all returned rows. Requires a couple of for loops and maps and sets to keep track of things.
     *
     * This verification concerns itself with new database, not old database.
     *
     * @return report of reachability of data
     */
    @GetMapping("/data_reachable")
    public String readDataReachable() {
        StringBuilder reportHtml = new StringBuilder();

        List<Name>             names             = holderIRepositories.getNameRepository().findAll();
        List<SystemGroup>      systemGroups      = holderIRepositories.getSystemGroupRepository().findAll();
        List<System>           systems           = holderIRepositories.getSystemRepository().findAll();
        List<Subsystem>        subsystems        = holderIRepositories.getSubsystemRepository().findAll();
        List<Discipline>       disciplines       = holderIRepositories.getDisciplineRepository().findAll();
        List<DeviceGroup>      deviceGroups      = holderIRepositories.getDeviceGroupRepository().findAll();
        List<DeviceType>       deviceTypes       = holderIRepositories.getDeviceTypeRepository().findAll();

        prepareLogReport("readDataReachable, find data,                names.size:            " + names.size(), reportHtml);
        prepareLogReport("readDataReachable, find data,                systemGroups.size:     " + systemGroups.size(), reportHtml);
        prepareLogReport("readDataReachable, find data,                systems.size:          " + systems.size(), reportHtml);
        prepareLogReport("readDataReachable, find data,                subsystems.size:       " + subsystems.size(), reportHtml);
        prepareLogReport("readDataReachable, find data,                disciplines.size:      " + disciplines.size(), reportHtml);
        prepareLogReport("readDataReachable, find data,                deviceGroups.size:     " + deviceGroups.size(), reportHtml);
        prepareLogReport("readDataReachable, find data,                deviceTypes.size:      " + deviceTypes.size(), reportHtml);

        HashSet<Long> setIdName        = new HashSet<>();
        HashSet<Long> setIdSystemGroup = new HashSet<>();
        HashSet<Long> setIdSystem      = new HashSet<>();
        HashSet<Long> setIdSubsystem   = new HashSet<>();
        HashSet<Long> setIdDiscipline  = new HashSet<>();
        HashSet<Long> setIdDeviceGroup = new HashSet<>();
        HashSet<Long> setIdDeviceType  = new HashSet<>();

        List<Name>             namesByUuid             = null;
        List<SystemGroup>      systemGroupsByUuid      = null;
        List<System>           systemsByUuid           = null;
        List<Subsystem>        subsystemsByUuid        = null;
        List<Discipline>       disciplinesByUuid       = null;
        List<DeviceGroup>      deviceGroupsByUuid      = null;
        List<DeviceType>       deviceTypesByUuid       = null;

        prepareLogReport("readDataReachable, check, before", reportHtml);

        // get started
        //     go through list(s)
        //     get entries per uuid - to mimic find history through REST API by uuid
        //     keep track of ids in set(s)
        //     ok if set(s) size same as list(s) size
        //     any entry not in set or once in set

        for (Name name : names) {
            // to mimic - Find history for name by uuid
            namesByUuid = holderIRepositories.getNameRepository().findByUuid(name.getUuid().toString());
            for (Name nameByUuid : namesByUuid) {
                setIdName.add(nameByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after name,        setIdName.size:        " + setIdName.size(), reportHtml);

        for (SystemGroup systemGroup : systemGroups) {
            // to mimic - Find history for system group by uuid
            systemGroupsByUuid = holderIRepositories.getSystemGroupRepository().findByUuid(systemGroup.getUuid().toString());
            for (SystemGroup systemGroupByUuid : systemGroupsByUuid) {
                setIdSystemGroup.add(systemGroupByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after systemgroup, setIdSystemGroup.size: " + setIdSystemGroup.size(), reportHtml);

        for (System system : systems) {
            // to mimic - Find history for system by uuid
            systemsByUuid = holderIRepositories.getSystemRepository().findByUuid(system.getUuid().toString());
            for (System systemByUuid : systemsByUuid) {
                setIdSystem.add(systemByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after system,      setIdSystem.size:      " + setIdSystem.size(), reportHtml);

        for (Subsystem subsystem : subsystems) {
            // to mimic - Find history for subsystem by uuid
            subsystemsByUuid = holderIRepositories.getSubsystemRepository().findByUuid(subsystem.getUuid().toString());
            for (Subsystem subsystemByUuid : subsystemsByUuid) {
                setIdSubsystem.add(subsystemByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after subsystem,   setIdSubsystem.size:   " + setIdSubsystem.size(), reportHtml);

        for (Discipline discipline : disciplines) {
            // to mimic - Find history for discipline by uuid
            disciplinesByUuid = holderIRepositories.getDisciplineRepository().findByUuid(discipline.getUuid().toString());
            for (Discipline disciplineByUuid : disciplinesByUuid) {
                setIdDiscipline.add(disciplineByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after discipline,  setIdDiscipline.size:  " + setIdDiscipline.size(), reportHtml);

        for (DeviceGroup deviceGroup : deviceGroups) {
            // to mimic - Find history for device group by uuid
            deviceGroupsByUuid = holderIRepositories.getDeviceGroupRepository().findByUuid(deviceGroup.getUuid().toString());
            for (DeviceGroup deviceGroupByUuid : deviceGroupsByUuid) {
                setIdDeviceGroup.add(deviceGroupByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after devicegroup, setIdDeviceGroup.size: " + setIdDeviceGroup.size(), reportHtml);

        for (DeviceType deviceType : deviceTypes) {
            // to mimic - Find history for device type by uuid
            deviceTypesByUuid = holderIRepositories.getDeviceTypeRepository().findByUuid(deviceType.getUuid().toString());
            for (DeviceType deviceTypeByUuid : deviceTypesByUuid) {
                setIdDeviceType.add(deviceTypeByUuid.getId());
            }
        }

        prepareLogReport("readDataReachable, check, after devicetype,  setIdDeviceType.size:  " + setIdDeviceType.size(), reportHtml);

        boolean ok = names.size() == setIdName.size()
                && systemGroups.size() == setIdSystemGroup.size()
                && systems.size() == setIdSystem.size()
                && subsystems.size() == setIdSubsystem.size()
                && disciplines.size() == setIdDiscipline.size()
                && deviceGroups.size() == setIdDeviceGroup.size()
                && deviceTypes.size() == setIdDeviceType.size();

        prepareLogReport("readDataReachable, check, after", reportHtml);
        prepareLogReport("readDataReachable, ok: " + ok, reportHtml);

        return reportHtml.toString();
    }

    /**
     * Verify difference between old REST API and new REST API.
     *
     * Amount is less in new than in old. Verify that all content in new REST API is in old REST API and that difference is old/obsolete data.
     *
     * @return
     */
    @GetMapping("/restapi_oldvsnew")
    public String readRestApiOldVsNew() {
        StringBuilder reportHtml = new StringBuilder();

        // prepare old REST API

        List<DeviceRevision> deviceRevisions = deviceRevisionRepository.findAll();

        NameViewProvider nameViewProvider = new NameViewProvider();
        nameViewProvider.update(deviceRevisions);

        // prepare new REST API

        List<Name> names       = holderIRepositories.getNameRepository().findLatestNotDeleted();
        List<Name> namesLatest = holderIRepositories.getNameRepository().findLatest();

        prepareLogReport("readRestApiOldVsNew, find data,                 deviceRevisions.size:                                " + deviceRevisions.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, find data,                 nameViewProvider.getNameRevisions.entrySet.size:     " + nameViewProvider.getNameRevisions().entrySet().size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, find data,                 names.size:                                          " + names.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, find data,                 namesLatest.size:                                    " + namesLatest.size(), reportHtml);

        // prepare comparison

        HashMap<Long, DeviceRevision> mapIdDeviceRevision           = new HashMap<>((int)(nameViewProvider.getNameRevisions().entrySet().size()/0.75 + 2));
        HashMap<Long, DeviceRevision> mapIdDeviceRevisionDifference = new HashMap<>((int)(nameViewProvider.getNameRevisions().entrySet().size()/0.75 + 2));
        HashMap<Long, UUID>           mapIdUuidDeviceRevision       = new HashMap<>((int)(nameViewProvider.getNameRevisions().entrySet().size()/0.75 + 2));
        for (Map.Entry<String, DeviceRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            mapIdDeviceRevision.put(entry.getValue().getId(), entry.getValue());
            mapIdDeviceRevisionDifference.put(entry.getValue().getId(), entry.getValue());
            mapIdUuidDeviceRevision.put(entry.getValue().getId(), entry.getValue().getDevice().getUuid());
        }

        HashMap<Long, Name> mapIdName   = new HashMap<>((int)(names.size()/0.75 + 2));
        for (Name name : names) {
            mapIdName.put(name.getId(), name);
        }
        HashMap<UUID, Name> mapUuidNameLatest = new HashMap<>((int)(namesLatest.size()/0.75 + 2));
        for (Name name : namesLatest) {
            mapUuidNameLatest.put(name.getUuid(), name);
        }

        prepareLogReport("readRestApiOldVsNew, utility,                   mapIdDeviceRevision.size:                            " + mapIdDeviceRevision.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, utility,                   mapIdDeviceRevisionDifference.size:                  " + mapIdDeviceRevisionDifference.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, utility,                   mapIdUuidDeviceRevision.size:                        " + mapIdUuidDeviceRevision.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, utility,                   mapIdName.size:                                      " + mapIdName.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, utility,                   mapUuidNameLatest.size:                              " + mapUuidNameLatest.size(), reportHtml);

        prepareLogReport("readRestApiOldVsNew, check, before", reportHtml);

        // compare old REST API with new REST API
        // 1. difference

        boolean idInOld = true;
        int countIdInOld = 0;
        int countNotIdInOld = 0;
        for (Name name : names) {
            idInOld = mapIdDeviceRevisionDifference.containsKey(name.getId());
            if (idInOld) {
                countIdInOld++;
            } else if (!idInOld) {
                countNotIdInOld++;
                // break;
            }
            mapIdDeviceRevisionDifference.remove(name.getId());
        }

        prepareLogReport("readRestApiOldVsNew, check, after difference 1, mapIdDeviceRevision.size:                            " + mapIdDeviceRevision.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, check, after difference 1, mapIdDeviceRevisionDifference.size:                  " + mapIdDeviceRevisionDifference.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, check, after difference 1, mapIdUuidDeviceRevision.size:                        " + mapIdUuidDeviceRevision.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, check, after difference 1, mapIdName.size:                                      " + mapIdName.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, check, after difference 1, mapUuidNameLatest.size:                              " + mapUuidNameLatest.size(), reportHtml);
        prepareLogReport("readRestApiOldVsNew, check, after difference 1, countIdInOld:                                        " + countIdInOld, reportHtml);
        prepareLogReport("readRestApiOldVsNew, check, after difference 1, countNotIdInOld:                                     " + countNotIdInOld, reportHtml);

        // 2. difference is old/obsolete data
        //        id corresponding to uuid should not be last in line
        //        <--> last in line should be deleted

        ArrayList<DeviceRevision> listDeviceRevisionWrong = new ArrayList<>();
        for (Entry<Long, DeviceRevision> entry : mapIdDeviceRevisionDifference.entrySet()) {
            UUID uuid = mapIdUuidDeviceRevision.get(entry.getKey());
            Name name = uuid != null ? mapUuidNameLatest.get(uuid) : null;

            // something may be wrong
            //     if latest for devicerevision is in names then ok
            //     else if latest for devicerevision is deleted then ok
            //     else something is wrong
            //            if (!latestInLine && !name.isDeleted()) {
            //     <-->
            //     name != null && (name.isLatest() || name.isDeleted()) --> ok, else wrong
            if (!(name != null && (name.isLatest() || name.isDeleted()))) {
                // something is wrong
                listDeviceRevisionWrong.add(entry.getValue());
            }
        }

        prepareLogReport("readRestApiOldVsNew, check, after difference 2, listDeviceRevisionWrong.size:                        " + listDeviceRevisionWrong.size(), reportHtml);

        boolean ok = (mapIdDeviceRevisionDifference.size() == (nameViewProvider.getNameRevisions().entrySet().size() - names.size()))
                && (listDeviceRevisionWrong.size() == 0);

        prepareLogReport("readRestApiOldVsNew, check, after", reportHtml);
        prepareLogReport("readRestApiOldVsNew, ok: " + ok, reportHtml);

        return reportHtml.toString();
    }

    /**
     * Utility method for preparing reports for logging purposes.
     *
     * @param message
     * @param reportHtml
     * @param reportLog
     */
    private void prepareLogReport(String message, StringBuilder reportHtml) {
        if (!StringUtils.isEmpty(message)) {
            LOGGER.log(Level.INFO, message);
            reportHtml.append(message + NEW_LINE_BR);
        }
    }

}
