/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.rest.api.v1.IStructures;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.service.StructuresService;
import org.openepics.names.util.ExceptionUtil;
import org.openepics.names.util.LogUtil;
import org.openepics.names.util.ServiceHttpStatusException;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * This part of REST API provides structures data for Naming application.
 *
 * @author Lars Johansson
 */
@RestController
@EnableAutoConfiguration
public class StructuresController implements IStructures {

    // note
    //     global exception handler available

    // TODO validate authority
    //          either
    //              no or
    //              naming user
    //              naming admin
    //              naming user & admin

    private static final Logger LOGGER = Logger.getLogger(StructuresController.class.getName());

    private StructuresService structuresService;

    @Autowired
    public StructuresController(
            StructuresService structuresService) {
        this.structuresService = structuresService;
    }

    @Override
    public List<StructureElement> createStructures(List<StructureElement> structureElements) {
        // validate authority
        //     naming user & admin
        // validate
        // do

        try {
            structuresService.validateStructuresCreate(structureElements);
            return structuresService.createStructures(structureElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<StructureElement> readStructures(
            Type type, Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return structuresService.readStructures(
                    type, statuses, deleted, queryFields, queryValues,
                    orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<StructureElement> readStructuresChildren(
            Type type, String uuid,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return structuresService.readStructuresChildren(
                    type, uuid,
                    orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<StructureElement> readStructuresMnemonic(
            String mnemonic,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return structuresService.readStructuresMnemonic(mnemonic, orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<StructureElement> readStructuresMnemonicpath(
            String mnemonicpath,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return structuresService.readStructuresMnemonicpath(mnemonicpath, orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<StructureElement> readStructuresHistory(
            String uuid, Type type,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        try {
            return structuresService.readStructuresHistory(
                    uuid, type,
                    orderBy, isAsc, offset, limit);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public String equivalenceMnemonic(String mnemonic) {
        try {
            return structuresService.equivalenceMnemonic(mnemonic);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> existsStructure(Type type, String mnemonic) {
        try {
            return new ResponseEntity<>(new ResponseBoolean(structuresService.existsStructure(type, mnemonic)), Response.HEADER_JSON, HttpStatus.OK);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()), Response.HEADER_JSON, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.HEADER_JSON, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> isValidToCreateStructure(Type type, String mnemonicpath) {
        try {
            return new ResponseEntity<>(new ResponseBoolean(structuresService.isValidToCreateStructure(type, mnemonicpath)), Response.HEADER_JSON, HttpStatus.OK);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()), Response.HEADER_JSON, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.HEADER_JSON, HttpStatus.OK);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresCreate(List<StructureElement> structureElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            try {
                structuresService.validateStructuresCreate(structureElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresUpdate(List<StructureElement> structureElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            try {
                structuresService.validateStructuresUpdate(structureElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresDelete(List<StructureElement> structureElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            try {
                structuresService.validateStructuresDelete(structureElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresApprove(List<StructureElement> structureElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            try {
                structuresService.validateStructuresApprove(structureElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresCancel(List<StructureElement> structureElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            try {
                structuresService.validateStructuresCancel(structureElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresReject(List<StructureElement> structureElements) {
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            try {
                structuresService.validateStructuresReject(structureElement);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceHttpStatusException e) {
                LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails()));
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
                LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
                if (response) {
                    response = false;
                    reason = ExceptionUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.HEADER_JSON, HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<StructureElement> updateStructures(List<StructureElement> structureElements) {
        try {
            structuresService.validateStructuresUpdate(structureElements);
            return structuresService.updateStructures(structureElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }


    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<StructureElement> deleteStructures(List<StructureElement> structureElements) {
        try {
            structuresService.validateStructuresDelete(structureElements);
            return structuresService.deleteStructures(structureElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<StructureElement> approveStructures(List<StructureElement> structureElements) {
        try {
            structuresService.validateStructuresApprove(structureElements);
            return structuresService.approveStructures(structureElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<StructureElement> cancelStructures(List<StructureElement> structureElements) {
        try {
            structuresService.validateStructuresCancel(structureElements);
            return structuresService.cancelStructures(structureElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

    @Override
    public List<StructureElement> rejectStructures(List<StructureElement> structureElements) {
        try {
            structuresService.validateStructuresReject(structureElements);
            return structuresService.rejectStructures(structureElements);
        } catch (ServiceHttpStatusException e) {
            LogUtil.logServiceHttpStatusException(LOGGER, Level.SEVERE, e);
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            LogUtil.logStackTraceElements(LOGGER, Level.SEVERE, e);
            throw e;
        }
    }

}
