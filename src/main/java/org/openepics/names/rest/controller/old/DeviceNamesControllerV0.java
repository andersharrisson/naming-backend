/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller.old;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import io.swagger.v3.oas.annotations.Hidden;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.model.Name;
import org.openepics.names.rest.beans.old.DeviceNameElement;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderSystemDeviceStructure;
import org.openepics.names.util.NamingConventionUtil;
import org.openepics.names.util.old.DeviceNameElementUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * This part of REST API provides device name data for Naming application.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/rest/deviceNames")
@EnableAutoConfiguration
public class DeviceNamesControllerV0 {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /deviceNames                                - findNames()
           read      GET /deviceNames/search/{name}                  - findNamesSearch(String)
           read      GET /deviceNames/system/{system}                - findNamesBySystem(String)
           read      GET /deviceNames/system/search/{system}         - findNamesBySystemSearch(String)
           read      GET /deviceNames/subsystem/{subsystem}          - findNamesBySubsystem(String)
           read      GET /deviceNames/subsystem/search/{subsystem}   - findNamesBySubsystemSearch(String)
           read      GET /deviceNames/discipline/{discipline}        - findNamesByDiscipline(String)
           read      GET /deviceNames/discipline/search/{discipline} - findNamesByDisciplineSearch(String)
           read      GET /deviceNames/devicetype/{devicetype}        - findNamesByDeviceType(String)
           read      GET /deviceNames/devicetype/search/{devicetype} - findNamesByDeviceTypeSearch(String)
           read      GET /deviceNames/{uuid}                         - findName(String) (uuid or name)
     */

    private static final Logger LOGGER = Logger.getLogger(DeviceNamesControllerV0.class.getName());

    private HolderIRepositories holderIRepositories;

    @Autowired
    public DeviceNamesControllerV0(
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository) {

        holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository);
    }

    /**
     * Find valid names.
     *
     * @return all valid names
     */
    @GetMapping
    public List<DeviceNameElement> findNames() {
        List<Name> names = holderIRepositories.getNameRepository().findLatestNotDeleted();

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (Name name : names) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
        }

        LOGGER.log(Level.INFO, "findNames, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by name search.
     * <br/>
     * Note
     * <ul>
     * <li>name (search, case sensitive, regex)
     * <li>search done for all parts of name
     * </ul>
     *
     * @param name name to search for
     * @return a list of names
     */
    @GetMapping("/search/{name}")
    public List<DeviceNameElement> findNamesSearch(@PathVariable("name") String name) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(name);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return deviceNameElements;
        }

        List<Name> names = holderIRepositories.getNameRepository().findLatestNotDeleted();

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (Name namee : names) {
            if (pattern.matcher(namee.getConventionName()).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(namee, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesSearch, name:                    " + name);
        LOGGER.log(Level.INFO, "findNamesSearch, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by system.
     * Note system (exact match, case sensitive).
     *
     * @param system system to look for
     * @return a list of names
     */
    @GetMapping("/system/{system}")
    public List<DeviceNameElement> findNamesBySystem(@PathVariable("system") String system) {
        // note
        //     exact match
        //     case sensitive

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        List<Name> namesSystemGroup            = holderIRepositories.getNameRepository().findLatestBySystemGroupMnemonic(system);
        for (Name name : namesSystemGroup) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
        }
        List<Name> namesSystem                 = holderIRepositories.getNameRepository().findLatestBySystemMnemonic(system);
        List<Name> namesSystemThroughSubsystem = holderIRepositories.getNameRepository().findLatestBySystemMnemonicThroughSubsystem(system);
        for (Name name : namesSystem) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
        }
        for (Name name : namesSystemThroughSubsystem) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
        }

        LOGGER.log(Level.INFO, "findNamesBySystem, system:                  " + system);
        LOGGER.log(Level.INFO, "findNamesBySystem, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by system search.
     * Note system (search, case sensitive, regex).
     *
     * @param system system to search for
     * @return a list of names
     */
    @GetMapping("/system/search/{system}")
    public List<DeviceNameElement> findNamesBySystemSearch(@PathVariable("system") String system) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(system);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return deviceNameElements;
        }

        List<Name> names = holderIRepositories.getNameRepository().findLatestNotDeleted();

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (Name name : names) {
            String sub = NamingConventionUtil.extractSystem(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesSearch, system:                  " + system);
        LOGGER.log(Level.INFO, "findNamesSearch, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by subsystem.
     * Note subsystem (exact match, case sensitive).
     *
     * @param subsystem subsystem to look for
     * @return a list of names
     */
    @GetMapping("/subsystem/{subsystem}")
    public List<DeviceNameElement> findNamesBySubsystem(@PathVariable("subsystem") String subsystem) {
        // note
        //     exact match
        //     case sensitive

        List<Name> names = holderIRepositories.getNameRepository().findLatestBySubsystemMnemonic(subsystem);

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        if (!names.isEmpty()) {
            // initiate holder of containers for system and device structure content, for performance reasons
            HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

            for (Name name : names) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesBySubsystem, subsystem:               " + subsystem);
        LOGGER.log(Level.INFO, "findNamesBySubsystem, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by subsystem search.
     * Note subsystem (search, case sensitive, regex).
     *
     * @param subsystem subsystem to search for
     * @return a list of names
     */
    @GetMapping("/subsystem/search/{subsystem}")
    public List<DeviceNameElement> findNamesBySubsystemSearch(@PathVariable("subsystem") String subsystem) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(subsystem);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return deviceNameElements;
        }

        List<Name> names = holderIRepositories.getNameRepository().findLatestNotDeleted();

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (Name name : names) {
            String sub = NamingConventionUtil.extractSubsystem(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesBySubsystemSearch, subsystem:               " + subsystem);
        LOGGER.log(Level.INFO, "findNamesBySubsystemSearch, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by discipline.
     * Note discipline (exact match, case sensitive).
     *
     * @param discipline discipline to look for
     * @return a list of names
     */
    @GetMapping("/discipline/{discipline}")
    public List<DeviceNameElement> findNamesByDiscipline(@PathVariable("discipline") String discipline) {
        // note
        //     exact match
        //     case sensitive

        List<Name> names = holderIRepositories.getNameRepository().findLatestByDisciplineMnemonic(discipline);

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        if (!names.isEmpty()) {
            // initiate holder of containers for system and device structure content, for performance reasons
            HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

            for (Name name : names) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesByDiscipline, discipline:              " + discipline);
        LOGGER.log(Level.INFO, "findNamesByDiscipline, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by discipline search.
     * Note discipline (search, case sensitive, regex).
     *
     * @param discipline discipline to search for
     * @return a list of names
     */
    @GetMapping("/discipline/search/{discipline}")
    public List<DeviceNameElement> findNamesByDisciplineSearch(@PathVariable("discipline") String discipline) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(discipline);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return deviceNameElements;
        }

        List<Name> names = holderIRepositories.getNameRepository().findLatestNotDeleted();

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (Name name : names) {
            String sub = NamingConventionUtil.extractDiscipline(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesByDisciplineSearch, discipline:              " + discipline);
        LOGGER.log(Level.INFO, "findNamesByDisciplineSearch, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by device type.
     * Note device type (exact match, case sensitive).
     *
     * @param deviceType device type to look for
     * @return a list of names
     */
    @GetMapping("/devicetype/{devicetype}")
    public List<DeviceNameElement> findNamesByDeviceType(@PathVariable("devicetype") String deviceType) {
        // note
        //     exact match
        //     case sensitive

        List<Name> names = holderIRepositories.getNameRepository().findLatestByDeviceTypeMnemonic(deviceType);

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        if (!names.isEmpty()) {
            // initiate holder of containers for system and device structure content, for performance reasons
            HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

            for (Name name : names) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesByDeviceType, deviceType:              " + deviceType);
        LOGGER.log(Level.INFO, "findNamesByDeviceType, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by device type search.
     * Note device type (search, case sensitive, regex).
     *
     * @param deviceType device type to search for
     * @return a list of names
     */
    @GetMapping("/devicetype/search/{devicetype}")
    public List<DeviceNameElement> findNamesByDeviceTypeSearch(@PathVariable("devicetype") String deviceType) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(deviceType);
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return deviceNameElements;
        }

        List<Name> names = holderIRepositories.getNameRepository().findLatestNotDeleted();

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (Name name : names) {
            String sub = NamingConventionUtil.extractDeviceType(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holder));
            }
        }

        LOGGER.log(Level.INFO, "findNamesByDeviceTypeSearch, deviceType:              " + deviceType);
        LOGGER.log(Level.INFO, "findNamesByDeviceTypeSearch, deviceNameElements.size: " + deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find name by uuid or name (exact match, case sensitive).
     *
     * @param uuid string uuid or device name
     * @return name (most recent)
     */
    @GetMapping("/{uuid}")
    public DeviceNameElement findName(@PathVariable("uuid") String uuid) {
        // note
        //     exact match
        //     case sensitive

        Name name = null;
        try {
            UUID.fromString(uuid);
            name = holderIRepositories.getNameRepository().findLatestByUuid(uuid);
        } catch (IllegalArgumentException e) {
            name = holderIRepositories.getNameRepository().findLatestByConventionName(uuid);
        }
        DeviceNameElement deviceNameElement = DeviceNameElementUtil.getDeviceNameElement(name, holderIRepositories);

        LOGGER.log(Level.INFO, "findName, uuid:              " + uuid);
        LOGGER.log(Level.INFO, "findName, deviceNameElement: " + deviceNameElement);

        return deviceNameElement;
    }

}
