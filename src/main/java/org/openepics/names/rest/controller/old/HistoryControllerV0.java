/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller.old;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.old.HistoryElement;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.old.HistoryElementUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.Hidden;

/**
 * This part of REST API provides history for names and name part data for Naming application.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/rest/history")
@EnableAutoConfiguration
public class HistoryControllerV0 {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /history/parts/uuid/{uuid}       - findNamePartHistoryForUuid(String)
           read      GET /history/deviceNames/uuid/{uuid} - findNameHistoryForUuid(String)
     */

    private static final Logger LOGGER = Logger.getLogger(HistoryControllerV0.class.getName());

    private static final long THOUSAND_MILLISECONDS = 1000;

    private HolderIRepositories holderIRepositories;

    @Autowired
    public HistoryControllerV0(
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository) {

        holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository);
    }

    /**
     * Find history for name part by uuid.
     * Note uuid (exact match).
     *
     * @param uuid uuid to look for
     * @return a list of entries for history of name part
     */
    @GetMapping("/parts/uuid/{uuid}")
    public List<HistoryElement> findNamePartHistoryForUuid(@PathVariable("uuid") String uuid) {
        // check parameters
        // prepare retrieval of information
        // retrieve information
        // prepare return elements

        boolean hasUuid = !StringUtils.isEmpty(uuid);

        final List<HistoryElement> historyElements = Lists.newArrayList();
        if (!hasUuid) {
            return historyElements;
        }

        List<SystemGroup> systemGroups = holderIRepositories.getSystemGroupRepository().findByUuid(uuid);
        List<System>      systems      = holderIRepositories.getSystemRepository().findByUuid(uuid);
        List<Subsystem>   subsystems   = holderIRepositories.getSubsystemRepository().findByUuid(uuid);

        List<Discipline>  disciplines  = holderIRepositories.getDisciplineRepository().findByUuid(uuid);
        List<DeviceGroup> deviceGroups = holderIRepositories.getDeviceGroupRepository().findByUuid(uuid);
        List<DeviceType>  deviceTypes  = holderIRepositories.getDeviceTypeRepository().findByUuid(uuid);

        if (!systemGroups.isEmpty()) {
            for (SystemGroup systemGroup : systemGroups) {
                // one or two return elements
                //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
                //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
                //     processed == null                                               --> one entry   (requested)

                if (systemGroup.getProcessed() != null && ((systemGroup.getProcessed().getTime() - systemGroup.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(systemGroup));
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(systemGroup));
                } else if (systemGroup.getProcessed() != null && ((systemGroup.getProcessed().getTime() - systemGroup.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(systemGroup));
                } else {
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(systemGroup));
                }
            }
        } else if (!systems.isEmpty()) {
            for (System system : systems) {
                // one or two return elements
                //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
                //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
                //     processed == null                                               --> one entry   (requested)

                if (system.getProcessed() != null && ((system.getProcessed().getTime() - system.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(system));
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(system));
                } else if (system.getProcessed() != null && ((system.getProcessed().getTime() - system.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(system));
                } else {
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(system));
                }
            }
        } else if (!subsystems.isEmpty()) {
            for (Subsystem subsystem : subsystems) {
                // one or two return elements
                //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
                //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
                //     processed == null                                               --> one entry   (requested)

                if (subsystem.getProcessed() != null && ((subsystem.getProcessed().getTime() - subsystem.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(subsystem));
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(subsystem));
                } else if (subsystem.getProcessed() != null && ((subsystem.getProcessed().getTime() - subsystem.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(subsystem));
                } else {
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(subsystem));
                }
            }
        } else if (!disciplines.isEmpty()) {
            for (Discipline discipline : disciplines) {
                // one or two return elements
                //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
                //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
                //     processed == null                                               --> one entry   (requested)

                if (discipline.getProcessed() != null && ((discipline.getProcessed().getTime() - discipline.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(discipline));
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(discipline));
                } else if (discipline.getProcessed() != null && ((discipline.getProcessed().getTime() - discipline.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(discipline));
                } else {
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(discipline));
                }
            }
        } else if (!deviceGroups.isEmpty()) {
            for (DeviceGroup deviceGroup : deviceGroups) {
                // one or two return elements
                //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
                //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
                //     processed == null                                               --> one entry   (requested)

                if (deviceGroup.getProcessed() != null && ((deviceGroup.getProcessed().getTime() - deviceGroup.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(deviceGroup));
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(deviceGroup));
                } else if (deviceGroup.getProcessed() != null && ((deviceGroup.getProcessed().getTime() - deviceGroup.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(deviceGroup));
                } else {
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(deviceGroup));
                }
            }
        } else if (!deviceTypes.isEmpty()) {
            for (DeviceType deviceType : deviceTypes) {
                // one or two return elements
                //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
                //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
                //     processed == null                                               --> one entry   (requested)

                if (deviceType.getProcessed() != null && ((deviceType.getProcessed().getTime() - deviceType.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(deviceType));
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(deviceType));
                } else if (deviceType.getProcessed() != null && ((deviceType.getProcessed().getTime() - deviceType.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    historyElements.add(HistoryElementUtil.getHistoryElementProcessed(deviceType));
                } else {
                    historyElements.add(HistoryElementUtil.getHistoryElementRequested(deviceType));
                }
            }
        }

        LOGGER.log(Level.INFO, "findNamePartHistoryForUuid, uuid:                 " + uuid);
        LOGGER.log(Level.INFO, "findNamePartHistoryForUuid, historyElements.size: " + historyElements.size());

        return historyElements;
    }

    /**
     * Find history for name by uuid.
     * Note uuid (exact match).
     *
     * @param uuid uuid to look for
     * @return a list of entries for history of name part
     */
    @GetMapping("/deviceNames/uuid/{uuid}")
    public List<HistoryElement> findNameHistoryForUuid(@PathVariable("uuid") String uuid) {
        // check parameters
        // prepare retrieval of information
        // retrieve information
        // prepare return elements

        boolean hasUuid = !StringUtils.isEmpty(uuid);

        final List<HistoryElement> historyElements = Lists.newArrayList();
        if (!hasUuid) {
            return historyElements;
        }

        List<Name> names = holderIRepositories.getNameRepository().findByUuid(uuid);
        for (Name name : names) {
            historyElements.add(HistoryElementUtil.getHistoryElement(name));
        }

        LOGGER.log(Level.INFO, "findNameHistoryForUuid, uuid:                 " + uuid);
        LOGGER.log(Level.INFO, "findNameHistoryForUuid, historyElements.size: " + historyElements.size());

        return historyElements;
    }

}
