/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Bean (data transfer object) for communication and (json, xml) serialization.
 *
 * @author Lars Johansson
 */
public class StructureElement extends NameStructureElement implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8892788697188691804L;

    private Type type;
    private UUID parent;
    private String name;
    private String mnemonic;
    private String mnemonicpath;
    private Integer level;

    /**
     * Public constructor.
     */
    public StructureElement() {
    }

    /**
     * Public constructor.
     *
     * @param type type
     * @param uuid uuid
     * @param parent parent uuid
     * @param name name
     * @param mnemonic mnemonic
     * @param mnemonicpath mnemonic path
     * @param level level
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     */
    public StructureElement(
            Type type,
            UUID uuid,
            UUID parent,
            String name, String mnemonic, String mnemonicpath, Integer level,
            String description, Status status, Boolean latest, Boolean deleted,
            Date when, String who, String comment) {

        super(uuid, description, status, latest, deleted, when, who, comment);

        setType(type);
        setParent(parent);
        setName(name);
        setMnemonic(mnemonic);
        setMnemonicpath(mnemonicpath);
        setLevel(level);
    }

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public UUID getParent() {
        return parent;
    }
    public void setParent(UUID parent) {
        this.parent = parent;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMnemonic() {
        return mnemonic;
    }
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    public String getMnemonicpath() {
        return mnemonicpath;
    }
    public void setMnemonicpath(String mnemonicpath) {
        this.mnemonicpath = mnemonicpath;
    }
    public Integer getLevel() {
        return level;
    }
    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((StructureElement) obj);
    }

    public boolean equals(StructureElement other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getType() == null) {
            if (other.getType() != null)
                return false;
        } else if (!getType().equals(other.getType()))
            return false;
        if (getParent() == null) {
            if (other.getParent() != null)
                return false;
        } else if (!getParent().equals(other.getParent()))
            return false;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (getMnemonic() == null) {
            if (other.getMnemonic() != null)
                return false;
        } else if (!getMnemonic().equals(other.getMnemonic()))
            return false;
        if (getMnemonicpath() == null) {
            if (other.getMnemonicpath() != null)
                return false;
        } else if (!getMnemonicpath().equals(other.getMnemonicpath()))
            return false;
        if (getLevel() == null) {
            if (other.getLevel() != null)
                return false;
        } else if (!getLevel().equals(other.getLevel()))
            return false;

        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"type\": "           + getType());
        sb.append(", \"uuid\": "         + getUuid());
        sb.append(", \"parent_uuid\": "  + getParent());
        sb.append(", \"name\": "         + getName());
        sb.append(", \"mnemonic\": "     + getMnemonic());
        sb.append(", \"mnemonicpath\": " + getMnemonicpath());
        sb.append(", \"level\": "        + getLevel());
        sb.append(", \"description\": "  + getDescription());
        sb.append(", \"status\": "       + getStatus());
        sb.append(", \"latest\": "       + isLatest());
        sb.append(", \"deleted\": "      + isDeleted());
        sb.append(", \"when\": "         + getWhen());
        sb.append(", \"who\": "          + getWho());
        sb.append(", \"comment\": "      + getComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"type\": "           + getType());
        sb.append(", \"uuid\": "         + getUuid());
        sb.append(", \"mnemonic\": "     + getMnemonic());
        sb.append(", \"status\": "       + getStatus());
        sb.append(", \"latest\": "       + isLatest());
        sb.append(", \"deleted\": "      + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}
