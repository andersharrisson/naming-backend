/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Bean (data transfer object) for communication and (json, xml) serialization.
 *
 * @author Lars Johansson
 */
public abstract class NameStructureElement implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1124618368080704934L;

    private UUID uuid;
    private String description;
    private Status status;
    private Boolean latest;
    private Boolean deleted;
    private Date when;
    private String who;
    private String comment;

    /**
     * Public constructor.
     */
    public NameStructureElement() {
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     */
    public NameStructureElement(
            UUID uuid,
            String description,
            Status status, Boolean latest, Boolean deleted,
            Date when, String who, String comment) {

        setUuid(uuid);
        setDescription(description);
        setStatus(status);
        setLatest(latest);
        setDeleted(deleted);
        setWhen(when);
        setWho(who);
        setComment(comment);
    }

    public UUID getUuid() {
        return uuid;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Boolean isLatest() {
        return latest;
    }
    public void setLatest(Boolean latest) {
        this.latest = latest;
    }
    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Date getWhen() {
        return when;
    }
    public void setWhen(Date when) {
        this.when = when;
    }
    public String getWho() {
        return who;
    }
    public void setWho(String who) {
        this.who = who;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals((NameStructureElement) obj);
    }

    public boolean equals(NameStructureElement other) {
        if (other == null)
            return false;

        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;
        if (getStatus() == null) {
            if (other.getStatus() != null)
                return false;
        } else if (!getStatus().equals(other.getStatus()))
            return false;
        if (isLatest() == null) {
            if (other.isLatest() != null)
                return false;
        } else if (!isLatest().equals(other.isLatest()))
            return false;
        if (isDeleted() == null) {
            if (other.isDeleted() != null)
                return false;
        } else if (!isDeleted().equals(other.isDeleted()))
            return false;
        if (getWhen() == null) {
            if (other.getWhen() != null)
                return false;
        } else if (!getWhen().equals(other.getWhen()))
            return false;
        if (getWho() == null) {
            if (other.getWho() != null)
                return false;
        } else if (!getWho().equals(other.getWho()))
            return false;
        if (getComment() == null) {
            if (other.getComment() != null)
                return false;
        } else if (!getComment().equals(other.getComment()))
            return false;

        return true;
    }

}
