/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Bean (data transfer object) for communication and (json, xml) serialization.
 *
 * @author Lars Johansson
 */
public class NameElement extends NameStructureElement implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5998490678202969520L;

    private UUID systemgroup;
    private UUID system;
    private UUID subsystem;
    private UUID devicetype;
    private String systemstructure;
    private String devicestructure;
    private String index;
    private String name;

    /**
     * Public constructor.
     */
    public NameElement () {
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param systemgroup system group
     * @param system system
     * @param subsystem subsystem
     * @param devicetype device type
     * @param systemstructure system structure mnemonic path
     * @param devicestructure device structure mnemonic path
     * @param index instance index
     * @param name name
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     */
    public NameElement (
            UUID uuid,
            UUID systemgroup, UUID system, UUID subsystem, UUID devicetype,
            String systemstructure, String devicestructure,
            String index, String name,
            String description, Status status, Boolean latest, Boolean deleted,
            Date when, String who, String comment) {

        super(uuid, description, status, latest, deleted, when, who, comment);

        setSystemgroup(systemgroup);
        setSystem(system);
        setSubsystem(subsystem);
        setDevicetype(devicetype);
        setSystemstructure(systemstructure);
        setDevicestructure(devicestructure);
        setIndex(index);
        setName(name);
    }

    public UUID getSystemgroup() {
        return systemgroup;
    }
    public void setSystemgroup(UUID systemgroup) {
        this.systemgroup = systemgroup;
    }
    public UUID getSystem() {
        return system;
    }
    public void setSystem(UUID system) {
        this.system = system;
    }
    public UUID getSubsystem() {
        return subsystem;
    }
    public void setSubsystem(UUID subsystem) {
        this.subsystem = subsystem;
    }
    public UUID getDevicetype() {
        return devicetype;
    }
    public void setDevicetype(UUID devicetype) {
        this.devicetype = devicetype;
    }
    public String getSystemstructure() {
        return systemstructure;
    }
    public void setSystemstructure(String systemstructure) {
        this.systemstructure = systemstructure;
    }
    public String getDevicestructure() {
        return devicestructure;
    }
    public void setDevicestructure(String devicestructure) {
        this.devicestructure = devicestructure;
    }
    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // utility method
    public void setNameAndIndex(String name, String index) {
    	this.index = index;
    	this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((NameElement) obj);
    }

    public boolean equals(NameElement other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getSystemgroup() == null) {
            if (other.getSystemgroup() != null)
                return false;
        } else if (!getSystemgroup().equals(other.getSystemgroup()))
            return false;
        if (getSystem() == null) {
            if (other.getSystem() != null)
                return false;
        } else if (!getSystem().equals(other.getSystem()))
            return false;
        if (getSubsystem() == null) {
            if (other.getSubsystem() != null)
                return false;
        } else if (!getSubsystem().equals(other.getSubsystem()))
            return false;
        if (getDevicetype() == null) {
            if (other.getDevicetype() != null)
                return false;
        } else if (!getDevicetype().equals(other.getDevicetype()))
            return false;
        if (getSystemstructure() == null) {
            if (other.getSystemstructure() != null)
                return false;
        } else if (!getSystemstructure().equals(other.getSystemstructure()))
            return false;
        if (getDevicestructure() == null) {
            if (other.getDevicestructure() != null)
                return false;
        } else if (!getDevicestructure().equals(other.getDevicestructure()))
            return false;
        if (getIndex() == null) {
            if (other.getIndex() != null)
                return false;
        } else if (!getIndex().equals(other.getIndex()))
            return false;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;

        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "              + getUuid());
        sb.append(", \"systemgroup\": "     + getSystemgroup());
        sb.append(", \"system\": "          + getSystem());
        sb.append(", \"subsystem\": "       + getSubsystem());
        sb.append(", \"devicetype\": "      + getDevicetype());
        sb.append(", \"systemstructure\": " + getSystemstructure());
        sb.append(", \"devicestructure\": " + getDevicestructure());
        sb.append(", \"index\": "           + getIndex());
        sb.append(", \"name\": "            + getName());
        sb.append(", \"description\": "     + getDescription());
        sb.append(", \"status\": "          + getStatus());
        sb.append(", \"latest\": "          + isLatest());
        sb.append(", \"deleted\": "         + isDeleted());
        sb.append(", \"when\": "            + getWhen());
        sb.append(", \"who\": "             + getWho());
        sb.append(", \"comment\": "         + getComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "              + getUuid());
        sb.append(", \"name\": "            + getName());
        sb.append(", \"status\": "          + getStatus());
        sb.append(", \"latest\": "          + isLatest());
        sb.append(", \"deleted\": "         + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}
