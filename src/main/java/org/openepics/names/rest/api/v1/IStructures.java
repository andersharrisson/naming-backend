/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.api.v1;

import java.util.List;

import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides structures data for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "Structures",
     description = "handle structures data for Naming application")
@RequestMapping("/api/v1/structures")
public interface IStructures {

    /*
       Type             - SYSTEMGROUP, SYSTEM, SUBSYSTEM, DISCIPLINE, DEVICEGROUP, DEVICETYPE
       Status           - APPROVED, ARCHIVED, CANCELLED, PENDING, REJECTED
       FieldStructure   - UUID, PARENT, NAME, MNEMONIC, MNEMONICEQUIVALENCE, MNEMONICPATH, DESCRIPTION

       StructureElement - type            (Type)
                          uuid            (UUID)
                          parent          (UUID)
                          name            (String)
                          mnemonic        (String)
                          mnemonicpath    (String)
                          level           (Integer)
                          description     (String)
                          status          (Status)
                          latest          (Boolean)
                          deleted         (Boolean)
                          when            (Date)
                          who             (String)
                          comment         (String)

       authentication/authorization
           3 levels - no, user, administrator
               no            - read
               user          - create, update, delete
               administrator - approve, reject (, checkdevices)

       mnemonic path for structure includes all levels of mnemonics

       Methods
           create    POST   /structures                                       - createStructures               (List<StructureElement>)
           ----------------------------------------------------------------------------------------------------
           read      GET    /structures/{type}                                - readStructures                 (Type, Status[], Boolean, FieldStructure[], String[], FieldStructure, Boolean, Integer, Integer)
           read      GET    /structures/children/{type}/{uuid}                - readStructuresChildren         (Type, String, FieldStructure, Boolean, Integer, Integer)
           read      GET    /structures/mnemonic/{mnemonic}                   - readStructuresMnemonic         (String, FieldStructure, Boolean, Integer, Integer, FieldStructure, Boolean, Integer, Integer)
           read      GET    /structures/mnemonicpath/{mnemonicpath}           - readStructuresMnemonicpath     (String, FieldStructure, Boolean, Integer, Integer, FieldStructure, Boolean, Integer, Integer)
           read      GET    /structures/history/{uuid}                        - readStructuresHistory          (String, Type, FieldStructure, Boolean, Integer, Integer)
           ----------------------------------------------------------------------------------------------------
           read      GET    /structures/equivalence/{mnemonic}                - equivalenceMnemonic            (String)
           read      GET    /structures/exists/{type}/{mnemonic}              - existsStructure                (Type, String)
           read      GET    /structures/isvalidtocreate/{type}/{mnemonicpath} - isValidToCreateStructure       (Type, String)
           ----------------------------------------------------------------------------------------------------
           read      GET    /structures/validatecreate                        - validateStructuresCreate       (List<StructureElement>)
           read      GET    /structures/validateupdate                        - validateStructuresUpdate       (List<StructureElement>)
           read      GET    /structures/validatedelete                        - validateStructuresDelete       (List<StructureElement>)
           read      GET    /structures/validateapprove                       - validateStructuresApprove      (List<StructureElement>)
           read      GET    /structures/validatecancel                        - validateStructuresCancel       (List<StructureElement>)
           read      GET    /structures/validatereject                        - validateStructuresReject       (List<StructureElement>)
           ----------------------------------------------------------------------------------------------------
           update    PUT    /structures                                       - updateStructures               (List<StructureElement>)
           ----------------------------------------------------------------------------------------------------
           delete    DELETE /structures                                       - deleteStructures               (List<StructureElement>)
           ----------------------------------------------------------------------------------------------------
           update    PATCH  /structures/approve                               - approveStructures              (List<StructureElement>)
           update    PATCH  /structures/cancel                                - cancelStructures               (List<StructureElement>)
           update    PATCH  /structures/reject                                - rejectStructures               (List<StructureElement>)
     */

    /**
     * Create (propose) structures by list of structure elements.
     * Return list of created structure elements (proposals).
     *
     * <p>
     * StructureElement attributes required:
     * <ul>
     * <li>type</li>
     * <li>parent (System, Subsystem, DeviceGroup, DeviceType)</li>
     * <li>name</li>
     * <li>mnemonic (System, Subsystem, Discipline, DeviceType)(may be set for SystemGroup, not allowed for DeviceGroup)</li>
     * <li>description</li>
     * <li>comment</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return list of structure elements for created structures (proposals)
     */
    @Operation(
            summary     = "Create (propose) structures by list of structure elements",
            description = "Create (propose) structures by list of structure elements. "
                        + "Return array of created structure elements (proposals)."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements for created structures (proposals).", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.POST,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<StructureElement> createStructures(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Find valid structures (search).
     * Return list of structure elements.
     *
     * @param type type of structure to search in
     * @param statuses statuses of structures to search for
     * @param deleted if deleted-only structures are to be included (false for non-deleted-only structures, true for deleted-only structures, not used for both cases)
     * @param queryFields search fields
     * @param queryValues search values corresponding to search fields
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find valid structures (search)",
            description = "Find valid structures (search). "
                        + "Return list of structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements.",   content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/{type}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @GetMapping
    public List<StructureElement> readStructures(
            @Parameter(in = ParameterIn.PATH,  description = "type of structure to search in") @PathVariable("type") Type type,
            @Parameter(in = ParameterIn.QUERY, description = "statuses of structures to search for") @RequestParam(required = false) Status[] statuses,
            @Parameter(in = ParameterIn.QUERY, description = "if deleted-only names are to be included, false for non-deleted-only names, true for deleted-only names, not used for both cases") @RequestParam(required = false) Boolean deleted,
            @Parameter(in = ParameterIn.QUERY, description = "search fields") @RequestParam(required = false) FieldStructure[] queryFields,
            @Parameter(in = ParameterIn.QUERY, description = "search values corresponding to search fields") @RequestParam(required = false) String[] queryValues,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find valid children structures by type and parent uuid (exact match).
     *
     * @param type type of structure to search in
     * @param uuid uuid to find structure for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find valid children structures by type and parent uuid (exact match)",
            description = "Find valid children structures by type and parent uuid (exact match)."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements.",   content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/children/{type}/{uuid}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @GetMapping
    public List<StructureElement> readStructuresChildren(
            @Parameter(in = ParameterIn.PATH,  description = "type of structure to search in") @PathVariable("type") Type type,
            @Parameter(in = ParameterIn.PATH,  description = "uuid to find structure for") @PathVariable("uuid") String uuid,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find valid structures by mnemonic (search).
     * Return list of structure elements.
     *
     * @param mnemonic mnemonic to search for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find valid structures by mnemonic (search)",
            description = "Find valid structures by mnemonic (search). "
                        + "Return list of structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements.",   content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/mnemonic/{mnemonic}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @GetMapping
    public List<StructureElement> readStructuresMnemonic(
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic to search for") @PathVariable("mnemonic") String mnemonic,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find valid structures by mnemonic path (search).
     * Return list of structure elements.
     *
     * @param mnemonicpath mnemonic path to search for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find valid structures by mnemonic path (search)",
            description = "Find valid structures by mnemonic path (search). "
                        + "Return list of structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements.",   content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/mnemonicpath/{mnemonicpath}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @GetMapping
    public List<StructureElement> readStructuresMnemonicpath(
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic path to search for") @PathVariable("mnemonicpath") String mnemonicpath,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find history for structure by uuid (exact match).
     * Return list of structure elements.
     *
     * @param uuid uuid to find history for
     * @param type type of structure to search in
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find history for structure by uuid (exact match)",
            description = "Find history for structure by uuid (exact match). "
                        + "Return list of structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements.",   content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/history/{uuid}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public List<StructureElement> readStructuresHistory(
            @Parameter(in = ParameterIn.PATH,  description = "uuid to find structure for") @PathVariable("uuid") String uuid,
            @Parameter(in = ParameterIn.QUERY, description = "type of structure to search in") @RequestParam(required = false) Type type,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return mnemonic equivalence for mnemonic.
     *
     * @param mnemonic mnemonic to get mnemonic equivalence for
     * @return mnemonic equivalence
     */
    @Operation(
            summary     = "Return mnemonic equivalence for mnemonic",
            description = "Return mnemonic equivalence for mnemonic."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return mnemonic equivalence.",          content = @Content(mediaType = "text/plain", schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))

    })
    @RequestMapping(
            value = "/equivalence/{mnemonic}",
            method = RequestMethod.GET)
    public String equivalenceMnemonic(
            @Parameter(in = ParameterIn.PATH, description = "mnemonic to get mnemonic equivalence for") @PathVariable("mnemonic") String mnemonic);

    /**
     * Return if mnemonic exists in structure (exact match).
     *
     * <p>
     * Returned object has three fields (response, message, details).
     * <ul>
     * <li>response: boolean (true/false)</li>
     * <li>message: reason, if method fails</li>
     * <li>details: details, if method fails</li>
     * </ul>
     * </p>
     *
     * @param type type of structure to search in
     * @param mnemonic mnemonic to find structure for
     * @return if mnemonic exists in structure
     */
    @Operation(
            summary     = "Return if mnemonic exists in structure (exact match)",
            description = "Return if mnemonic exists in structure (exact match). "
                        + "Returned object has three fields (message, details, response)."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if name exists, false otherwise. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/exists/{type}/{mnemonic}",
            method = RequestMethod.GET)
    public ResponseEntity<ResponseBoolean> existsStructure(
            @Parameter(in = ParameterIn.PATH,  description = "type of structure to search in") @PathVariable("type") Type type,
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic to find structure for") @PathVariable("mnemonic") String mnemonic);

    /**
     * Return if mnemonic path is valid to create in structure (exact match).
     * Method answers question 'would it be ok to create given mnemonic path in structure?'.
     *
     * <p>
     * Note that method can not fully answer question posed above. One reason is that system group
     * may have empty mnemonic. Another reason is that device groups has no mnemonic and is between
     * discipline and device type. Thus it can not be found which discipline (uuid-wise) that
     * a device type belongs.
     * </p>
     *
     * @param type type of structure to search in
     * @param mnemonicpath mnemonic path to find structure for
     * @return if mnemonic path is valid to create in structure
     */
    @Operation(
            summary     = "Return if mnemonic path is valid to create in structure",
            description = "Return if mnemonic path is valid to create in structure. "
                        + "Method answers question 'would it be ok to create given mnemonic path in structure?'."
                        + "\n\n"
                        + "Note that method can not fully answer question posed above. One reason is that system group "
                        + "may have empty mnemonic. Another reason is that device groups has no mnemonic and is "
                        + "between discipline and device type. Thus it can not be found which discipline (uuid-wise) "
                        + "that a device type belongs."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if structure is valid to create, false otherwise. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/isvalidtocreate/{type}/{mnemonicpath}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> isValidToCreateStructure(
            @Parameter(in = ParameterIn.PATH,  description = "type of structure to search in") @PathVariable("type") Type type,
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic path to find structure for") @PathVariable("mnemonicpath") String mnemonicpath);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return if structure elements are valid to create (propose).
     * If structure elements are valid to create, successful create of structures can be expected.
     *
     * <p>
     * Returned object has four fields (message, details, response, responses).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>response:  boolean (true/false), overall result of method</li>
     * <li>responses: list of response objects (with fields reason, details, response), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return if list of structure elements is valid to create (propose)
     */
    @Operation(
            summary     = "Return if structure elements are valid to create (propose)",
            description = "Return if structure elements are valid to create (propose). "
                        + "If structure elements are valid to create, successful create of structures can be expected."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all structure elements validated ok, false otherwise, responses contain array with result for each structure element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validatecreate",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresCreate(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Return if structure elements are valid to update (propose).
     * If structure elements are valid to update, successful update of structures can be expected.
     *
     * <p>
     * Returned object has four fields (message, details, response, responses).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>response:  boolean (true/false), overall result of method</li>
     * <li>responses: list of response objects (with fields reason, details, response), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return if list of structure elements is valid to update (propose)
     */
    @Operation(
            summary     = "Return if structure elements are valid to update (propose)",
            description = "Return if structure elements are valid to update (propose). "
                        + "If structure elements are valid to update, successful update of structures can be expected."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all structure elements validated ok, false otherwise, responses contain array with result for each structure element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validateupdate",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresUpdate(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Return if structure elements are valid to delete (propose).
     * If structure elements are valid to delete, successful delete of structures can be expected.
     *
     * <p>
     * Returned object has four fields (message, details, response, responses).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>response:  boolean (true/false), overall result of method</li>
     * <li>responses: list of response objects (with fields reason, details, response), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return if list of structure elements is valid to delete (propose)
     */
    @Operation(
            summary     = "Return if structure elements are valid to delete (propose)",
            description = "Return if structure elements are valid to delete (propose). "
                        + "If structure elements are valid to delete, successful delete of structures can be expected."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all structure elements validated ok, false otherwise, responses contain array with result for each structure element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validatedelete",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresDelete(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Return if structure elements are valid to approve.
     * If structure elements are valid to approve, successful approve of structures can be expected.
     *
     * <p>
     * Returned object has four fields (message, details, response, responses).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>response:  boolean (true/false), overall result of method</li>
     * <li>responses: list of response objects (with fields reason, details, response), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return if list of structure elements is valid to approve
     */
    @Operation(
            summary     = "Return if structure elements are valid to approve",
            description = "Return if structure elements are valid to approve. "
                        + "If structure elements are valid to approve, successful approve of structures can be expected."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all structure elements validated ok, false otherwise, responses contain array with result for each structure element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validateapprove",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresApprove(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Return if structure elements are valid to cancel.
     * If structure elements are valid to cancel, successful cancel of structures can be expected.
     *
     * <p>
     * Returned object has four fields (message, details, response, responses).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>response:  boolean (true/false), overall result of method</li>
     * <li>responses: list of response objects (with fields reason, details, response), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return if list of structure elements is valid to cancel
     */
    @Operation(
            summary     = "Return if structure elements are valid to cancel",
            description = "Return if structure elements are valid to cancel. "
                        + "If structure elements are valid to cancel, successful cancel of structures can be expected."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all structure elements validated ok, false otherwise, responses contain array with result for each structure element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validatecancel",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresCancel(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Return if structure elements are valid to reject.
     * If structure elements are valid to reject, successful reject of structures can be expected.
     *
     * <p>
     * Returned object has four fields (message, details, response, responses).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>response:  boolean (true/false), overall result of method</li>
     * <li>responses: list of response objects (with fields reason, details, response), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElements list of structure elements
     * @return if list of structure elements is valid to reject
     */
    @Operation(
            summary     = "Return if structure elements are valid to reject",
            description = "Return if structure elements are valid to reject. "
                        + "If structure elements are valid to reject, successful reject of structures can be expected."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all structure elements validated ok, false otherwise, responses contain array with result for each structure element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validatereject",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresReject(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Update (propose) structures by list of structure elements.
     * Return list of updated structure elements (proposals).
     *
     * @param structureElements list of structure elements
     * @return list of structure elements for updated structures (proposals)
     */
    @Operation(
            summary     = "Update (propose) structures by list of structure elements",
            description = "Update (propose) structures by list of structure elements. "
                        + "Return array of updated structure elements (proposals)."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements for updated structures (proposals).", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.PUT,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<StructureElement> updateStructures(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Delete (propose) structures by list of structure elements.
     * Return list of deleted structure elements (proposals).
     *
     * @param structureElements list of structure elements
     * @return list of structure elements for deleted structures (proposals)
     */
    @Operation(
            summary     = "Delete (propose) structures by list of structure elements",
            description = "Delete (propose) structures by list of structure elements. "
                        + "Return array of deleted structure elements (proposals)."
                        + "\n\n"
                        + "Structure element attributes required: \n"
                        + "- type \n"
                        + "- parent (System, Subsystem, DeviceGroup, DeviceType) \n"
                        + "- name \n"
                        + "- mnemonic (System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup) \n"
                        + "- description \n"
                        + "- comment"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements for deleted structures (proposals).", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.DELETE,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<StructureElement> deleteStructures(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Approve structures (proposals) by list of structure elements.
     * Return list of approved structure elements.
     *
     * @param structureElements list of structure elements
     * @return list of structure elements for approved structures
     */
    @Operation(
            summary     = "Approve structures (proposals) by list of structure elements",
            description = "Approve structures (proposals) by list of structure elements. "
                        + "Return list of approved structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements for approved structures.", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/approve",
            method = RequestMethod.PATCH,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<StructureElement> approveStructures(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Cancel structures (proposals) by list of structure elements.
     * Return list of cancelled structure elements.
     *
     * @param structureElements list of structure elements
     * @return list of structure elements for cancelled structures
     */
    @Operation(
            summary     = "Cancel structures (proposals) by list of structure elements",
            description = "Cancel structures (proposals) by list of structure elements. "
                        + "Return list of cancelled structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements for cancelled structures.", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/cancel",
            method = RequestMethod.PATCH,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<StructureElement> cancelStructures(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

    /**
     * Reject structures (proposals) by list of structure elements.
     * Return list of rejected structure elements.
     *
     * @param structureElements list of structure elements
     * @return list of structure elements for rejected structures
     */
    @Operation(
            summary     = "Reject structures (proposals) by list of structure elements",
            description = "Reject structures (proposals) by list of structure elements. "
                        + "Return list of rejected structure elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of structure elements for rejected structures.", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/reject",
            method = RequestMethod.PATCH,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<StructureElement> rejectStructures(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of structure elements", required = true) @RequestBody(required = true) List<StructureElement> structureElements);

}
