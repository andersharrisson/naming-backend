/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.api.v1;

import java.util.List;

import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides names data for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "Names",
     description = "handle names data for Naming application")
@RequestMapping("/api/v1/names")
public interface INames {

    /*
       FieldName   - UUID, NAME, NAMEEQUIVALENCE, SYSTEMSTRUCTURE, DEVICESTRUCTURE, DESCRIPTION

       NameElement - uuid               (UUID)
                     systemgroup        (UUID)
                     system             (UUID)
                     subsystem          (UUID)
                     devicetype         (UUID)
                     systemstructure    (String)(mnemonic path)
                     devicestructure    (String)(mnemonic path)
                     index              (String)
                     name               (String)
                     description        (String)
                     status             (Status)
                     latest             (Boolean)
                     deleted            (Boolean)
                     when               (Date)
                     who                (String)
                     comment            (String)

       authentication/authorization
           3 levels - no, user, administrator
               no            - read
               user          - create, update, delete
             ( administrator )

       Methods
           create    POST   /names                                - createNames(List<NameElement>)
           ----------------------------------------------------------------------------------------------------
           read      GET    /names                                - readNames(Boolean, FieldName[], String[], FieldName, Boolean, Integer, Integer)
           read      GET    /names/{name}                         - readNames(String, FieldName, Boolean, Integer, Integer)
           read      GET    /names/systemstructure/{mnemonicpath} - readNamesSystemStructure(String, FieldName, Boolean, Integer, Integer)
           read      GET    /names/devicestructure/{mnemonicpath} - readNamesDeviceStructure(String, FieldName, Boolean, Integer, Integer)
           read      GET    /names/history/{uuid}                 - readNamesHistory(String, FieldName, Boolean, Integer, Integer)
           ----------------------------------------------------------------------------------------------------
           read      GET    /names/equivalence/{name}             - equivalenceName(String)
           read      GET    /names/exists/{name}                  - existsName(String)
           read      GET    /names/islegacy/{name}                - isLegacyName(String)
           read      GET    /names/isvalidtocreate/{name}         - isValidToCreateName(String)
           ----------------------------------------------------------------------------------------------------
           read      GET    /names/validatecreate                 - validateNamesCreate(List<NameElement>)
           read      GET    /names/validateupdate                 - validateNamesUpdate(List<NameElement>)
           read      GET    /names/validatedelete                 - validateNamesDelete(List<NameElement>)
           ----------------------------------------------------------------------------------------------------
           update    PUT    /names                                - updateNames(List<NameElement>)
           ----------------------------------------------------------------------------------------------------
           delete    DELETE /names                                - deleteNames(List<NameElement>)

       Note
           read      GET    /names/{name} - both name and uuid (name - exact and search, uuid exact)
     */

    /**
     * Create names by list of name elements.
     * Return list of created name elements.
     *
     * @param nameElements list of name elements
     * @return list of created name elements
     */
    @Operation(
            summary     = "Create names by array of name elements",
            description = "Create names by array of name elements. "
                        + "Return array of created name elements."
                        + "\n\n"
                        + "Name element attributes required: \n"
                        + "- name \n"
                        + "- description \n"
                        + "- comment \n"
                        + "- either of \n"
                        + "-- subsystem \n"
                        + "-- subsystem, device type, index \n"
                        + "-- system \n"
                        + "-- system, device type, index \n"
                        + "-- system group \n"
                        + "-- system group, device type, index"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of created name elements.", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",             content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",   content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.POST,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<NameElement> createNames(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of name elements", required = true) @RequestBody(required = true) List<NameElement> nameElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Find valid names (search).
     * Return list of name elements.
     *
     * @param deleted if deleted-only names are to be included (false for non-deleted-only names, true for deleted-only names, not used for both cases)
     * @param queryFields search fields
     * @param queryValues search values corresponding to search fields
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid names (search)",
            description = "Find valid names (search). "
                        + "Return array of name elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of name elements.",        content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.GET,
            produces = {"application/json"})
    public List<NameElement> readNames(
            @Parameter(in = ParameterIn.QUERY, description = "if deleted-only names are to be included, false for non-deleted-only names, true for deleted-only names, not used for both cases") @RequestParam(required = false) Boolean deleted,
            @Parameter(in = ParameterIn.QUERY, description = "search fields") @RequestParam(required = false) FieldName[] queryFields,
            @Parameter(in = ParameterIn.QUERY, description = "search values corresponding to search fields") @RequestParam(required = false) String[] queryValues,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find valid names by name or uuid (search).
     * Return list of name elements.
     *
     * @param name name or uuid to search for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid names by name or uuid (search)",
            description = "Find valid names by name or uuid (search). "
                        + "Return array of name elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of name elements.",        content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/{name}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public List<NameElement> readNames(
            @Parameter(in = ParameterIn.PATH,  description = "name or uuid to search for") @PathVariable("name") String name,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find valid names by system structure mnemonic path (search).
     * Return list of name elements.
     *
     * @param mnemonicpath mnemonic path to search for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid names by system structure mnemonic path (search)",
            description = "Find valid names by system structure mnemonic path (search). "
                        + "Return array of name elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of name elements.",        content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/systemstructure/{mnemonicpath}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public List<NameElement> readNamesSystemStructure(
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic path to search for") @PathVariable("mnemonicpath") String mnemonicpath,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find valid names by device structure mnemonic path (search).
     * Return list of name elements.
     *
     * @param mnemonicpath mnemonic path to search for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid names by device structure mnemonic path (search)",
            description = "Find valid names by device structure mnemonic path (search). "
                        + "Return array of name elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of name elements.",        content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/devicestructure/{mnemonicpath}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public List<NameElement> readNamesDeviceStructure(
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic path to search for") @PathVariable("mnemonicpath") String mnemonicpath,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    /**
     * Find history for name by uuid (exact match).
     * Return list of name elements.
     *
     * @param uuid uuid to find history for
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param offset offset, page starting from 0
     * @param limit limit, page size
     * @return list of name elements
     */
    @Operation(
            summary     = "Find history for name by uuid (exact match)",
            description = "Find history for name by uuid (exact match). "
                        + "Return array of name elements."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of name elements.",        content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/history/{uuid}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public List<NameElement> readNamesHistory(
            @Parameter(in = ParameterIn.PATH,  description = "uuid to find history for") @PathVariable("uuid") String uuid,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "offset, page starting from 0") @RequestParam(required = false) Integer offset,
            @Parameter(in = ParameterIn.QUERY, description = "limit, page size") @RequestParam(required = false) Integer limit);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return name equivalence for name.
     *
     * @param name name to get name equivalence for
     * @return name equivalence
     */
    @Operation(
            summary     = "Return name equivalence for name",
            description = "Return name equivalence for name."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return name equivalence.",              content = @Content(mediaType = "text/plain", schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))

    })
    @RequestMapping(
            value = "/equivalence/{name}",
            method = RequestMethod.GET)
    public String equivalenceName(
             @Parameter(in = ParameterIn.PATH, description = "name to get name equivalence for") @PathVariable("name") String name);

    /**
     * Return if name exists (exact match).
     *
     * <p>
     * Response is true if name exists, false otherwise. Message and details are available if no response is available.
     * </p>
     *
     * @param name name to check if it exists
     * @return if name exists
     */
    @Operation(
            summary     = "Return if name exists (exact match)",
            description = "Return if name exists (exact match)."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if name exists, false otherwise. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/exists/{name}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> existsName(
             @Parameter(in = ParameterIn.PATH, description = "name to check if it exists") @PathVariable("name") String name);

    /**
     * Return if name is legacy name (exact match).
     * A name is considered legacy name if one or more of its parents is deleted.
     *
     * <p>
     * Response is true if name is legacy name, false otherwise. Message and details are available if no response is available.
     * </p>
     *
     * @param name name to check if it is legacy name
     * @return if name is legacy name
     */
    @Operation(
            summary     = "Return if name is legacy name (exact match)",
            description = "Return if name is legacy name (exact match). "
                        + "A name is considered legacy name if one or more of its parents is deleted."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if name is legacy name, false otherwise. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/islegacy/{name}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> isLegacyName(
            @Parameter(in = ParameterIn.PATH, description = "name to check if it is legacy name") @PathVariable("name") String name);

    /**
     * Return if name is valid to create (exact match).
     * Method answers question 'would it be ok to create given name?'.
     *
     * <p>
     * Response is true if name is valid to create, false otherwise. Message and details are available if no response is available.
     * </p>
     *
     * @param name name to check if it is valid to create
     * @return if name is valid to create
     */
    @Operation(
            summary     = "Return if name is valid to create (exact match)",
            description = "Return if name is valid to create (exact match). "
                        + "Method answers question 'would it be ok to create given name?'."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if name is valid to create, false otherwise. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/isvalidtocreate/{name}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> isValidToCreateName(
            @Parameter(in = ParameterIn.PATH, description = "name to check if it is valid to create") @PathVariable("name") String name);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return if name elements are valid to create.
     * If names are valid to create, successful create of names can be expected.
     *
     * <p>
     * Response is true if all name elements validated ok, false otherwise, responses contain array with result for each name element. Message and details are available if no response is available.
     * </p>
     *
     * @param nameElements list of name elements
     * @return if list of name elements is valid to create
     */
    @Operation(
            summary     = "Return if name elements are valid to create",
            description = "Return if name elements are valid to create. "
                        + "If names are valid to create, successful create of names can be expected."
                        + "\n\n"
                        + "Name element attributes required: \n"
                        + "- name \n"
                        + "- description \n"
                        + "- comment \n"
                        + "- either of \n"
                        + "-- subsystem \n"
                        + "-- subsystem, device type, index \n"
                        + "-- system \n"
                        + "-- system, device type, index \n"
                        + "-- system group \n"
                        + "-- system group, device type, index"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all name elements validated ok, false otherwise, responses contain array with result for each name element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validatecreate",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateNamesCreate(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of name elements", required = true) @RequestBody(required = true) List<NameElement> nameElements);

    /**
     * Return if name elements are valid to update.
     * If names are valid to update, successful update of names can be expected.
     *
     * <p>
     * Response is true if all name elements validated ok, false otherwise, responses contain array with result for each name element. Message and details are available if no response is available.
     * </p>
     *
     * @param nameElements list of name elements
     * @return if list of name elements is valid to update
     */
    @Operation(
            summary     = "Return if name elements are valid to update",
            description = "Return if name elements are valid to update. "
                        + "If names are valid to update, successful update of names can be expected."
                        + "\n\n"
                        + "Name element attributes required: \n"
                        + "- name \n"
                        + "- description \n"
                        + "- comment \n"
                        + "- either of \n"
                        + "-- subsystem \n"
                        + "-- subsystem, device type, index \n"
                        + "-- system \n"
                        + "-- system, device type, index \n"
                        + "-- system group \n"
                        + "-- system group, device type, index"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all name elements validated ok, false otherwise, responses contain array with result for each name element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validateupdate",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateNamesUpdate(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of name elements", required = true) @RequestBody(required = true) List<NameElement> nameElements);

    /**
     * Return if name elements are valid to delete.
     * If names are valid to update, successful delete of names can be expected.
     *
     * <p>
     * Response is true if all name elements validated ok, false otherwise, responses contain array with result for each name element. Message and details are available if no response is available.
     * </p>
     *
     * @param nameElements list of name elements
     * @return if list of name elements is valid to delete
     */
    @Operation(
            summary     = "Return if name elements are valid to delete",
            description = "Return if name elements are valid to delete. "
                        + "If names are valid to update, successful delete of names can be expected."
                        + "\n\n"
                        + "Name element attributes required: \n"
                        + "- name \n"
                        + "- description \n"
                        + "- comment \n"
                        + "- either of \n"
                        + "-- subsystem \n"
                        + "-- subsystem, device type, index \n"
                        + "-- system \n"
                        + "-- system, device type, index \n"
                        + "-- system group \n"
                        + "-- system group, device type, index"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Response is true if all name elements validated ok, false otherwise, responses contain array with result for each name element. Message and details are available if no response is available.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",  content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            value = "/validatedelete",
            method = RequestMethod.GET,
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateNamesDelete(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of name elements", required = true) @RequestBody(required = true) List<NameElement> nameElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Update names by list of name elements.
     * Returns list of updated name elements.
     *
     * @param nameElements list of name elements
     * @return list of updated name elements
     */
    @Operation(
            summary     = "Update names by list of name elements",
            description = "Update names by list of name elements. "
                        + "Return array of updated name elements."
                        + "\n\n"
                        + "Name element attributes required: \n"
                        + "- name \n"
                        + "- description \n"
                        + "- comment \n"
                        + "- either of \n"
                        + "-- subsystem \n"
                        + "-- subsystem, device type, index \n"
                        + "-- system \n"
                        + "-- system, device type, index \n"
                        + "-- system group \n"
                        + "-- system group, device type, index"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of updated name elements.", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",             content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",   content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.PUT,
            produces = {"application/json"},
            consumes = {"application/json"})
    @PutMapping
    public List<NameElement> updateNames(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of name elements", required = true) @RequestBody(required = true) List<NameElement> nameElements);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Delete names by list of name elements.
     * Returns list of deleted name elements.
     *
     * @param uuid uuid
     * @return list of deleted name elements
     */
    @Operation(
            summary     = "Delete names by list of name elements",
            description = "Delete names by list of name elements. "
                        + "Return array of deleted name elements."
                        + "\n\n"
                        + "Name element attributes required: \n"
                        + "- name \n"
                        + "- description \n"
                        + "- comment \n"
                        + "- either of \n"
                        + "-- subsystem \n"
                        + "-- subsystem, device type, index \n"
                        + "-- system \n"
                        + "-- system, device type, index \n"
                        + "-- system group \n"
                        + "-- system group, device type, index"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Method completed OK. Return array of deleted name elements.", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request. Message and details are available.",             content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error. Message and details are available.",   content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @RequestMapping(
            method = RequestMethod.DELETE,
            produces = {"application/json"},
            consumes = {"application/json"})
    public List<NameElement> deleteNames(
            @Parameter(in = ParameterIn.DEFAULT, description = "array of name elements", required = true) @RequestBody(required = true) List<NameElement> nameElements);

}
