/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This entity represents a name.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "name")
public class Name extends NameStructure implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7313392211143183879L;

    private String systemgroup_uuid;
    private String system_uuid;
    private String subsystem_uuid;
    private String devicetype_uuid;
    private String instance_index;
    private String convention_name;
    private String convention_name_equivalence;

    public UUID getSystemgroupUuid() {
        return systemgroup_uuid != null ? UUID.fromString(systemgroup_uuid) : null;
    }
    public void setSystemgroupUuid(UUID systemgroup_uuid) {
        this.systemgroup_uuid = systemgroup_uuid != null ? systemgroup_uuid.toString() : null;
    }
    public UUID getSystemUuid() {
        return system_uuid != null ? UUID.fromString(system_uuid) : null;
    }
    public void setSystemUuid(UUID system_uuid) {
        this.system_uuid = system_uuid != null ? system_uuid.toString() : null;
    }
    public UUID getSubsystemUuid() {
        return subsystem_uuid != null ? UUID.fromString(subsystem_uuid) : null;
    }
    public void setSubsystemUuid(UUID subsystem_uuid) {
        this.subsystem_uuid = subsystem_uuid != null ? subsystem_uuid.toString() : null;
    }
    public UUID getDevicetypeUuid() {
        return devicetype_uuid != null ? UUID.fromString(devicetype_uuid) : null;
    }
    public void setDevicetypeUuid(UUID devicetype_uuid) {
        this.devicetype_uuid = devicetype_uuid != null ? devicetype_uuid.toString() : null;
    }
    public String getInstanceIndex() {
        return instance_index;
    }
    public void setInstanceIndex(String instance_index) {
        this.instance_index = instance_index;
    }
    public String getConventionName() {
        return convention_name;
    }
    public void setConventionName(String convention_name) {
        this.convention_name = convention_name;
    }
    public String getConventionNameEquivalence() {
        return convention_name_equivalence;
    }
    public void setConventionNameEquivalence(String convention_name_equivalence) {
        this.convention_name_equivalence = convention_name_equivalence;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((Name) obj);
    }

    public boolean equals(Name other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getSystemgroupUuid() == null) {
            if (other.getSystemgroupUuid() != null)
                return false;
        } else if (!getSystemgroupUuid().equals(other.getSystemgroupUuid()))
            return false;
        if (getSystemUuid() == null) {
            if (other.getSystemUuid() != null)
                return false;
        } else if (!getSystemUuid().equals(other.getSystemUuid()))
            return false;
        if (getSubsystemUuid() == null) {
            if (other.getSubsystemUuid() != null)
                return false;
        } else if (!getSubsystemUuid().equals(other.getSubsystemUuid()))
            return false;
        if (getDevicetypeUuid() == null) {
            if (other.getDevicetypeUuid() != null)
                return false;
        } else if (!getDevicetypeUuid().equals(other.getDevicetypeUuid()))
            return false;
        if (getInstanceIndex() == null) {
            if (other.getInstanceIndex() != null)
                return false;
        } else if (!getInstanceIndex().equals(other.getInstanceIndex()))
            return false;
        if (getConventionName() == null) {
            if (other.getConventionName() != null)
                return false;
        } else if (!getConventionName().equals(other.getConventionName()))
            return false;
        if (getConventionNameEquivalence() == null) {
            if (other.getConventionNameEquivalence() != null)
                return false;
        } else if (!getConventionNameEquivalence().equals(other.getConventionNameEquivalence()))
            return false;

        return true;
    }

    public boolean equalsId(Object other) {
        return other instanceof Name && ((Name) other).getId().equals(getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                            + getId());
        sb.append(", \"version\": "                     + getVersion());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"systemgroup_uuid\": "            + getSystemgroupUuid());
        sb.append(", \"system_uuid\": "                 + getSystemUuid());
        sb.append(", \"subsystem_uuid\": "              + getSubsystemUuid());
        sb.append(", \"devicetype_uuid\": "             + getDevicetypeUuid());
        sb.append(", \"instance_index\": "              + getInstanceIndex());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"convention_name_equivalence\": " + getConventionNameEquivalence());
        sb.append(", \"description\": "                 + getDescription());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"latest\": "                      + isLatest());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append(", \"requested\": "                   + getRequested());
        sb.append(", \"requested_by\": "                + getRequestedBy());
        sb.append(", \"requested_comment\": "           + getRequestedComment());
        sb.append(", \"processed\": "                   + getProcessed());
        sb.append(", \"processed_by\": "                + getProcessedBy());
        sb.append(", \"processed_comment\": "           + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                            + getId());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"latest\": "                      + isLatest());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}
