/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This entity represents a device group name part.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "devicegroup")
public class DeviceGroup extends Structure {

    /**
     *
     */
    private static final long serialVersionUID = -8762287473417706804L;

    private String parent_uuid;

    public UUID getParentUuid() {
        return parent_uuid != null ? UUID.fromString(parent_uuid) : null;
    }
    public void setParentUuid(UUID parent_uuid) {
        this.parent_uuid = parent_uuid != null ? parent_uuid.toString() : null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((DeviceGroup) obj);
    }

    public boolean equals(DeviceGroup other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getParentUuid() == null) {
            if (other.getParentUuid() != null)
                return false;
        } else if (!getParentUuid().equals(other.getParentUuid()))
            return false;

        return true;
    }

    public boolean equalsId(Object other) {
        return other instanceof DeviceGroup && ((DeviceGroup) other).getId().equals(getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                     + getId());
        sb.append(", \"version\": "              + getVersion());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"parent_uuid\": "          + getParentUuid());
        sb.append(", \"name\": "                 + getName());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"mnemonic_equivalence\": " + getMnemonicEquivalence());
        sb.append(", \"description\": "          + getDescription());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"latest\": "               + isLatest());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append(", \"requested\": "            + getRequested());
        sb.append(", \"requested_by\": "         + getRequestedBy());
        sb.append(", \"requested_comment\": "    + getRequestedComment());
        sb.append(", \"processed\": "            + getProcessed());
        sb.append(", \"processed_by\": "         + getProcessedBy());
        sb.append(", \"processed_comment\": "    + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": " + getId());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"latest\": "               + isLatest());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}
