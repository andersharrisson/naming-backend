/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import org.openepics.names.rest.beans.Status;

/**
 * A superclass implementing properties required by JPA. It should be extended by
 * name and structure classes that need to be persisted to the database.
 *
 * @author Lars Johansson
 */
@MappedSuperclass
public class NameStructure extends Persistable implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 736094142259082938L;

    private String uuid;
    private String description;
    @Enumerated(EnumType.STRING)
    private Status status;
    private Boolean latest;
    private Boolean deleted;
    private Date requested;
    private String requested_by;
    private String requested_comment;
    private Date processed;
    private String processed_by;
    private String processed_comment;

    public UUID getUuid() {
        return uuid != null ? UUID.fromString(uuid) : null;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid != null ? uuid.toString() : null;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Boolean isLatest() {
        return latest;
    }
    public void setLatest(Boolean latest) {
        this.latest = latest;
    }
    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Date getRequested() {
        return requested;
    }
    public void setRequested(Date requested) {
        this.requested = requested;
    }
    public String getRequestedBy() {
        return requested_by;
    }
    public void setRequestedBy(String requested_by) {
        this.requested_by = requested_by;
    }
    public String getRequestedComment() {
        return requested_comment;
    }
    public void setRequestedComment(String requested_comment) {
        this.requested_comment = requested_comment;
    }
    public Date getProcessed() {
        return processed;
    }
    public void setProcessed(Date processed) {
        this.processed = processed;
    }
    public String getProcessedBy() {
        return processed_by;
    }
    public void setProcessedBy(String processed_by) {
        this.processed_by = processed_by;
    }
    public String getProcessedComment() {
        return processed_comment;
    }
    public void setProcessedComment(String processed_comment) {
        this.processed_comment = processed_comment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals((NameStructure) obj);
    }

    public boolean equals(NameStructure other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;
        if (getStatus() == null) {
            if (other.getStatus() != null)
                return false;
        } else if (!getStatus().equals(other.getStatus()))
            return false;
        if (isLatest() == null) {
            if (other.isLatest() != null)
                return false;
        } else if (!isLatest().equals(other.isLatest()))
            return false;
        if (isDeleted() == null) {
            if (other.isDeleted() != null)
                return false;
        } else if (!isDeleted().equals(other.isDeleted()))
            return false;
        if (getRequested() == null) {
            if (other.getRequested() != null)
                return false;
        } else if (!getRequested().equals(other.getRequested()))
            return false;
        if (getRequestedBy() == null) {
            if (other.getRequestedBy() != null)
                return false;
        } else if (!getRequestedBy().equals(other.getRequestedBy()))
            return false;
        if (getRequestedComment() == null) {
            if (other.getRequestedComment() != null)
                return false;
        } else if (!getRequestedComment().equals(other.getRequestedComment()))
            return false;
        if (getProcessed() == null) {
            if (other.getProcessed() != null)
                return false;
        } else if (!getProcessed().equals(other.getProcessed()))
            return false;
        if (getProcessedBy() == null) {
            if (other.getProcessedBy() != null)
                return false;
        } else if (!getProcessedBy().equals(other.getProcessedBy()))
            return false;
        if (getProcessedComment() == null) {
            if (other.getProcessedComment() != null)
                return false;
        } else if (!getProcessedComment().equals(other.getProcessedComment()))
            return false;

        return true;
    }

    public boolean equalsId(Object other) {
        return other instanceof NameStructure && ((NameStructure) other).getId().equals(getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid());
    }

}
