/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This entity represents a discipline name part.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "discipline")
public class Discipline extends Structure {

    /**
     *
     */
    private static final long serialVersionUID = 8729921221024362502L;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((Discipline) obj);
    }

    public boolean equals(Discipline other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        return true;
    }

    public boolean equalsId(Object other) {
        return other instanceof Discipline && ((Discipline) other).getId().equals(getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                     + getId());
        sb.append(", \"version\": "              + getVersion());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"name\": "                 + getName());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"mnemonic_equivalence\": " + getMnemonicEquivalence());
        sb.append(", \"description\": "          + getDescription());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"latest\": "               + isLatest());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append(", \"requested\": "            + getRequested());
        sb.append(", \"requested_by\": "         + getRequestedBy());
        sb.append(", \"requested_comment\": "    + getRequestedComment());
        sb.append(", \"processed\": "            + getProcessed());
        sb.append(", \"processed_by\": "         + getProcessedBy());
        sb.append(", \"processed_comment\": "    + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": " + getId());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"latest\": "               + isLatest());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}
