/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

/**
 * A superclass implementing properties required by JPA. It should be extended by
 * System structure and Device structure classes that need to be persisted to the database.
 *
 * @author Lars Johansson
 */
@MappedSuperclass
public class Structure extends NameStructure implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5980090194197159918L;

    private String name;
    private String mnemonic;
    private String mnemonic_equivalence;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMnemonic() {
        return mnemonic;
    }
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    public String getMnemonicEquivalence() {
        return mnemonic_equivalence;
    }
    public void setMnemonicEquivalence(String mnemonic_equivalence) {
        this.mnemonic_equivalence = mnemonic_equivalence;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals((Structure) obj);
    }

    public boolean equals(Structure other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (getMnemonic() == null) {
            if (other.getMnemonic() != null)
                return false;
        } else if (!getMnemonic().equals(other.getMnemonic()))
            return false;
        if (getMnemonicEquivalence() == null) {
            if (other.getMnemonicEquivalence() != null)
                return false;
        } else if (!getMnemonicEquivalence().equals(other.getMnemonicEquivalence()))
            return false;

        return true;
    }

    public boolean equalsId(Object other) {
        return other instanceof Structure && ((Structure) other).getId().equals(getId());
    }

}
