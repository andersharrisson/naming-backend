/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.List;

import org.openepics.names.repository.model.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find device type structure (name part) information from JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IDeviceTypeRepository extends JpaRepository<DeviceType, Long> {

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.uuid = ?1")
    DeviceType findLatestByUuid(String uuid);

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.deleted = false AND dt.mnemonic = ?1")
    DeviceType findLatestNotDeletedByMnemonic(String mnemonic);

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.deleted = false AND dt.uuid = ?1")
    DeviceType findLatestNotDeletedByUuid(String uuid);

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.deleted = false AND dt.parent_uuid = ?1 and dt.mnemonic = ?2")
    DeviceType findLatestNotDeletedByParentAndMnemonic(String uuid, String mnemonic);

    @Query("FROM DeviceType dt WHERE dt.uuid = ?1")
    List<DeviceType> findByUuid(String uuid);

    @Query("FROM DeviceType dt WHERE dt.latest = true")
    List<DeviceType> findLatest();

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.mnemonic = ?1")
    List<DeviceType> findLatestByMnemonic(String mnemonic);

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.deleted = false")
    List<DeviceType> findLatestNotDeleted();

    @Query("FROM DeviceType dt WHERE dt.latest = true AND dt.deleted = false AND dt.parent_uuid = ?1")
    List<DeviceType> findLatestNotDeletedByParent(String uuid);

}
