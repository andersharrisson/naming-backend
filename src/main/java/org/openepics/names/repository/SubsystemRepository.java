/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.BooleanUtils;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.springframework.stereotype.Repository;

/**
 * Handle subsystem information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class SubsystemRepository {

    private static final String PERCENT = "%";

    @PersistenceContext
    private EntityManager em;

    /**
     * Count subsystems.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @return count of subsystems
     */
    public Long countSubsystems(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues) {
        return countSubsystems(statuses, deleted, queryFields, queryValues, Boolean.FALSE);
    }

    /**
     * Count subsystems.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @return count of subsystems
     */
    public Long countSubsystems(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory) {

        // note
        //     use of function for mnemonic path
        // where
        //     statuses
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Subsystem> from = cq.from(Subsystem.class);

        cq.where(cb.and(preparePredicatesSubsystems(cb, from, statuses, deleted, queryFields, queryValues, includeHistory).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find subsystems.
     *
     * @param status status
     * @param deleted deleted
     * @param queryField query field
     * @param queryValue query value
     * @return list of subsystems
     */
    public List<Subsystem> readSubsystems(
            Status status, Boolean deleted, FieldStructure queryField, String queryValue) {

        return readSubsystems(
                status != null ? new Status[] {status} : null,
                deleted,
                queryField != null ? new FieldStructure[] {queryField} : null,
                queryValue != null ? new String[] {queryValue} : null,
                Boolean.FALSE,
                null, null, null, null);
    }

    /**
     * Find subsystems.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of subsystems
     */
    public List<Subsystem> readSubsystems(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readSubsystems(
                statuses, deleted, queryFields, queryValues, Boolean.FALSE,
                orderBy, isAsc, offset, limit);
    }

    /**
     * Find subsystems.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of subsystems
     */
    public List<Subsystem> readSubsystems(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // note
        //     use of function for mnemonic path
        // where
        //     statuses
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Subsystem> cq = cb.createQuery(Subsystem.class);
        Root<Subsystem> from = cq.from(Subsystem.class);

        cq.where(cb.and(preparePredicatesSubsystems(cb, from, statuses, deleted, queryFields, queryValues, includeHistory).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            if (BooleanUtils.toBoolean(isAsc)) {
                if (FieldStructure.NAME.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("name")));
                } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("mnemonic")));
                } else if (FieldStructure.MNEMONICEQUIVALENCE.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("mnemonic_equivalence")));
                } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
                    cq.orderBy(cb.asc(cb.function("get_mnemonic_path_subsystem", String.class, from.get("uuid"))));
                } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("description")));
                } else {
                    cq.orderBy(cb.asc(from.get("convention_name")));
                }
            } else {
                if (FieldStructure.NAME.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("name")));
                } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("mnemonic")));
                } else if (FieldStructure.MNEMONICEQUIVALENCE.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("mnemonic_equivalence")));
                } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
                    cq.orderBy(cb.desc(cb.function("get_mnemonic_path_subsystem", String.class, from.get("uuid"))));
                } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("description")));
                } else {
                    cq.orderBy(cb.desc(from.get("name")));
                }
            }
        }

        TypedQuery<Subsystem> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for subsystems.
     *
     * @param cb criteria builder
     * @param from criteria query root
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesSubsystems(
            CriteriaBuilder cb, Root<Subsystem> from,
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory) {

        List<Predicate> predicates = new ArrayList<>();

        if (!Boolean.TRUE.equals(includeHistory)) {
            // exclude (approved and not latest)
            Predicate predicateApproved  = cb.equal(from.get("status"), Status.APPROVED);
            Predicate predicateNotLatest = cb.equal(from.get("latest"), Boolean.FALSE);
            Predicate predicateExclude   = cb.not(cb.and(predicateApproved, predicateNotLatest));
            predicates.add(predicateExclude);
        }

        if (statuses != null) {
            List<Predicate> predicatesStatus = new ArrayList<>();
            for (Status status : statuses) {
                predicatesStatus.add(cb.equal(from.get("status"), status));
            }
            predicates.add(cb.or((Predicate[]) predicatesStatus.toArray(new Predicate[0])));
        }
        if (deleted != null) {
            predicates.add(cb.equal(from.get("deleted"), deleted));
        }
        if (queryFields != null) {
            for (int i=0; i<queryFields.length; i++) {
                String queryValue = queryValues[i];

                // jpa query characters % and _
                // remove excess % characters
                if (queryValue.startsWith(PERCENT)) {
                    while (queryValue.startsWith(PERCENT)) {
                        queryValue = queryValue.substring(1);
                    }
                    queryValue = PERCENT + queryValue;
                }
                if (queryValue.endsWith(PERCENT)) {
                    while (queryValue.endsWith(PERCENT)) {
                        queryValue = queryValue.substring(0, queryValue.length()-1);
                    }
                    queryValue = queryValue + PERCENT;
                }

                switch (queryFields[i]) {
                    case UUID:
                        predicates.add(cb.and(cb.equal(from.get("uuid"), queryValue)));
                        break;
                    case PARENT:
                        predicates.add(cb.and(cb.equal(from.get("parent_uuid"), queryValue)));
                        break;
                    case NAME:
                        predicates.add(cb.and(cb.like(from.get("name"), queryValue)));
                        break;
                    case MNEMONIC:
                        predicates.add(cb.and(cb.like(from.get("mnemonic"), queryValue)));
                        break;
                    case MNEMONICEQUIVALENCE:
                        predicates.add(cb.and(cb.like(from.get("mnemonic_equivalence"), queryValue)));
                        break;
                    case MNEMONICPATH:
                        predicates.add(cb.and(cb.like(cb.function("get_mnemonic_path_subsystem", String.class, from.get("uuid")), queryValue)));
                        break;
                    case DESCRIPTION:
                        predicates.add(cb.and(cb.like(from.get("description"), queryValue)));
                        break;
                    default:
                        continue;
                }
            }
        }

        return predicates;
    }

    /**
     * Persist subsystem into persistence context.
     *
     * @param subsystem subsystem
     */
    public void createSubsystem(Subsystem subsystem) {
        em.persist(subsystem);
    }

    /**
     * Merge subsystem into persistence context.
     *
     * @param subsystem subsystem
     */
    public void updateSubsystem(Subsystem subsystem) {
        em.merge(subsystem);
    }

}
