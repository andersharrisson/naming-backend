/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.BooleanUtils;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.springframework.stereotype.Repository;

/**
 * Handle system group information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class SystemGroupRepository {

    private static final String PERCENT = "%";

    @PersistenceContext
    private EntityManager em;

    /**
     * Count system groups.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @return count of system groups
     */
    public Long countSystemGroups(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues) {
        return countSystemGroups(statuses, deleted, queryFields, queryValues, Boolean.FALSE);
    }

    /**
     * Count system groups.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @return count of system groups
     */
    public Long countSystemGroups(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory) {

        // where
        //     statuses
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<SystemGroup> from = cq.from(SystemGroup.class);

        cq.where(cb.and(preparePredicatesSystemGroups(cb, from, statuses, deleted, queryFields, queryValues, includeHistory).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find system groups.
     *
     * @param status status
     * @param deleted deleted
     * @param queryField query field
     * @param queryValue query value
     * @return list of system groups
     */
    public List<SystemGroup> readSystemGroups(
            Status status, Boolean deleted, FieldStructure queryField, String queryValue) {

        return readSystemGroups(
                status != null ? new Status[] {status} : null,
                deleted,
                queryField != null ? new FieldStructure[] {queryField} : null,
                queryValue != null ? new String[] {queryValue} : null,
                Boolean.FALSE,
                null, null, null, null);
    }

    /**
     * Find system groups.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of system groups
     */
    public List<SystemGroup> readSystemGroups(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readSystemGroups(
                statuses, deleted, queryFields, queryValues, Boolean.FALSE,
                orderBy, isAsc, offset, limit);
    }

    /**
     * Find system groups.
     *
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of system groups
     */
    public List<SystemGroup> readSystemGroups(
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // where
        //     statuses,
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SystemGroup> cq = cb.createQuery(SystemGroup.class);
        Root<SystemGroup> from = cq.from(SystemGroup.class);

        cq.where(cb.and(preparePredicatesSystemGroups(cb, from, statuses, deleted, queryFields, queryValues, includeHistory).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            if (BooleanUtils.toBoolean(isAsc)) {
                if (FieldStructure.NAME.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("name")));
                } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("mnemonic")));
                } else if (FieldStructure.MNEMONICEQUIVALENCE.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("mnemonic_equivalence")));
                } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("mnemonic")));
                } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("description")));
                } else {
                    cq.orderBy(cb.asc(from.get("convention_name")));
                }
            } else {
                if (FieldStructure.NAME.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("name")));
                } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("mnemonic")));
                } else if (FieldStructure.MNEMONICEQUIVALENCE.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("mnemonic_equivalence")));
                } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("mnemonic")));
                } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("description")));
                } else {
                    cq.orderBy(cb.desc(from.get("name")));
                }
            }
        }

        TypedQuery<SystemGroup> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for system groups.
     *
     * @param cb criteria builder
     * @param from criteria query root
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesSystemGroups(
            CriteriaBuilder cb, Root<SystemGroup> from,
            Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory) {

        List<Predicate> predicates = new ArrayList<>();

        if (!Boolean.TRUE.equals(includeHistory)) {
            // exclude (approved and not latest)
            Predicate predicateApproved  = cb.equal(from.get("status"), Status.APPROVED);
            Predicate predicateNotLatest = cb.equal(from.get("latest"), Boolean.FALSE);
            Predicate predicateExclude   = cb.not(cb.and(predicateApproved, predicateNotLatest));
            predicates.add(predicateExclude);
        }

        if (statuses != null) {
            List<Predicate> predicatesStatus = new ArrayList<>();
            for (Status status : statuses) {
                predicatesStatus.add(cb.equal(from.get("status"), status));
            }
            predicates.add(cb.or((Predicate[]) predicatesStatus.toArray(new Predicate[0])));
        }
        if (deleted != null) {
            predicates.add(cb.equal(from.get("deleted"), deleted));
        }
        if (queryFields != null) {
            for (int i=0; i<queryFields.length; i++) {
                String queryValue = queryValues[i];

                // jpa query characters % and _
                // remove excess % characters
                if (queryValue.startsWith(PERCENT)) {
                    while (queryValue.startsWith(PERCENT)) {
                        queryValue = queryValue.substring(1);
                    }
                    queryValue = PERCENT + queryValue;
                }
                if (queryValue.endsWith(PERCENT)) {
                    while (queryValue.endsWith(PERCENT)) {
                        queryValue = queryValue.substring(0, queryValue.length()-1);
                    }
                    queryValue = queryValue + PERCENT;
                }

                switch (queryFields[i]) {
                    case UUID:
                        predicates.add(cb.and(cb.equal(from.get("uuid"), queryValue)));
                        break;
                    case NAME:
                        predicates.add(cb.and(cb.like(from.get("name"), queryValue)));
                        break;
                    case MNEMONIC:
                        predicates.add(cb.and(cb.like(from.get("mnemonic"), queryValue)));
                        break;
                    case MNEMONICEQUIVALENCE:
                        predicates.add(cb.and(cb.like(from.get("mnemonic_equivalence"), queryValue)));
                        break;
                    case MNEMONICPATH:
                        predicates.add(cb.and(cb.like(from.get("mnemonic"), queryValue)));
                        break;
                    case DESCRIPTION:
                        predicates.add(cb.and(cb.like(from.get("description"), queryValue)));
                        break;
                    default:
                        continue;
                }
            }
        }

        return predicates;
    }

    /**
     * Persist system group into persistence context.
     *
     * @param systemGroup system group
     */
    public void createSystemGroup(SystemGroup systemGroup) {
        em.persist(systemGroup);
    }

    /**
     * Merge system group into persistence context.
     *
     * @param systemGroup system group
     */
    public void updateSystemGroup(SystemGroup systemGroup) {
        em.merge(systemGroup);
    }

}
