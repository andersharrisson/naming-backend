/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.BooleanUtils;
import org.openepics.names.repository.model.Name;
import org.openepics.names.rest.beans.FieldName;
import org.springframework.stereotype.Repository;

/**
 * Handle name information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class NameRepository {

    private static final String PERCENT = "%";

    @PersistenceContext
    private EntityManager em;

    /**
     * Count names.
     *
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @return count of names
     */
    public Long countNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues) {
        return countNames(deleted, queryFields, queryValues, Boolean.FALSE);
    }

    /**
     * Count names.
     *
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @return count of names
     */
    public Long countNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues, Boolean includeHistory) {

        // note
        //     use of function for mnemonic path
        // where
        //     latest, deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Name> from = cq.from(Name.class);

        cq.where(cb.and(preparePredicatesNames(cb, from, deleted, queryFields, queryValues, includeHistory).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find names.
     *
     * @param deleted deleted
     * @param queryField query field
     * @param queryValue query value
     * @return list of names
     */
    public List<Name> readNames(
            Boolean deleted, FieldName queryField, String queryValue) {

        return readNames(
                deleted,
                queryField != null ? new FieldName[] {queryField} : null,
                queryValue != null ? new String[] {queryValue} : null,
                Boolean.FALSE,
                null, null, null, null);
    }

    /**
     * Find names.
     *
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of names
     */
    public List<Name> readNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readNames(
                deleted, queryFields, queryValues, Boolean.FALSE,
                orderBy, isAsc, offset, limit);
    }

    /**
     * Find names.
     *
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of names
     */
    public List<Name> readNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues, Boolean includeHistory,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // note
        //     use of function for mnemonic path
        // where
        //     latest, deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Name> cq = cb.createQuery(Name.class);
        Root<Name> from = cq.from(Name.class);

        cq.where(cb.and(preparePredicatesNames(cb, from, deleted, queryFields, queryValues, includeHistory).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            if (BooleanUtils.toBoolean(isAsc)) {
                if (FieldName.NAME.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("convention_name")));
                } else if (FieldName.NAMEEQUIVALENCE.equals(orderBy)) {
                        cq.orderBy(cb.asc(from.get("convention_name_equivalence")));
                } else if (FieldName.SYSTEMSTRUCTURE.equals(orderBy)) {
                    cq.orderBy(cb.asc(cb.function("get_mnemonic_path_system_structure", String.class, from.get("convention_name"))));
                } else if (FieldName.DEVICESTRUCTURE.equals(orderBy)) {
                    cq.orderBy(cb.asc(cb.function("get_mnemonic_path_device_structure", String.class, from.get("convention_name"))));
                } else if (FieldName.DESCRIPTION.equals(orderBy)) {
                    cq.orderBy(cb.asc(from.get("description")));
                } else {
                    cq.orderBy(cb.asc(from.get("convention_name")));
                }
            } else {
                if (FieldName.NAME.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("convention_name")));
                } else if (FieldName.NAMEEQUIVALENCE.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("convention_name_equivalence")));
                } else if (FieldName.SYSTEMSTRUCTURE.equals(orderBy)) {
                    cq.orderBy(cb.desc(cb.function("get_mnemonic_path_system_structure", String.class, from.get("convention_name"))));
                } else if (FieldName.DEVICESTRUCTURE.equals(orderBy)) {
                    cq.orderBy(cb.desc(cb.function("get_mnemonic_path_device_structure", String.class, from.get("convention_name"))));
                } else if (FieldName.DESCRIPTION.equals(orderBy)) {
                    cq.orderBy(cb.desc(from.get("description")));
                } else {
                    cq.orderBy(cb.desc(from.get("convention_name")));
                }
            }
        }

        TypedQuery<Name> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for names.
     *
     * @param cb criteria builder
     * @param from criteria query root
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesNames(
            CriteriaBuilder cb, Root<Name> from,
            Boolean deleted, FieldName[] queryFields, String[] queryValues, Boolean includeHistory) {

        List<Predicate> predicates = new ArrayList<>();

        if (!Boolean.TRUE.equals(includeHistory)) {
            // exclude (not latest)
            Predicate predicateNotLatest = cb.equal(from.get("latest"), Boolean.FALSE);
            Predicate predicateExclude   = cb.not(cb.and(predicateNotLatest));
            predicates.add(predicateExclude);
        }

        if (deleted != null) {
            predicates.add(cb.equal(from.get("deleted"), deleted));
        }
        if (queryFields != null) {
            for (int i=0; i<queryFields.length; i++) {
                String queryValue = queryValues[i];

                // jpa query characters % and _
                // remove excess % characters
                if (queryValue.startsWith(PERCENT)) {
                    while (queryValue.startsWith(PERCENT)) {
                        queryValue = queryValue.substring(1);
                    }
                    queryValue = PERCENT + queryValue;
                }
                if (queryValue.endsWith(PERCENT)) {
                    while (queryValue.endsWith(PERCENT)) {
                        queryValue = queryValue.substring(0, queryValue.length()-1);
                    }
                    queryValue = queryValue + PERCENT;
                }

                switch (queryFields[i]) {
                    case UUID:
                        predicates.add(cb.and(cb.equal(from.get("uuid"), queryValue)));
                        break;
                    case NAME:
                        predicates.add(cb.and(cb.like(from.get("convention_name"), queryValue)));
                        break;
                    case NAMEEQUIVALENCE:
                        predicates.add(cb.and(cb.like(from.get("convention_name_equivalence"), queryValue)));
                        break;
                    case SYSTEMSTRUCTURE:
                        predicates.add(cb.and(cb.like(cb.function("get_mnemonic_path_system_structure", String.class, from.get("convention_name")), queryValue)));
                        break;
                    case DEVICESTRUCTURE:
                        predicates.add(cb.and(cb.like(cb.function("get_mnemonic_path_device_structure", String.class, from.get("convention_name")), queryValue)));
                        break;
                    case DESCRIPTION:
                        predicates.add(cb.and(cb.like(from.get("description"), queryValue)));
                        break;
                    default:
                        continue;
                }
            }
        }

        return predicates;
    }

    /**
     * Persist name into persistence context.
     *
     * @param name name
     */
    public void createName(Name name) {
        em.persist(name);
    }

    /**
     * Merge name into persistence context.
     *
     * @param name name
     */
    public void updateName(Name name) {
        em.merge(name);
    }

}
