/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.List;

import org.openepics.names.repository.model.Subsystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find subsystem structure (name part)information from JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface ISubsystemRepository extends JpaRepository<Subsystem, Long> {

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.uuid = ?1")
    Subsystem findLatestByUuid(String uuid);

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.deleted = false AND sub.mnemonic = ?1")
    Subsystem findLatestNotDeletedByMnemonic(String mnemonic);

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.deleted = false AND sub.uuid = ?1")
    Subsystem findLatestNotDeletedByUuid(String uuid);

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.deleted = false AND sub.parent_uuid = ?1 and sub.mnemonic = ?2")
    Subsystem findLatestNotDeletedByParentAndMnemonic(String uuid, String mnemonic);

    @Query("FROM Subsystem sub WHERE sub.uuid = ?1")
    List<Subsystem> findByUuid(String uuid);

    @Query("FROM Subsystem sub WHERE sub.latest = true")
    List<Subsystem> findLatest();

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.mnemonic = ?1")
    List<Subsystem> findLatestByMnemonic(String mnemonic);

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.deleted = false")
    List<Subsystem> findLatestNotDeleted();

    @Query("FROM Subsystem sub WHERE sub.latest = true AND sub.deleted = false AND sub.parent_uuid = ?1")
    List<Subsystem> findLatestNotDeletedByParent(String uuid);

}
