/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.List;

import org.openepics.names.repository.model.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find name information from JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface INameRepository extends JpaRepository<Name, Long> {

    @Query("FROM Name n WHERE n.latest = true AND n.uuid = ?1")
    Name findLatestByUuid(String uuid);

    @Query("FROM Name n WHERE n.latest = true AND n.convention_name = ?1")
    Name findLatestByConventionName(String conventionName);

    @Query("FROM Name n WHERE n.uuid = ?1")
    List<Name> findByUuid(String uuid);

    @Query("FROM Name n WHERE n.latest = true")
    List<Name> findLatest();

    @Query("FROM Name n WHERE n.latest = true AND n.deleted = false")
    List<Name> findLatestNotDeleted();

    @Query("SELECT n FROM Name n, SystemGroup sg "
            + "WHERE n.latest = true "
            + "AND sg.uuid = n.systemgroup_uuid "
            + "AND sg.latest = true "
            + "AND sg.mnemonic = ?1")
    List<Name> findLatestBySystemGroupMnemonic(String mnemonic);

    @Query("SELECT n FROM Name n, System sys "
            + "WHERE n.latest = true "
            + "AND sys.uuid = n.system_uuid "
            + "AND sys.latest = true "
            + "AND sys.mnemonic = ?1")
    List<Name> findLatestBySystemMnemonic(String mnemonic);

    @Query("SELECT n FROM Name n, Subsystem sub, System sys "
            + "WHERE n.latest = true "
            + "AND sub.uuid = n.subsystem_uuid "
            + "AND sub.latest = true "
            + "AND sys.uuid = sub.parent_uuid  "
            + "AND sys.latest = true "
            + "AND sys.mnemonic = ?1")
    List<Name> findLatestBySystemMnemonicThroughSubsystem(String mnemonic);

    @Query("SELECT n FROM Name n, Subsystem sub "
            + "WHERE n.latest = true "
            + "AND sub.uuid = n.subsystem_uuid "
            + "AND sub.latest = true "
            + "AND sub.mnemonic = ?1")
    List<Name> findLatestBySubsystemMnemonic(String mnemonic);

    @Query("SELECT n FROM Name n, DeviceType dt, DeviceGroup dg, Discipline di "
            + "WHERE n.latest = true "
            + "AND dt.uuid = n.devicetype_uuid "
            + "AND dt.latest = true "
            + "AND dg.uuid = dt.parent_uuid "
            + "AND dg.latest = true "
            + "AND di.uuid = dg.parent_uuid "
            + "AND di.latest = true "
            + "AND di.mnemonic = ?1")
    List<Name> findLatestByDisciplineMnemonic(String mnemonic);

    @Query("SELECT n FROM Name n, DeviceType dt "
            + "WHERE n.latest = true "
            + "AND dt.uuid = n.devicetype_uuid "
            + "AND dt.latest = true "
            + "AND dt.mnemonic = ?1")
    List<Name> findLatestByDeviceTypeMnemonic(String mnemonic);

}
