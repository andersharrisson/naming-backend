/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.old.nameviews;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openepics.names.old.model.DeviceRevision;

/**
 * Implementation of part of NameViewProvider.
 * Been that generates the nameView tree and updates name views.
 *
 * @author karinrathsman
 * @author Lars Johansson
 *
 */
public class NameViewProvider implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6916728601880286405L;

    private NameRevisions nameRevisions;

    public NameViewProvider() {
        nameRevisions = new NameRevisions();
    }

    public void update(List<DeviceRevision> newRevisions) {
        for (DeviceRevision revision : newRevisions) {
            nameRevisions.update(revision);
        }
    }

    public String equivalenceClassRepresentative(String name) {
        return name != null
                ? name.toUpperCase()
                        .replaceAll("(?<=[A-Za-z])0+", "")
                        .replace('I', '1').replace('L', '1').replace('O', '0')
                        .replaceAll("(?<!\\d)0+(?=\\d)", "")
                : null;
    }

    /**
     * @return the restfulNameRevisionMap that maps a given device name with the
     * latest revision with that name.
     */
    public NameRevisions getNameRevisions() {
        return nameRevisions;
    }

    /**
     * This class handles map of key-value with equivalence class representative of convention name as key
     * and name revision as value. It is used to build structure of name data.
     *
     * <p>
     * Equivalence class representative of convention name is used as key as it ensure uniqueness of names
     * when treating similar looking names. Name revision represents database entry.
     */
    public class NameRevisions {

        private Map<String, DeviceRevision> deviceRevisionMap;

        /**
         * Constructs new map for equivalence class representative of convention name and name revision objects.
         */
        public NameRevisions() {
            deviceRevisionMap = new HashMap<>();
        }

        private String key(String name) {
            return equivalenceClassRepresentative(name);
        }

        /**
         * Retrieves content from nameRevisionMap for given parameter as <tt>uuid</tt> or <tt>name</tt>.
         * Among usage is REST API and its methods.
         *
         * @param string uuid or name
         * @return name revision
         */
        public DeviceRevision get(String string) {
            // note
            //     equivalenceClassRepresentative of name as key in deviceRevisionMap
            //
            //     method not to call key method with input string directly since that would mean
            //     calling equivalenceClassRepresentative twice, which would potentially strip
            //     information from input string, e.g. CRY0 would become CRY
            //
            //     method works with
            //     - name
            //     - equivalenceClassRepresentative of name
            //     - uuid
            //
            //     1. consider input equivalenceClassRepresentative
            //     2. consider input name (make equivalenceClassRepresentative)
            //     3. consider input uuid

            String key = string;
            if (deviceRevisionMap.containsKey(key)) {
                return deviceRevisionMap.get(key);
            } else if (deviceRevisionMap.containsKey(key(key))) {
                return deviceRevisionMap.get(key(key));
            } else {
                return null;
            }
        }

        /**
         * Updates the deviceRevisionMap if the specified revision supersedes the
         * existing revision with the same name
         *
         * @param revision
         */
        protected void update(DeviceRevision revision) {
            String key = key(revision.getConventionName());
            DeviceRevision otherRevision = deviceRevisionMap.get(key);
            if (revision.supersede(otherRevision)) {
                deviceRevisionMap.put(key, revision);
            }
        }

        /**
         * Return key set for device revision map.
         *
         * @return key set for device revision map
         *
         * @see NameRevisions
         */
        public Set<String> keySet() {
            return deviceRevisionMap.keySet();
        }

        /**
         * Return entry set for device revision map.
         *
         * @return entry set for device revision map
         *
         * @see NameRevisions
         */
        public Set<Map.Entry<String, DeviceRevision>> entrySet() {
            return deviceRevisionMap.entrySet();
        }

        /**
         * Return if device revision map contains given key.
         *
         * @param name key for device revision map
         * @return if device revision map contains given key
         *
         * @see NameRevisions
         */
        public boolean contains(String name) {
            return deviceRevisionMap.containsKey(key(name));
        }

    }

}
