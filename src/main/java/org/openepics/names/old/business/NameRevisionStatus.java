/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.old.business;

import org.openepics.names.old.model.NamePartRevisionStatus;

/**
 * Enum that handles the status of a revision.
 *
 * @author karinrathsman
 *
 */
public enum NameRevisionStatus {
    APPROVED(),
    CANCELLED(),
    PENDING(),
    REJECTED();

    static NameRevisionStatus get(NamePartRevisionStatus status) {
        // To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     *
     * @return true if the revision is unapproved and pending
     */
    public boolean isPending() {
        return equals(PENDING);
    }

    /**
     *
     * @return true if the revision approved
     */
    public boolean isApproved() {
        return equals(APPROVED);
    }

    /**
     *
     * @return true if the revision is unapproved and cancelled or rejcted
     */
    public boolean isCancelled() {
        return equals(CANCELLED) || equals(REJECTED);
    }

    /**
     *
     * @return true if the revision is unapproved and rejcted
     */
    public boolean isRejected() {
        return equals(REJECTED);
    }
}
