/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.old;

import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.old.PartElement;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.NamingConventionUtil;

/**
 * Utility class to assist in populating part elements based on repository content.
 *
 * @author Lars Johansson
 */
public class PartElementUtil {

    protected static final String SYSTEM_STRUCTURE = "System Structure";
    protected static final String DEVICE_STRUCTURE = "Device Structure";
    protected static final String DEVICE_REGISTRY  = "Device Registry";

    protected static final String ONE   = "1";
    protected static final String TWO   = "2";
    protected static final String THREE = "3";

    /**
     * This class is not to be instantiated.
     */
    private PartElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return part element for system group.
     *
     * @param systemGroup system group
     * @return part element
     */
    public static PartElement getPartElement(SystemGroup systemGroup) {
        if (systemGroup == null) {
            return null;
        }

        return new PartElement(
                SYSTEM_STRUCTURE,
                systemGroup.getUuid(),
                systemGroup.getName(),
                systemGroup.getName(),
                systemGroup.getMnemonic(),
                systemGroup.getMnemonic(),
                ONE,
                systemGroup.getDescription(),
                systemGroup.getStatus().toString());
    }

    /**
     * Populate and return part element for system.
     *
     * @param system system
     * @return part element
     */
    public static PartElement getPartElement(System system, HolderIRepositories holderIRepositories) {
        if (system == null) {
            return null;
        }

        SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

        return new PartElement(
                SYSTEM_STRUCTURE,
                system.getUuid(),
                system.getName(),
                NamingConventionUtil.mnemonicPath2String(systemGroup.getName(), system.getName()),
                system.getMnemonic(),
                NamingConventionUtil.mnemonicPath2String(systemGroup.getMnemonic(), system.getMnemonic()),
                TWO,
                system.getDescription(),
                system.getStatus().toString());
    }

    /**
     * Populate and return part element for subsystem.
     *
     * @param subsystem subsystem
     * @return part element
     */
    public static PartElement getPartElement(Subsystem subsystem, HolderIRepositories holderIRepositories) {
        if (subsystem == null) {
            return null;
        }

        System      system      = holderIRepositories.getSystemRepository().findLatestByUuid(subsystem.getParentUuid().toString());
        SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

        return new PartElement(
                SYSTEM_STRUCTURE,
                subsystem.getUuid(),
                subsystem.getName(),
                NamingConventionUtil.mnemonicPath2String(systemGroup.getName(), system.getName(), subsystem.getName()),
                subsystem.getMnemonic(),
                NamingConventionUtil.mnemonicPath2String(systemGroup.getMnemonic(), system.getMnemonic(), subsystem.getMnemonic()),
                THREE,
                subsystem.getDescription(),
                subsystem.getStatus().toString());
    }

    /**
     * Populate and return part element for discipline.
     *
     * @param discipline discipline
     * @return part element
     */
    public static PartElement getPartElement(Discipline discipline) {
        if (discipline == null) {
            return null;
        }

        return new PartElement(
                DEVICE_STRUCTURE,
                discipline.getUuid(),
                discipline.getName(),
                discipline.getName(),
                discipline.getMnemonic(),
                discipline.getMnemonic(),
                ONE,
                discipline.getDescription(),
                discipline.getStatus().toString());
    }

    /**
     * Populate and return part element for device type.
     *
     * @param deviceType device type
     * @return part element
     */
    public static PartElement getPartElement(DeviceType deviceType, HolderIRepositories holderIRepositories) {
        if (deviceType == null) {
            return null;
        }

        DeviceGroup deviceGroup = holderIRepositories.getDeviceGroupRepository().findLatestByUuid(deviceType.getParentUuid().toString());
        Discipline  discipline  = holderIRepositories.getDisciplineRepository().findLatestByUuid(deviceGroup.getParentUuid().toString());

        return new PartElement(
                DEVICE_STRUCTURE,
                deviceType.getUuid(),
                deviceType.getName(),
                NamingConventionUtil.mnemonicPath2String(discipline.getName(), deviceType.getName()),
                NamingConventionUtil.mnemonicPath2String(discipline.getMnemonic(), deviceType.getMnemonic()),
                deviceType.getMnemonic(),
                THREE,
                deviceType.getDescription(),
                deviceType.getStatus().toString());
    }

}
