/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.old;

import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.old.DeviceNameElement;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderSystemDeviceStructure;
import org.openepics.names.util.NameUtil;

/**
 * Utility class to assist in populating device name elements based on repository content.
 *
 * @author Lars Johansson
 */
public class DeviceNameElementUtil {

    protected static final String ACTIVE   = "ACTIVE";
    protected static final String DELETED  = "DELETED";

    /**
     * This class is not to be instantiated.
     */
    private DeviceNameElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return device name element for name.
     *
     * @param name name
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return device name element
     */
    public static DeviceNameElement getDeviceNameElement(Name name, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (name == null) {
            return null;
        }

        // using holder of containers for system and device structure content
        //     for performance reasons to speed up preparation of what is to be returned

        // find out how to populate return element for system structure, device structure
        int levelSystemStructure = NameUtil.getLevelSystemStructure(name);
        int levelDeviceStructure = NameUtil.getLevelDeviceStructure(name);

        // levelSystemStructure -1 ---> error
        // levelDeviceStructure -1 ---> error

        final DeviceNameElement deviceNameElement = new DeviceNameElement();

        // populate return element for system structure
        switch (levelSystemStructure) {
        case 3: {
            Subsystem   subsystem   = holderSystemDeviceStructure.findSubsystemByUuid(name.getSubsystemUuid());
            System      system      = holderSystemDeviceStructure.findSystemByUuid(subsystem.getParentUuid());
            SystemGroup systemGroup = holderSystemDeviceStructure.findSystemGroupByUuid(system.getParentUuid());

            deviceNameElement.setSubsystem(subsystem.getMnemonic());
            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 2: {
            System      system      = holderSystemDeviceStructure.findSystemByUuid(name.getSystemUuid());
            SystemGroup systemGroup = holderSystemDeviceStructure.findSystemGroupByUuid(system.getParentUuid());

            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 1: {
            SystemGroup systemGroup = holderSystemDeviceStructure.findSystemGroupByUuid(name.getSystemgroupUuid());

            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        // populate return element for device structure
        switch (levelDeviceStructure) {
        case 3: {
            DeviceType  deviceType  = holderSystemDeviceStructure.findDeviceTypeByUuid(name.getDevicetypeUuid());
            DeviceGroup deviceGroup = holderSystemDeviceStructure.findDeviceGroupByUuid(deviceType.getParentUuid());
            Discipline  discipline  = holderSystemDeviceStructure.findDisciplineByUuid(deviceGroup.getParentUuid());

            deviceNameElement.setDeviceType(deviceType.getMnemonic());
            deviceNameElement.setDiscipline(discipline.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        deviceNameElement.setUuid(name.getUuid());
        deviceNameElement.setInstanceIndex(name.getInstanceIndex());
        deviceNameElement.setName(name.getConventionName());
        deviceNameElement.setDescription(name.getDescription());
        deviceNameElement.setStatus(name.isDeleted() ? DELETED : ACTIVE);

        return deviceNameElement;
    }

    /**
     * Populate and return device name element for name.
     *
     * @param name name
     * @param holderIRepositories holder of references to repositories
     * @return device name element
     */
    public static DeviceNameElement getDeviceNameElement(Name name, HolderIRepositories holderIRepositories) {
        if (name == null) {
            return null;
        }

        // find out how to populate return element for system structure, device structure
        int levelSystemStructure = NameUtil.getLevelSystemStructure(name);
        int levelDeviceStructure = NameUtil.getLevelDeviceStructure(name);

        // levelSystemStructure -1 ---> error
        // levelDeviceStructure -1 ---> error

        final DeviceNameElement deviceNameElement = new DeviceNameElement();

        // populate return element for system structure
        switch (levelSystemStructure) {
        case 3: {
            Subsystem   subsystem   = holderIRepositories.getSubsystemRepository().findLatestByUuid(name.getSubsystemUuid().toString());
            System      system      = holderIRepositories.getSystemRepository().findLatestByUuid(subsystem.getParentUuid().toString());
            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            deviceNameElement.setSubsystem(subsystem.getMnemonic());
            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 2: {
            System      system      = holderIRepositories.getSystemRepository().findLatestByUuid(name.getSystemUuid().toString());
            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 1: {
            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(name.getSystemgroupUuid().toString());

            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        // populate return element for device structure
        switch (levelDeviceStructure) {
        case 3: {
            DeviceType  deviceType  = holderIRepositories.getDeviceTypeRepository().findLatestByUuid(name.getDevicetypeUuid().toString());
            DeviceGroup deviceGroup = holderIRepositories.getDeviceGroupRepository().findLatestByUuid(deviceType.getParentUuid().toString());
            Discipline  discipline  = holderIRepositories.getDisciplineRepository().findLatestByUuid(deviceGroup.getParentUuid().toString());

            deviceNameElement.setDeviceType(deviceType.getMnemonic());
            deviceNameElement.setDiscipline(discipline.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        deviceNameElement.setUuid(name.getUuid());
        deviceNameElement.setInstanceIndex(name.getInstanceIndex());
        deviceNameElement.setName(name.getConventionName());
        deviceNameElement.setDescription(name.getDescription());
        deviceNameElement.setStatus(name.isDeleted() ? DELETED : ACTIVE);

        return deviceNameElement;
    }

}
