/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.openepics.names.repository.model.Name;

/**
 * Utility class to assist in handling of name content.
 *
 * @author Lars Johansson
 */
public class NameUtil {

    /**
     * This class is not to be instantiated.
     */
    private NameUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return level of system structure for name.
     * <ul>
     * <li>1  <--> system group</li>
     * <li>2  <--> system</li>
     * <li>3  <--> subsystem</li>
     * <li>-1 <--> invalid</li>
     * </ul>
     *
     * @param name name content
     * @return level of system structure
     */
    public static int getLevelSystemStructure(Name name) {
        if (name == null) {
            return -1;
        }

        int count = 0;
        if (name.getSystemgroupUuid() != null) {
            count++;
        };
        if (name.getSystemUuid() != null) {
            count++;
        }
        if (name.getSubsystemUuid() != null) {
            count++;
        }
        if (count != 1) {
            return -1;
        }

        return name.getSubsystemUuid() != null
                ? 3
                : name.getSystemUuid() != null
                        ? 2
                        : name.getSystemgroupUuid() != null
                                ? 1
                                : -1;
    }

    /**
     * Return level of device structure for name.
     * <ul>
     * <li>3  <--> device type</li>
     * <li>-1 <--> invalid</li>
     * </ul>
     *
     * @param name content
     * @return level of device structure
     */
    public static int getLevelDeviceStructure(Name name) {
        if (name == null) {
            return -1;
        }

        return name.getDevicetypeUuid() != null
                ? 3
                : -1;
    }

//  /**
//  * Return system structure mnemonic path for name.
//  *
//  * @param name name
//  * @param levelSystemStructure level of system structure
//  * @param holderIRepositories holder of references to repositories
//  * @return system structure mnemonic path
//  */
// private static String getMnemonicPathSystemStructure(Name name, int levelSystemStructure, HolderIRepositories holderIRepositories) {
//     // populate return element for system structure
//     switch (levelSystemStructure) {
//     case 3: {
//         Subsystem   subsystem   = holderIRepositories.getSubsystemRepository().findLatestByUuid(name.getSubsystemUuid().toString());
//         System      system      = holderIRepositories.getSystemRepository().findLatestByUuid(subsystem.getParentUuid().toString());
//
//         return system.getMnemonic() + "-" + subsystem.getMnemonic();
//     }
//     case 2: {
//         System      system      = holderIRepositories.getSystemRepository().findLatestByUuid(name.getSystemUuid().toString());
//
//         return system.getMnemonic();
//     }
//     case 1: {
//         SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(name.getSystemgroupUuid().toString());
//
//         return systemGroup.getMnemonic();
//     }
//     default:
//         // error
//         return null;
//     }
// }
//
// /**
//  * Return device structure mnemonic path for name.
//  *
//  * @param name name
//  * @param levelDeviceStructure level of device structure
//  * @param holderIRepositories holder of references to repositories
//  * @return device structure mnemonic path
//  */
// private static String getMnemonicPathDeviceStructure(Name name, int levelDeviceStructure, HolderIRepositories holderIRepositories) {
//     // populate return element for device structure
//     switch (levelDeviceStructure) {
//     case 3: {
//         DeviceType  deviceType  = holderIRepositories.getDeviceTypeRepository().findLatestByUuid(name.getDevicetypeUuid().toString());
//         DeviceGroup deviceGroup = holderIRepositories.getDeviceGroupRepository().findLatestByUuid(deviceType.getParentUuid().toString());
//         Discipline  discipline  = holderIRepositories.getDisciplineRepository().findLatestByUuid(deviceGroup.getParentUuid().toString());
//
//         return discipline.getMnemonic() + "-" + deviceType.getMnemonic();
//     }
//     default:
//         // error
//         return null;
//     }
//
// }
//
// /**
//  * Return system structure mnemonic path for name.
//  *
//  * @param name name
//  * @param levelSystemStructure level of system structure
//  * @param holderSystemDeviceStructure holder of containers for system and device structure content
//  * @return system structure mnemonic path
//  */
// private static String getMnemonicPathSystemStructure(Name name, int levelSystemStructure, HolderSystemDeviceStructure holderSystemDeviceStructure) {
//     // populate return element for system structure
//     switch (levelSystemStructure) {
//     case 3: {
//         Subsystem   subsystem   = holderSystemDeviceStructure.findSubsystemByUuid(name.getSubsystemUuid());
//         System      system      = holderSystemDeviceStructure.findSystemByUuid(subsystem.getParentUuid());
//
//         return system.getMnemonic() + "-" + subsystem.getMnemonic();
//     }
//     case 2: {
//         System      system      = holderSystemDeviceStructure.findSystemByUuid(name.getSystemUuid());
//
//         return system.getMnemonic();
//     }
//     case 1: {
//         SystemGroup systemGroup = holderSystemDeviceStructure.findSystemGroupByUuid(name.getSystemgroupUuid());
//
//         return systemGroup.getMnemonic();
//     }
//     default:
//         // error
//         return null;
//     }
// }
//
// /**
//  * Return device structure mnemonic path for name.
//  *
//  * @param name name
//  * @param levelDeviceStructure level of device structure
//  * @param holderSystemDeviceStructure holder of containers for system and device structure content
//  * @return device structure mnemonic path
//  */
// private static String getMnemonicPathDeviceStructure(Name name, int levelDeviceStructure, HolderSystemDeviceStructure holderSystemDeviceStructure) {
//     // populate return element for device structure
//     switch (levelDeviceStructure) {
//     case 3: {
//         DeviceType  deviceType  = holderSystemDeviceStructure.findDeviceTypeByUuid(name.getDevicetypeUuid());
//         DeviceGroup deviceGroup = holderSystemDeviceStructure.findDeviceGroupByUuid(deviceType.getParentUuid());
//         Discipline  discipline  = holderSystemDeviceStructure.findDisciplineByUuid(deviceGroup.getParentUuid());
//
//         return discipline.getMnemonic() + "-" + deviceType.getMnemonic();
//     }
//     default:
//         // error
//         return null;
//     }
//
// }

}
