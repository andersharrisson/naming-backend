/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.springframework.http.HttpStatus;

/**
 * Exception class to assist in handling of service layer exceptions.
 *
 * @author Lars Johansson
 */
public class ServiceHttpStatusException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 5346696855950320329L;

    private final HttpStatus httpStatus;
    private final String details;
    private final String userFriendly;

    /**
     * Public constructor.
     *
     * @param httpStatus http status
     * @param message message
     * @param cause cause
     */
    public ServiceHttpStatusException(HttpStatus httpStatus, String message, String details, String userFriendly, Throwable cause) {
        super(message, cause);
        this.httpStatus = httpStatus;
        this.details = details;
        this.userFriendly = userFriendly;
    }

    /**
     * Return http status of exception.
     *
     * @return http status
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    /**
     * Returns details of exception.
     *
     * @return details
     */
    public String getDetails() {
        return details;
    }

    /**
     * Return user friendly of exception.
     *
     * @return user friendly
     */
    public String getUserFriendly() {
        return userFriendly;
    }

}
