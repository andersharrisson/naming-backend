/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.response;

import org.springframework.http.HttpHeaders;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
public class Response {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APP_JSON     = "application/json";

    public static final HttpHeaders HEADER_JSON = new HttpHeaders();

    private String message = null;
    private String details = null;

    public Response() {
        HEADER_JSON.add(Response.CONTENT_TYPE, Response.APP_JSON);
    }

    public Response(String message) {
        this();

        this.message = message;
        this.details = "";
    }

    public Response(String message, String details) {
        this();

        this.message = message;
        this.details = details;
    }

    public String getMessage() {
        return this.message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return this.details;
    }
    public void setDetails(String details) {
        this.details = details;
    }

}
