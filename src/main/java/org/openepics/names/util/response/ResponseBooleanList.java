/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.response;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
public class ResponseBooleanList extends ResponseBoolean {

    private List<ResponseBoolean> responses = Lists.newArrayList();

    public ResponseBooleanList() {
        super();
    }

    public ResponseBooleanList(Boolean response) {
        super(response);
    }

    public ResponseBooleanList(Boolean response, String message) {
        super(response, message, "");
    }

    public ResponseBooleanList(Boolean response, String message, String details) {
        super(response, message, details);
    }

    public ResponseBooleanList(List<ResponseBoolean> responses, Boolean response) {
        super(response);
        this.responses = responses;
    }

    public ResponseBooleanList(List<ResponseBoolean> responses, Boolean response, String message) {
        super(response, message, "");
        this.responses = responses;
    }

    public ResponseBooleanList(List<ResponseBoolean> responses, Boolean response, String message, String details) {
        super(response, message, details);
        this.responses = responses;
    }

    public List<ResponseBoolean> getResponses() {
        return this.responses;
    }

}
