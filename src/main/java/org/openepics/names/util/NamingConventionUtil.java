/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Arrays;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.Discipline;

/**
 * Utility class to provide split of device name in naming convention into name parts
 * for system structure and device structure.
 *
 * <p>
 * Note delimiter characters, in and between, system structure and device structure.
 * <ul>
 * <li>-
 * <li>:
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see NamingConvention
 */
public class NamingConventionUtil {

    // Note
    //
    //     ================================================================================
    //     This class handles system structure and device structure of name convention
    //     ================================================================================
    //     ESS Naming Convention
    //         system structure:device structure:process variable
    //         ------------------------------------------------------------
    //         system structure need     to be part of name
    //         device structure need not to be part of name
    //         process variable      not to be part of name
    //         ------------------------------------------------------------
    //         system structure
    //             system group
    //             system
    //             subsystem
    //         device structure
    //             discipline
    //             device type
    //             instance index (name)
    //         ------------------------------------------------------------
    //         system group is optional
    //         instance index is optional
    //     ================================================================================
    //     Kind of names handled
    //         CONVENTIONNAME_010
    //         CONVENTIONNAME_011
    //         CONVENTIONNAME_010_111
    //         CONVENTIONNAME_011_110
    //         CONVENTIONNAME_011_111
    //         CONVENTIONNAME_111_110
    //         CONVENTIONNAME_111_111
    //         ------------------------------------------------------------
    //         system
    //         system-subsystem
    //         system:discipline-deviceType-instanceIndex
    //         system-subsystem:discipline-deviceType
    //         system-subsystem:discipline-deviceType-instanceIndex
    //         systemGroup-system-subsystem:discipline-deviceType
    //         systemGroup-system-subsystem:discipline-deviceType-instanceIndex
    //     ================================================================================
    //     delimiters
    //         :
    //         -
    //     ================================================================================
    //     This class to be able to handle past and present names
    //     ================================================================================

    // p&id - pipeline & instrumentation diagram
    public static final String DISCIPLINE_P_ID       = "P&ID";
    public static final String DISCIPLINE_SCIENTIFIC = "Scientific";

    private static final String[] DISCIPLINES_P_ID           = {"Cryo", "EMR", "HVAC", "Proc", "SC", "Vac", "WtrC"};
    private static final String[] MNEMONIC_PATH_P_ID_NUMERIC = {"SC-IOC"};

    private static final String DELIMITER_EXTRA = ":";
    private static final String DELIMITER_INTRA = "-";

    /**
     * This class is not to be instantiated.
     */
    private NamingConventionUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return string array with disciplines for p & id (pipeline & instrumentation diagram).
     *
     * @return string array with disciplines for p & id (pipeline & instrumentation diagram)
     */
    public static String[] getDisciplinesPID() {
        return Arrays.copyOf(DISCIPLINES_P_ID, DISCIPLINES_P_ID.length);
    }

    /**
     * Return string array with mnemonic paths for p & id (pipeline & instrumentation diagram) numeric.
     *
     * @return string array with mnemonic paths for p & id (pipeline & instrumentation diagram) numeric
     */
    public static String[] getMnemonicPathsPIDNumeric() {
        return Arrays.copyOf(MNEMONIC_PATH_P_ID_NUMERIC, MNEMONIC_PATH_P_ID_NUMERIC.length);
    }

    /**
     * Extract system group from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return system group
     */
    public static String extractSystemGroup(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 1 || st1.countTokens() == 2 || st1.countTokens() == 3) {
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract system from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return system
     */
    public static String extractSystem(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 1 || st1.countTokens() == 2 || st1.countTokens() == 3) {
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    st2.nextToken();
                    return st2.nextToken();
                } else if (st2.countTokens() == 2 || st2.countTokens() == 1) {
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract subsystem from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return subsystem
     */
    public static String extractSubsystem(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 1 || st1.countTokens() == 2 || st1.countTokens() == 3) {
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    st2.nextToken();
                    st2.nextToken();
                    return st2.nextToken();
                } else if (st2.countTokens() == 2) {
                    st2.nextToken();
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract discipline from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return discipline
     */
    public static String extractDiscipline(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 2 || st1.countTokens() == 3) {
                st1.nextToken();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3 || st2.countTokens() == 2) {
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract device type from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return device type
     */
    public static String extractDeviceType(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 2 || st1.countTokens() == 3) {
                st1.nextToken();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3 || st2.countTokens() == 2) {
                    st2.nextToken();
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract instance index from ESS convention name or <tt>null</tt> if it can not be extracted.
     *
     * @param conventionName ESS convention name
     * @return instance index
     */
    public static String extractInstanceIndex(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            StringTokenizer st1 = new StringTokenizer(conventionName, DELIMITER_EXTRA, false);
            if (st1.countTokens() == 2 || st1.countTokens() == 3) {
                st1.nextToken();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), DELIMITER_INTRA, false);
                if (st2.countTokens() == 3) {
                    st2.nextToken();
                    st2.nextToken();
                    return st2.nextToken();
                }
            }
        }
        return null;
    }

    /**
     * Extract mnemonic path for system structure from ESS convention name.
     *
     * @param conventionName ESS convention name
     * @return mnemonic path for system structure
     */
    public static String extractMnemonicPathSystemStructure(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            int index = conventionName.indexOf(DELIMITER_EXTRA);
            if (index != -1) {
                return conventionName.substring(0, index);
            } else {
                return conventionName;
            }
        }
        return null;
    }

    /**
     * Extract mnemonic path for device structure from ESS convention name.
     * Instance index is not part of mnemonic path for device structure.
     *
     * @param conventionName ESS convention name
     * @return mnemonic path for device structure
     */
    public static String extractMnemonicPathDeviceStructure(String conventionName) {
        if (!StringUtils.isEmpty(conventionName)) {
            int index = conventionName.indexOf(DELIMITER_EXTRA);
            if (index != -1) {
                String discipline = extractDiscipline(conventionName);
                String deviceType = extractDeviceType(conventionName);
                if (!StringUtils.isEmpty(discipline) && !StringUtils.isEmpty(deviceType)) {
                    return discipline + DELIMITER_INTRA + deviceType;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * Return instance index style.
     *
     * @param conventionName convention name
     * @return instance index style
     */
    public static String getInstanceIndexStyle(String conventionName) {
        String indexStyle = null;
        String mnemonicPathDeviceStructure = extractMnemonicPathDeviceStructure(conventionName);
        if (!StringUtils.isEmpty(mnemonicPathDeviceStructure)) {
            String discipline = extractDiscipline(conventionName);
            indexStyle = DISCIPLINE_SCIENTIFIC;
            for (String str : getDisciplinesPID()) {
                if (StringUtils.equals(str, discipline)) {
                    indexStyle = DISCIPLINE_P_ID;
                    break;
                }
            }
        }
        return indexStyle;
    }

    /**
     * Return instance index style.
     *
     * @param discipline discipline
     * @return instance index style
     */
    public static String getInstanceIndexStyle(Discipline discipline) {
        String indexStyle = null;
        if (discipline != null && !StringUtils.isEmpty(discipline.getMnemonic())) {
            indexStyle = DISCIPLINE_SCIENTIFIC;
            for (String str : getDisciplinesPID()) {
                if (StringUtils.equals(str, discipline.getMnemonic())) {
                    indexStyle = DISCIPLINE_P_ID;
                    break;
                }
            }
        }
        return indexStyle;
    }

    /**
     * Return if discipline is PID discipline.
     *
     * @param discipline discipline
     * @return if discipline is PID
     */
    public static boolean isDisciplinePID(String discipline) {
        if (!StringUtils.isEmpty(discipline)) {
            for (String s : getDisciplinesPID()) {
                if (s.equals(discipline)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return if mnemonic path device structure is PID numeric.
     *
     * @param mnemonicPath mnemonic path device structure
     * @return if mnemonic path device structure is PID numeric
     */
    public static boolean isMnemonicPathDeviceStructurePIDNumeric(String mnemonicPath) {
        if (!StringUtils.isEmpty(mnemonicPath)) {
            for (String s : getMnemonicPathsPIDNumeric()) {
                if (StringUtils.equals(s, mnemonicPath)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return if convention name is considered OFFSITE.
     *
     * @param conventionName ESS convention name
     * @return               if convention name is considered OFFSITE
     */
    public static boolean isOffsite(String conventionName) {
        return !StringUtils.isEmpty(extractSystemGroup(conventionName))
                && !StringUtils.isEmpty(extractSystem(conventionName))
                && !StringUtils.isEmpty(extractSubsystem(conventionName));
    }

    /**
     * Utility method to convert a mnemonic path (an array of strings) to a string
     * with {@link NamingConventionUtil#DELIMITER_INTRA} as path separator.
     *
     * @param mnemonicPath array of strings
     * @return mnemonic path
     */
    public static String mnemonicPath2String(String... mnemonicPath) {
        if (mnemonicPath != null) {
            String value = StringUtils.join(mnemonicPath, DELIMITER_INTRA);
            value = StringUtils.replace(value, DELIMITER_INTRA + DELIMITER_INTRA, DELIMITER_INTRA);
            return StringUtils.strip(value, DELIMITER_INTRA);
        }
        return null;
    }

    /**
     * Utility method to convert a string to a mnemonic path (an array of strings)
     * with {@link NamingConventionUtil#DELIMITER_INTRA} as path separator.
     *
     * @param name name
     * @return mnemonic path (an array of strings)
     */
    public static String[] string2MnemonicPath(String name) {
        if (name != null) {
            return StringUtils.split(name, DELIMITER_INTRA);
        }
        return null;
    }

    /**
     * Utility method to return mnemonic path system structure for name.
     *
     * @param mnemonicPathSystemStructure mnemonic path system structure
     * @return mnemonic path system structure for name
     */
    public static String mnemonicPathSystemStructure4Name(String mnemonicPathSystemStructure) {
        // mnemonicPathSystemStructure A-B-C ---> B-C
        // mnemonicPathSystemStructure A-B   ---> A-B
        // mnemonicPathSystemStructure A     ---> A

        String[] mnemonicPath = NamingConventionUtil.string2MnemonicPath(mnemonicPathSystemStructure);
        if (mnemonicPath != null && mnemonicPath.length == 3) {
            return mnemonicPath[1] + "-" + mnemonicPath[2];
        }
        return mnemonicPathSystemStructure;
    }

}
