/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Date;
import java.util.UUID;

import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.NameStructure;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;

/**
 * Utility class to assist in populating name elements based on repository content.
 * <br/><br/>
 * Different strategies for population of name elements are used in different methods.
 * The difference in strategies is for finding out system structure and device structure mnemonic paths.
 *
 * @author Lars Johansson
 */
public class NameElementUtil {

    /**
     * This class is not to be instantiated.
     */
    private NameElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return name element for name.
     *
     * @param name name
     * @return name element
     */
    public static NameElement getNameElement(Name name) {
        if (name == null) {
            return null;
        }

        return getNameElement(
                name.getUuid(),
                name.getSystemgroupUuid(), name.getSystemUuid(), name.getSubsystemUuid(), name.getDevicetypeUuid(),
                NamingConventionUtil.extractMnemonicPathSystemStructure(name.getConventionName()),
                NamingConventionUtil.extractMnemonicPathDeviceStructure(name.getConventionName()),
                name.getInstanceIndex(), name.getConventionName(),
                name.getDescription(), Status.APPROVED, name.isLatest(), name.isDeleted(),
                name.getRequested(), name.getRequestedBy(), name.getRequestedComment());
    }

    /**
     * Populate and return name element.
     *
     * @param uuid uuid
     * @param systemgroup system group
     * @param system system
     * @param subsystem subsystem
     * @param devicetype device type
     * @param systemstructure system structure mnemonic path
     * @param devicestructure device structure mnemonic path
     * @param index instance index
     * @param name name
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     * @return name element
     */
    protected static NameElement getNameElement(
            UUID uuid,
            UUID systemgroup, UUID system, UUID subsystem, UUID devicetype,
            String systemstructure, String devicestructure,
            String index, String name,
            String description, Status status, Boolean latest, Boolean deleted,
            Date when, String who, String comment) {

        return new NameElement(
                uuid,
                systemgroup, system, subsystem, devicetype,
                systemstructure, devicestructure,
                index, name,
                description, status, latest, deleted,
                when, who, comment);
    }

    private static boolean hasSameContent(NameElement nameElement, NameStructure nameStructure) {
        /*
           NameElement
               x uuid
               x description
               x status
               x latest
               x deleted
           NameStructure
               x uuid
               x description
               x status
               x latest
               x deleted
         */

        if (nameElement == null && nameStructure == null)
            return true;
        if (nameElement == null)
            return false;
        if (nameStructure == null)
            return false;

        if (nameElement.getUuid() == null) {
            if (nameStructure.getUuid() != null)
                return false;
        } else if (!nameElement.getUuid().equals(nameStructure.getUuid()))
            return false;
        if (nameElement.getDescription() == null) {
            if (nameStructure.getDescription() != null)
                return false;
        } else if (!nameElement.getDescription().equals(nameStructure.getDescription()))
            return false;
        if (nameElement.getStatus() == null) {
            if (nameStructure.getStatus() != null)
                return false;
        } else if (!nameElement.getStatus().equals(nameStructure.getStatus()))
            return false;
        if (nameElement.isLatest() == null) {
            if (nameStructure.isLatest() != null)
                return false;
        } else if (!nameElement.isLatest().equals(nameStructure.isLatest()))
            return false;
        if (nameElement.isDeleted() == null) {
            if (nameStructure.isDeleted() != null)
                return false;
        } else if (!nameElement.isDeleted().equals(nameStructure.isDeleted()))
            return false;

        return true;
    }

    public static boolean hasSameContent(NameElement nameElement, Name name) {
        /*
           NameElement
               x systemgroup
               x system
               x subsystem
               x devicetype
               x index
               x name
           Name
               x systemgroup_uuid
               x system_uuid
               x subsystem_uuid
               x devicetype_uuid
               x instance_index
               x convention_name
         */

        if (!hasSameContent(nameElement, (NameStructure) name))
            return false;

        if (nameElement.getSystemgroup() == null) {
            if (name.getSystemgroupUuid() != null)
                return false;
        } else if (!nameElement.getSystemgroup().equals(name.getSystemgroupUuid()))
            return false;
        if (nameElement.getSystem() == null) {
            if (name.getSystemUuid() != null)
                return false;
        } else if (!nameElement.getSystem().equals(name.getSystemUuid()))
            return false;
        if (nameElement.getSubsystem() == null) {
            if (name.getSubsystemUuid() != null)
                return false;
        } else if (!nameElement.getSubsystem().equals(name.getSubsystemUuid()))
            return false;
        if (nameElement.getDevicetype() == null) {
            if (name.getDevicetypeUuid() != null)
                return false;
        } else if (!nameElement.getDevicetype().equals(name.getDevicetypeUuid()))
            return false;
        if (nameElement.getIndex() == null) {
            if (name.getInstanceIndex() != null)
                return false;
        } else if (!nameElement.getIndex().equals(name.getInstanceIndex()))
            return false;
        if (nameElement.getName() == null) {
            if (name.getConventionName() != null)
                return false;
        } else if (!nameElement.getName().equals(name.getConventionName()))
            return false;

        return true;
    }

//    /**
//     * Populate and return name element for name.
//     *
//     * @param name name
//     * @param holderSystemDeviceStructure holder of containers for system and device structure content
//     * @return name element
//     */
//    public static NameElement getNameElement(Name name, HolderSystemDeviceStructure holderSystemDeviceStructure) {
//        if (name == null) {
//            return null;
//        }
//
//        // find out how to populate return element for system structure, device structure
//        // levelSystemStructure -1 ---> error
//        // levelDeviceStructure -1 ---> error
//
//        return new NameElement(
//                name.getUuid(),
//                name.getSystemgroupUuid(), name.getSystemUuid(), name.getSubsystemUuid(), name.getDevicetypeUuid(),
//                getMnemonicPathSystemStructure(
//                        name,
//                        StructureUtil.getLevelSystemStructure(name),
//                        holderSystemDeviceStructure),
//                getMnemonicPathDeviceStructure(
//                        name,
//                        StructureUtil.getLevelDeviceStructure(name),
//                        holderSystemDeviceStructure),
//                name.getInstanceIndex(), name.getConventionName(),
//                name.getDescription(), Status.APPROVED, name.isLatest(), name.isDeleted(),
//                name.getRequested(), name.getRequestedBy(), name.getRequestedComment());
//    }
//
//    /**
//     * Populate and return name element for name.
//     *
//     * @param name name
//     * @param holderIRepositories holder of references to repositories
//     * @return name element
//     */
//    public static NameElement getNameElement(Name name, HolderIRepositories holderIRepositories) {
//        if (name == null) {
//            return null;
//        }
//
//        // find out how to populate return element for system structure, device structure
//        // levelSystemStructure -1 ---> error
//        // levelDeviceStructure -1 ---> error
//
//        return new NameElement(
//                name.getUuid(),
//                name.getSystemgroupUuid(), name.getSystemUuid(), name.getSubsystemUuid(), name.getDevicetypeUuid(),
//                getMnemonicPathSystemStructure(
//                        name,
//                        StructureUtil.getLevelSystemStructure(name),
//                        holderIRepositories),
//                getMnemonicPathDeviceStructure(
//                        name,
//                        StructureUtil.getLevelDeviceStructure(name),
//                        holderIRepositories),
//                name.getInstanceIndex(), name.getConventionName(),
//                name.getDescription(), Status.APPROVED, name.isLatest(), name.isDeleted(),
//                name.getRequested(), name.getRequestedBy(), name.getRequestedComment());
//    }

}
