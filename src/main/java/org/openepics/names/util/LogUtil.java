/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class to assist in handling of logs.
 *
 * @author Lars Johansson
 */
public class LogUtil {

    private static final String ORG_OPENEPICS_NAMES                     = "org.openepics.names";
    private static final String ORG_OPENEPICS_NAMES_UTIL_EXCEPTION_UTIL = "org.openepics.names.util.ExceptionUtil";

    /**
     * This class is not to be instantiated.
     */
    private LogUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Log service http status exception.
     *
     * @param logger logger
     * @param level log level
     * @param e service https status exception
     */
    public static void logServiceHttpStatusException(Logger logger, Level level, ServiceHttpStatusException e) {
        if (logger == null || level == null || e == null) {
            return;
        }

        String msg = !StringUtils.isEmpty(e.getMessage())
                ? e.getMessage()
                : "";
        String details = !StringUtils.isEmpty(e.getDetails())
                ? " ### " + e.getDetails()
                : "";
        String httpStatus = e.getHttpStatus() != null
                ? " ### " + e.getHttpStatus().toString()
                : "";
        msg = msg + details + httpStatus;

        logger.log(level, msg);
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param e service https status exception
     */
    public static void logStackTraceElements(Logger logger, Level level, ServiceHttpStatusException e) {
        logStackTraceElements(logger, Level.SEVERE, e, 10, ORG_OPENEPICS_NAMES, ORG_OPENEPICS_NAMES_UTIL_EXCEPTION_UTIL);
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param e exception
     */
    public static void logStackTraceElements(Logger logger, Level level, Exception e) {
        logStackTraceElements(logger, Level.SEVERE, e, 10, ORG_OPENEPICS_NAMES, ORG_OPENEPICS_NAMES_UTIL_EXCEPTION_UTIL);
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param e service https status exception
     * @param maxNumberOfLogs max number of logs
     * @param filterInclude filter include
     * @param filterExclude filter exclude
     */
    public static void logStackTraceElements(Logger logger, Level level, ServiceHttpStatusException e, int maxNumberOfLogs, String filterInclude, String filterExclude) {
        if (logger == null || level == null || e == null || maxNumberOfLogs <= 0) {
            return;
        }

        logStackTraceElements(logger, level, e.getStackTrace(), maxNumberOfLogs, filterInclude, filterExclude);
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param e exception
     * @param maxNumberOfLogs max number of logs
     * @param filterInclude filter include
     * @param filterExclude filter exclude
     */
    public static void logStackTraceElements(Logger logger, Level level, Exception e, int maxNumberOfLogs, String filterInclude, String filterExclude) {
        if (logger == null || level == null || e == null || maxNumberOfLogs <= 0) {
            return;
        }

        logStackTraceElements(logger, level, e.getStackTrace(), maxNumberOfLogs, filterInclude, filterExclude);
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param stackTraceElements stack trace elements
     * @param maxNumberOfLogs max number of logs
     * @param filterInclude filter include
     * @param filterExclude filter exclude
     */
    private static void logStackTraceElements(Logger logger, Level level, StackTraceElement[] stackTraceElements, int maxNumberOfLogs, String filterInclude, String filterExclude) {
        if (logger == null || level == null || stackTraceElements == null || maxNumberOfLogs <= 0) {
            return;
        }

        String str;
        int count = 0;
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            str = stackTraceElement.toString();

            if  ((StringUtils.isEmpty(filterInclude) || str.contains(filterInclude))
                    && !(!StringUtils.isEmpty(filterExclude) && str.contains(filterExclude))) {
                count++;
                logger.log(level, str);
                if (count == maxNumberOfLogs) {
                    break;
                }
            }
        }
    }

}
