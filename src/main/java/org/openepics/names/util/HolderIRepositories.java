/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;

/**
 * Utility class and holder of references to repositories (interfaces).
 *
 * @author Lars Johansson
 */
public class HolderIRepositories {

    private INameRepository nameRepository;

    private ISystemGroupRepository systemGroupRepository;
    private ISystemRepository systemRepository;
    private ISubsystemRepository subsystemRepository;

    private IDisciplineRepository disciplineRepository;
    private IDeviceGroupRepository deviceGroupRepository;
    private IDeviceTypeRepository deviceTypeRepository;

    /**
     * Public constructor to populate references to repositories.
     *
     * @param nameRepository reference to name repository
     * @param systemGroupRepository reference to system group repository
     * @param systemRepository reference to system repository
     * @param subsystemRepository reference to subsystem repository
     * @param disciplineRepository reference to discipline repository
     * @param deviceGroupRepository reference to device group repository
     * @param deviceTypeRepository reference to device type repository
     */
    public HolderIRepositories(
            INameRepository nameRepository,
            ISystemGroupRepository systemGroupRepository,
            ISystemRepository systemRepository,
            ISubsystemRepository subsystemRepository,
            IDisciplineRepository disciplineRepository,
            IDeviceGroupRepository deviceGroupRepository,
            IDeviceTypeRepository deviceTypeRepository) {
        this.nameRepository = nameRepository;
        this.systemGroupRepository = systemGroupRepository;
        this.systemRepository = systemRepository;
        this.subsystemRepository = subsystemRepository;
        this.disciplineRepository = disciplineRepository;
        this.deviceGroupRepository = deviceGroupRepository;
        this.deviceTypeRepository = deviceTypeRepository;
    }

    /**
     * Return reference to name repository.
     *
     * @return reference to name repository
     */
    public INameRepository getNameRepository() {
        return nameRepository;
    };

    /**
     * Return reference to system group repository.
     *
     * @return reference to system group repository
     */
    public ISystemGroupRepository getSystemGroupRepository() {
        return systemGroupRepository;
    };
    /**
     * Return reference to system repository.
     *
     * @return reference to system repository
     */
    public ISystemRepository getSystemRepository() {
        return systemRepository;
    };
    /**
     * Return reference to subsystem repository.
     *
     * @return reference to subsystem repository
     */
    public ISubsystemRepository getSubsystemRepository() {
        return subsystemRepository;
    };

    /**
     * Return reference to discipline repository.
     *
     * @return reference to discipline repository
     */
    public IDisciplineRepository getDisciplineRepository() {
        return disciplineRepository;
    };
    /**
     * Return reference to device group repository.
     *
     * @return reference to device group repository
     */
    public IDeviceGroupRepository getDeviceGroupRepository() {
        return deviceGroupRepository;
    };
    /**
     * Return reference to device type repository.
     *
     * @return reference to device type repository
     */
    public IDeviceTypeRepository getDeviceTypeRepository() {
        return deviceTypeRepository;
    };

}
