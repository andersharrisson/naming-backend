/*
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.util;

import org.openepics.names.rest.beans.Type;

/**
 * An interface defining the naming convention to be used by the Naming application.
 * It includes:
 * <ul>
 * <li> name validation rules
 * <li> name uniqueness rules
 * <li> form of composite names
 * </ul>
 *
 * <p>
 * The used naming convention is configured through beans.xml using the CDI alternatives mechanism.
 *
 * <p>
 * A device name consists of  elements having general structure
 * <ul>
 * <li> <pre>Sys-Sub:Dis-Dev-Idx</pre>
 * <li> <pre>Sup-Sys-Sub:Dis-Dev-Idx</pre>
 * </ul>
 * It consists of of <br/><br/>
 * <ul>
 * <li> System structure
 * <ul>
 * <li> System group (Sup)(optional)
 * <li> System (Sys)
 * <li> Subsystem (Sub)
 * </ul>
 * <li> Device structure
 * <ul>
 * <li> Discipline (Dis)
 * <li> Device type (Dev)
 * </ul>
 * <li> Device
 * <ul>
 * <li> Instance index (Idx)
 * </ul>
 * </ul>
 *
 * A device name thus consists of an system structure element, a device structure element and an instance index.
 * Together, elements make a device name. In addition, there is intermediate level in device structure
 * (device group, between discipline and device type) for grouping purposes.<br/><br/>
 *
 * <p>
 * An element in the name structure is typically handled through its mnemonic path and type.
 * <ul>
 * <li> mnemonic path is a list of mnemonics starting from the root of the hierarchy to the intended mnemonic
 * <li> root of hierarchy is system group (system structure) or discipline (device structure)
 * <li> type is structure that mnemonic path belongs, system or device structure
 * </ul>
 *
 * <p>
 * Elements consist of full name, mnemonic, description and follow rules, individually and together
 * (system structure, device structure, device). Among rules:
 * <ul>
 * <li> empty
 * <li> space
 * <li> value
 * <li> alphanumeric
 * <li> letter case
 * <li> length
 * <li> equivalence class representative
 * </ul>
 *
 * <p>
 * Key concepts
 * <ul>
 * <li> mnemonic required or not
 * <li> mnemonic valid or not
 * <li> if mnemonic can coexist with other mnemonic
 * <li> equivalence, uniqueness of names when treating similar looking names
 * </ul>
 *
 * <p>
 * Note
 * <ul>
 * <li> system structure is logical structure
 * <li> depth of system and device structures is 3
 * <li> names typically handled and referred to through mnemonics
 * <li> naming convention also referred to as convention
 * </ul>
 *
 * @author Marko Kolar
 * @author Karin Rathsman
 * @author Lars Johansson
 *
 * @see <a href="https://confluence.esss.lu.se/display/NC/ESS+Naming+Convention"/>
 *               https://confluence.esss.lu.se/display/NC/ESS+Naming+Convention</a>
 * @see <a href="https://chess.esss.lu.se/enovia/tvc-action/showObject/dmg_TechnicalSpecification/ESS-0000757/valid">
 *               https://chess.esss.lu.se/enovia/tvc-action/showObject/dmg_TechnicalSpecification/ESS-0000757/valid</a>
 *
 * @see NamingConventionUtil
 */
public interface NamingConvention {

    /**
     * Return equivalence class representative for given name. This is used to ensure uniqueness of names
     * when treating similar looking names, e.g. 0 vs. O, 1 vs. l treated as as equal.
     *
     * @param name      name of which to determine the equivalence class representative
     * @return          equivalence class representative for given name
     */
    String equivalenceClassRepresentative(String name);

    /**
     * Return if the convention name's instance index is valid according to convention rules,
     * in the context of system structure and device structure.
     *
     * @param conventionName convention name
     * @return <tt>true</tt> if the convention name's instance index is valid according to convention rules,
     *         in the context of system structure and device structure
     */
    boolean isInstanceIndexValid(String conventionName);

    /**
     * Return if the convention name's instance index is valid according to convention rules,
     * in the context of system structure and device structure.
     *
     * @param conventionName convention name
     * @param overrideRuleset if ruleset for instance index is to be overridden, e.g. for super user
     * @return <tt>true</tt> if the convention name's instance index is valid according to convention rules,
     *         in the context of system structure and device structure
     */
    boolean isInstanceIndexValid(String conventionName, boolean overrideRuleset);

    /**
     * Return if mnemonic path is valid within the application according to the convention rules.
     *
     * @param mnemonicPath      list of mnemonics starting from the root of the hierarchy to the mnemonic to be tested
     * @return                  <tt>true</tt> if mnemonic path is valid within the application according to the convention rules
     */
    boolean isMnemonicPathValid(String mnemonicPath);

    /**
     * Return if the mnemonic is required or if it can be <tt>null</tt>, i.e. mnemonic not part of the name.
     *
     * @param type type
     * @return                  <tt>true</tt> if the mnemonic is required or if it can be <tt>null</tt>, i.e. mnemonic
     */
    boolean isMnemonicRequired(Type type);

    /**
     * Return validation for given type and mnemonic according to convention rules.
     *
     * @param type              type specifying whether mnemonic belongs to the system structure or the device structure
     * @param mnemonic          mnemonic to be tested
     * @return                  validation for given mnemonic path and type, according to convention rules
     */
    MnemonicValidation validateMnemonic(Type type, String mnemonic);

}
