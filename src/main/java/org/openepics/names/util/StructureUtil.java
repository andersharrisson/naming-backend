/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;

/**
 * Utility class to assist in handling of structure (name part) content.
 *
 * @author Lars Johansson
 */
public class StructureUtil {

    /**
     * This class is not to be instantiated.
     */
    private StructureUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return mnemonic path for system.
     *
     * @param system system
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(System system, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (system == null || holderSystemDeviceStructure == null) {
            return null;
        }

        SystemGroup systemGroup = holderSystemDeviceStructure.findSystemGroupByUuid(system.getParentUuid());
        return systemGroup != null && !StringUtils.isEmpty(systemGroup.getMnemonic())
                ? systemGroup.getMnemonic() + "-" + system.getMnemonic()
                : system.getMnemonic();
    }

    /**
     * Return mnemonic path for subsystem.
     *
     * @param subsystem subsystem
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(Subsystem subsystem, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (subsystem == null || holderSystemDeviceStructure == null) {
            return null;
        }

        System      system      = holderSystemDeviceStructure.findSystemByUuid(subsystem.getParentUuid());
        SystemGroup systemGroup = system != null
                ? holderSystemDeviceStructure.findSystemGroupByUuid(system.getParentUuid())
                : null;
        return systemGroup != null && !StringUtils.isEmpty(systemGroup.getMnemonic()) && system != null
                ? systemGroup.getMnemonic() + "-" + system.getMnemonic() + "-" + subsystem.getMnemonic()
                : system != null
                    ? system.getMnemonic() + "-" + subsystem.getMnemonic()
                    : subsystem.getMnemonic();

    }

    /**
     * Return mnemonic path for device group.
     *
     * @param deviceGroup device group
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(DeviceGroup deviceGroup, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (deviceGroup == null || holderSystemDeviceStructure == null) {
            return null;
        }

        Discipline discipline = holderSystemDeviceStructure.findDisciplineByUuid(deviceGroup.getParentUuid());
        return discipline != null
                ? discipline.getMnemonic()
                : null;
    }

    /**
     * Return mnemonic path for device type.
     *
     * @param deviceType device type
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(DeviceType deviceType, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (deviceType == null || holderSystemDeviceStructure == null) {
            return null;
        }

        DeviceGroup deviceGroup = holderSystemDeviceStructure.findDeviceGroupByUuid(deviceType.getParentUuid());
        Discipline  discipline  = deviceGroup != null
                ? holderSystemDeviceStructure.findDisciplineByUuid(deviceGroup.getParentUuid())
                : null;
        return discipline != null && deviceGroup != null
                ? discipline.getMnemonic() + "-" + deviceType.getMnemonic()
                : deviceType.getMnemonic();
    }

    // ---------------------------------------------------

    /**
     * Return a list of mnemonic paths for system groups.
     *
     * @param holder holder of containers for system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for system groups
     */
    public static List<String> getMnemonicPathsSystemGroup(HolderSystemDeviceStructure holder, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<UUID, SystemGroup> entry : holder.getUuidSystemGroups().entrySet()) {
            if (!StringUtils.isEmpty(entry.getValue().getMnemonic())) {
                value = entry.getValue().getMnemonic();
            }

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for systems.
     *
     * @param holder holder of containers for system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for systems
     */
    public static List<String> getMnemonicPathsSystem(HolderSystemDeviceStructure holder, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<UUID, System> entry : holder.getUuidSystems().entrySet()) {
            SystemGroup systemGroup = holder.findSystemGroupByUuid(entry.getValue().getParentUuid());
            if (!StringUtils.isEmpty(systemGroup.getMnemonic())) {
                value = systemGroup.getMnemonic() + "-" + entry.getValue().getMnemonic();
            } else {
                value = entry.getValue().getMnemonic();
            }

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for subsystems.
     *
     * @param holder holder of containers for system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for subsystems
     */
    public static List<String> getMnemonicPathsSubsystem(HolderSystemDeviceStructure holder, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<UUID, Subsystem> entry : holder.getUuidSubsystems().entrySet()) {
            System system = holder.findSystemByUuid(entry.getValue().getParentUuid());
            SystemGroup systemGroup = holder.findSystemGroupByUuid(system.getParentUuid());
            if (!StringUtils.isEmpty(systemGroup.getMnemonic())) {
                value = systemGroup.getMnemonic() + "-" + system.getMnemonic() + "-" + entry.getValue().getMnemonic();
            } else {
                value = system.getMnemonic() + "-" + entry.getValue().getMnemonic();
            }

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for disciplines.
     *
     * @param holder holder of containers for system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for disciplines
     */
    public static List<String> getMnemonicPathsDiscipline(HolderSystemDeviceStructure holder, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<UUID, Discipline> entry : holder.getUuidDisciplines().entrySet()) {
            if (!StringUtils.isEmpty(entry.getValue().getMnemonic())) {
                value = entry.getValue().getMnemonic();
            }

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for device types.
     *
     * @param holder holder of containers for system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for device types
     */
    public static List<String> getMnemonicPathsDeviceType(HolderSystemDeviceStructure holder, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<UUID, DeviceType> entry : holder.getUuidDeviceTypes().entrySet()) {
            DeviceGroup deviceGroup = holder.findDeviceGroupByUuid(entry.getValue().getParentUuid());
            Discipline discipline = holder.findDisciplineByUuid(deviceGroup.getParentUuid());
            value = discipline.getMnemonic() + "-" + entry.getValue().getMnemonic();

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

}
