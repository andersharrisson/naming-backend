/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.springframework.http.HttpStatus;

/**
 * Utility class to assist in handling of validation.
 *
 * @author Lars Johansson
 */
public class ValidateUtil {

    private static enum NameChoice      {CREATE, UPDATE, DELETE};
    private static enum StructureChoice {CREATE, UPDATE, DELETE, APPROVE, REJECT, CANCEL};

    public static final String STRUCTURE               = "structure";
    public static final String NAME                    = "name";
    public static final String SYSTEMGROUP             = "system group";
    public static final String SYSTEM                  = "system";
    public static final String SUBSYSTEM               = "subsystem";
    public static final String DISCIPLINE              = "discipline";
    public static final String DEVICEGROUP             = "device group";
    public static final String DEVICETYPE              = "device type";

    public static final String SPACE                   = " ";

    public static final String ARE_NOT_CORRECT         = "are not correct";
    public static final String EXISTS                  = "exists";
    public static final String IS_DELETED              = "is deleted";
    public static final String IS_NOT_AVAILABLE        = "is not available";
    public static final String IS_NOT_CORRECT          = "is not correct";
    public static final String IS_NOT_DELETED          = "is not deleted";
    public static final String IS_NOT_VALID            = "is not valid";
    public static final String WAS_NOT_UPDATED         = "was not updated";

    // NameElement
    //     uuid,
    //     systemgroup, system, subsystem, devicetype, systemstructure, devicestructure,
    //     index, name,
    //     description, status, latest, deleted, when, who, comment
    // StructureElement
    //     type, uuid, parent uuid,
    //     name, mnemonic, mnemonic path, level,
    //     description, status, latest, deleted, when, who, comment

    /**
     * This class is not to be instantiated.
     */
    private ValidateUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Validate comment.
     *
     * @param comment comment
     */
    public static void validateInputComment(String comment) {
        // available
        validateCondition(!StringUtils.isEmpty(comment), HttpStatus.BAD_REQUEST,
                "comment " + ValidateUtil.IS_NOT_AVAILABLE, comment);
    }

    /**
     * Validate description.
     *
     * @param description description
     */
    public static void validateInputDescription(String description) {
        // available
        validateCondition(!StringUtils.isEmpty(description), HttpStatus.BAD_REQUEST,
                "description " + ValidateUtil.IS_NOT_AVAILABLE, description);
    }

    /**
     * Validate mnemonic.
     *
     * @param mnemonic mnemonic
     */
    public static void validateInputMnemonic(String mnemonic) {
        // available
        validateCondition(!StringUtils.isEmpty(mnemonic), HttpStatus.BAD_REQUEST,
                "mnemonic " + ValidateUtil.IS_NOT_AVAILABLE, mnemonic);
    }

    /**
     * Validate mnemonic path.
     *
     * @param mnemonicpath mnemonic path
     */
    public static void validateInputMnemonicpath(String mnemonicpath) {
        // available
        validateCondition(!StringUtils.isEmpty(mnemonicpath), HttpStatus.BAD_REQUEST,
                "mnemonicpath " + ValidateUtil.IS_NOT_AVAILABLE, mnemonicpath);
    }

    /**
     * Validate name.
     *
     * @param name name
     */
    public static void validateInputName(String name) {
        // available
        validateCondition(!StringUtils.isEmpty(name), HttpStatus.BAD_REQUEST,
                "name " + ValidateUtil.IS_NOT_AVAILABLE, name);
    }

    /**
     * Validate status.
     *
     * @param status status
     */
    public static void validateInputStatus(Status status) {
        validateInputStatus(status, null);
    }

    /**
     * Validate status.
     *
     * @param status status
     * @param expected expected status
     */
    public static void validateInputStatus(Status status, Status expected) {
        // available
        validateCondition(status != null, HttpStatus.BAD_REQUEST,
                "status " + ValidateUtil.IS_NOT_AVAILABLE, null);
        // expected status
        if (expected != null) {
            validateCondition(expected.equals(status), HttpStatus.BAD_REQUEST,
                    "status " + ValidateUtil.IS_NOT_CORRECT, status.toString());
        }
    }

    /**
     * Validate type.
     *
     * @param type type
     */
    public static void validateInputType(Type type) {
        validateInputType(type, null);
    }

    /**
     * Validate type.
     *
     * @param type type
     * @param expected expected type
     */
    public static void validateInputType(Type type, Type expected) {
        // available
        validateCondition(type != null, HttpStatus.BAD_REQUEST,
                "type " + ValidateUtil.IS_NOT_AVAILABLE, null);
        // expected type
        if (expected != null) {
            validateCondition(expected.equals(type), HttpStatus.BAD_REQUEST,
                    "type " + ValidateUtil.IS_NOT_CORRECT, type.toString());
        }
    }

    /**
     * Validate uuid.
     *
     * @param uuid uuid
     */
    public static void validateInputUuid(String uuid) {
        // available
        // correct
        validateCondition(!StringUtils.isEmpty(uuid), HttpStatus.BAD_REQUEST,
                "uuid " + ValidateUtil.IS_NOT_AVAILABLE, uuid);
        try {
            UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest(
                    "uuid " + ValidateUtil.IS_NOT_CORRECT, uuid, null);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate parameters for read names.
     *
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     */
    public static void validateNamesInputRead(
            Boolean deleted, FieldName[] queryFields, String[] queryValues,
            Boolean includeHistory,
            FieldName orderBy, Boolean isAsc,
            Integer offset, Integer limit) {

        // validate input
        //     queryFields and queryValues
        //         either
        //             both null
        //             both non-null, same length, non-empty
        //         uuid

        boolean condition = ((queryFields == null && queryValues == null)
                || (queryFields != null && queryValues != null && queryFields.length == queryValues.length && queryFields.length > 0));
        ValidateUtil.validateCondition(condition, HttpStatus.BAD_REQUEST,
                "url and parameters " + ValidateUtil.ARE_NOT_CORRECT, "queryFields, queryValues with different lengths or empty");
        if (queryFields != null) {
            for (int i=0; i<queryFields.length; i++) {
                if (FieldName.UUID.equals(queryFields[i])) {
                    ValidateUtil.validateInputUuid(queryValues[i]);
                }
            }
        }
    }

    /**
     * Validate name element parameters (input) for create.
     *
     * @param nameElement name element
     */
    public static void validateNameElementInputCreate(NameElement nameElement) {
        validateNameElementInput(nameElement, NameChoice.CREATE);
    }

    /**
     * Validate name element parameters (input) for update.
     *
     * @param nameElement name element
     */
    public static void validateNameElementInputUpdate(NameElement nameElement) {
        validateNameElementInput(nameElement, NameChoice.UPDATE);
    }

    /**
     * Validate name element parameters (input) for delete.
     *
     * @param nameElement name element
     */
    public static void validateNameElementInputDelete(NameElement nameElement) {
        validateNameElementInput(nameElement, NameChoice.DELETE);
    }

    /**
     * Validate name element parameters (input).
     *
     * @param nameElement name element
     * @param nameChoice name choice
     */
    private static void validateNameElementInput(NameElement nameElement, NameChoice nameChoice) {
        // attributes
        //     not check
        //    	   uuid                   - n.a set server side
        //    	   systemstructure        - n.a system structure mnemonic path
        //    	   devicestructure        - n.a device structure mnemonic path
        //    	   devicetype             - possibly validate uuid also validate data
        //    	   index                  - possibly also validate data
        //    	   name                   - possibly also validate data
        //    	   status                 - n.a. set server side
        //    	   latest                 - n.a. set server side
        //    	   deleted                - n.a. set server side
        //    	   when                   - n.a. set server side
        //    	   who                    - n.a. set server side
        //     check
        //    	   systemgroup            - 1 either 1,2,3 possibly validate uuid also validate data
        //    	   system                 - 2 either 1,2,3 possibly validate uuid also validate data
        //    	   subsystem              - 3 either 1,2,3 possibly validate uuid also validate data
        //    	   description            - required
        //    	   comment                - required

        if (nameElement == null || nameChoice == null) {
            return;
        }

        if (!NameChoice.CREATE.equals(nameChoice)) {
            validateInputUuid(nameElement.getUuid() !=  null ? nameElement.getUuid().toString() : null);
        }

        validateInputDescription(nameElement.getDescription());
        validateInputComment(nameElement.getComment());

        // uuid vs string
        int count = 0;
        if (nameElement.getSystemgroup() != null) {
            count++;
        };
        if (nameElement.getSystem() != null) {
            count++;
        }
        if (nameElement.getSubsystem() != null) {
            count++;
        }
        // correct
        ValidateUtil.validateCondition(count == 1, HttpStatus.BAD_REQUEST,
                "system structure uuid " + ValidateUtil.IS_NOT_CORRECT, nameElement.toString());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate name element data for create.
     *
     * @param nameElement name element
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param nameRepository name repository
     * @param holder holder
     */
    public static void validateNameElementDataCreate(NameElement nameElement, EssNamingConvention namingConvention, HolderIRepositories holderIRepositories, NameRepository nameRepository, HolderSystemDeviceStructure holder) {
        validateNameElementData(nameElement, namingConvention, holderIRepositories, nameRepository, holder, NameChoice.CREATE);
    }

    /**
     * Validate name element data for update.
     *
     * @param nameElement name element
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param nameRepository name repositories
     * @param holder holder
     */
    public static void validateNameElementDataUpdate(NameElement nameElement, EssNamingConvention namingConvention, HolderIRepositories holderIRepositories, NameRepository nameRepository, HolderSystemDeviceStructure holder) {
        validateNameElementData(nameElement, namingConvention, holderIRepositories, nameRepository, holder, NameChoice.UPDATE);
    }

    /**
     * Validate name element data for delete.
     *
     * @param nameElement name element
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param nameRepository name repositories
     * @param holder holder
     */
    public static void validateNameElementDataDelete(NameElement nameElement, EssNamingConvention namingConvention, HolderIRepositories holderIRepositories, NameRepository nameRepository, HolderSystemDeviceStructure holder) {
        validateNameElementData(nameElement, namingConvention, holderIRepositories, nameRepository, holder, NameChoice.DELETE);
    }

    /**
     * Validate name element data.
     *
     * @param nameElement name element
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param nameRepository name repository
     * @param holder holder
     * @param nameChoice name choice
     */
    private static void validateNameElementData(NameElement nameElement, EssNamingConvention namingConvention, HolderIRepositories holderIRepositories, NameRepository nameRepository, HolderSystemDeviceStructure holder, NameChoice nameChoice) {
        // attributes
        //     not check
        //         uuid
        //         systemstructure
        //         devicestructure
        //         description
        //         status
        //         latest
        //         deleted
        //         when
        //         who
        //         comment
        //     check
        //         systemgroup, system, subsystem - in repository
        //             found
        //             not deleted
        //         devicetype                     - possibly, in repository
        //             found
        //             not deleted
        //         index                          - possibly, naming convention rules
        //             index to match name index
        //             valid
        //         name                           - naming convention rules
        //             mnemonic paths for system structure uuid to match name mnemonic paths
        //             mnemonic paths for device structure uuid to match name mnemonic paths
        //             name not exists
        //             name equivalence not exists

        if (nameElement == null || namingConvention == null || holderIRepositories == null || nameRepository == null || holder == null || nameChoice == null) {
            return;
        }

        // name
        //     create or update? same or not?
        //     update --> retrieve name and check
        Name name = null;
        if (NameChoice.UPDATE.equals(nameChoice) || NameChoice.DELETE.equals(nameChoice)) {
            List<Name> names = nameRepository.readNames(false, FieldName.UUID, nameElement.getUuid().toString());
            validateCondition(names != null && names.size() == 1, HttpStatus.BAD_REQUEST,
                    ValidateUtil.NAME + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, nameElement.toString());

            name = names.get(0);
        }

        SystemGroup systemGroup = null;
        System system           = null;
        Subsystem subsystem     = null;
        DeviceType deviceType   = null;

        String mnemonicPathSystemStructure = null;
        String mnemonicPathDeviceStructure = null;

        boolean condition = true;

        if (NameChoice.CREATE.equals(nameChoice) || NameChoice.UPDATE.equals(nameChoice)) {
            // system structure
            if (nameElement.getSystemgroup() != null) {
                systemGroup = holderIRepositories.getSystemGroupRepository().findLatestByUuid(nameElement.getSystemgroup().toString());
                ValidateUtil.validateCondition(systemGroup != null, HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, nameElement.toString());
                ValidateUtil.validateCondition(!systemGroup.isDeleted(), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_DELETED, nameElement.toString());
                mnemonicPathSystemStructure = systemGroup.getMnemonic();
            } else if (nameElement.getSystem() != null) {
                system = holderIRepositories.getSystemRepository().findLatestByUuid(nameElement.getSystem().toString());
                ValidateUtil.validateCondition(system != null, HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, nameElement.toString());
                ValidateUtil.validateCondition(!system.isDeleted(), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_DELETED, nameElement.toString());
                mnemonicPathSystemStructure = StructureUtil.getMnemonicPath(system, holder);
                String[] mnemonicpath = NamingConventionUtil.string2MnemonicPath(mnemonicPathSystemStructure);
                mnemonicPathSystemStructure = mnemonicpath != null && mnemonicpath.length == 2
                        ? mnemonicpath[1]
                        : null;
            } else if (nameElement.getSubsystem() != null) {
                subsystem = holderIRepositories.getSubsystemRepository().findLatestByUuid(nameElement.getSubsystem().toString());
                ValidateUtil.validateCondition(subsystem != null, HttpStatus.BAD_REQUEST, ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, nameElement.toString());
                ValidateUtil.validateCondition(!subsystem.isDeleted(), HttpStatus.BAD_REQUEST, ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_DELETED, nameElement.toString());
                mnemonicPathSystemStructure = StructureUtil.getMnemonicPath(subsystem, holder);
                String[] mnemonicpath = NamingConventionUtil.string2MnemonicPath(mnemonicPathSystemStructure);
                mnemonicPathSystemStructure = mnemonicpath != null && mnemonicpath.length == 3
                        ? mnemonicpath[1] + "-" + mnemonicpath[2]
                        : mnemonicpath != null && mnemonicpath.length == 2
                            ? mnemonicpath[0] + "-" + mnemonicpath[1]
                            : null;
            } else {
                throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest("system structure uuid " + IS_NOT_CORRECT, nameElement.toString(), null);
            }
            // mnemonicPathSystemStructure = NamingConventionUtil.mnemonicPathSystemStructure4Name(mnemonicPathSystemStructure);

            // device structure
            if (nameElement.getDevicetype() != null) {
                deviceType = holderIRepositories.getDeviceTypeRepository().findLatestByUuid(nameElement.getDevicetype().toString());
                ValidateUtil.validateCondition(deviceType != null, HttpStatus.BAD_REQUEST, ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, nameElement.toString());
                ValidateUtil.validateCondition(!deviceType.isDeleted(), HttpStatus.BAD_REQUEST, ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_DELETED, nameElement.toString());
                mnemonicPathDeviceStructure = StructureUtil.getMnemonicPath(deviceType, holder);
            }

            // index
            String extractedInstanceIndex = NamingConventionUtil.extractInstanceIndex(nameElement.getName());

            if (!StringUtils.isEmpty(nameElement.getIndex()) && StringUtils.isEmpty(extractedInstanceIndex)) {
                throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest("convention name " + IS_NOT_CORRECT, nameElement.toString(), null);
            } else if (StringUtils.isEmpty(nameElement.getIndex()) && !StringUtils.isEmpty(extractedInstanceIndex)) {
                throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest("instance index " + IS_NOT_CORRECT, nameElement.toString(), null);
            }

            if (!StringUtils.isEmpty(nameElement.getIndex())) {
                condition = StringUtils.equals(nameElement.getIndex(), NamingConventionUtil.extractInstanceIndex(nameElement.getName()));
                validateCondition(condition, HttpStatus.BAD_REQUEST, "instance index " + ValidateUtil.IS_NOT_CORRECT, nameElement.toString());

                // TODO overrideRuleset depend on user authority
                condition = namingConvention.isInstanceIndexValid(nameElement.getName(), false);
                validateCondition(condition, HttpStatus.BAD_REQUEST, "instance index " + ValidateUtil.IS_NOT_VALID, nameElement.toString());
            }

            // name
            //     system structure
            //     device structure
            if (!StringUtils.isEmpty(nameElement.getName())) {
                condition = StringUtils.equals(mnemonicPathSystemStructure, NamingConventionUtil.extractMnemonicPathSystemStructure(nameElement.getName()));
                validateCondition(condition, HttpStatus.BAD_REQUEST, "convention name " + ValidateUtil.IS_NOT_CORRECT, nameElement.toString());
                condition = StringUtils.equals(mnemonicPathDeviceStructure, NamingConventionUtil.extractMnemonicPathDeviceStructure(nameElement.getName()));
                validateCondition(condition, HttpStatus.BAD_REQUEST, "convention name " + ValidateUtil.IS_NOT_CORRECT, nameElement.toString());
            } else {
                // if no name, then no index, we're left with system structure, possibly also device structure
                if (!StringUtils.isEmpty(mnemonicPathSystemStructure) && !StringUtils.isEmpty(mnemonicPathDeviceStructure)) {
                    nameElement.setName(mnemonicPathSystemStructure + ":" + mnemonicPathDeviceStructure);
                } else if (!StringUtils.isEmpty(mnemonicPathSystemStructure)) {
                    nameElement.setName(mnemonicPathSystemStructure);
                }
            }

            // name
            // name equivalence
            //     check if (create || update && name not same)
            //     may also trace & trace, and check name through its parents and index, should result in same name as in nameelement
            if (NameChoice.CREATE.equals(nameChoice) || (NameChoice.UPDATE.equals(nameChoice) && !StringUtils.equals(nameElement.getName(), name.getConventionName()))) {
                List<Name> names = nameRepository.readNames(false, FieldName.NAME, nameElement.getName());
                condition = names == null || names.isEmpty();
                validateCondition(condition, HttpStatus.BAD_REQUEST, "convention name " + ValidateUtil.EXISTS, nameElement.toString());

                names = nameRepository.readNames(false, FieldName.NAMEEQUIVALENCE, namingConvention.equivalenceClassRepresentative(nameElement.getName()));
                condition = names == null || names.isEmpty();
                validateCondition(condition, HttpStatus.BAD_REQUEST, "convention name equivalence " + ValidateUtil.EXISTS, nameElement.toString());
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate name data.
     * This method corresponds to name element data for create, albeit in a different way.
     *
     * @param name name
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param nameRepository name repository
     * @param holder holder
     *
     * @see ValidateUtil#validateNameDataCreate(NameElement, EssNamingConvention, HolderIRepositories, NameRepository, HolderSystemDeviceStructure)
     */
    public static void validateNameDataCreate(String name, EssNamingConvention namingConvention, HolderIRepositories holderIRepositories, NameRepository nameRepository, HolderSystemDeviceStructure holder) {
        if (name == null || namingConvention == null || holderIRepositories == null || nameRepository == null || holder == null) {
            return;
        }

        // find out system group, system, subsystem       + check if valid
        // find out discipline, device group, device type + check if valid
        // find out instance index                        + check if valid
        // check name equivalence

        String sg  = NamingConventionUtil.extractSystemGroup(name);
        String sys = NamingConventionUtil.extractSystem(name);
        String sub = NamingConventionUtil.extractSubsystem(name);
        String dt  = NamingConventionUtil.extractDeviceType(name);
        String idx = NamingConventionUtil.extractInstanceIndex(name);

        int count = 0;
        if (sg != null) {
            count++;
        };
        if (sys != null) {
            count++;
        }
        if (sub != null) {
            count++;
        }
        if (count > 2 || count < 1) {
            throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest(
                    "system structure " + ValidateUtil.IS_NOT_CORRECT, name, null);
        } else if (    (!StringUtils.isEmpty(sg) && !StringUtils.isEmpty(sys))
                    || (!StringUtils.isEmpty(sg) && !StringUtils.isEmpty(sub))) {
            throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest(
                    "system structure " + ValidateUtil.IS_NOT_CORRECT, name, null);
        }

        SystemGroup systemGroup = null;
        System system           = null;
        Subsystem subsystem     = null;
        DeviceType deviceType   = null;

        String mnemonicPathSystemStructure = null;
        String mnemonicPathDeviceStructure = null;

        boolean condition = true;

        // ensure that system structure parents and device structure parents are available, latest and not deleted
        //    if (system group) {
        //    } else {
        //        if (system) {
        //        } else {
        //             (if not system then error)
        //        }
        //        if (subsystem) {
        //        }
        //    }
        //    if (device type) {
        //    }

        // system structure
        if (!StringUtils.isEmpty(sg)) {
            systemGroup = holderIRepositories.getSystemGroupRepository().findLatestNotDeletedByMnemonic(sg);
            ValidateUtil.validateCondition(systemGroup != null, HttpStatus.BAD_REQUEST,
                    ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, name);
            mnemonicPathSystemStructure = systemGroup.getMnemonic();
        } else {
            if (!StringUtils.isEmpty(sys)) {
                system = holderIRepositories.getSystemRepository().findLatestNotDeletedByMnemonic(sys);
                ValidateUtil.validateCondition(system != null, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, name);
                mnemonicPathSystemStructure = StructureUtil.getMnemonicPath(system, holder);
            } else {
                throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest(
                        ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, name, null);
            }
            // TODO is this condition correct?
            ValidateUtil.validateCondition(system != null, HttpStatus.BAD_REQUEST,
                    ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, name);
            if (!StringUtils.isEmpty(sub)) {
                subsystem = holderIRepositories.getSubsystemRepository().findLatestNotDeletedByParentAndMnemonic(system.getUuid().toString(), sub);
                ValidateUtil.validateCondition(subsystem != null, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, name);
                mnemonicPathSystemStructure = StructureUtil.getMnemonicPath(subsystem, holder);
            }
        }

        // device structure
        if (!StringUtils.isEmpty(dt)) {
            deviceType = holderIRepositories.getDeviceTypeRepository().findLatestNotDeletedByMnemonic(dt);
            ValidateUtil.validateCondition(deviceType != null, HttpStatus.BAD_REQUEST,
                    ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_AVAILABLE, name);
            mnemonicPathDeviceStructure = StructureUtil.getMnemonicPath(deviceType, holder);
        }

        // index
        if (!StringUtils.isEmpty(idx)) {
            // TODO overrideRuleset depend on user authority
            condition = namingConvention.isInstanceIndexValid(name, false);
            validateCondition(condition, HttpStatus.BAD_REQUEST,
                    "instance index " + ValidateUtil.IS_NOT_VALID, name);
        }

        // name
        //     mnemonic paths for found system structure to end with extracted mnemonic path for system structure
        //     mnemonic paths for found device structure to end with extracted mnemonic path for device structure
        //     convention name not exists
        //     convention name equivalence not exists
        condition = StringUtils.endsWith(mnemonicPathSystemStructure, NamingConventionUtil.extractMnemonicPathSystemStructure(name));
        validateCondition(condition, HttpStatus.BAD_REQUEST,
                "convention name " + ValidateUtil.IS_NOT_CORRECT, name);

        condition = StringUtils.endsWith(mnemonicPathDeviceStructure, NamingConventionUtil.extractMnemonicPathDeviceStructure(name));
        validateCondition(condition, HttpStatus.BAD_REQUEST,
                "convention name " + ValidateUtil.IS_NOT_CORRECT, name);

        List<Name> names = nameRepository.readNames(false, FieldName.NAME, name);
        condition = names == null || names.isEmpty();
        validateCondition(condition, HttpStatus.BAD_REQUEST,
                "convention name " + ValidateUtil.EXISTS, name);

        names = nameRepository.readNames(false, FieldName.NAMEEQUIVALENCE, namingConvention.equivalenceClassRepresentative(name));
        condition = names == null || names.isEmpty();
        validateCondition(condition, HttpStatus.BAD_REQUEST,
                "convention name equivalence " + ValidateUtil.EXISTS, name);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate parameters for read structures.
     *
     * @param type type
     * @param statuses statuses
     * @param deleted deleted
     * @param queryFields query fields
     * @param queryValues query values
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     */
    public static void validateStructuresInputRead(
            Type type, Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues,
            Boolean includeHistory,
            FieldStructure orderBy, Boolean isAsc,
            Integer offset, Integer limit) {

        // validate input
        //     type
        //     queryFields and queryValues
        //         either
        //             both null
        //             both non-null, same length, non-empty
        //         uuid

        ValidateUtil.validateInputType(type);
        boolean condition = ((queryFields == null && queryValues == null)
                || (queryFields != null && queryValues != null && queryFields.length == queryValues.length && queryFields.length > 0));
        ValidateUtil.validateCondition(condition, HttpStatus.BAD_REQUEST,
                "url and parameters " + ValidateUtil.ARE_NOT_CORRECT, "queryFields, queryValues with different lengths or empty");
        if (queryFields != null) {
            for (int i=0; i<queryFields.length; i++) {
                if (FieldStructure.UUID.equals(queryFields[i])) {
                    ValidateUtil.validateInputUuid(queryValues[i]);
                }
            }

        }
    }

    /**
     * Validate structure element parameters (input) for create.
     *
     * @param structureElement structure element
     */
    public static void validateStructureElementInputCreate(StructureElement structureElement, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElement, namingConvention, StructureChoice.CREATE);
    }

    /**
     * Validate structure element parameters (input) for update.
     *
     * @param structureElement structure element
     */
    public static void validateStructureElementInputUpdate(StructureElement structureElement, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElement, namingConvention, StructureChoice.UPDATE);
    }

    /**
     * Validate structure element parameters (input) for delete.
     *
     * @param structureElement structure element
     */
    public static void validateStructureElementInputDelete(StructureElement structureElement, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElement, namingConvention, StructureChoice.DELETE);
    }

    /**
     * Validate structure element parameters (input) for approve.
     *
     * @param structureElement structure element
     */
    public static void validateStructureElementInputApprove(StructureElement structureElement, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElement, namingConvention, StructureChoice.APPROVE);
    }

    /**
     * Validate structure element parameters (input) for cancel.
     *
     * @param structureElement structure element
     */
    public static void validateStructureElementInputCancel(StructureElement structureElement, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElement, namingConvention, StructureChoice.CANCEL);
    }

    /**
     * Validate structure element parameters (input) for reject.
     *
     * @param structureElement structure element
     */
    public static void validateStructureElementInputReject(StructureElement structureElement, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElement, namingConvention, StructureChoice.REJECT);
    }

    /**
     * Validate structure element parameters (input).
     *
     * @param structureElement structure element
     * @param structureChoice structure choice
     */
    private static void validateStructureElementInput(StructureElement structureElement, EssNamingConvention namingConvention, StructureChoice structureChoice) {
        // check structure element input
        //     parent uuid            - also validate data, parent uuid available if type is system, subsystem, device group, device type
        //     name                   - required
        //     mnemonic               - required except for device group
        //     description            - required
        //     comment                - required

        if (structureElement == null || structureChoice == null) {
            return;
        }

        if (!StructureChoice.CREATE.equals(structureChoice)) {
            validateInputUuid(structureElement.getUuid() !=  null ? structureElement.getUuid().toString() : null);
        }

        validateInputType(structureElement.getType());

        if (Type.SYSTEM.equals(structureElement.getType())) {
            validateCondition(structureElement.getParent() != null, HttpStatus.BAD_REQUEST, "parent uuid " + ValidateUtil.IS_NOT_AVAILABLE, structureElement.toString());
            ValidateUtil.validateInputUuid(structureElement.getParent().toString());
        } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
            validateCondition(structureElement.getParent() != null, HttpStatus.BAD_REQUEST, "parent uuid " + ValidateUtil.IS_NOT_AVAILABLE, structureElement.toString());
            ValidateUtil.validateInputUuid(structureElement.getParent().toString());
        } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
            validateCondition(structureElement.getParent() != null, HttpStatus.BAD_REQUEST, "parent uuid " + ValidateUtil.IS_NOT_AVAILABLE, structureElement.toString());
            ValidateUtil.validateInputUuid(structureElement.getParent().toString());
        } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
            validateCondition(structureElement.getParent() != null, HttpStatus.BAD_REQUEST, "parent uuid " + ValidateUtil.IS_NOT_AVAILABLE, structureElement.toString());
            ValidateUtil.validateInputUuid(structureElement.getParent().toString());
        }

        validateInputName(structureElement.getName());

        // validate mnemonic
        //     validate mnemonic input (value itself, not in relation to other values)
        //     validateMnemonic takes isMnemonicRequired into account
        MnemonicValidation mnemonicValidation = namingConvention.validateMnemonic(structureElement.getType(), structureElement.getMnemonic());
        validateCondition(MnemonicValidation.VALID.equals(mnemonicValidation), HttpStatus.BAD_REQUEST, "mnemonic " + ValidateUtil.IS_NOT_VALID, structureElement.toString());

        validateInputDescription(structureElement.getDescription());
        validateInputComment(structureElement.getComment());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate structure element data for create.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureElementDataCreate(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder) {
        validateStructureElementDataInItself(structureElement, namingConvention, holderRepositories, holder, StructureChoice.CREATE);
        validateStructureElementDataRelativeOtherData(structureElement, namingConvention, holderRepositories, holder, StructureChoice.CREATE);
    }

    /**
     * Validate structure element data for update.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureElementDataUpdate(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder) {
        validateStructureElementDataInItself(structureElement, namingConvention, holderRepositories, holder, StructureChoice.UPDATE);
        validateStructureElementDataRelativeOtherData(structureElement, namingConvention, holderRepositories, holder, StructureChoice.UPDATE);
    }

    /**
     * Validate structure element data for delete.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureElementDataDelete(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder) {
        validateStructureElementDataInItself(structureElement, namingConvention, holderRepositories, holder, StructureChoice.DELETE);
        validateStructureElementDataRelativeOtherData(structureElement, namingConvention, holderRepositories, holder, StructureChoice.DELETE);
    }

    /**
     * Validate structure element data for approve.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureElementDataApprove(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder) {
        validateStructureElementDataInItself(structureElement, namingConvention, holderRepositories, holder, StructureChoice.APPROVE);
        validateStructureElementDataRelativeOtherData(structureElement, namingConvention, holderRepositories, holder, StructureChoice.APPROVE);
    }

    /**
     * Validate structure element data for cancel.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureElementDataCancel(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder) {
        validateStructureElementDataInItself(structureElement, namingConvention, holderRepositories, holder, StructureChoice.CANCEL);
        validateStructureElementDataRelativeOtherData(structureElement, namingConvention, holderRepositories, holder, StructureChoice.CANCEL);
    }

    /**
     * Validate structure element data for reject.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureElementDataReject(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder) {
        validateStructureElementDataInItself(structureElement, namingConvention, holderRepositories, holder, StructureChoice.REJECT);
        validateStructureElementDataRelativeOtherData(structureElement, namingConvention, holderRepositories, holder, StructureChoice.REJECT);
    }

    /**
     * Validate structure element data in itself.
     *
     * @param structureElement
     * @param namingConvention
     * @param holderRepositories
     * @param holder
     * @param structureChoice
     */
    public static void validateStructureElementDataInItself(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder, StructureChoice structureChoice) {
        // check structure element data in itself
        //     update, delete
        //         uuid - approved, latest, not deleted - list size 1
        //         possibly
        //             no pending entry waiting to be approved, cancelled, rejected (pending with higher id than currently approved)
        //     approve, reject, cancel
        //         uuid - pending, latest, not deleted  - list size 1

        if (structureElement == null || namingConvention == null || holderRepositories == null || holder == null || structureChoice == null) {
            return;
        }

        if (StructureChoice.UPDATE.equals(structureChoice) || StructureChoice.DELETE.equals(structureChoice)) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(systemGroups != null && systemGroups.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(systems != null && systems.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(subsystems != null && subsystems.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(disciplines != null && disciplines.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.DISCIPLINE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(deviceGroups != null && deviceGroups.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(deviceTypes != null && deviceTypes.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            }
        } else if (StructureChoice.APPROVE.equals(structureChoice) || StructureChoice.CANCEL.equals(structureChoice) || StructureChoice.REJECT.equals(structureChoice)) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(systemGroups != null && systemGroups.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(systems != null && systems.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(subsystems != null && subsystems.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(disciplines != null && disciplines.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.DISCIPLINE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(deviceGroups != null && deviceGroups.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(deviceTypes != null && deviceTypes.size() == 1, HttpStatus.BAD_REQUEST,
                        ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, structureElement.toString());
            }
        }
    }

    /**
     * Validate structure element data relative other data.
     *
     * @param structureElement structure element
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holder holder
     * @param structureChoice structure choice
     */
    public static void validateStructureElementDataRelativeOtherData(StructureElement structureElement, EssNamingConvention namingConvention, HolderRepositories holderRepositories, HolderSystemDeviceStructure holder, StructureChoice structureChoice) {
        // check structure element data in relation to other data
        //     create, update
        //         parent uuid          - (if applicable) approved, latest, not deleted
        //         mnemonic             - (if not same)   not exists
        //         mnemonic equivalence - (if not same)   equivalence not exists
        //     approve
        //         uuid - pending, not latest, deleted
        //         no or less checks if entry to be deleted                - approve (delete)
        //         more or same checks as above if entry is not be deleted - approve (create), approve (update)
        //         need checks as content may have changed from time of create, update, delete to time of approve
        //     checks on mnemonic, mnemonic equivalence are to ensure can coexist, can move
        //     possibly
        //         additional checks if names are affected
        //         comment not same as previous comment

        if (structureElement == null || namingConvention == null || holderRepositories == null || holder == null || structureChoice == null) {
            return;
        }

        String details = structureElement.toString();
        if (StructureChoice.CREATE.equals(structureChoice) || StructureChoice.UPDATE.equals(structureChoice)) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // note rules for mnemonic for system group
                if (!StringUtils.isEmpty(structureElement.getMnemonic())) {
                    // mnemonic
                    String message = ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                    List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.MNEMONIC, structureElement.getMnemonic());
                    validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systemGroups == null || systemGroups.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                    validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systemGroups == null || systemGroups.isEmpty() || systemGroups.size() == 1 && systemGroups.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);

                    // mnemonic equivalence
                    systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()));
                    validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systemGroups == null || systemGroups.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                    validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systemGroups == null || systemGroups.isEmpty() || systemGroups.size() == 1 && systemGroups.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);
                }
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // parent uuid
                String message = ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.UUID, structureElement.getParent().toString());
                ValidateUtil.validateCondition(systemGroups != null && systemGroups.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent
                // mnemonic
                message = ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<System> systems = holderRepositories.getSystemRepository().readSystems(new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT, FieldStructure.MNEMONIC}, new String[] {systemGroups.get(0).getUuid().toString(), structureElement.getMnemonic()}, null, null, null, null);
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systems == null || systems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systems == null || systems.isEmpty() || systems.size() == 1 && systems.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // parent
                // mnemonic equivalence
                systems = holderRepositories.getSystemRepository().readSystems(new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT, FieldStructure.MNEMONICEQUIVALENCE}, new String[] {systemGroups.get(0).getUuid().toString(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic())}, null, null, null, null);
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systems == null || systems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systems == null || systems.isEmpty() || systems.size() == 1 && systems.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // parent uuid
                String message = ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.UUID, structureElement.getParent().toString());
                ValidateUtil.validateCondition(systems != null && systems.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent
                // mnemonic
                message = ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT, FieldStructure.MNEMONIC}, new String[] {systems.get(0).getUuid().toString(), structureElement.getMnemonic()}, null, null, null, null);
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, subsystems == null || subsystems.isEmpty(), HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, subsystems == null || subsystems.isEmpty() || subsystems.size() == 1 && subsystems.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // parent
                // mnemonic equivalence
                subsystems = holderRepositories.getSubsystemRepository().readSubsystems(new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT, FieldStructure.MNEMONICEQUIVALENCE}, new String[] {systems.get(0).getUuid().toString(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic())}, null, null, null, null);
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, subsystems == null || subsystems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, subsystems == null || subsystems.isEmpty() || subsystems.size() == 1 && subsystems.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                // mnemonic
                String message = ValidateUtil.DISCIPLINE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.MNEMONIC, structureElement.getMnemonic());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, disciplines == null || disciplines.isEmpty(), HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, disciplines == null || disciplines.isEmpty() || disciplines.size() == 1 && disciplines.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()));
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, disciplines == null || disciplines.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, disciplines == null || disciplines.isEmpty() || disciplines.size() == 1 && disciplines.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // parent uuid
                String message = ValidateUtil.DISCIPLINE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.UUID, structureElement.getParent().toString());
                ValidateUtil.validateCondition(disciplines != null && disciplines.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // note rules for mnemonic for device group

                // mnemonic
                message = ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                ValidateUtil.validateCondition(StringUtils.isEmpty(structureElement.getMnemonic()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                // parent uuid
                String message = ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.APPROVED, false, FieldStructure.UUID, structureElement.getParent().toString());
                ValidateUtil.validateCondition(deviceGroups != null && deviceGroups.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent
                // mnemonic
                message = ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT, FieldStructure.MNEMONIC}, new String[] {deviceGroups.get(0).getUuid().toString(), structureElement.getMnemonic()}, null, null, null, null);
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty(), HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty() || deviceTypes.size() == 1 && deviceTypes.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT, FieldStructure.MNEMONICEQUIVALENCE}, new String[] {deviceGroups.get(0).getUuid().toString(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic())}, null, null, null, null);
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty() || deviceTypes.size() == 1 && deviceTypes.get(0).getUuid().equals(structureElement.getUuid()), HttpStatus.BAD_REQUEST, message, details);
            }
        } else if (StructureChoice.APPROVE.equals(structureChoice)) {
            //
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                String message = ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<SystemGroup> toBeApproved = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(toBeApproved != null && toBeApproved.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // mnemonic
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.MNEMONIC, toBeApproved.get(0).getMnemonic());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systemGroups == null || systemGroups.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systemGroups == null || systemGroups.isEmpty() || systemGroups.size() == 1 && systemGroups.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, toBeApproved.get(0).getMnemonicEquivalence());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systemGroups == null || systemGroups.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systemGroups == null || systemGroups.isEmpty() || systemGroups.size() == 1 && systemGroups.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                String message = ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<System> toBeApproved = holderRepositories.getSystemRepository().readSystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(toBeApproved != null && toBeApproved.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent uuid
                message = ValidateUtil.SYSTEMGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.UUID, toBeApproved.get(0).getParentUuid().toString());
                ValidateUtil.validateCondition(systemGroups != null && systemGroups.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // mnemonic
                message = ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.MNEMONIC, toBeApproved.get(0).getMnemonic());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systems == null || systems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systems == null || systems.isEmpty() || systems.size() == 1 && systems.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, toBeApproved.get(0).getMnemonicEquivalence());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, systems == null || systems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, systems == null || systems.isEmpty() || systems.size() == 1 && systems.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                String message = ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Subsystem> toBeApproved = holderRepositories.getSubsystemRepository().readSubsystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(toBeApproved != null && toBeApproved.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent uuid
                message = ValidateUtil.SYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.UUID, toBeApproved.get(0).getParentUuid().toString());
                ValidateUtil.validateCondition(systems != null && systems.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // mnemonic
                message = ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.APPROVED, false, FieldStructure.MNEMONIC, toBeApproved.get(0).getMnemonic());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, subsystems == null || subsystems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, subsystems == null || subsystems.isEmpty() || subsystems.size() == 1 && subsystems.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, toBeApproved.get(0).getMnemonicEquivalence());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, subsystems == null || subsystems.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, subsystems == null || subsystems.isEmpty() || subsystems.size() == 1 && subsystems.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                String message = ValidateUtil.DISCIPLINE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Discipline> toBeApproved = holderRepositories.getDisciplineRepository().readDisciplines(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(toBeApproved != null && toBeApproved.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // mnemonic
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.MNEMONIC, toBeApproved.get(0).getMnemonic());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, disciplines == null || disciplines.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, disciplines == null || disciplines.isEmpty() || disciplines.size() == 1 && disciplines.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, toBeApproved.get(0).getMnemonicEquivalence());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, disciplines == null || disciplines.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, disciplines == null || disciplines.isEmpty() || disciplines.size() == 1 && disciplines.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                String message = ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<DeviceGroup> toBeApproved = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(toBeApproved != null && toBeApproved.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent uuid
                message = ValidateUtil.DISCIPLINE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.UUID, toBeApproved.get(0).getParentUuid().toString());
                ValidateUtil.validateCondition(disciplines != null && disciplines.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // note rules for mnemonic for device group

                // mnemonic
                message = ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                ValidateUtil.validateCondition(StringUtils.isEmpty(structureElement.getMnemonic()), HttpStatus.BAD_REQUEST, message, details);
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                String message = ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<DeviceType> toBeApproved = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                ValidateUtil.validateCondition(toBeApproved != null && toBeApproved.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // parent uuid
                message = ValidateUtil.DEVICEGROUP + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.APPROVED, false, FieldStructure.UUID, toBeApproved.get(0).getParentUuid().toString());
                ValidateUtil.validateCondition(deviceGroups != null && deviceGroups.size() == 1, HttpStatus.BAD_REQUEST, message, details);

                // mnemonic
                message = ValidateUtil.DEVICETYPE + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT;
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.APPROVED, false, FieldStructure.MNEMONIC, toBeApproved.get(0).getMnemonic());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty() || deviceTypes.size() == 1 && deviceTypes.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);

                // mnemonic equivalence
                deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.APPROVED, false, FieldStructure.MNEMONICEQUIVALENCE, toBeApproved.get(0).getMnemonicEquivalence());
                validateConditionIfStructureChoice(StructureChoice.CREATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty(),   HttpStatus.BAD_REQUEST, message, details);
                validateConditionIfStructureChoice(StructureChoice.UPDATE, structureChoice, deviceTypes == null || deviceTypes.isEmpty() || deviceTypes.size() == 1 && deviceTypes.get(0).getUuid().equals(toBeApproved.get(0).getUuid()), HttpStatus.BAD_REQUEST, message, details);
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate structure data.
     * This method corresponds to structure element data for create, albeit in a different way.
     *
     * @param type type
     * @param mnemonicpath mnemonic path
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param holder holder
     */
    public static void validateStructureDataCreate(Type type, String mnemonicpath, EssNamingConvention namingConvention, HolderIRepositories holderIRepositories, HolderSystemDeviceStructure holder) {
        if (type == null || mnemonicpath == null || namingConvention == null || holderIRepositories == null || holder == null) {
            return;
        }

        String[] path = NamingConventionUtil.string2MnemonicPath(mnemonicpath);
        validateCondition(path != null && path.length >= 1 && path.length <= 3,
                HttpStatus.BAD_REQUEST, ValidateUtil.STRUCTURE + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path + ", " + mnemonicpath);

        if (Type.SYSTEMGROUP.equals(type)) {
            validateCondition(path.length == 1, HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEMGROUP + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path.length + ", " + mnemonicpath);

            // system group may have empty path but there will be mnemonicpath in this context

            // mnemonic
            SystemGroup sg = holderIRepositories.getSystemGroupRepository().findLatestNotDeletedByMnemonic(path[0]);
            ValidateUtil.validateCondition(sg == null, HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEMGROUP + " mnemonic path duplicate", path[0] + ", " + mnemonicpath);

            // mnemonic equivalence
            List<SystemGroup> systemGroups = holderIRepositories.getSystemGroupRepository().findLatestNotDeleted();
            for (SystemGroup systemGroup : systemGroups) {
                validateCondition(!StringUtils.equals(systemGroup.getMnemonicEquivalence(), namingConvention.equivalenceClassRepresentative(path[0])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEMGROUP + " mnemonic path equivalence duplicate", path[0] + ", " + mnemonicpath);
            }
        } else if (Type.SYSTEM.equals(type)) {
            validateCondition(path.length == 1 || path.length == 2, HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path.length + ", " + mnemonicpath);

            // path with 2 elements - system group and system ---> check both as pair
            // path with 1 element  - system group or  system ---> check both individually
            // check mnemonic, mnemonic equivalence

            // mnemonic
            if (path.length == 2) {
                validateCondition(!StringUtils.equals(path[0], path[1]), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }
            List<String> mnemonicPaths = StructureUtil.getMnemonicPathsSystem(holder, false, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateCondition(!StringUtils.equals(mnemonicpath, existingPath), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }

            // mnemonic equivalence
            if (path.length == 2) {
                validateCondition(!StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[1])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }
            String mnemonicpathEquivalence = namingConvention.equivalenceClassRepresentative(mnemonicpath);
            mnemonicPaths = StructureUtil.getMnemonicPathsSystem(holder, true, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateCondition(!StringUtils.equals(mnemonicpathEquivalence, namingConvention.equivalenceClassRepresentative(existingPath)), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path equivalence duplicate", mnemonicpath);
            }
        } else if (Type.SUBSYSTEM.equals(type)) {
            validateCondition(path.length == 2 || path.length == 3, HttpStatus.BAD_REQUEST, ValidateUtil.SUBSYSTEM + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path.length + ", " + mnemonicpath);

            // mnemonic
            if (path.length == 2) {
                validateCondition(!StringUtils.equals(path[0], path[1]), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            } else {
                validateCondition(!StringUtils.equals(path[0], path[1]), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
                validateCondition(!StringUtils.equals(path[0], path[2]), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
                validateCondition(!StringUtils.equals(path[1], path[2]), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }
            List<String> mnemonicPaths = StructureUtil.getMnemonicPathsSubsystem(holder, false, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateCondition(!StringUtils.equals(mnemonicpath, existingPath), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }

            // mnemonic equivalence
            if (path.length == 2) {
                validateCondition(!StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[1])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            } else {
                validateCondition(!StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[1])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
                validateCondition(!StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[2])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
                validateCondition(!StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[1]), namingConvention.equivalenceClassRepresentative(path[2])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }
            String mnemonicpathEquivalence = namingConvention.equivalenceClassRepresentative(mnemonicpath);
            mnemonicPaths = StructureUtil.getMnemonicPathsSubsystem(holder, true, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateCondition(!StringUtils.equals(mnemonicpathEquivalence, namingConvention.equivalenceClassRepresentative(existingPath)), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path equivalence duplicate", mnemonicpath);
            }
        } else if (Type.DISCIPLINE.equals(type)) {
            validateCondition(path.length == 1, HttpStatus.BAD_REQUEST, ValidateUtil.DISCIPLINE + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path[0] + ", " + mnemonicpath);

            // mnemonic
            Discipline di = holderIRepositories.getDisciplineRepository().findLatestNotDeletedByMnemonic(path[0]);
            ValidateUtil.validateCondition(di == null, HttpStatus.BAD_REQUEST, ValidateUtil.DISCIPLINE + " mnemonic path duplicate", path[0] + ", " + mnemonicpath);

            // mnemonic equivalence
            List<Discipline> disciplines = holderIRepositories.getDisciplineRepository().findLatestNotDeleted();
            for (Discipline discipline : disciplines) {
                validateCondition(!StringUtils.equals(discipline.getMnemonicEquivalence(), namingConvention.equivalenceClassRepresentative(path[0])),
                        HttpStatus.BAD_REQUEST, ValidateUtil.DISCIPLINE + " mnemonic path equivalence duplicate", path[0] + ", " + mnemonicpath);
            }
        } else if (Type.DEVICEGROUP.equals(type)) {
            throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest(ValidateUtil.DEVICEGROUP + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path.length + ", " + mnemonicpath, null);
        } else if (Type.DEVICETYPE.equals(type)) {
            validateCondition(path.length == 2, HttpStatus.BAD_REQUEST, ValidateUtil.STRUCTURE + " " + ValidateUtil.DEVICETYPE + " mnemonic path " + ValidateUtil.IS_NOT_VALID, path.length + ", " + mnemonicpath);

            // discipline
            Discipline discipline = holderIRepositories.getDisciplineRepository().findLatestNotDeletedByMnemonic(path[0]);
            ValidateUtil.validateCondition(discipline != null, HttpStatus.BAD_REQUEST, ValidateUtil.DISCIPLINE + " " + ValidateUtil.IS_NOT_VALID, path[0] + ", " + mnemonicpath);

            // mnemonic
            if (path.length == 2) {
                validateCondition(!StringUtils.equals(path[0], path[1]), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);
            }

            // mnemonic equivalence
            validateCondition(!StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[1])), HttpStatus.BAD_REQUEST, ValidateUtil.SYSTEM + " mnemonic path duplicate", mnemonicpath);

            // since device group is between discipline and device type and device group has no mnemonic,
            //     it can not be traced to which device group that this device type belongs,
            //     therefore it can not be known tO which mnemonic line it belongs
            // therefore this method considered ok as long as mnemonic not empty and mnemonic not duplicate
            // this is ensured by path length 2
            // rest of checks in validatecreate
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate condition and throw ServiceHttpStatusException with reason if validation fails.
     *
     * @param condition condition
     * @param status http status
     * @param message message
     * @param details details
     *
     * @see ServiceHttpStatusException
     */
    public static void validateCondition(boolean condition, HttpStatus status, String message, String details) {
        if (!condition) {
            switch(status) {
                case BAD_REQUEST:
                    throw ExceptionUtil.createServiceHttpStatusExceptionBadRequest(message, details, null);
                default:
                    throw ExceptionUtil.createServiceHttpStatusExceptionInternalServerError(message, details, null);
            }
        }
    }

    /**
     * Validate condition if precondition is fulfilled and throw ServiceHttpStatusException with reason if validation fails.
     *
     * @param precondition precondition
     * @param condition condition
     * @param status http status
     * @param message message
     * @param details details
     *
     * @see ServiceHttpStatusException
     */
    public static void validateConditionIfPrecondition(boolean precondition, boolean condition, HttpStatus status, String message, String details) {
        if (precondition) {
            validateCondition(condition, status, message, details);
        }
    }

    /**
     * Validate condition if precondition (StructureChoice) is fulfilled and throw ServiceHttpStatusException with reason if validation fails.
     *
     * @param expected expected structure choice
     * @param actual actual structure choice
     * @param condition condition
     * @param status http status
     * @param message message
     * @param details details
     *
     * @see ServiceHttpStatusException
     */
    public static void validateConditionIfStructureChoice(StructureChoice expected, StructureChoice actual, boolean condition, HttpStatus status, String message, String details) {
        validateConditionIfPrecondition(expected != null && expected.equals(actual), condition, status, message, details);
    }



}
