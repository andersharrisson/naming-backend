/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Utility class to assist in handling of exceptions.
 * <br/><br/>
 * Note
 * <ul>
 * <li>400 {@link HttpStatus#BAD_REQUEST}</li>
 * <li>401 {@link HttpStatus#UNAUTHORIZED}</li>
 * <li>403 {@link HttpStatus#FORBIDDEN}</li>
 * <li>404 {@link HttpStatus#NOT_FOUND}</li>
 * <li>409 {@link HttpStatus#CONFLICT}</li>
 * <li>500 {@link HttpStatus#INTERNAL_SERVER_ERROR}</li>
 * <li>501 {@link HttpStatus#NOT_IMPLEMENTED}</li>
 * </ul>
 *
 * @author Lars Johansson
 */
public class ExceptionUtil {

    public static final String ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT = "One or more elements are not correct.";
    public static final String OPERATION_COULD_NOT_BE_PERFORMED     = "Operation could not be performed.";

    /**
     * This class is not to be instantiated.
     */
    private ExceptionUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Convert service http status exception to response status exception.
     *
     * @param e service http status exception
     * @return response status exception
     */
    public static ResponseStatusException convertException(ServiceHttpStatusException e) {
        switch (e.getHttpStatus()) {
            case BAD_REQUEST:
                throw ExceptionUtil.createResponseStatusExceptionBadRequest();
            case UNAUTHORIZED:
                throw ExceptionUtil.createResponseStatusExceptionUnauthorized();
            case FORBIDDEN:
                throw ExceptionUtil.createResponseStatusExceptionForbidden();
            case NOT_FOUND:
                throw ExceptionUtil.createResponseStatusExceptionNotFound();
            case CONFLICT:
                throw ExceptionUtil.createResponseStatusExceptionConflict();
            case INTERNAL_SERVER_ERROR:
                throw ExceptionUtil.createResponseStatusExceptionInternalServerError();
            case NOT_IMPLEMENTED:
                throw ExceptionUtil.createResponseStatusExceptionNotImplemented();
            default:
                throw ExceptionUtil.createResponseStatusExceptionInternalServerError();
        }
    }

    /**
     * Create response status exception.
     * Intended for communication from server to client.
     *
     * @param status http status
     * @param reason reason
     * @param cause cause
     * @return response status exception
     */
    protected static ResponseStatusException createResponseStatusException(HttpStatus status, String reason, Throwable cause) {
        return new ResponseStatusException(status, reason, cause);
    }

    /**
     * Create response status exception for {@link HttpStatus#BAD_REQUEST}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    protected static ResponseStatusException createResponseStatusExceptionBadRequest() {
        return createResponseStatusException(HttpStatus.BAD_REQUEST, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }

    /**
     * Create response status exception for {@link HttpStatus#UNAUTHORIZED}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    protected static ResponseStatusException createResponseStatusExceptionUnauthorized() {
        return createResponseStatusException(HttpStatus.UNAUTHORIZED, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }

    /**
     * Create response status exception for {@link HttpStatus#FORBIDDEN}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    protected static ResponseStatusException createResponseStatusExceptionForbidden() {
        return createResponseStatusException(HttpStatus.FORBIDDEN, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }

    /**
     * Create response status exception for {@link HttpStatus#NOT_FOUND}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    protected static ResponseStatusException createResponseStatusExceptionNotFound() {
        return createResponseStatusException(HttpStatus.NOT_FOUND, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }

    /**
     * Create response status exception for {@link HttpStatus#CONFLICT}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    protected static ResponseStatusException createResponseStatusExceptionConflict() {
        return createResponseStatusException(HttpStatus.CONFLICT, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }


    /**
     * Create response status exception for {@link HttpStatus#INTERNAL_SERVER_ERROR}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    public static ResponseStatusException createResponseStatusExceptionInternalServerError() {
        return createResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }

    /**
     * Create response status exception for {@link HttpStatus#NOT_IMPLEMENTED}.
     * Intended for communication from server to client.
     *
     * @return response status exception
     */
    public static ResponseStatusException createResponseStatusExceptionNotImplemented() {
        return createResponseStatusException(HttpStatus.NOT_IMPLEMENTED, OPERATION_COULD_NOT_BE_PERFORMED, null);
    }

    /**
     * Create service http status exception.
     * Intended for communication inside server.
     *
     * @param status http status
     * @param message message
     * @param details details
     * @param userFriendly user friendly
     * @param cause cause
     * @return service http status exception
     */
    protected static ServiceHttpStatusException createServiceHttpStatusException(HttpStatus status, String message, String details, String userFriendly, Throwable cause) {
        return new ServiceHttpStatusException(status, message, details, userFriendly, cause);
    }

    /**
     * Create service http status exception for {@link HttpStatus#BAD_REQUEST}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionBadRequest(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.BAD_REQUEST, message, details, userFriendly, null);
    }

    /**
     * Create service http status exception for {@link HttpStatus#UNAUTHORIZED}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionUnauthorized(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.UNAUTHORIZED, message, details, userFriendly, null);
    }

    /**
     * Create service http status exception for {@link HttpStatus#FORBIDDEN}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionForbidden(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.FORBIDDEN, message, details, userFriendly, null);
    }

    /**
     * Create service http status exception for {@link HttpStatus#NOT_FOUND}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionNotFound(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.NOT_FOUND, message, details, userFriendly, null);
    }

    /**
     * Create service http status exception for {@link HttpStatus#CONFLICT}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionConflict(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.CONFLICT, message, details, userFriendly, null);
    }

    /**
     * Create service http status exception for {@link HttpStatus#INTERNAL_SERVER_ERROR}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionInternalServerError(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message, details, userFriendly, null);
    }

    /**
     * Create service http status exception for {@link HttpStatus#NOT_IMPLEMENTED}.
     * Intended for communication inside server.
     *
     * @param message message
     * @return service http status exception
     */
    public static ServiceHttpStatusException createServiceHttpStatusExceptionNotImplemented(String message, String details, String userFriendly) {
        return createServiceHttpStatusException(HttpStatus.NOT_IMPLEMENTED, message, details, userFriendly, null);
    }

}
