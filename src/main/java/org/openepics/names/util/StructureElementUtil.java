/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.NameStructure;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in populating structure elements based on repository content.
 *
 * @author Lars Johansson
 */
public class StructureElementUtil {

    public static enum StructureChoice {HISTORY, STRUCTURE};

    private static final long THOUSAND_MILLISECONDS = 1000;

    /**
     * This class is not to be instantiated.
     */
    private StructureElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return list of structure elements for system groups.
     *
     * @param systemGroups system groups
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective gives one StructureElement object (processed).
     *                        History perspective gives two StructureElement objects (processed, requested).
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForSystemGroups(List<SystemGroup> systemGroups, HolderSystemDeviceStructure holderSystemDeviceStructure, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (SystemGroup systemGroup : systemGroups) {
            // one or two return elements
            //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
            //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
            //     processed == null                                               --> one entry   (requested)

            if (StructureChoice.HISTORY.equals(structureChoice)) {
                if (systemGroup.getProcessed() != null && ((systemGroup.getProcessed().getTime() - systemGroup.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(systemGroup, holderSystemDeviceStructure));
                    structureElements.add(getStructureElementRequested(systemGroup, holderSystemDeviceStructure));
                } else if (systemGroup.getProcessed() != null && ((systemGroup.getProcessed().getTime() - systemGroup.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(systemGroup, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementRequested(systemGroup, holderSystemDeviceStructure));
                }
            } else {
                if (Status.PENDING.equals(systemGroup.getStatus())) {
                    structureElements.add(getStructureElementRequested(systemGroup, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementProcessed(systemGroup, holderSystemDeviceStructure));
                }
            }
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for systems.
     *
     * @param systems systems
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForSystems(List<System> systems, HolderSystemDeviceStructure holderSystemDeviceStructure, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (System system : systems) {
            // one or two return elements
            //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
            //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
            //     processed == null                                               --> one entry   (requested)

            if (StructureChoice.HISTORY.equals(structureChoice)) {
                if (system.getProcessed() != null && ((system.getProcessed().getTime() - system.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(system, holderSystemDeviceStructure));
                    structureElements.add(getStructureElementRequested(system, holderSystemDeviceStructure));
                } else if (system.getProcessed() != null && ((system.getProcessed().getTime() - system.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(system, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementRequested(system, holderSystemDeviceStructure));
                }
            } else {
                if (Status.PENDING.equals(system.getStatus())) {
                    structureElements.add(getStructureElementRequested(system, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementProcessed(system, holderSystemDeviceStructure));
                }
            }
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for subsystems.
     *
     * @param subsystems subsystems
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForSubsystems(List<Subsystem> subsystems, HolderSystemDeviceStructure holderSystemDeviceStructure, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (Subsystem subsystem : subsystems) {
            // one or two return elements
            //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
            //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
            //     processed == null                                               --> one entry   (requested)

            if (StructureChoice.HISTORY.equals(structureChoice)) {
                if (subsystem.getProcessed() != null && ((subsystem.getProcessed().getTime() - subsystem.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(subsystem, holderSystemDeviceStructure));
                    structureElements.add(getStructureElementRequested(subsystem, holderSystemDeviceStructure));
                } else if (subsystem.getProcessed() != null && ((subsystem.getProcessed().getTime() - subsystem.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(subsystem, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementRequested(subsystem, holderSystemDeviceStructure));
                }
            } else {
                if (Status.PENDING.equals(subsystem.getStatus())) {
                    structureElements.add(getStructureElementRequested(subsystem, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementProcessed(subsystem, holderSystemDeviceStructure));
                }
            }
        }
        return structureElements;
    }

    /**
     * Populate and return list of structure elements for disciplines.
     *
     * @param disciplines disciplines
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForDisciplines(List<Discipline> disciplines, HolderSystemDeviceStructure holderSystemDeviceStructure, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (Discipline discipline : disciplines) {
            // one or two return elements
            //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
            //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
            //     processed == null                                               --> one entry   (requested)

            if (StructureChoice.HISTORY.equals(structureChoice)) {
                if (discipline.getProcessed() != null && ((discipline.getProcessed().getTime() - discipline.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(discipline, holderSystemDeviceStructure));
                    structureElements.add(getStructureElementRequested(discipline, holderSystemDeviceStructure));
                } else if (discipline.getProcessed() != null && ((discipline.getProcessed().getTime() - discipline.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(discipline, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementRequested(discipline, holderSystemDeviceStructure));
                }
            } else {
                if (Status.PENDING.equals(discipline.getStatus())) {
                    structureElements.add(getStructureElementRequested(discipline, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementProcessed(discipline, holderSystemDeviceStructure));
                }
            }
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for device groups.
     *
     * @param deviceGroups device groups
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForDeviceGroups(List<DeviceGroup> deviceGroups, HolderSystemDeviceStructure holderSystemDeviceStructure, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (DeviceGroup deviceGroup : deviceGroups) {
            // one or two return elements
            //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
            //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
            //     processed == null                                               --> one entry   (requested)

            if (StructureChoice.HISTORY.equals(structureChoice)) {
                if (deviceGroup.getProcessed() != null && ((deviceGroup.getProcessed().getTime() - deviceGroup.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(deviceGroup, holderSystemDeviceStructure));
                    structureElements.add(getStructureElementRequested(deviceGroup, holderSystemDeviceStructure));
                } else if (deviceGroup.getProcessed() != null && ((deviceGroup.getProcessed().getTime() - deviceGroup.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(deviceGroup, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementRequested(deviceGroup, holderSystemDeviceStructure));
                }
            } else {
                if (Status.PENDING.equals(deviceGroup.getStatus())) {
                    structureElements.add(getStructureElementRequested(deviceGroup, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementProcessed(deviceGroup, holderSystemDeviceStructure));
                }
            }
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for device types.
     *
     * @param deviceTypes device types
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForDeviceTypes(List<DeviceType> deviceTypes, HolderSystemDeviceStructure holderSystemDeviceStructure, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (DeviceType deviceType : deviceTypes) {
            // one or two return elements
            //     processed != null and processed != requested (>  1s difference) --> two entries (processed, requested)
            //     processed != null and processed == requested (<= 1s difference) --> one entry   (processed initial)
            //     processed == null                                               --> one entry   (requested)

            if (StructureChoice.HISTORY.equals(structureChoice)) {
                if (deviceType.getProcessed() != null && ((deviceType.getProcessed().getTime() - deviceType.getRequested().getTime()) > THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(deviceType, holderSystemDeviceStructure));
                    structureElements.add(getStructureElementRequested(deviceType, holderSystemDeviceStructure));
                } else if (deviceType.getProcessed() != null && ((deviceType.getProcessed().getTime() - deviceType.getRequested().getTime()) <= THOUSAND_MILLISECONDS)) {
                    structureElements.add(getStructureElementProcessed(deviceType, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementRequested(deviceType, holderSystemDeviceStructure));
                }
            } else {
                if (Status.PENDING.equals(deviceType.getStatus())) {
                    structureElements.add(getStructureElementRequested(deviceType, holderSystemDeviceStructure));
                } else {
                    structureElements.add(getStructureElementProcessed(deviceType, holderSystemDeviceStructure));
                }
            }
        }
        return structureElements;
    }

    /**
     * Populate and return structure element for system group with focus on processed.
     *
     * @param systemGroup system group
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(SystemGroup systemGroup, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (systemGroup == null) {
            return null;
        }

//        String mnemonicpath = holderSystemDeviceStructure != null
//                ? systemGroup.getMnemonic()
//                : null;
        String mnemonicpath = !StringUtils.isEmpty(systemGroup.getMnemonic())
                ? systemGroup.getMnemonic()
                : null;

        return getStructureElement(
                Type.SYSTEMGROUP,
                systemGroup.getUuid(),
                null,
                systemGroup.getName(), systemGroup.getMnemonic(), mnemonicpath, 1,
                systemGroup.getDescription(), systemGroup.getStatus(), systemGroup.isLatest(), systemGroup.isDeleted(),
                systemGroup.getProcessed(), systemGroup.getProcessedBy(), systemGroup.getProcessedComment());
    }
    /**
     * Populate and return structure element for system group with focus on requested.
     *
     * @param systemGroup system group
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementRequested(SystemGroup systemGroup, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (systemGroup == null) {
            return null;
        }

//        String mnemonicpath = holderSystemDeviceStructure != null
//                ? systemGroup.getMnemonic()
//                : null;
        String mnemonicpath = !StringUtils.isEmpty(systemGroup.getMnemonic())
                ? systemGroup.getMnemonic()
                : null;

        return getStructureElement(
                Type.SYSTEMGROUP,
                systemGroup.getUuid(),
                null,
                systemGroup.getName(), systemGroup.getMnemonic(), mnemonicpath, 1,
                systemGroup.getDescription(), Status.PENDING, systemGroup.isLatest(), systemGroup.isDeleted(),
                systemGroup.getRequested(), systemGroup.getRequestedBy(), systemGroup.getRequestedComment());
    }
    /**
     * Populate and return structure element for system with focus on processed.
     *
     * @param system system
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(System system, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (system == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(system, holderSystemDeviceStructure);

        return getStructureElement(
                Type.SYSTEM,
                system.getUuid(),
                system.getParentUuid(),
                system.getName(), system.getMnemonic(), mnemonicpath, 2,
                system.getDescription(), system.getStatus(), system.isLatest(), system.isDeleted(),
                system.getProcessed(), system.getProcessedBy(), system.getProcessedComment());
    }
    /**
     * Populate and return structure element for system group with focus on requested.
     *
     * @param system system
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementRequested(System system, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (system == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(system, holderSystemDeviceStructure);

        return getStructureElement(
                Type.SYSTEM,
                system.getUuid(),
                system.getParentUuid(),
                system.getName(), system.getMnemonic(), mnemonicpath, 2,
                system.getDescription(), Status.PENDING, system.isLatest(), system.isDeleted(),
                system.getRequested(), system.getRequestedBy(), system.getRequestedComment());
    }
    /**
     * Populate and return structure element for subsystem with focus on processed.
     *
     * @param subsystem subsystem
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(Subsystem subsystem, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (subsystem == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(subsystem, holderSystemDeviceStructure);

        return getStructureElement(
                Type.SUBSYSTEM,
                subsystem.getUuid(),
                subsystem.getParentUuid(),
                subsystem.getName(), subsystem.getMnemonic(), mnemonicpath, 3,
                subsystem.getDescription(), subsystem.getStatus(), subsystem.isLatest(), subsystem.isDeleted(),
                subsystem.getProcessed(), subsystem.getProcessedBy(), subsystem.getProcessedComment());
    }
    /**
     * Populate and return structure element for subsystem with focus on requested.
     *
     * @param subsystem subsystem
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementRequested(Subsystem subsystem, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (subsystem == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(subsystem, holderSystemDeviceStructure);

        return getStructureElement(
                Type.SUBSYSTEM,
                subsystem.getUuid(),
                subsystem.getParentUuid(),
                subsystem.getName(), subsystem.getMnemonic(), mnemonicpath, 3,
                subsystem.getDescription(), Status.PENDING, subsystem.isLatest(), subsystem.isDeleted(),
                subsystem.getRequested(), subsystem.getRequestedBy(), subsystem.getRequestedComment());
    }

    /**
     * Populate and return structure element for discipline with focus on processed.
     *
     * @param discipline discipline
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(Discipline discipline, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (discipline == null) {
            return null;
        }

//        String mnemonicpath = holderSystemDeviceStructure != null
//                ? discipline.getMnemonic()
//                : null;
        String mnemonicpath = !StringUtils.isEmpty(discipline.getMnemonic())
                ? discipline.getMnemonic()
                : null;

        return getStructureElement(
                Type.DISCIPLINE,
                discipline.getUuid(),
                null,
                discipline.getName(), discipline.getMnemonic(), mnemonicpath, 1,
                discipline.getDescription(), discipline.getStatus(), discipline.isLatest(), discipline.isDeleted(),
                discipline.getProcessed(), discipline.getProcessedBy(), discipline.getProcessedComment());
    }
    /**
     * Populate and return structure element for discipline with focus on requested.
     *
     * @param discipline discipline
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementRequested(Discipline discipline, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (discipline == null) {
            return null;
        }

//        String mnemonicpath = holderSystemDeviceStructure != null
//                ? discipline.getMnemonic()
//                : null;
        String mnemonicpath = !StringUtils.isEmpty(discipline.getMnemonic())
                ? discipline.getMnemonic()
                : null;

        return getStructureElement(
                Type.DISCIPLINE,
                discipline.getUuid(),
                null,
                discipline.getName(), discipline.getMnemonic(), mnemonicpath, 1,
                discipline.getDescription(), Status.PENDING, discipline.isLatest(), discipline.isDeleted(),
                discipline.getRequested(), discipline.getRequestedBy(), discipline.getRequestedComment());
    }
    /**
     * Populate and return structure element for device group with focus on processed.
     *
     * @param deviceGroup device group
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(DeviceGroup deviceGroup, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (deviceGroup == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(deviceGroup, holderSystemDeviceStructure);

        return getStructureElement(
                Type.DEVICEGROUP,
                deviceGroup.getUuid(),
                deviceGroup.getParentUuid(),
                deviceGroup.getName(), deviceGroup.getMnemonic(), mnemonicpath, 2,
                deviceGroup.getDescription(), deviceGroup.getStatus(), deviceGroup.isLatest(), deviceGroup.isDeleted(),
                deviceGroup.getProcessed(), deviceGroup.getProcessedBy(), deviceGroup.getProcessedComment());
    }
    /**
     * Populate and return structure element for device group with focus on requested.
     *
     * @param deviceGroup device group
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementRequested(DeviceGroup deviceGroup, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (deviceGroup == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(deviceGroup, holderSystemDeviceStructure);

        return getStructureElement(
                Type.DEVICEGROUP,
                deviceGroup.getUuid(),
                deviceGroup.getParentUuid(),
                deviceGroup.getName(), deviceGroup.getMnemonic(), mnemonicpath, 2,
                deviceGroup.getDescription(), Status.PENDING, deviceGroup.isLatest(), deviceGroup.isDeleted(),
                deviceGroup.getRequested(), deviceGroup.getRequestedBy(), deviceGroup.getRequestedComment());
    }
    /**
     * Populate and return structure element for device type with focus on processed.
     *
     * @param deviceType device type
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(DeviceType deviceType, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (deviceType == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(deviceType, holderSystemDeviceStructure);

        return getStructureElement(
                Type.DEVICETYPE,
                deviceType.getUuid(),
                deviceType.getParentUuid(),
                deviceType.getName(), deviceType.getMnemonic(), mnemonicpath, 3,
                deviceType.getDescription(), deviceType.getStatus(), deviceType.isLatest(), deviceType.isDeleted(),
                deviceType.getProcessed(), deviceType.getProcessedBy(), deviceType.getProcessedComment());
    }
    /**
     * Populate and return structure element for device type with focus on requested.
     *
     * @param deviceType device type
     * @param holderSystemDeviceStructure holder of containers for system and device structure content
     * @return  structure element
     */
    public static StructureElement getStructureElementRequested(DeviceType deviceType, HolderSystemDeviceStructure holderSystemDeviceStructure) {
        if (deviceType == null) {
            return null;
        }

        String mnemonicpath = StructureUtil.getMnemonicPath(deviceType, holderSystemDeviceStructure);

        return getStructureElement(
                Type.DEVICETYPE,
                deviceType.getUuid(),
                deviceType.getParentUuid(),
                deviceType.getName(), deviceType.getMnemonic(), mnemonicpath, 3,
                deviceType.getDescription(), Status.PENDING, deviceType.isLatest(), deviceType.isDeleted(),
                deviceType.getRequested(), deviceType.getRequestedBy(), deviceType.getRequestedComment());
    }

    /**
     * Populate and return structure element.
     *
     * @param type type
     * @param uuid uuid
     * @param parent parent uuid
     * @param name name
     * @param mnemonic mnemonic
     * @param mnemonicpath mnemonic path
     * @param level level
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     * @return structure element
     */
    protected static StructureElement getStructureElement(
            Type type,
            UUID uuid,
            UUID parent,
            String name, String mnemonic, String mnemonicpath, Integer level,
            String description, Status status, Boolean latest, Boolean deleted,
            Date when, String who, String comment) {

        return new StructureElement(
                type,
                uuid,
                parent,
                name, mnemonic, mnemonicpath, level,
                description, status, latest, deleted,
                when, who, comment);
    }


    private static boolean hasSameContent(StructureElement structureElement, NameStructure nameStructure) {
        /*
           StructureElement
               x uuid
               x description
               x status
               x latest
               x deleted
           NameStructure
               x uuid
               x description
               x status
               x latest
               x deleted
         */

        if (structureElement == null && nameStructure == null)
            return true;
        if (structureElement == null)
            return false;
        if (nameStructure == null)
            return false;

        if (structureElement.getUuid() == null) {
            if (nameStructure.getUuid() != null)
                return false;
        } else if (!structureElement.getUuid().equals(nameStructure.getUuid()))
            return false;
        if (structureElement.getDescription() == null) {
            if (nameStructure.getDescription() != null)
                return false;
        } else if (!structureElement.getDescription().equals(nameStructure.getDescription()))
            return false;
        if (structureElement.getStatus() == null) {
            if (nameStructure.getStatus() != null)
                return false;
        } else if (!structureElement.getStatus().equals(nameStructure.getStatus()))
            return false;
        if (structureElement.isLatest() == null) {
            if (nameStructure.isLatest() != null)
                return false;
        } else if (!structureElement.isLatest().equals(nameStructure.isLatest()))
            return false;
        if (structureElement.isDeleted() == null) {
            if (nameStructure.isDeleted() != null)
                return false;
        } else if (!structureElement.isDeleted().equals(nameStructure.isDeleted()))
            return false;

        return true;
    }

    private static boolean hasSameContent(StructureElement structureElement, Structure structure) {
        /*
           StructureElement
               x name
               x mnemonic
           Structure
               x name
               x mnemonic
         */

        if (!hasSameContent(structureElement, (NameStructure) structure))
            return false;

        if (structureElement.getName() == null) {
            if (structure.getName() != null)
                return false;
        } else if (!structureElement.getName().equals(structure.getName()))
            return false;
        if (structureElement.getMnemonic() == null) {
            if (structure.getMnemonic() != null)
                return false;
        } else if (!structureElement.getMnemonic().equals(structure.getMnemonic()))
            return false;

        return true;
    }

    public static boolean hasSameContent(StructureElement structureElement, SystemGroup systemGroup) {
        /*
           StructureElement
               x type
           SystemGroup
         */

        if (!hasSameContent(structureElement, (Structure) systemGroup))
            return false;

        if (!Type.SYSTEMGROUP.equals(structureElement.getType())) {
            return false;
        }

        return true;
    }

    public static boolean hasSameContent(StructureElement structureElement, System system) {
        /*
           StructureElement
               x type
               x parent
           System
               x parent_uuid
         */

        if (!hasSameContent(structureElement, (Structure) system))
            return false;

        if (!Type.SYSTEMGROUP.equals(structureElement.getType())) {
            return false;
        }
        if (structureElement.getParent() == null) {
            if (system.getParentUuid() != null)
                return false;
        } else if (!structureElement.getParent().equals(system.getParentUuid()))
            return false;

        return true;
    }

    public static boolean hasSameContent(StructureElement structureElement, Subsystem subsystem) {
        /*
           StructureElement
               x type
               x parent
           Subsystem
               x parent_uuid
         */

        if (!hasSameContent(structureElement, (Structure) subsystem))
            return false;

        if (!Type.SYSTEMGROUP.equals(structureElement.getType())) {
            return false;
        }
        if (structureElement.getParent() == null) {
            if (subsystem.getParentUuid() != null)
                return false;
        } else if (!structureElement.getParent().equals(subsystem.getParentUuid()))
            return false;

        return true;
    }

    public static boolean hasSameContent(StructureElement structureElement, Discipline discipline) {
        /*
           StructureElement
               x type
           Discipline
         */

        if (!hasSameContent(structureElement, (Structure) discipline))
            return false;

        if (!Type.SYSTEMGROUP.equals(structureElement.getType())) {
            return false;
        }

        return true;
    }

    public static boolean hasSameContent(StructureElement structureElement, DeviceGroup deviceGroup) {
        /*
           StructureElement
               x type
               x parent
           Subsystem
               x parent_uuid
         */

         if (!hasSameContent(structureElement, (Structure) deviceGroup))
             return false;

         if (!Type.SYSTEMGROUP.equals(structureElement.getType())) {
             return false;
         }
         if (structureElement.getParent() == null) {
             if (deviceGroup.getParentUuid() != null)
                 return false;
         } else if (!structureElement.getParent().equals(deviceGroup.getParentUuid()))
             return false;

         return true;
    }

    public static boolean hasSameContent(StructureElement structureElement, DeviceType deviceType) {
        /*
           StructureElement
               x type
               x parent
           DeviceType
               x parent_uuid
         */

         if (!hasSameContent(structureElement, (Structure) deviceType))
             return false;

         if (!Type.SYSTEMGROUP.equals(structureElement.getType())) {
             return false;
         }
         if (structureElement.getParent() == null) {
             if (deviceType.getParentUuid() != null)
                 return false;
         } else if (!structureElement.getParent().equals(deviceType.getParentUuid()))
             return false;

         return true;
    }

}
