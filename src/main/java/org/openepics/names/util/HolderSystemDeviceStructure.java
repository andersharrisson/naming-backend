/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;

/**
 * Utility class and holder of containers for system and device structure content
 * that are used to populate REST API return elements.
 * <p>
 * Class is used for performance reasons to speed up preparation of what is to be returned.
 * </p>
 *
 * @author Lars Johansson
 */
public class HolderSystemDeviceStructure {

    private HashMap<UUID, SystemGroup> systemGroups;
    private HashMap<UUID, System>      systems;
    private HashMap<UUID, Subsystem>   subsystems;

    private HashMap<UUID, Discipline>  disciplines;
    private HashMap<UUID, DeviceGroup> deviceGroups;
    private HashMap<UUID, DeviceType>  deviceTypes;

    private boolean includeDeleted = true;

    /**
    * Public constructor to prepare containers for system and device structure content.
    *
    * @param holderIRepositories
    */
   public HolderSystemDeviceStructure (HolderIRepositories holderIRepositories) {
       this(holderIRepositories, true);
   }

    /**
     * Public constructor to prepare containers for system and device structure content.
     *
     * @param holderIRepositories holder of references to repositories
     * @param includeDeleted include deleted structure entries
     */
    public HolderSystemDeviceStructure (HolderIRepositories holderIRepositories, boolean includeDeleted) {
        List<SystemGroup> systemGroups = null;
        List<System> systems = null;
        List<Subsystem> subsystems = null;
        List<Discipline> disciplines = null;
        List<DeviceGroup> deviceGroups = null;
        List<DeviceType> deviceTypes = null;

        if (includeDeleted) {
            systemGroups = holderIRepositories.getSystemGroupRepository().findLatest();
            systems = holderIRepositories.getSystemRepository().findLatest();
            subsystems = holderIRepositories.getSubsystemRepository().findLatest();
            disciplines = holderIRepositories.getDisciplineRepository().findLatest();
            deviceGroups = holderIRepositories.getDeviceGroupRepository().findLatest();
            deviceTypes = holderIRepositories.getDeviceTypeRepository().findLatest();
        } else {
            systemGroups = holderIRepositories.getSystemGroupRepository().findLatestNotDeleted();
            systems = holderIRepositories.getSystemRepository().findLatestNotDeleted();
            subsystems = holderIRepositories.getSubsystemRepository().findLatestNotDeleted();
            disciplines = holderIRepositories.getDisciplineRepository().findLatestNotDeleted();
            deviceGroups = holderIRepositories.getDeviceGroupRepository().findLatestNotDeleted();
            deviceTypes = holderIRepositories.getDeviceTypeRepository().findLatestNotDeleted();
        }

        // initial capacity
        //     default load factor 0.75
        //     set at proper size to avoid rehashing
        //     size/0.75+1 (may be rounded downwards so possibly +2 instead)
        this.systemGroups = new HashMap<>((int)(systemGroups.size()/0.75 + 2));
        this.systems      = new HashMap<>((int)(systems.size()/0.75 + 2));
        this.subsystems   = new HashMap<>((int)(subsystems.size()/0.75 + 2));
        this.disciplines  = new HashMap<>((int)(disciplines.size()/0.75 + 2));
        this.deviceGroups = new HashMap<>((int)(deviceGroups.size()/0.75 + 2));
        this.deviceTypes  = new HashMap<>((int)(deviceTypes.size()/0.75 + 2));

        for (SystemGroup systemGroup : systemGroups) {
            this.systemGroups.put(systemGroup.getUuid(), systemGroup);
        }
        for (System system : systems) {
            this.systems.put(system.getUuid(), system);
        }
        for (Subsystem subsystem : subsystems) {
            this.subsystems.put(subsystem.getUuid(), subsystem);
        }
        for (Discipline discipline : disciplines) {
            this.disciplines.put(discipline.getUuid(), discipline);
        }
        for (DeviceGroup deviceGroup : deviceGroups) {
            this.deviceGroups.put(deviceGroup.getUuid(), deviceGroup);
        }
        for (DeviceType deviceType : deviceTypes) {
            this.deviceTypes.put(deviceType.getUuid(), deviceType);
        }
    }

    /**
     * Find system group name part by uuid.
     *
     * @param uuid uuid
     * @return system group name part
     */
    public SystemGroup findSystemGroupByUuid(UUID uuid) {
        return systemGroups.get(uuid);
    }
    /**
     * Find system name part by uuid.
     *
     * @param uuid uuid
     * @return system name part
     */
    public System findSystemByUuid(UUID uuid) {
        return systems.get(uuid);
    }
    /**
     * Find subsystem name part by uuid.
     *
     * @param uuid uuid
     * @return subsystem name part
     */
    public Subsystem findSubsystemByUuid(UUID uuid) {
        return subsystems.get(uuid);
    }

    /**
     * Find discipline name part by uuid.
     *
     * @param uuid uuid
     * @return discipline name part
     */
    public Discipline findDisciplineByUuid(UUID uuid) {
        return disciplines.get(uuid);
    }
    /**
     * Find device group name part by uuid.
     *
     * @param uuid uuid
     * @return device group name part
     */
    public DeviceGroup findDeviceGroupByUuid(UUID uuid) {
        return deviceGroups.get(uuid);
    }
    /**
     * Find device type name part by uuid.
     *
     * @param uuid uuid
     * @return device type name part
     */
    public DeviceType findDeviceTypeByUuid(UUID uuid) {
        return deviceTypes.get(uuid);
    }

    /**
     * Return if include deleted structure entries.
     *
     * @return if include deleted structure entries
     */
    public boolean getIncludeDeleted() {
        return includeDeleted;
    }

    /**
     * Return map with key-value pairs (uuid and system group).
     *
     * @return map with key-value pairs (uuid and system group)
     */
    public HashMap<UUID, SystemGroup> getUuidSystemGroups() {
        return systemGroups;
    }

    /**
     * Return map with key-value pairs (uuid and system).
     *
     * @return map with key-value pairs (uuid and system)
     */
    public HashMap<UUID, System> getUuidSystems() {
        return systems;
    }

    /**
     * Return map with key-value pairs (uuid and subsystem).
     *
     * @return map with key-value pairs (uuid and subsystem)
     */
    public HashMap<UUID, Subsystem> getUuidSubsystems() {
        return subsystems;
    }

    /**
     * Return map with key-value pairs (uuid and discipline).
     *
     * @return map with key-value pairs (uuid and discipline)
     */
    public HashMap<UUID, Discipline> getUuidDisciplines() {
        return disciplines;
    }

    /**
     * Return map with key-value pairs (uuid and device group).
     *
     * @return map with key-value pairs (uuid and device group)
     */
    public HashMap<UUID, DeviceGroup> getUuidDeviceGroups() {
        return deviceGroups;
    }

    /**
     * Return map with key-value pairs (uuid and device type).
     *
     * @return map with key-value pairs (uuid and device type)
     */
    public HashMap<UUID, DeviceType> getUuidDeviceTypes() {
        return deviceTypes;
    }
}
