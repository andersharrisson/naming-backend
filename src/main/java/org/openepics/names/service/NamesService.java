/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderSystemDeviceStructure;
import org.openepics.names.util.NameElementUtil;
import org.openepics.names.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides names services.
 *
 * @author Lars Johansson
 */
@Service
public class NamesService {

    // HolderIRepositories and HolderSystemDeviceStructure may or may not be used for preparation of what to return

    // for each method
    //     document what values come from NameElement and what values come from persistence layer
    //     somehow provide this information to user

    private static final Logger LOGGER = Logger.getLogger(NamesService.class.getName());

    private EssNamingConvention namingConvention;

    private HolderIRepositories holderIRepositories;
    private NameRepository nameRepository;

    @Autowired
    public NamesService(
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository,
            NameRepository nameRepository) {

        this.namingConvention = new EssNamingConvention();
        this.holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository);
        this.nameRepository = nameRepository;
    }

    @Transactional
    public List<NameElement> createNames(List<NameElement> nameElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         for each name element
        //             create name, latest, with data
        //             add name element for created
        //     return
        //         name elements for created names

        LOGGER.log(Level.INFO, "createNames, nameElements.size:        " + String.valueOf(nameElements != null ? nameElements.size() : "null"));

        // do
        String requestedBy = "test who";
        final List<NameElement> createdNameElements = Lists.newArrayList();
        for (NameElement nameElement : nameElements) {
            // create
            Name name = new Name();
            setAttributes(name,
                    UUID.randomUUID(),
                    nameElement.getSystemgroup(), nameElement.getSystem(), nameElement.getSubsystem(), nameElement.getDevicetype(),
                    nameElement.getIndex(), nameElement.getName(), namingConvention.equivalenceClassRepresentative(nameElement.getName()),
                    nameElement.getDescription(), Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    new Date(), requestedBy, nameElement.getComment());

            nameRepository.createName(name);

            // possibly validate that created
            //     approved, latest, not deleted, uuid

            // add
            createdNameElements.add(NameElementUtil.getNameElement(name));
        }

        LOGGER.log(Level.INFO, "createNames, createdNameElements.size: " + createdNameElements.size());
        return createdNameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    public List<NameElement> readNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readNames(deleted, queryFields, queryValues, Boolean.FALSE, orderBy, isAsc, offset, limit);
    }

    public List<NameElement> readNames(
            Boolean deleted, FieldName[] queryFields, String[] queryValues, Boolean includeHistory,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        LOGGER.log(Level.INFO, "readNames, deleted:            " + deleted);
        LOGGER.log(Level.INFO, "readNames, queryFields.length: " + String.valueOf(queryFields != null ? queryFields.length : "null"));
        LOGGER.log(Level.INFO, "readNames, queryValues.length: " + String.valueOf(queryValues != null ? queryValues.length : "null"));
        LOGGER.log(Level.INFO, "readNames, includeHistory:     " + includeHistory);
        LOGGER.log(Level.INFO, "readNames, orderBy:            " + orderBy);
        LOGGER.log(Level.INFO, "readNames, isAsc:              " + isAsc);
        LOGGER.log(Level.INFO, "readNames, offset:             " + offset);
        LOGGER.log(Level.INFO, "readNames, limit:              " + limit);
        if (queryFields != null && queryFields.length > 0) {
            for (FieldName queryField : queryFields) {
                LOGGER.log(Level.INFO, "readNames, queryField:         " + queryField);
            }
        }
        if (queryValues != null && queryValues.length > 0) {
            for (String queryValue : queryValues) {
                LOGGER.log(Level.INFO, "readNames, queryValue:         " + queryValue);
            }
        }

        // validate input
        //     queryFields and queryValues
        //         uuid
        // do
        //     read names
        // return
        //     name elements for names

        // validate input
        ValidateUtil.validateNamesInputRead(
                deleted, queryFields, queryValues,
                includeHistory,
                orderBy, isAsc,
                offset, limit);

        // do
        List<Name> names = nameRepository.readNames(deleted, queryFields, queryValues, includeHistory, orderBy, isAsc, offset, limit);

        final List<NameElement> nameElements = Lists.newArrayList();
        for (Name name : names) {
            nameElements.add(NameElementUtil.getNameElement(name));
        }

        LOGGER.log(Level.INFO, "readNames, nameElements.size:  " + nameElements.size());
        return nameElements;
    }

    public List<NameElement> readNames(
            String name,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        LOGGER.log(Level.INFO, "readNames, name:              " + name);

        // validate input
        //     name or uuid
        //     to work with both uuid and name
        // do
        //     name or uuid
        // return
        //     name elements

        // validate input
        ValidateUtil.validateInputName(name);

        // do
        final List<NameElement> nameElements = Lists.newArrayList();
        try {
            UUID.fromString(name);
            Name latestByUuid = holderIRepositories.getNameRepository().findLatestByUuid(name);
            if (latestByUuid != null) {
                nameElements.add(NameElementUtil.getNameElement(latestByUuid));
            }
        } catch (IllegalArgumentException e) {
            nameElements.addAll(readNames(false, new FieldName[] {FieldName.NAME}, new String[] {name}, orderBy, isAsc, offset, limit));
        }

        LOGGER.log(Level.INFO, "readNames, nameElements.size: " + nameElements.size());
        return nameElements;
    }

    public List<NameElement> readNamesSystemStructure(
            String mnemonicpath,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        LOGGER.log(Level.INFO, "readNamesSystemStructure, mnemonicpath: " + mnemonicpath);

        // validate input
        //     mnemonic path
        // do
        //     mnemonic path
        // return
        //     name elements

        // validate input
        ValidateUtil.validateInputMnemonicpath(mnemonicpath);

        // do
        return readNames(false, new FieldName[] {FieldName.SYSTEMSTRUCTURE}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit);
    }

    public List<NameElement> readNamesDeviceStructure(
            String mnemonicpath,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        LOGGER.log(Level.INFO, "readNamesDeviceStructure, mnemonicpath: " + mnemonicpath);

        // validate input
        //     mnemonic path
        // do
        //     mnemonic path
        // return
        //     name elements

        // validate input
        ValidateUtil.validateInputMnemonicpath(mnemonicpath);

        // do
        return readNames(false, new FieldName[] {FieldName.DEVICESTRUCTURE}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit);
    }

    public List<NameElement> readNamesHistory(
            String uuid,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        LOGGER.log(Level.INFO, "readNamesHistory, uuid:              " + uuid);

        // note
        //     HolderIRepositories and HolderSystemDeviceStructure may or may not be used for preparation of what to return
        // validate input
        //     uuid
        // do
        //     read history for name
        // return
        //     name elements for names

        // validate input
        ValidateUtil.validateInputUuid(uuid);

        // do
        List<NameElement> nameElements = readNames(null, new FieldName[] {FieldName.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit);

        Collections.sort(nameElements, new Comparator<NameElement>() {
            @Override
            public int compare(NameElement e1, NameElement e2) {
                return e1.getWhen().compareTo(e2.getWhen());
            }
        });

        LOGGER.log(Level.INFO, "readNamesHistory, nameElements.size: " + nameElements.size());

        return nameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    public String equivalenceName(String name) {
        LOGGER.log(Level.INFO, "equivalenceName, name: " + name);

        // validate input
        // do
        //     exists

        // validate input
        ValidateUtil.validateInputName(name);

        // do
        return namingConvention.equivalenceClassRepresentative(name);
    }

    public Boolean existsName(String name) {
        LOGGER.log(Level.INFO, "existsName, name: " + name);

        // validate input
        // do
        //     exists

        // validate input
        ValidateUtil.validateInputName(name);

        // do
        List<Name> names = nameRepository.readNames(false, FieldName.NAME, name);
        return !names.isEmpty();
    }

    public Boolean isLegacyName(String name) {
        LOGGER.log(Level.INFO, "isLegacyName, name: " + name);

        // validate input
        // do
        //     exists

        // validate input
        ValidateUtil.validateInputName(name);

        // do
        List<Name> names = nameRepository.readNames(false, FieldName.NAME, name);
        ValidateUtil.validateCondition(names != null && names.size() == 1, HttpStatus.BAD_REQUEST, "name not available", name);

        Name toBeChecked = names.get(0);

        // system structure
        if (toBeChecked.getSystemgroupUuid() != null) {
            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestNotDeletedByUuid(toBeChecked.getSystemgroupUuid().toString());
            ValidateUtil.validateCondition(systemGroup != null, HttpStatus.BAD_REQUEST, "system group not available", name);

            if (systemGroup.isDeleted()) {
                return Boolean.TRUE;
            }
        } else if (toBeChecked.getSystemUuid() != null) {
            org.openepics.names.repository.model.System system = holderIRepositories.getSystemRepository().findLatestNotDeletedByUuid(toBeChecked.getSystemUuid().toString());
            ValidateUtil.validateCondition(system != null, HttpStatus.BAD_REQUEST, "system not available", name);

            if (system.isDeleted()) {
                return Boolean.TRUE;
            }

            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestNotDeletedByUuid(system.getParentUuid().toString());
            ValidateUtil.validateCondition(systemGroup != null, HttpStatus.BAD_REQUEST, "system group not available", name);

            if (systemGroup.isDeleted()) {
                return Boolean.TRUE;
            }
        } else if (toBeChecked.getSubsystemUuid() != null) {
            Subsystem subsystem = holderIRepositories.getSubsystemRepository().findLatestNotDeletedByUuid(toBeChecked.getSubsystemUuid().toString());
            ValidateUtil.validateCondition(subsystem != null, HttpStatus.BAD_REQUEST, "subsystem not available", name);

            if (subsystem.isDeleted()) {
                return Boolean.TRUE;
            }

            org.openepics.names.repository.model.System system = holderIRepositories.getSystemRepository().findLatestNotDeletedByUuid(subsystem.getParentUuid().toString());
            ValidateUtil.validateCondition(system != null, HttpStatus.BAD_REQUEST, "system not available", name);

            if (system.isDeleted()) {
                return Boolean.TRUE;
            }

            SystemGroup systemGroup = holderIRepositories.getSystemGroupRepository().findLatestNotDeletedByUuid(system.getParentUuid().toString());
            ValidateUtil.validateCondition(systemGroup != null, HttpStatus.BAD_REQUEST, "system group not available", name);

            if (systemGroup.isDeleted()) {
                return Boolean.TRUE;
            }
        }
        // device structure
        if (toBeChecked.getDevicetypeUuid() != null) {
            DeviceType deviceType = holderIRepositories.getDeviceTypeRepository().findLatestNotDeletedByUuid(toBeChecked.getDevicetypeUuid().toString());
            ValidateUtil.validateCondition(deviceType != null, HttpStatus.BAD_REQUEST, "device type not available", name);

            if (deviceType.isDeleted()) {
                return Boolean.TRUE;
            }

            DeviceGroup deviceGroup = holderIRepositories.getDeviceGroupRepository().findLatestNotDeletedByUuid(deviceType.getParentUuid().toString());
            ValidateUtil.validateCondition(deviceGroup != null, HttpStatus.BAD_REQUEST, "device group not available", name);

            if (deviceGroup.isDeleted()) {
                return Boolean.TRUE;
            }

            Discipline discipline = holderIRepositories.getDisciplineRepository().findLatestNotDeletedByUuid(deviceType.getParentUuid().toString());
            ValidateUtil.validateCondition(discipline != null, HttpStatus.BAD_REQUEST, "device group not available", name);

            if (discipline.isDeleted()) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public Boolean isValidToCreateName(String name) {
        LOGGER.log(Level.INFO, "isValidToCreateName, name: " + name);

        // validate input
        // validate data
        //     not exists

        // validate input
        ValidateUtil.validateInputName(name);

        // initiate holder of containers for system and device structure content, for performance reasons
        //     note false to not include deleted entries
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories, false);

        // validate data
        ValidateUtil.validateNameDataCreate(name, namingConvention, holderIRepositories, nameRepository, holder);

        return Boolean.TRUE;
    }

    // ----------------------------------------------------------------------------------------------------

    public void validateNamesCreate(NameElement nameElement) {
        validateNamesCreate(nameElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateNamesCreate(NameElement nameElement, HolderSystemDeviceStructure holder) {
        // validate authority
        //     elsewhere
        //         naming user & admin
        // validate input
        //     name element
        //         validate input itself
        // validate data
        //     name element
        //         validate towards repository

        // validate input
        ValidateUtil.validateNameElementInputCreate(nameElement);

        // validate data
        ValidateUtil.validateNameElementDataCreate(nameElement, namingConvention, holderIRepositories, nameRepository, holder);
    }
    public void validateNamesCreate(List<NameElement> nameElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (NameElement nameElement : nameElements) {
            validateNamesCreate(nameElement, holder);
        }
    }

    public void validateNamesUpdate(NameElement nameElement) {
        validateNamesUpdate(nameElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateNamesUpdate(NameElement nameElement, HolderSystemDeviceStructure holder) {
        // validate authority
        //     elsewhere
        //         naming user & admin
        // validate input
        //     name element
        //         validate input itself
        // validate data
        //     name element
        //         validate towards repository

        // validate input
        ValidateUtil.validateNameElementInputUpdate(nameElement);

        // validate data
        ValidateUtil.validateNameElementDataUpdate(nameElement, namingConvention, holderIRepositories, nameRepository, holder);
    }
    public void validateNamesUpdate(List<NameElement> nameElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (NameElement nameElement : nameElements) {
            validateNamesUpdate(nameElement, holder);
        }
    }

    public void validateNamesDelete(NameElement nameElement) {
        validateNamesDelete(nameElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateNamesDelete(NameElement nameElement, HolderSystemDeviceStructure holder) {
        //     validate authority
        //         elsewhere
        //             naming user & admin
        //     validate input
        //         uuid
        //     validate data
        //         retrieve name (uuid, latest, not deleted)

        // validate input
        ValidateUtil.validateNameElementInputDelete(nameElement);

        // validate data
        ValidateUtil.validateNameElementDataDelete(nameElement, namingConvention, holderIRepositories, nameRepository, holder);
    }
    public void validateNamesDelete(List<NameElement> nameElements) {
        //     validate authority
        //         elsewhere
        //             naming user & admin
        //     validate input
        //         name element
        //             validate input itself
        //     validate data
        //         name element
        //             validate towards repository

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (NameElement nameElement : nameElements) {
            validateNamesDelete(nameElement, holder);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<NameElement> updateNames(List<NameElement> nameElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         for each name element
        //             update name to not latest
        //             insert name to latest, not deleted, with data
        //             read
        //     return
        //         name elements for updated names

        LOGGER.log(Level.INFO, "updateNames, nameElements.size:        " + String.valueOf(nameElements != null ? nameElements.size() : "null"));

        // do
        String requestedBy = "test who";
        final List<NameElement> updatedNameElements = Lists.newArrayList();
        for (NameElement nameElement : nameElements) {
            // update not latest, not deleted
            // create latest, not deleted

            Name name = holderIRepositories.getNameRepository().findLatestByUuid(nameElement.getUuid().toString());
            if (name == null) {
                continue;
            }

            // skip if name element has same content as name
            //     proceed without fail
            if (NameElementUtil.hasSameContent(nameElement, name)) {
                continue;
            }

            // update
            name.setLatest(Boolean.FALSE);
            nameRepository.updateName(name);

            // create
            name = new Name();
            setAttributes(name,
                    nameElement.getUuid(),
                    nameElement.getSystemgroup(), nameElement.getSystem(), nameElement.getSubsystem(), nameElement.getDevicetype(),
                    nameElement.getIndex(), nameElement.getName(), namingConvention.equivalenceClassRepresentative(nameElement.getName()),
                    nameElement.getDescription(), Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    new Date(), requestedBy, nameElement.getComment());

            nameRepository.createName(name);

            // possibly validate that updated

            // add
            updatedNameElements.add(NameElementUtil.getNameElement(name));
        }

        LOGGER.log(Level.INFO, "updateNames, updatedNameElements.size: " + updatedNameElements.size());
        return updatedNameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<NameElement> deleteNames(List<NameElement> nameElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         update name to not latest
        //         insert name to latest, deleted
        //         read
        //     return
        //         name element for deleted name

        LOGGER.log(Level.INFO, "deleteNames, nameElements.size:        " + String.valueOf(nameElements != null ? nameElements.size() : "null"));

        // do
        String requestedBy = "test who";
        final List<NameElement> deletedNameElements = Lists.newArrayList();
        for (NameElement nameElement : nameElements) {
            Name name = holderIRepositories.getNameRepository().findLatestByUuid(nameElement.getUuid().toString());
            if (name == null) {
                continue;
            }

            name.setLatest(Boolean.FALSE);
            nameRepository.updateName(name);

            name = new Name();
            setAttributes(name,
                    nameElement.getUuid(),
                    nameElement.getSystemgroup(), nameElement.getSystem(), nameElement.getSubsystem(), nameElement.getDevicetype(),
                    nameElement.getIndex(), nameElement.getName(), namingConvention.equivalenceClassRepresentative(nameElement.getName()),
                    nameElement.getDescription(), Status.APPROVED, Boolean.TRUE, Boolean.TRUE,
                    new Date(), requestedBy, nameElement.getComment());

            nameRepository.createName(name);

            // possibly validate that deleted

            // add
            deletedNameElements.add(NameElementUtil.getNameElement(name));
        }

        LOGGER.log(Level.INFO, "deleteNames, deletedNameElements.size: " + deletedNameElements.size());
        return deletedNameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to help set attributes for Name class.
     *
     * @param name name
     * @param uuid uuid
     * @param systemgroupUuid system group uuid
     * @param systemUuid system uuid
     * @param subsystemUuid subsystem uuid
     * @param devicetypeUuid device type uuid
     * @param index index
     * @param conventionName convention name
     * @param conventionNameEquivalence convention name equivalence
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     */
    private void setAttributes(Name name,
            UUID uuid, UUID systemgroupUuid, UUID systemUuid, UUID subsystemUuid, UUID devicetypeUuid,
            String index, String conventionName, String conventionNameEquivalence,
            String description, Status status, Boolean latest, Boolean deleted,
            Date requested, String requestedBy, String requestedComment) {
        name.setUuid(uuid);
        name.setSystemgroupUuid(systemgroupUuid);
        name.setSystemUuid(systemUuid);
        name.setSubsystemUuid(subsystemUuid);
        name.setDevicetypeUuid(devicetypeUuid);
        name.setInstanceIndex(index);
        name.setConventionName(conventionName);
        name.setConventionNameEquivalence(conventionNameEquivalence);
        name.setDescription(description);
        name.setStatus(Status.APPROVED);
        name.setLatest(latest);
        name.setDeleted(deleted);
        name.setRequested(requested);
        name.setRequestedBy(requestedBy);
        name.setRequestedComment(requestedComment);

    }
}
