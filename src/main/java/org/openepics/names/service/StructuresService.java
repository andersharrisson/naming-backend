/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.DeviceGroupRepository;
import org.openepics.names.repository.DeviceTypeRepository;
import org.openepics.names.repository.DisciplineRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.SubsystemRepository;
import org.openepics.names.repository.SystemGroupRepository;
import org.openepics.names.repository.SystemRepository;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderRepositories;
import org.openepics.names.util.HolderSystemDeviceStructure;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.StructureElementUtil.StructureChoice;
import org.openepics.names.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides structures services.
 *
 * @author Lars Johansson
 */
@Service
public class StructuresService {

    // HolderIRepositories and HolderSystemDeviceStructure may or may not be used for preparation of what to return

    // for each method
    //     document what values come from StructureElement and what values come from persistence layer
    //     somehow provide this information to user

    // latest
    //     automatically not show structures that do not come into play
    //     = automatically exclude (approved and not latest)
    //     otherwise refer to history

    private static final Logger LOGGER = Logger.getLogger(StructuresService.class.getName());

    private EssNamingConvention namingConvention;

    private HolderIRepositories holderIRepositories;
    private HolderRepositories holderRepositories;

    @Autowired
    public StructuresService(
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository,
            NameRepository nameRepository,
            SystemGroupRepository systemGroupRepository,
            SystemRepository systemRepository,
            SubsystemRepository subsystemRepository,
            DisciplineRepository disciplineRepository,
            DeviceGroupRepository deviceGroupRepository,
            DeviceTypeRepository deviceTypeRepository) {

        this.namingConvention = new EssNamingConvention();
        this.holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository);
        this.holderRepositories = new HolderRepositories(
                nameRepository,
                systemGroupRepository,
                systemRepository,
                subsystemRepository,
                disciplineRepository,
                deviceGroupRepository,
                deviceTypeRepository);
    }

    @Transactional
    public List<StructureElement> createStructures(List<StructureElement> structureElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         for each structure element
        //             create structure to pending, not latest, not deleted, with data
        //             add structure element for created
        //     return
        //         structure elements for created structures

        LOGGER.log(Level.INFO, "createStructures, structureElements.size:        " + String.valueOf(structureElements != null ? structureElements.size() : "null"));

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // do
        String requestedBy = "test who";
        final List<StructureElement> createdStructureElements = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // note rules for mnemonic for system group
                String mnemonic = structureElement.getMnemonic();
                mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;

                // create
                SystemGroup systemGroup = new SystemGroup();
                setAttributes(systemGroup,
                        UUID.randomUUID(),
                        structureElement.getName(), mnemonic, !StringUtils.isEmpty(mnemonic) ? namingConvention.equivalenceClassRepresentative(mnemonic) : null,
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSystemGroupRepository().createSystemGroup(systemGroup);

                // possibly validate that created

                // add
                createdStructureElements.add(StructureElementUtil.getStructureElementRequested(systemGroup, holder));
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // create
                System system = new System();
                setAttributes(system,
                        UUID.randomUUID(), structureElement.getParent(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSystemRepository().createSystem(system);

                // possibly validate that created

                // add
                createdStructureElements.add(StructureElementUtil.getStructureElementRequested(system, holder));
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // create
                Subsystem subsystem = new Subsystem();
                setAttributes(subsystem,
                        UUID.randomUUID(), structureElement.getParent(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSubsystemRepository().createSubsystem(subsystem);

                // possibly validate that created

                // add
                createdStructureElements.add(StructureElementUtil.getStructureElementRequested(subsystem, holder));
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                // create
                Discipline discipline = new Discipline();
                setAttributes(discipline,
                        UUID.randomUUID(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDisciplineRepository().createDiscipline(discipline);

                // possibly validate that created

                // add
                createdStructureElements.add(StructureElementUtil.getStructureElementRequested(discipline, holder));
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // note rules for mnemonic for device group
                String mnemonic = structureElement.getMnemonic();
                mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;

                // create
                DeviceGroup deviceGroup = new DeviceGroup();
                setAttributes(deviceGroup,
                        UUID.randomUUID(), structureElement.getParent(),
                        structureElement.getName(), mnemonic, !StringUtils.isEmpty(mnemonic) ? namingConvention.equivalenceClassRepresentative(mnemonic) : null,
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDeviceGroupRepository().createDeviceGroup(deviceGroup);

                // possibly validate that created

                // add
                createdStructureElements.add(StructureElementUtil.getStructureElementRequested(deviceGroup, holder));
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                // create
                DeviceType deviceType = new DeviceType();
                setAttributes(deviceType,
                        UUID.randomUUID(), structureElement.getParent(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDeviceTypeRepository().createDeviceType(deviceType);

                // possibly validate that created

                // add
                createdStructureElements.add(StructureElementUtil.getStructureElementRequested(deviceType, holder));
            }
        }

        LOGGER.log(Level.INFO, "createStructures, createdStructureElements.size: " + createdStructureElements.size());
        return createdStructureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    public List<StructureElement> readStructures(
            Type type, Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readStructures(type, statuses, deleted, queryFields, queryValues, Boolean.FALSE, orderBy, isAsc, offset, limit, StructureChoice.STRUCTURE);
    }

    //need to have public static enum StructureChoice {HISTORY, STRUCTURE};
    public List<StructureElement> readStructures(
            Type type, Status[] statuses, Boolean deleted, FieldStructure[] queryFields, String[] queryValues, Boolean includeHistory,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit,
            StructureChoice structureChoice) {

        LOGGER.log(Level.INFO, "readStructures, type:                   " + type);
        LOGGER.log(Level.INFO, "readStructures, statuses.length:        " + String.valueOf(statuses != null ? statuses.length : "null"));
        LOGGER.log(Level.INFO, "readStructures, deleted:                " + deleted);
        LOGGER.log(Level.INFO, "readStructures, queryFields.length:     " + String.valueOf(queryFields != null ? queryFields.length : "null"));
        LOGGER.log(Level.INFO, "readStructures, queryValues.length:     " + String.valueOf(queryValues != null ? queryValues.length : "null"));
        LOGGER.log(Level.INFO, "readStructures, includeHistory:         " + includeHistory);
        LOGGER.log(Level.INFO, "readStructures, orderBy:                " + orderBy);
        LOGGER.log(Level.INFO, "readStructures, isAsc:                  " + isAsc);
        LOGGER.log(Level.INFO, "readStructures, offset:                 " + offset);
        LOGGER.log(Level.INFO, "readStructures, limit:                  " + limit);
        LOGGER.log(Level.INFO, "readStructures, structureChoice:        " + structureChoice);
        if (statuses != null && statuses.length > 0) {
            for (Status status : statuses) {
                LOGGER.log(Level.INFO, "readStructures, status:                 " + status);
            }
        }
        if (queryFields != null && queryFields.length > 0) {
            for (FieldStructure queryField : queryFields) {
                LOGGER.log(Level.INFO, "readStructures, queryField:             " + queryField);
            }
        }
        if (queryValues != null && queryValues.length > 0) {
            for (String queryValue : queryValues) {
                LOGGER.log(Level.INFO, "readStructures, queryValue:             " + queryValue);
            }
        }

        // validate input
        //     type
        //     queryFields and queryValues
        //         uuid
        // do
        //     read structures
        // return
        //     structure elements for structures

        // validate input
        ValidateUtil.validateStructuresInputRead(
                type, statuses, deleted, queryFields, queryValues,
                includeHistory,
                orderBy, isAsc,
                offset, limit);

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // do
        final List<StructureElement> structureElements = Lists.newArrayList();
        if (Type.SYSTEMGROUP.equals(type)) {
            List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(
                    statuses, deleted, queryFields, queryValues,
                    includeHistory,
                    orderBy, isAsc,
                    offset, limit);
            LOGGER.log(Level.INFO, "readStructures, systemGroups.size:      " + systemGroups.size());
            structureElements.addAll(StructureElementUtil.getStructureElementsForSystemGroups(systemGroups, holder, structureChoice));
        } else if (Type.SYSTEM.equals(type)) {
            List<System> systems = holderRepositories.getSystemRepository().readSystems(
                    statuses, deleted, queryFields, queryValues,
                    includeHistory,
                    orderBy, isAsc,
                    offset, limit);
            LOGGER.log(Level.INFO, "readStructures, systems.size:           " + systems.size());
            structureElements.addAll(StructureElementUtil.getStructureElementsForSystems(systems, holder, structureChoice));
        } else if (Type.SUBSYSTEM.equals(type)) {
            List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(
                    statuses, deleted, queryFields, queryValues,
                    includeHistory,
                    orderBy, isAsc,
                    offset, limit);
            LOGGER.log(Level.INFO, "readStructures, subsystems.size:        " + subsystems.size());
            structureElements.addAll(StructureElementUtil.getStructureElementsForSubsystems(subsystems, holder, structureChoice));
        } else if (Type.DISCIPLINE.equals(type)) {
            List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(
                    statuses, deleted, queryFields, queryValues,
                    includeHistory,
                    orderBy, isAsc,
                    offset, limit);
            LOGGER.log(Level.INFO, "readStructures, disciplines.size:       " + disciplines.size());
            structureElements.addAll(StructureElementUtil.getStructureElementsForDisciplines(disciplines, holder, structureChoice));
        } else if (Type.DEVICEGROUP.equals(type)) {
            List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(
                    statuses, deleted, queryFields, queryValues,
                    includeHistory,
                    orderBy, isAsc,
                    offset, limit);
            LOGGER.log(Level.INFO, "readStructures, deviceGroups.size:      " + deviceGroups.size());
            structureElements.addAll(StructureElementUtil.getStructureElementsForDeviceGroups(deviceGroups, holder, structureChoice));
        } else if (Type.DEVICETYPE.equals(type)) {
            List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(
                    statuses, deleted, queryFields, queryValues,
                    includeHistory,
                    orderBy, isAsc,
                    offset, limit);
            LOGGER.log(Level.INFO, "readStructures, deviceTypes.size:       " + deviceTypes.size());
            structureElements.addAll(StructureElementUtil.getStructureElementsForDeviceTypes(deviceTypes, holder, structureChoice));
        }

        LOGGER.log(Level.INFO, "readStructures, structureElements.size: " + structureElements.size());
        return structureElements;
    }

    public List<StructureElement> readStructuresChildren(
            Type type, String uuid,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // validate input
        //     type, uuid
        // do
        //     read structure latest by uuid for type
        // return
        //     structure elements for structures

        LOGGER.log(Level.INFO, "readStructuresChildren, type:                   " + type);
        LOGGER.log(Level.INFO, "readStructuresChildren, uuid:                   " + uuid);

        // validate input
        ValidateUtil.validateInputType(type);
        ValidateUtil.validateInputUuid(uuid);

        // do
        List<StructureElement> structureElements = Lists.newArrayList();
        if (Type.SYSTEMGROUP.equals(type)) {
            structureElements.addAll(readStructures(Type.SYSTEM, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT}, new String[] {uuid}, orderBy, isAsc, offset, limit));
        } else if (Type.SYSTEM.equals(type)) {
            structureElements.addAll(readStructures(Type.SUBSYSTEM, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT}, new String[] {uuid}, orderBy, isAsc, offset, limit));
        } else if (Type.SUBSYSTEM.equals(type)) {
            // no children in structures
        } else if (Type.DISCIPLINE.equals(type)) {
            structureElements.addAll(readStructures(Type.DEVICEGROUP, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT}, new String[] {uuid}, orderBy, isAsc, offset, limit));
        } else if (Type.DEVICEGROUP.equals(type)) {
            structureElements.addAll(readStructures(Type.DEVICETYPE, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.PARENT}, new String[] {uuid}, orderBy, isAsc, offset, limit));
        } else if (Type.DEVICETYPE.equals(type)) {
            // no children in structures
        }
        LOGGER.log(Level.INFO, "readStructuresChildren, structureElements.size: " + structureElements.size());
        return structureElements;
    }

    public List<StructureElement> readStructuresMnemonic(
            String mnemonic,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // validate input
        //     mnemonic
        // do
        //     read structure latest by mnemonic
        // return
        //     structure elements for structures

        // validate input
        ValidateUtil.validateInputMnemonic(mnemonic);

        // do
        List<StructureElement> structureElements = Lists.newArrayList();
        structureElements.addAll(readStructures(Type.SYSTEMGROUP, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONIC}, new String[] {mnemonic}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.SYSTEM,      new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONIC}, new String[] {mnemonic}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.SUBSYSTEM,   new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONIC}, new String[] {mnemonic}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.DISCIPLINE,  new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONIC}, new String[] {mnemonic}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.DEVICEGROUP, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONIC}, new String[] {mnemonic}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.DEVICETYPE,  new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONIC}, new String[] {mnemonic}, orderBy, isAsc, offset, limit));

        // TODO handle orderBy, isAsc, offset, limit

        return structureElements;
    }

    public List<StructureElement> readStructuresMnemonicpath(
            String mnemonicpath,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // validate input
        //     mnemonicpath
        // do
        //     read structure latest by mnemonicpath
        // return
        //     structure elements for structures

        // validate input
        ValidateUtil.validateInputMnemonic(mnemonicpath);

        // do
        List<StructureElement> structureElements = Lists.newArrayList();
        structureElements.addAll(readStructures(Type.SYSTEMGROUP, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONICPATH}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.SYSTEM,      new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONICPATH}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.SUBSYSTEM,   new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONICPATH}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.DISCIPLINE,  new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONICPATH}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.DEVICEGROUP, new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONICPATH}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit));
        structureElements.addAll(readStructures(Type.DEVICETYPE,  new Status[] {Status.APPROVED}, false, new FieldStructure[] {FieldStructure.MNEMONICPATH}, new String[] {mnemonicpath}, orderBy, isAsc, offset, limit));

        // TODO handle orderBy, isAsc, offset, limit

        return structureElements;
    }

    /**
     * Read history for structure by uuid.
     *
     * @param uuid uuid
     * @param type type
     * @return list of structure elements
     */
    public List<StructureElement> readStructuresHistory(
            String uuid, Type type,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // note
        //     HolderIRepositories and HolderSystemDeviceStructure may or may not be used for preparation of what to return
        //     type may speed up read
        // validate input
        //     uuid
        // do
        //     read history for structure
        // return
        //     structure elements for structures

        // validate input
        ValidateUtil.validateInputUuid(uuid);

        boolean type_systemGroup = type != null && Type.SYSTEMGROUP.equals(type);
        boolean type_system      = type != null && Type.SYSTEM.equals(type);
        boolean type_subsystem   = type != null && Type.SUBSYSTEM.equals(type);
        boolean type_discipline  = type != null && Type.DISCIPLINE.equals(type);
        boolean type_deviceGroup = type != null && Type.DEVICEGROUP.equals(type);
        boolean type_deviceType  = type != null && Type.DEVICETYPE.equals(type);
        boolean type_either      = type_systemGroup || type_system || type_subsystem || type_discipline || type_deviceGroup || type_deviceType;

        // mnemonic path does not make same sense for history
        // (very) tricky to find mnemonic path for uuid at proper time (history)
        // therefore empty mnemonic path for history for structure

        // do
        final List<StructureElement> structureElements = Lists.newArrayList();
        if (type_either) {
            if (type_systemGroup) {
                structureElements.addAll(readStructures(Type.SYSTEMGROUP, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            } else if (type_system) {
                structureElements.addAll(readStructures(Type.SYSTEM, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            } else if (type_subsystem) {
                structureElements.addAll(readStructures(Type.SUBSYSTEM, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            } else if (type_discipline) {
                structureElements.addAll(readStructures(Type.DISCIPLINE, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            } else if (type_deviceGroup) {
                structureElements.addAll(readStructures(Type.DEVICEGROUP, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            } else if (type_deviceType) {
                structureElements.addAll(readStructures(Type.DEVICETYPE, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            }
        } else {
            // go through all structures and see if / where uuid is found

            structureElements.addAll(readStructures(Type.SYSTEMGROUP, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            structureElements.addAll(readStructures(Type.SYSTEM, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            structureElements.addAll(readStructures(Type.SUBSYSTEM, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            structureElements.addAll(readStructures(Type.DISCIPLINE, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            structureElements.addAll(readStructures(Type.DEVICEGROUP, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
            structureElements.addAll(readStructures(Type.DEVICETYPE, null, null, new FieldStructure[] {FieldStructure.UUID}, new String[] {uuid}, Boolean.TRUE, orderBy, isAsc, offset, limit, StructureChoice.HISTORY));
        }

        Collections.sort(structureElements, new Comparator<StructureElement>() {
            @Override
            public int compare(StructureElement e1, StructureElement e2) {
                return e1.getWhen().compareTo(e2.getWhen());
            }
        });

        return structureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    public String equivalenceMnemonic(String mnemonic) {
        // validate input
        // do
        //     exists

        // validate input
        ValidateUtil.validateInputMnemonic(mnemonic);

        // do
        return namingConvention.equivalenceClassRepresentative(mnemonic);
    }

    public Boolean existsStructure(Type type, String mnemonic) {
        // validate input
        // do
        //     exists

        // validate input
        ValidateUtil.validateInputType(type);
        ValidateUtil.validateInputMnemonic(mnemonic);

        // do
        if (Type.SYSTEMGROUP.equals(type)) {
            List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.MNEMONIC, mnemonic);
            return !systemGroups.isEmpty();
        } else if (Type.SYSTEM.equals(type)) {
            List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.MNEMONIC, mnemonic);
            return !systems.isEmpty();
        } else if (Type.SUBSYSTEM.equals(type)) {
            List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.APPROVED, false, FieldStructure.MNEMONIC, mnemonic);
            return !subsystems.isEmpty();
        } else if (Type.DISCIPLINE.equals(type)) {
            List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.MNEMONIC, mnemonic);
            return !disciplines.isEmpty();
        } else if (Type.DEVICEGROUP.equals(type)) {
            List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.APPROVED, false, FieldStructure.MNEMONIC, mnemonic);
            return !deviceGroups.isEmpty();
        } else if (Type.DEVICETYPE.equals(type)) {
            List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.APPROVED, false, FieldStructure.MNEMONIC, mnemonic);
            return !deviceTypes.isEmpty();
        }

        return Boolean.FALSE;
    }

    public Boolean isValidToCreateStructure(Type type, String mnemonicpath) {
        // validate input
        // validate data
        //     not exists

        // validate input
        ValidateUtil.validateInputType(type);
        ValidateUtil.validateInputMnemonicpath(mnemonicpath);

        // initiate holder of containers for system and device structure content, for performance reasons
        //     note false to not include deleted entries
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories, false);

        // validate data
        ValidateUtil.validateStructureDataCreate(type, mnemonicpath, namingConvention, holderIRepositories, holder);

        return Boolean.TRUE;
    }

    // ----------------------------------------------------------------------------------------------------

    public void validateStructuresCreate(StructureElement structureElement) {
        validateStructuresCreate(structureElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateStructuresCreate(StructureElement structureElement, HolderSystemDeviceStructure holder) {
        // validate input
        //     type
        //     structure element
        //         validate input itself
        // validate data
        //     structure element
        //         validate data
        //             itself
        //             relative other data

        // validate input
        ValidateUtil.validateStructureElementInputCreate(structureElement, namingConvention);

        // validate data
        ValidateUtil.validateStructureElementDataCreate(structureElement, namingConvention, holderRepositories, holder);
    }
    public void validateStructuresCreate(List<StructureElement> structureElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (StructureElement structureElement : structureElements) {
            validateStructuresCreate(structureElement, holder);
        }
    }

    public void validateStructuresUpdate(StructureElement structureElement) {
        validateStructuresUpdate(structureElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateStructuresUpdate(StructureElement structureElement, HolderSystemDeviceStructure holder) {
        // validate input
        //     type
        //     structure element
        //         validate input itself
        // validate data
        //     structure element
        //         validate data
        //             itself
        //             relative other data

        // validate input
        ValidateUtil.validateStructureElementInputUpdate(structureElement, namingConvention);

        // validate data
        ValidateUtil.validateStructureElementDataUpdate(structureElement, namingConvention, holderRepositories, holder);
    }
    public void validateStructuresUpdate(List<StructureElement> structureElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (StructureElement structureElement : structureElements) {
            validateStructuresUpdate(structureElement, holder);
        }
    }

    public void validateStructuresDelete(StructureElement structureElement) {
        validateStructuresDelete(structureElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateStructuresDelete(StructureElement structureElement, HolderSystemDeviceStructure holder) {
        // validate input
        //     type
        //     structure element
        //         validate input itself
        // validate data
        //     structure element
        //         validate data
        //             itself
        //             relative other data

        // validate input
        ValidateUtil.validateStructureElementInputDelete(structureElement, namingConvention);

        // validate data
        ValidateUtil.validateStructureElementDataDelete(structureElement, namingConvention, holderRepositories, holder);
    }
    public void validateStructuresDelete(List<StructureElement> structureElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (StructureElement structureElement : structureElements) {
            validateStructuresDelete(structureElement, holder);
        }
    }

    public void validateStructuresApprove(StructureElement structureElement) {
        validateStructuresApprove(structureElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateStructuresApprove(StructureElement structureElement, HolderSystemDeviceStructure holder) {
        // validate input
        //     type
        //     structure element
        //         validate input itself
        // validate data
        //     structure element
        //         validate data
        //             itself
        //             relative other data

        // validate input
        ValidateUtil.validateStructureElementInputApprove(structureElement, namingConvention);

        // validate data
        ValidateUtil.validateStructureElementDataApprove(structureElement, namingConvention, holderRepositories, holder);
    }
    public void validateStructuresApprove(List<StructureElement> structureElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (StructureElement structureElement : structureElements) {
            validateStructuresApprove(structureElement, holder);
        }
    }

    public void validateStructuresCancel(StructureElement structureElement) {
        validateStructuresCancel(structureElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateStructuresCancel(StructureElement structureElement, HolderSystemDeviceStructure holder) {
        // validate input
        //     type
        //     structure element
        //         validate input itself
        // validate data
        //     structure element
        //         validate data
        //             itself
        //             relative other data

        // validate input
        ValidateUtil.validateStructureElementInputCancel(structureElement, namingConvention);

        // validate data
        ValidateUtil.validateStructureElementDataCancel(structureElement, namingConvention, holderRepositories, holder);
    }
    public void validateStructuresCancel(List<StructureElement> structureElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (StructureElement structureElement : structureElements) {
            validateStructuresCancel(structureElement, holder);
        }
    }

    public void validateStructuresReject(StructureElement structureElement) {
        validateStructuresReject(structureElement, new HolderSystemDeviceStructure(holderIRepositories));
    }
    public void validateStructuresReject(StructureElement structureElement, HolderSystemDeviceStructure holder) {
        // validate input
        //     type
        //     structure element
        //         validate input itself
        // validate data
        //     structure element
        //         validate data
        //             itself
        //             relative other data

        // validate input
        ValidateUtil.validateStructureElementInputReject(structureElement, namingConvention);

        // validate data
        ValidateUtil.validateStructureElementDataReject(structureElement, namingConvention, holderRepositories, holder);
    }
    public void validateStructuresReject(List<StructureElement> structureElements) {
        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        for (StructureElement structureElement : structureElements) {
            validateStructuresReject(structureElement, holder);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<StructureElement> updateStructures(List<StructureElement> structureElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         for each structure element
        //             create structure to pending, not latest, not deleted, with data
        //             add structure element for updated
        //     return
        //         structure elements for updated structures

        LOGGER.log(Level.INFO, "updateStructures, structureElements.size:        " + String.valueOf(structureElements != null ? structureElements.size() : "null"));

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // do
        String requestedBy = "test who";
        final List<StructureElement> updatedStructureElements = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // note rules for mnemonic for system group
                String mnemonic = structureElement.getMnemonic();
                mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;

                // create
                SystemGroup systemGroup = new SystemGroup();
                setAttributes(systemGroup,
                        structureElement.getUuid(),
                        structureElement.getName(), mnemonic, !StringUtils.isEmpty(mnemonic) ? namingConvention.equivalenceClassRepresentative(mnemonic) : null,
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSystemGroupRepository().createSystemGroup(systemGroup);

                // possibly validate that created

                // add
                updatedStructureElements.add(StructureElementUtil.getStructureElementRequested(systemGroup, holder));
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // create
                System system = new System();
                setAttributes(system,
                        structureElement.getUuid(), structureElement.getParent(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSystemRepository().createSystem(system);

                // possibly validate that created

                // add
                updatedStructureElements.add(StructureElementUtil.getStructureElementRequested(system, holder));
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // create
                Subsystem subsystem = new Subsystem();
                setAttributes(subsystem,
                        structureElement.getUuid(), structureElement.getParent(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSubsystemRepository().createSubsystem(subsystem);

                // possibly validate that created

                // add
                updatedStructureElements.add(StructureElementUtil.getStructureElementRequested(subsystem, holder));
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                // create
                Discipline discipline = new Discipline();
                setAttributes(discipline,
                        structureElement.getUuid(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDisciplineRepository().createDiscipline(discipline);

                // possibly validate that created

                // add
                updatedStructureElements.add(StructureElementUtil.getStructureElementRequested(discipline, holder));
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // note rules for mnemonic for device group
                String mnemonic = structureElement.getMnemonic();
                mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;

                // create
                DeviceGroup deviceGroup = new DeviceGroup();
                setAttributes(deviceGroup,
                        structureElement.getUuid(), structureElement.getParent(),
                        structureElement.getName(), mnemonic, !StringUtils.isEmpty(mnemonic) ? namingConvention.equivalenceClassRepresentative(mnemonic) : null,
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDeviceGroupRepository().createDeviceGroup(deviceGroup);

                // possibly validate that created

                // add
                updatedStructureElements.add(StructureElementUtil.getStructureElementRequested(deviceGroup, holder));
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                // create
                DeviceType deviceType = new DeviceType();
                setAttributes(deviceType,
                        structureElement.getUuid(), structureElement.getParent(),
                        structureElement.getName(), structureElement.getMnemonic(), namingConvention.equivalenceClassRepresentative(structureElement.getMnemonic()),
                        structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDeviceTypeRepository().createDeviceType(deviceType);

                // possibly validate that created

                // add
                updatedStructureElements.add(StructureElementUtil.getStructureElementRequested(deviceType, holder));
            }
        }

        LOGGER.log(Level.INFO, "updateStructures, updatedStructureElements.size: " + updatedStructureElements.size());
        return updatedStructureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<StructureElement> deleteStructures(List<StructureElement> structureElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         for each structure element
        //             find
        //             create structure to pending, not latest, deleted, with data
        //             add structure element for deleted
        //     return
        //         structure elements for deleted structures
        // TODO continue ---> validation error

        LOGGER.log(Level.INFO, "deleteStructures, structureElements.size:        " + String.valueOf(structureElements != null ? structureElements.size() : "null"));

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // do
        String requestedBy = "test who";
        final List<StructureElement> deletedStructureElements = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // find
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systemGroups == null || systemGroups.size() != 1) {
                    continue;
                }
                SystemGroup toBeDeleted = systemGroups.get(0);

                // create
                SystemGroup systemGroup = new SystemGroup();
                setAttributes(systemGroup,
                        toBeDeleted.getUuid(),
                        toBeDeleted.getName(), toBeDeleted.getMnemonic(), namingConvention.equivalenceClassRepresentative(toBeDeleted.getMnemonic()),
                        toBeDeleted.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSystemGroupRepository().createSystemGroup(systemGroup);

                // possibly validate that created

                // add
                deletedStructureElements.add(StructureElementUtil.getStructureElementRequested(systemGroup, holder));
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // find
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systems == null || systems.size() != 1) {
                    continue;
                }
                System toBeDeleted = systems.get(0);

                // create
                System system = new System();
                setAttributes(system,
                        toBeDeleted.getUuid(), toBeDeleted.getParentUuid(),
                        toBeDeleted.getName(), toBeDeleted.getMnemonic(), namingConvention.equivalenceClassRepresentative(toBeDeleted.getMnemonic()),
                        toBeDeleted.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSystemRepository().createSystem(system);

                // possibly validate that created

                // add
                deletedStructureElements.add(StructureElementUtil.getStructureElementRequested(system, holder));
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // find
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                if (subsystems == null || subsystems.size() != 1) {
                    continue;
                }
                Subsystem toBeDeleted = subsystems.get(0);

                // create
                Subsystem subsystem = new Subsystem();
                setAttributes(subsystem,
                        toBeDeleted.getUuid(), toBeDeleted.getParentUuid(),
                        toBeDeleted.getName(), toBeDeleted.getMnemonic(), namingConvention.equivalenceClassRepresentative(toBeDeleted.getMnemonic()),
                        toBeDeleted.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getSubsystemRepository().createSubsystem(subsystem);

                // possibly validate that created

                // add
                deletedStructureElements.add(StructureElementUtil.getStructureElementRequested(subsystem, holder));
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                // find
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                if (disciplines == null || disciplines.size() != 1) {
                    continue;
                }
                Discipline toBeDeleted = disciplines.get(0);

                // create
                Discipline discipline = new Discipline();
                setAttributes(discipline,
                        toBeDeleted.getUuid(),
                        toBeDeleted.getName(), toBeDeleted.getMnemonic(), namingConvention.equivalenceClassRepresentative(toBeDeleted.getMnemonic()),
                        toBeDeleted.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDisciplineRepository().createDiscipline(discipline);

                // possibly validate that created

                // add
                deletedStructureElements.add(StructureElementUtil.getStructureElementRequested(discipline, holder));
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // find
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceGroups == null || deviceGroups.size() != 1) {
                    continue;
                }
                DeviceGroup toBeDeleted = deviceGroups.get(0);

                // create
                DeviceGroup deviceGroup = new DeviceGroup();
                setAttributes(deviceGroup,
                        toBeDeleted.getUuid(), toBeDeleted.getParentUuid(),
                        toBeDeleted.getName(), toBeDeleted.getMnemonic(), namingConvention.equivalenceClassRepresentative(toBeDeleted.getMnemonic()),
                        toBeDeleted.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDeviceGroupRepository().createDeviceGroup(deviceGroup);

                // possibly validate that created

                // add
                deletedStructureElements.add(StructureElementUtil.getStructureElementRequested(deviceGroup, holder));
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                // find
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.APPROVED, false, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceTypes == null || deviceTypes.size() != 1) {
                    continue;
                }
                DeviceType toBeDeleted = deviceTypes.get(0);

                // create
                DeviceType deviceType = new DeviceType();
                setAttributes(deviceType,
                        toBeDeleted.getUuid(), toBeDeleted.getParentUuid(),
                        toBeDeleted.getName(), toBeDeleted.getMnemonic(), namingConvention.equivalenceClassRepresentative(toBeDeleted.getMnemonic()),
                        toBeDeleted.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                        new Date(), requestedBy, structureElement.getComment());

                holderRepositories.getDeviceTypeRepository().createDeviceType(deviceType);

                // possibly validate that created

                // add
                deletedStructureElements.add(StructureElementUtil.getStructureElementRequested(deviceType, holder));
            }
        }

        LOGGER.log(Level.INFO, "deleteStructures, deletedStructureElements.size: " + deletedStructureElements.size());
        return deletedStructureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<StructureElement> approveStructures(List<StructureElement> structureElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         update structure to status APPROVED
        //     return
        //         updated structure element
        // TODO continue ---> validation error

        LOGGER.log(Level.INFO, "approveStructures, structureElements.size:         " + String.valueOf(structureElements != null ? structureElements.size() : "null"));

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // approve
        //     set not latest for current latest
        //     set approved, latest for pending
        //     possibly
        //         if deleted, names that belong to structure may be deleted, otherwise they are alive but become legacy names - legacy way for now
        //         TODO if not deleted, rename names that belong to structure if different mnemonic
        //            if that way, then compare what's about to be approved with what is latest

        // do
        //     update
        //         set not latest for current latest
        //         set approved, latest for pending
        String processedBy = "test who";
        String processedComment = "test comment";
        final List<StructureElement> approvedStructureElements = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // find
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.APPROVED, Boolean.FALSE, FieldStructure.UUID, structureElement.getUuid().toString());
                SystemGroup systemGroup = null;
                if (systemGroups != null && systemGroups.size() == 1) {
                    systemGroup = systemGroups.get(0);

                    // update not latest
                    systemGroup.setLatest(Boolean.FALSE);
                    holderRepositories.getSystemGroupRepository().updateSystemGroup(systemGroup);
                }

                // find
                systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systemGroups == null || systemGroups.size() != 1) {
                    continue;
                }
                systemGroup = systemGroups.get(0);

                // approve
                setAttributesStatusProcessed(systemGroup, Status.APPROVED, new Date(), processedBy, processedComment);
                systemGroup.setLatest(Boolean.TRUE);
                holderRepositories.getSystemGroupRepository().updateSystemGroup(systemGroup);

                // possibly validate that approved

                // add
                approvedStructureElements.add(StructureElementUtil.getStructureElementProcessed(systemGroup, holder));
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // find
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.APPROVED, Boolean.FALSE, FieldStructure.UUID, structureElement.getUuid().toString());
                System system = null;
                if (systems != null && systems.size() == 1) {
                    system = systems.get(0);

                    // update not latest
                    system.setLatest(Boolean.FALSE);
                    holderRepositories.getSystemRepository().updateSystem(system);
                }

                // find
                systems = holderRepositories.getSystemRepository().readSystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systems == null || systems.size() != 1) {
                    continue;
                }
                system = systems.get(0);

                // approve
                setAttributesStatusProcessed(system, Status.APPROVED, new Date(), processedBy, processedComment);
                system.setLatest(Boolean.TRUE);
                holderRepositories.getSystemRepository().updateSystem(system);

                // possibly validate that approved

                // add
                approvedStructureElements.add(StructureElementUtil.getStructureElementProcessed(system, holder));
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // find
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.APPROVED, Boolean.FALSE, FieldStructure.UUID, structureElement.getUuid().toString());
                Subsystem subsystem = null;
                if (subsystems != null && subsystems.size() == 1) {
                    subsystem = subsystems.get(0);

                    // update not latest
                    subsystem.setLatest(Boolean.FALSE);
                    holderRepositories.getSubsystemRepository().updateSubsystem(subsystem);
                }

                // find
                subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (subsystems == null || subsystems.size() != 1) {
                    continue;
                }
                subsystem = subsystems.get(0);

                // approve
                setAttributesStatusProcessed(subsystem, Status.APPROVED, new Date(), processedBy, processedComment);
                subsystem.setLatest(Boolean.TRUE);
                holderRepositories.getSubsystemRepository().updateSubsystem(subsystem);

                // possibly validate that approved

                // add
                approvedStructureElements.add(StructureElementUtil.getStructureElementProcessed(subsystem, holder));
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.APPROVED, Boolean.FALSE, FieldStructure.UUID, structureElement.getUuid().toString());
                Discipline discipline = null;
                if (disciplines != null && disciplines.size() == 1) {
                    discipline = disciplines.get(0);

                    // update not latest
                    discipline.setLatest(Boolean.FALSE);
                    holderRepositories.getDisciplineRepository().updateDiscipline(discipline);
                }

                // find
                disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (disciplines == null || disciplines.size() != 1) {
                    continue;
                }
                discipline = disciplines.get(0);

                // approve
                setAttributesStatusProcessed(discipline, Status.APPROVED, new Date(), processedBy, processedComment);
                discipline.setLatest(Boolean.TRUE);
                holderRepositories.getDisciplineRepository().updateDiscipline(discipline);

                // possibly validate that approved

                // add
                approvedStructureElements.add(StructureElementUtil.getStructureElementProcessed(discipline, holder));
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // find
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.APPROVED, Boolean.FALSE, FieldStructure.UUID, structureElement.getUuid().toString());
                DeviceGroup deviceGroup = null;
                if (deviceGroups != null && deviceGroups.size() == 1) {
                    deviceGroup = deviceGroups.get(0);

                    // update not latest
                    deviceGroup.setLatest(Boolean.FALSE);
                    holderRepositories.getDeviceGroupRepository().updateDeviceGroup(deviceGroup);
                }

                // find
                deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceGroups == null || deviceGroups.size() != 1) {
                    continue;
                }
                deviceGroup = deviceGroups.get(0);

                // approve
                setAttributesStatusProcessed(deviceGroup, Status.APPROVED, new Date(), processedBy, processedComment);
                deviceGroup.setLatest(Boolean.TRUE);
                holderRepositories.getDeviceGroupRepository().updateDeviceGroup(deviceGroup);

                // possibly validate that approved

                // add
                approvedStructureElements.add(StructureElementUtil.getStructureElementProcessed(deviceGroup, holder));
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                // find
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.APPROVED, Boolean.FALSE, FieldStructure.UUID, structureElement.getUuid().toString());
                DeviceType deviceType = null;
                if (deviceTypes != null && deviceTypes.size() == 1) {
                    deviceType = deviceTypes.get(0);

                    // update not latest
                    deviceType.setLatest(Boolean.FALSE);
                    holderRepositories.getDeviceTypeRepository().updateDeviceType(deviceType);
                }

                // find
                deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceTypes == null || deviceTypes.size() != 1) {
                    continue;
                }
                deviceType = deviceTypes.get(0);

                // approve
                setAttributesStatusProcessed(deviceType, Status.APPROVED, new Date(), processedBy, processedComment);
                deviceType.setLatest(Boolean.TRUE);
                holderRepositories.getDeviceTypeRepository().updateDeviceType(deviceType);

                // possibly validate that approved

                // add
                approvedStructureElements.add(StructureElementUtil.getStructureElementProcessed(deviceType, holder));
            }
        }

        LOGGER.log(Level.INFO, "approveStructures, approvedStructureElements.size: " + approvedStructureElements.size());
        return approvedStructureElements;
    }

    @Transactional
    public List<StructureElement> cancelStructures(List<StructureElement> structureElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         update structure to status CANCELLED
        //     return
        //         updated structure element
        // TODO continue ---> validation error

        LOGGER.log(Level.INFO, "cancelStructures, structureElements.size:          " + String.valueOf(structureElements != null ? structureElements.size() : "null"));

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // do
        String processedBy = "test who";
        String processedComment = "test comment";
        final List<StructureElement> cancelledStructureElements = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // find
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systemGroups == null || systemGroups.size() != 1) {
                    continue;
                }
                SystemGroup systemGroup = systemGroups.get(0);

                // cancel
                setAttributesStatusProcessed(systemGroup, Status.CANCELLED, new Date(), processedBy, processedComment);
                holderRepositories.getSystemGroupRepository().updateSystemGroup(systemGroup);

                // possibly validate that cancelled

                // add
                cancelledStructureElements.add(StructureElementUtil.getStructureElementProcessed(systemGroup, holder));
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // find
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systems == null || systems.size() != 1) {
                    continue;
                }
                System system = systems.get(0);

                // cancel
                setAttributesStatusProcessed(system, Status.CANCELLED, new Date(), processedBy, processedComment);
                holderRepositories.getSystemRepository().updateSystem(system);

                // possibly validate that cancelled

                // add
                cancelledStructureElements.add(StructureElementUtil.getStructureElementProcessed(system, holder));
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // find
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (subsystems == null || subsystems.size() != 1) {
                    continue;
                }
                Subsystem subsystem = subsystems.get(0);

                // cancel
                setAttributesStatusProcessed(subsystem, Status.CANCELLED, new Date(), processedBy, processedComment);
                holderRepositories.getSubsystemRepository().updateSubsystem(subsystem);

                // possibly validate that cancelled

                // add
                cancelledStructureElements.add(StructureElementUtil.getStructureElementProcessed(subsystem, holder));
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                // find
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (disciplines == null || disciplines.size() != 1) {
                    continue;
                }
                Discipline discipline = disciplines.get(0);

                // cancel
                setAttributesStatusProcessed(discipline, Status.CANCELLED, new Date(), processedBy, processedComment);
                holderRepositories.getDisciplineRepository().updateDiscipline(discipline);

                // possibly validate that cancelled

                // add
                cancelledStructureElements.add(StructureElementUtil.getStructureElementProcessed(discipline, holder));
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // find
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceGroups == null || deviceGroups.size() != 1) {
                    continue;
                }
                DeviceGroup deviceGroup = deviceGroups.get(0);

                // cancel
                setAttributesStatusProcessed(deviceGroup, Status.CANCELLED, new Date(), processedBy, processedComment);
                holderRepositories.getDeviceGroupRepository().updateDeviceGroup(deviceGroup);

                // possibly validate that cancelled

                // add
                cancelledStructureElements.add(StructureElementUtil.getStructureElementProcessed(deviceGroup, holder));
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceTypes == null || deviceTypes.size() != 1) {
                    continue;
                }
                DeviceType deviceType = deviceTypes.get(0);

                // cancel
                setAttributesStatusProcessed(deviceType, Status.CANCELLED, new Date(), processedBy, processedComment);
                holderRepositories.getDeviceTypeRepository().updateDeviceType(deviceType);

                // possibly validate that cancelled

                // add
                cancelledStructureElements.add(StructureElementUtil.getStructureElementProcessed(deviceType, holder));
            }
        }

        LOGGER.log(Level.INFO, "cancelStructures, cancelledStructureElements.size: " + cancelledStructureElements.size());
        return cancelledStructureElements;
    }

    @Transactional
    public List<StructureElement> rejectStructures(List<StructureElement> structureElements) {
        // validate
        //     outside of @Transactional
        // transaction
        //     do
        //         update structure to status REJECTED
        //     return
        //         updated structure element
        // TODO continue ---> validation error

        LOGGER.log(Level.INFO, "rejectStructures, structureElements.size:         " + String.valueOf(structureElements != null ? structureElements.size() : "null"));

        // initiate holder of containers for system and device structure content, for performance reasons
        HolderSystemDeviceStructure holder = new HolderSystemDeviceStructure(holderIRepositories);

        // do
        String processedBy = "test who";
        String processedComment = "test comment";
        final List<StructureElement> rejectedStructureElements = Lists.newArrayList();
        for (StructureElement structureElement : structureElements) {
            if (Type.SYSTEMGROUP.equals(structureElement.getType())) {
                // find
                List<SystemGroup> systemGroups = holderRepositories.getSystemGroupRepository().readSystemGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systemGroups == null || systemGroups.size() != 1) {
                    continue;
                }
                SystemGroup systemGroup = systemGroups.get(0);

                // reject
                setAttributesStatusProcessed(systemGroup, Status.REJECTED, new Date(), processedBy, processedComment);
                holderRepositories.getSystemGroupRepository().updateSystemGroup(systemGroup);

                // possibly validate that rejected

                // add
                rejectedStructureElements.add(StructureElementUtil.getStructureElementProcessed(systemGroup, holder));
            } else if (Type.SYSTEM.equals(structureElement.getType())) {
                // find
                List<System> systems = holderRepositories.getSystemRepository().readSystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (systems == null || systems.size() != 1) {
                    continue;
                }
                System system = systems.get(0);

                // reject
                setAttributesStatusProcessed(system, Status.REJECTED, new Date(), processedBy, processedComment);
                holderRepositories.getSystemRepository().updateSystem(system);

                // possibly validate that rejected

                // add
                rejectedStructureElements.add(StructureElementUtil.getStructureElementProcessed(system, holder));
            } else if (Type.SUBSYSTEM.equals(structureElement.getType())) {
                // find
                List<Subsystem> subsystems = holderRepositories.getSubsystemRepository().readSubsystems(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (subsystems == null || subsystems.size() != 1) {
                    continue;
                }
                Subsystem subsystem = subsystems.get(0);

                // reject
                setAttributesStatusProcessed(subsystem, Status.REJECTED, new Date(), processedBy, processedComment);
                holderRepositories.getSubsystemRepository().updateSubsystem(subsystem);

                // possibly validate that rejected

                // add
                rejectedStructureElements.add(StructureElementUtil.getStructureElementProcessed(subsystem, holder));
            } else if (Type.DISCIPLINE.equals(structureElement.getType())) {
                // find
                List<Discipline> disciplines = holderRepositories.getDisciplineRepository().readDisciplines(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (disciplines == null || disciplines.size() != 1) {
                    continue;
                }
                Discipline discipline = disciplines.get(0);

                // reject
                setAttributesStatusProcessed(discipline, Status.REJECTED, new Date(), processedBy, processedComment);
                holderRepositories.getDisciplineRepository().updateDiscipline(discipline);

                // possibly validate that rejected

                // add
                rejectedStructureElements.add(StructureElementUtil.getStructureElementProcessed(discipline, holder));
            } else if (Type.DEVICEGROUP.equals(structureElement.getType())) {
                // find
                List<DeviceGroup> deviceGroups = holderRepositories.getDeviceGroupRepository().readDeviceGroups(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceGroups == null || deviceGroups.size() != 1) {
                    continue;
                }
                DeviceGroup deviceGroup = deviceGroups.get(0);

                // reject
                setAttributesStatusProcessed(deviceGroup, Status.REJECTED, new Date(), processedBy, processedComment);
                holderRepositories.getDeviceGroupRepository().updateDeviceGroup(deviceGroup);

                // possibly validate that rejected

                // add
                rejectedStructureElements.add(StructureElementUtil.getStructureElementProcessed(deviceGroup, holder));
            } else if (Type.DEVICETYPE.equals(structureElement.getType())) {
                List<DeviceType> deviceTypes = holderRepositories.getDeviceTypeRepository().readDeviceTypes(Status.PENDING, null, FieldStructure.UUID, structureElement.getUuid().toString());
                if (deviceTypes == null || deviceTypes.size() != 1) {
                    continue;
                }
                DeviceType deviceType = deviceTypes.get(0);

                // reject
                setAttributesStatusProcessed(deviceType, Status.REJECTED, new Date(), processedBy, processedComment);
                holderRepositories.getDeviceTypeRepository().updateDeviceType(deviceType);

                // possibly validate that rejected

                // add
                rejectedStructureElements.add(StructureElementUtil.getStructureElementProcessed(deviceType, holder));
            }
        }

        LOGGER.log(Level.INFO, "rejectStructures, rejectedStructureElements.size: " + rejectedStructureElements.size());
        return rejectedStructureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to help set attributes for Structure class, which in practice is either of its sub classes.
     *
     * @param structure structure
     * @param status status
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     */
    private void setAttributesStatusProcessed(Structure structure, Status status, Date processed, String processedBy, String processedComment) {
        structure.setStatus(status);
        structure.setProcessed(processed);
        structure.setProcessedBy(processedBy);
        structure.setProcessedComment(processedComment);
    }

    private void setAttributes(SystemGroup systemGroup, UUID uuid, String name, String mnemonic, String mnemonicEquivalence, String description, Status status, Boolean isLatest, Boolean isDeleted, Date requested, String requestedBy, String requestedComment) {
        setAttributesStructure(systemGroup, uuid, name, mnemonic, mnemonicEquivalence, description, status, isLatest, isDeleted, requested, requestedBy, requestedComment);
    }
    private void setAttributes(System system, UUID uuid, UUID parentUuid, String name, String mnemonic, String mnemonicEquivalence, String description, Status status, Boolean isLatest, Boolean isDeleted, Date requested, String requestedBy, String requestedComment) {
        system.setParentUuid(parentUuid);
        setAttributesStructure(system, uuid, name, mnemonic, mnemonicEquivalence, description, status, isLatest, isDeleted, requested, requestedBy, requestedComment);
    }
    private void setAttributes(Subsystem subsystem, UUID uuid, UUID parentUuid, String name, String mnemonic, String mnemonicEquivalence, String description, Status status, Boolean isLatest, Boolean isDeleted, Date requested, String requestedBy, String requestedComment) {
        subsystem.setParentUuid(parentUuid);
        setAttributesStructure(subsystem, uuid, name, mnemonic, mnemonicEquivalence, description, status, isLatest, isDeleted, requested, requestedBy, requestedComment);
    }
    private void setAttributes(Discipline discipline, UUID uuid, String name, String mnemonic, String mnemonicEquivalence, String description, Status status, Boolean isLatest, Boolean isDeleted, Date requested, String requestedBy, String requestedComment) {
        setAttributesStructure(discipline, uuid, name, mnemonic, mnemonicEquivalence, description, status, isLatest, isDeleted, requested, requestedBy, requestedComment);
    }
    private void setAttributes(DeviceGroup deviceGroup, UUID uuid, UUID parentUuid, String name, String mnemonic, String mnemonicEquivalence, String description, Status status, Boolean isLatest, Boolean isDeleted, Date requested, String requestedBy, String requestedComment) {
        deviceGroup.setParentUuid(parentUuid);
        setAttributesStructure(deviceGroup, uuid, name, mnemonic, mnemonicEquivalence, description, status, isLatest, isDeleted, requested, requestedBy, requestedComment);
    }
    private void setAttributes(DeviceType deviceType, UUID uuid, UUID parentUuid, String name, String mnemonic, String mnemonicEquivalence, String description, Status status, Boolean isLatest, Boolean isDeleted, Date requested, String requestedBy, String requestedComment) {
        deviceType.setParentUuid(parentUuid);
        setAttributesStructure(deviceType, uuid, name, mnemonic, mnemonicEquivalence, description, status, isLatest, isDeleted, requested, requestedBy, requestedComment);
    }

    /**
     * Utility method to help set attributes for Structure class, which in practice is either of its sub classes.
     *
     * @param structure structure
     * @param uuid uuid
     * @param name name
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     */
    private void setAttributesStructure(Structure structure,
            UUID uuid,
            String name, String mnemonic, String mnemonicEquivalence,
            String description, Status status, Boolean latest, Boolean deleted,
            Date requested, String requestedBy, String requestedComment) {
        structure.setUuid(uuid);
        structure.setName(name);
        structure.setMnemonic(mnemonic);
        structure.setMnemonicEquivalence(mnemonicEquivalence);
        structure.setDescription(description);
        structure.setStatus(status);
        structure.setLatest(latest);
        structure.setDeleted(deleted);
        structure.setRequested(requested);
        structure.setRequestedBy(requestedBy);
        structure.setRequestedComment(requestedComment);
    }

}
