/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.docker.ITUtil.AuthorizationChoice;
import org.openepics.names.docker.ITUtil.EndpointChoice;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose.yml / Dockerfile.
 *
 * <p>
 * Focus of this class is structures endpoint and device type.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
public class StructuresDeviceTypeIT {

    // note
    //     StructureElement - when, who, comment - depend on requested or processed
    //         requested, requested by, requested comment
    //         processed, processed by, processed comment
    //     if less than a second between requested and processed, then considered one entry with processed
    //     history
    //         mnemonic path does not make same sense for history
    //         (very) tricky to find mnemonic path for uuid at proper time (history)
    //         therefore empty mnemonic path for history for structure
    //         one history entry if less than one second between requested and processed, otherwise two history entries
    //     attributes for entry for operations - create, update, delete, approve, cancel, reject
    //         some set client side, others set server side
    //         client side
    //             type, uuid, parent uuid, name, mnemonic, description, comment
    //         may be set client side for test purposes

    @Container
    public static final DockerComposeContainer<?> ENVIRONMENT =
        new DockerComposeContainer<>(new File("docker-compose-it-db-schema-migration.yml"))
            .waitingFor(ITUtil.NAMING, Wait.forLogMessage(".*Started NamingApplication.*", 1));

    private static UUID disciplineUuid = null;
    private static UUID deviceGroupUuid = null;

    @BeforeAll
    public static void initAll() {
        // init discipline, device group

        StructureElement structureElement         = null;
        StructureElement approvedStructureElement = null;

        structureElement = new StructureElement(
              Type.DISCIPLINE, null, null,
              "name", "Di", "Di", 1,
              "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
              null, "test who", "test comment");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineUuid = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineUuid,
                "name", null, "Di", 2,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupUuid = approvedStructureElement.getUuid();
    }

    @Test
    public void checkCreate() {
        // purpose
        //     test conditions for create device type
        //         not create itself
        //
        // what - combination of
        //     read        exists in structure
        //     read        is valid to create
        //     read        validate create

        try {
            ObjectMapper mapper = new ObjectMapper();
            StructureElement structureElement = new StructureElement();
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/exists/DEVICETYPE/Cc");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/isvalidtocreate/DEVICETYPE/Cc");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/isvalidtocreate/DEVICETYPE/Db-Cc");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/isvalidtocreate/DEVICETYPE/Di-Cc");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[{asdf]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[{asdf]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setType(Type.DEVICETYPE);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setParent(deviceGroupUuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setName("name");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setMnemonic("Cc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setType(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkCreateMnemonic() {
        // purpose
        //     test conditions for create device type
        //         not create itself
        //
        // what - combination of
        //     read        validate create
        //
        // note
        //     mnemonic
        try {
            ObjectMapper mapper = new ObjectMapper();
            StructureElement structureElement = new StructureElement();
            String[] response = null;

            structureElement.setType(Type.DEVICETYPE);
            structureElement.setParent(deviceGroupUuid);
            structureElement.setName("name");
            structureElement.setDescription("description");
            structureElement.setComment("comment");

            // mnemonic rules

            structureElement.setMnemonic(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("C");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Cc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Ccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Cccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Ccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Cccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Ccccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Cccccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Ccccccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // mnemonic rules (2)

            structureElement.setMnemonic(" ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dev ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dev");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("000");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Dev0");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic(":");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dev:");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dev:   ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("1");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("12");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("123");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("1234");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("12345");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("123456");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("1234567");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("12345678");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("123456789");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
	    } catch (IOException e) {
	        fail();
	    } catch (InterruptedException e) {
	        fail();
	    } catch (Exception e) {
	        fail();
	    }
	}

    @Test
    public void createApprove() {
        // purpose
        //     test create and approve system
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create in order to approve

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement createdStructureElement  = null;
            StructureElement approvedStructureElement = null;

            structureElement = new StructureElement(
                  Type.DEVICETYPE, null, deviceGroupUuid,
                  "name", "Ca", "Di-Ca", 3,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(approvedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void createCancel() {
        // purpose
        //     test create and cancel system
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate cancel
        //     patch       cancel structures
        //
        // note
        //     create in order to cancel

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement          = null;
            StructureElement createdStructureElement   = null;
            StructureElement cancelledStructureElement = null;

            structureElement = new StructureElement(
                  Type.DEVICETYPE, null, deviceGroupUuid,
                  "name", "Cc", "Di-Cc", 3,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(cancelledStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void createReject() {
        // purpose
        //     test create and reject system
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate reject
        //     patch       reject structures
        //
        // note
        //     create in order to reject

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement createdStructureElement  = null;
            StructureElement rejectedStructureElement = null;

            structureElement = new StructureElement(
                  Type.DEVICETYPE, null, deviceGroupUuid,
                  "name", "Cr", "Di-Cr", 3,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(rejectedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkUpdate() {
        // purpose
        //     test conditions for update device type
        //         not update
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create, approve in order to update

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement structureElement         = new StructureElement();
            StructureElement approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Cu", "Di-Cu", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;
            uuid = approvedStructureElement.getUuid();

            // validate update

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setType(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.SUBSYSTEM);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.DEVICETYPE);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setUuid(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setUuid(uuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setParent(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setParent(deviceGroupUuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setName(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setName("name");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Cu");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setDescription(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setComment(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void updateApprove() {
        // purpose
        //     test update and approve device type
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     update      update structures
        //     patch       approve structures
        //
        // note
        //     create, approve in order to update, approve

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement updatedStructureElement  = null;
            StructureElement approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Ua", "Di-Ua", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update approve check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // update
            updatedStructureElement = ITUtilNameStructureElement.assertUpdate(structureElement);
            structureElement = updatedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(approvedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void updateCancel() {
        // purpose
        //     test update and cancel device type
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     read        validate cancel
        //     update      update structures
        //     patch       approve structures
        //     patch       cancel structures
        //
        // note
        //     create, approve in order to update, cancel

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement          = null;
            StructureElement updatedStructureElement   = null;
            StructureElement approvedStructureElement  = null;
            StructureElement cancelledStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Uc", "Di-Uc", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update cancel check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // update
            updatedStructureElement = ITUtilNameStructureElement.assertUpdate(structureElement);
            structureElement = updatedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(cancelledStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void updateReject() {
        // purpose
        //     test update and reject device type
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     read        validate reject
        //     update      update structures
        //     patch       approve structures
        //     patch       reject structures
        //
        // note
        //     create, approve in order to update, reject

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement updatedStructureElement  = null;
            StructureElement approvedStructureElement = null;
            StructureElement rejectedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Ur", "Di-Ur", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update reject check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // update
            updatedStructureElement = ITUtilNameStructureElement.assertUpdate(structureElement);
            structureElement = updatedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(rejectedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkDelete() {
        // purpose
        //     test conditions for delete device type
        //         not delete
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create, approve in order to delete

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement   structureElement         = new StructureElement();
            StructureElement   approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Cd", "Di-Cd", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;
            uuid = approvedStructureElement.getUuid();

            // validate delete

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setType(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.SUBSYSTEM);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.DEVICETYPE);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setUuid(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setUuid(uuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setParent(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setParent(deviceGroupUuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setName(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setName("name");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Cd");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setDescription(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setComment(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void deleteApprove() {
        // purpose
        //     test delete and approve device type
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     delete      delete structures
        //     patch       approve structures
        //
        // note
        //     create, approve in order to delete, approve

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement deletedStructureElement  = null;
            StructureElement approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Da", "Di-Da", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment delete approve check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // delete
            structureElement.setDeleted(Boolean.TRUE);
            deletedStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);
            structureElement = deletedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(approvedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void deleteCancel() {
        // purpose
        //     test delete and cancel device type
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     read        validate cancel
        //     update      delete structures
        //     patch       approve structures
        //     patch       cancel structures
        //
        // note
        //     create, approve in order to delete, cancel

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement          = null;
            StructureElement deletedStructureElement   = null;
            StructureElement approvedStructureElement  = null;
            StructureElement cancelledStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Dc", "Di-Dc", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update delete check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // delete
            deletedStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);
            structureElement = deletedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(cancelledStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void deleteReject() {
        // purpose
        //     test delete and reject device type
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     read        validate reject
        //     update      delete structures
        //     patch       approve structures
        //     patch       reject structures
        //
        // note
        //     create, approve in order to delete, reject

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement deletedStructureElement  = null;
            StructureElement approvedStructureElement = null;
            StructureElement rejectedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DEVICETYPE, null, deviceGroupUuid,
                    "name", "Dr", "Di-Dr", 3,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment delete reject check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // delete
            deletedStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);
            structureElement = deletedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(rejectedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryApprove() {
        // purpose
        //     test read system group in various ways for create, approve
        //         search
        //         latest
        //         history
        //
        // note
        //     create (and more) to read (with content)

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement   structureElement         = null;
            StructureElement   createdStructureElement  = null;
            StructureElement[] readStructureElements    = null;
            StructureElement   approvedStructureElement = null;
            int length = 0;

            structureElement = new StructureElement(
                  Type.DEVICETYPE, null, deviceGroupUuid,
                  "name", "Rsha", "Di-Rsha", 3,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            // read (1)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            length = readStructureElements.length;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=UUID&queryValues=" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Di-Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            // read (2)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            assertEquals(length, readStructureElements.length);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=UUID&queryValues=" + approvedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Di-Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + approvedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryCancel() {
        // purpose
        //     test read system group in various ways for create, cancel
        //         search
        //         latest
        //         history
        //
        // note
        //     create (and more) to read (with content)

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement   structureElement          = null;
            StructureElement   createdStructureElement   = null;
            StructureElement[] readStructureElements     = null;
            StructureElement   cancelledStructureElement = null;
            int length = 0;

            structureElement = new StructureElement(
                  Type.DEVICETYPE, null, deviceGroupUuid,
                  "name", "Rshc", "Di-Rshc", 3,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            // read (1)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            length = readStructureElements.length;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=UUID&queryValues=" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Di-Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            // read (2)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            assertEquals(length, readStructureElements.length);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=UUID&queryValues=" + cancelledStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Di-Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + cancelledStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryReject() {
        // purpose
        //     test read system group in various ways for create, reject
        //         search
        //         latest
        //         history
        //
        // note
        //     create (and more) to read (with content)

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement   structureElement         = null;
            StructureElement   createdStructureElement  = null;
            StructureElement[] readStructureElements    = null;
            StructureElement   rejectedStructureElement = null;
            int length = 0;

            structureElement = new StructureElement(
                  Type.DEVICETYPE, null, deviceGroupUuid,
                  "name", "Rshr", "Di-Rshr", 3,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            // read (1)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            length = readStructureElements.length;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=UUID&queryValues=" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLengthOne(readStructureElements);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Di-Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            // read (2)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            assertEquals(length, readStructureElements.length);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=UUID&queryValues=" + rejectedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Di-Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + rejectedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchStatusDeletedChildren() {
        // purpose
        //     test read system group in various ways
        //         status
        //       ( latest )
        //         deleted
        //         children
        //
        // what
        //     entries with different statuses
        //
        // note
        //     create (and more) to read (with content)
        //     querying for Status.APPROVED means latest approved

        StructureElement structureElement         = null;
        StructureElement responseStructureElement = null;
        UUID uuid = null;

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AA1", "Di-AA1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        uuid = responseStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AA2", "Di-AA2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AA3", "Di-AA3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AA4", "Di-AA4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AA5", "Di-AA5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AB1", "Di-AB1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AB2", "Di-AB2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AB3", "Di-AB3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AB4", "Di-AB4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AB5", "Di-AB5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AC1", "Di-AC1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AC2", "Di-AC2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AC3", "Di-AC3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AC4", "Di-AC4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AC5", "Di-AC5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AD1", "Di-AD1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AD2", "Di-AD2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AD3", "Di-AD3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AD4", "Di-AD4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AD5", "Di-AD5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        String description2 = "some other description";
        String comment2 = "some other comment";
        String description3 = "more description";
        String comment3 = "more comment";

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AE1", "Di-AE1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AE2", "Di-AE2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AE3", "Di-AE3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AE4", "Di-AE4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AE5", "Di-AE5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AF1", "Di-AF1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AF2", "Di-AF2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AF3", "Di-AF3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AF4", "Di-AF4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AF5", "Di-AF5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AG1", "Di-AG1", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AG2", "Di-AG2", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AG3", "Di-AG3", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AG4", "Di-AG4", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupUuid,
                "name", "AG5", "Di-AG5", 3,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        // 60 device type entries

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement[] readStructureElements = null;

            // from first structure element
            assertNotNull(uuid);

            // 45, not 60
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 45);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 10);

            // 20, not 35
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 20);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 10);

            // 30, not 45
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?statuses=PENDING&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 30);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=false&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 30);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=false&statuses=PENDING&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=false&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 15);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=false&statuses=CANCELLED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=false&statuses=REJECTED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=false&statuses=PENDING&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 20);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=true&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 15);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=true&statuses=PENDING&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=true&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=true&statuses=CANCELLED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=true&statuses=REJECTED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DEVICETYPE?deleted=true&statuses=PENDING&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 10);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/children/DEVICETYPE/" + uuid.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/children/DEVICEGROUP/" + deviceGroupUuid.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/children/DISCIPLINE/" + disciplineUuid.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void equivalenceMnemonic() {
        // purpose
        //     test mnemonic equivalence
        //
        // what
        //     read        equivalence mnemonic

        try {
            String[] response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/equivalence/Dt");
            ITUtil.assertResponseLength2CodeOKContent(response, "DT");
        } catch (IOException e) {
            fail();
        }
    }

}
