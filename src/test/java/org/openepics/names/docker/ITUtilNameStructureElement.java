/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.openepics.names.docker.ITUtil.AuthorizationChoice;
import org.openepics.names.docker.ITUtil.EndpointChoice;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.response.ResponseBooleanList;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class to help (Docker) integration tests for Naming and PostgreSQL.
 *
 * @author Lars Johansson
 */
public class ITUtilNameStructureElement {

    /**
     * This class is not to be instantiated.
     */
    private ITUtilNameStructureElement() {
        throw new IllegalStateException("Utility class");
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to set name element attributes name and index and then validate create and assert expected response.
     *
     * @param nameElement name element
     * @param name name
     * @param index index
     * @param expectedResponse expected response
     */
    public static void assertValidateCreate(NameElement nameElement, String name, String index, Boolean expectedResponse) {
    	nameElement.setNameAndIndex(name, index);
    	assertValidateCreate(nameElement, expectedResponse);
    }

    /**
     * Utility method to validate create and assert expected response.
     *
     * @param nameElement name element
     * @param expectedResponse expected response
     */
    static void assertValidateCreate(NameElement nameElement, Boolean expectedResponse) {
    	try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
	        ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
	        ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), expectedResponse);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Assert that array of name elements has expected length.
     *
     * @param actual array of name elements
     * @param expectedLength expected length of array
     */
    static void assertContentLength(NameElement[] actual, int expectedLength) {
        assertNotNull(actual);
        assertEquals(expectedLength, actual.length);
    }

    /**
     * Assert that array of name elements has length one and name element is available.
     *
     * @param actual array of name elements
     * @return name element
     */
    static NameElement assertContentLengthOne(NameElement[] actual) {
        assertContentLength(actual, 1);
        assertNotNull(actual[0]);
        return actual[0];
    }

    /**
     * Assert content of name element.
     *
     * @param actual name element
     * @param expectedUuid expected uuid
     * @param expectedSystemgroup expected system group
     * @param expectedSystem expected system
     * @param expectedSubsystem expected subsystem
     * @param expectedDevicetype expected device type
     * @param expectedSystemstructure expected system structure
     * @param expectedDevicestructure expected device structure
     * @param expectedIndex expected index
     * @param expectedName expected name
     * @param expectedDescription expected description
     * @param expectedStatus expected status
     * @param expectedIsLatest expected latest
     * @param expectedIsDeleted expected deleted
     * @param expectedWho expected who
     * @param expectedComment expected comment
     */
    static void assertContent(NameElement actual,
            UUID expectedUuid, UUID expectedSystemgroup, UUID expectedSystem, UUID expectedSubsystem, UUID expectedDevicetype,
            String expectedSystemstructure, String expectedDevicestructure,
            String expectedIndex, String expectedName,
            String expectedDescription, Status expectedStatus, Boolean expectedIsLatest, Boolean expectedIsDeleted,
            String expectedWho, String expectedComment) {
        assertNotNull(actual);
        assertEquals(expectedUuid, actual.getUuid());
        assertEquals(expectedSystemgroup, actual.getSystemgroup());
        assertEquals(expectedSystem, actual.getSystem());
        assertEquals(expectedSubsystem, actual.getSubsystem());
        assertEquals(expectedDevicetype, actual.getDevicetype());
        assertEquals(expectedSystemstructure, actual.getSystemstructure());
        assertEquals(expectedDevicestructure, actual.getDevicestructure());
        assertEquals(expectedIndex, actual.getIndex());
        assertEquals(expectedName, actual.getName());
        assertEquals(expectedDescription, actual.getDescription());
        assertEquals(expectedStatus, actual.getStatus());
        assertEquals(expectedIsLatest, actual.isLatest());
        assertEquals(expectedIsDeleted, actual.isDeleted());
        assertNotNull(actual.getWhen());
        assertEquals(expectedWho, actual.getWho());
        assertEquals(expectedComment, actual.getComment());
    }

    /**
     * Assert that arrays are equal with same length and same content in each array position.
     *
     * @param actual actual array of name elements
     * @param expected expected arbitrary number of name elements
     */
    static void assertEqualsNameElements(NameElement[] actual, NameElement... expected) {
        if (expected != null) {
            assertNotNull(actual);
            assertEquals(expected.length, actual.length);
            for (int i=0; i<expected.length; i++) {
                assertTrue(expected[i].equals(actual[i]));
            }
        } else {
            assertNull(actual);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to create a name element and assert result.
     *
     * @param nameElement name element
     * @return created name element
     */
    static NameElement assertCreate(NameElement nameElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            NameElement[] createdNameElements = null;
            NameElement   createdNameElement  = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            createdNameElements = mapper.readValue(response[1], NameElement[].class);
            createdNameElement = ITUtilNameStructureElement.assertContentLengthOne(createdNameElements);
            assertNotNull(createdNameElement.getUuid());

            uuid = createdNameElement.getUuid();
            nameElement.setUuid(uuid);

            ITUtilNameStructureElement.assertContent(createdNameElement,
                    uuid, nameElement.getSystemgroup(), nameElement.getSystem(), nameElement.getSubsystem(), nameElement.getDevicetype(),
                    nameElement.getSystemstructure(), nameElement.getDevicestructure(),
                    nameElement.getIndex(), nameElement.getName(),
                    nameElement.getDescription(), nameElement.getStatus(), Boolean.TRUE, Boolean.FALSE,
                    "test who", nameElement.getComment());
            assertNotNull(createdNameElement.getWhen());

            return createdNameElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to update a name element and assert result.
     *
     * @param nameElement name element
     * @return updated name element
     */
    static NameElement assertUpdate(NameElement nameElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            NameElement[] updatedNameElements = null;
            NameElement   updatedNameElement  = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPutPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            updatedNameElements = mapper.readValue(response[1], NameElement[].class);
            updatedNameElement = ITUtilNameStructureElement.assertContentLengthOne(updatedNameElements);
            assertNotNull(updatedNameElement.getUuid());

            uuid = updatedNameElement.getUuid();
            nameElement.setUuid(uuid);

            ITUtilNameStructureElement.assertContent(updatedNameElement,
                    uuid, nameElement.getSystemgroup(), nameElement.getSystem(), nameElement.getSubsystem(), nameElement.getDevicetype(),
                    nameElement.getSystemstructure(), nameElement.getDevicestructure(),
                    nameElement.getIndex(), nameElement.getName(),
                    nameElement.getDescription(), nameElement.getStatus(), Boolean.TRUE, Boolean.FALSE,
                    "test who", nameElement.getComment());
            assertNotNull(updatedNameElement.getWhen());

            return updatedNameElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to delete a name element and assert result.
     *
     * @param nameElement name element
     * @return deleted name element
     */
    static NameElement assertDelete(NameElement nameElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            NameElement[] deletedNameElements = null;
            NameElement   deletedNameElement  = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlDeletePathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            deletedNameElements = mapper.readValue(response[1], NameElement[].class);
            deletedNameElement = ITUtilNameStructureElement.assertContentLengthOne(deletedNameElements);
            assertNotNull(deletedNameElement.getUuid());

            uuid = deletedNameElement.getUuid();
            nameElement.setUuid(uuid);

            ITUtilNameStructureElement.assertContent(deletedNameElement,
                    uuid, nameElement.getSystemgroup(), nameElement.getSystem(), nameElement.getSubsystem(), nameElement.getDevicetype(),
                    nameElement.getSystemstructure(), nameElement.getDevicestructure(),
                    nameElement.getIndex(), nameElement.getName(),
                    nameElement.getDescription(), nameElement.getStatus(), Boolean.TRUE, Boolean.TRUE,
                    "test who", nameElement.getComment());
            assertNotNull(deletedNameElement.getWhen());

            return deletedNameElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Assert that array of structure elements has expected length.
     *
     * @param actual array of structure elements
     * @param expectedLength expected length of array
     */
    static void assertContentLength(StructureElement[] actual, int expectedLength) {
        assertNotNull(actual);
        assertEquals(expectedLength, actual.length);
    }

    /**
     * Assert that array of structure elements has length one and structure element is available.
     *
     * @param actual array of structure elements
     * @return structure element
     */
    static StructureElement assertContentLengthOne(StructureElement[] actual) {
        assertContentLength(actual, 1);
        assertNotNull(actual[0]);
        return actual[0];
    }

    /**
     * Assert content of structure element.
     *
     * @param actual structure element
     * @param expectedType expected type
     * @param expectedUuid expected uuid
     * @param expectedParent expected parent
     * @param expectedName expected name
     * @param expectedMnemonic expected mnemonic
     * @param expectedMnemonicpath expected mnemonic path
     * @param expectedLevel expected level
     * @param expectedDescription expected description
     * @param expectedStatus expected status
     * @param expectedIsLatest expected latest
     * @param expectedIsDeleted expected deleted
     * @param expectedWho expected who
     * @param expectedComment expected comment
     */
    static void assertContent(StructureElement actual,
            Type expectedType, UUID expectedUuid, UUID expectedParent,
            String expectedName, String expectedMnemonic, String expectedMnemonicpath, Integer expectedLevel,
            String expectedDescription, Status expectedStatus, Boolean expectedIsLatest, Boolean expectedIsDeleted,
            String expectedWho, String expectedComment) {
        assertNotNull(actual);
        assertEquals(expectedType, actual.getType());
        assertEquals(expectedUuid, actual.getUuid());
        assertEquals(expectedParent, actual.getParent());
        assertEquals(expectedName, actual.getName());
        assertEquals(expectedMnemonic, actual.getMnemonic());
        assertEquals(expectedMnemonicpath, actual.getMnemonicpath());
        assertEquals(expectedLevel, actual.getLevel());
        assertEquals(expectedDescription, actual.getDescription());
        assertEquals(expectedStatus, actual.getStatus());
        assertEquals(expectedIsLatest, actual.isLatest());
        assertEquals(expectedIsDeleted, actual.isDeleted());
        assertNotNull(actual.getWhen());
        assertEquals(expectedWho, actual.getWho());
        assertEquals(expectedComment, actual.getComment());
    }

    /**
     * Assert that arrays are equal with same length and same content in each array position.
     *
     * @param actual actual array of structure elements
     * @param expected expected arbitrary number of structure elements
     */
    static void assertEqualsStructureElements(StructureElement[] actual, StructureElement... expected) {
        if (expected != null) {
            assertNotNull(actual);
            assertEquals(expected.length, actual.length);
            for (int i=0; i<expected.length; i++) {
                assertTrue(expected[i].equals(actual[i]));
            }
        } else {
            assertNull(actual);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to create a structure element and assert result.
     *
     * @param structureElement structure element
     * @return created structure element
     */
    public static StructureElement assertCreate(StructureElement structureElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement[] createdStructureElements  = null;
            StructureElement   createdStructureElement   = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            createdStructureElements = mapper.readValue(response[1], StructureElement[].class);
            createdStructureElement = ITUtilNameStructureElement.assertContentLengthOne(createdStructureElements);
            assertNotNull(createdStructureElement.getUuid());

            uuid = createdStructureElement.getUuid();
            structureElement.setUuid(uuid);

            ITUtilNameStructureElement.assertContent(createdStructureElement,
                    structureElement.getType(), uuid, structureElement.getParent(),
                    structureElement.getName(), structureElement.getMnemonic(), structureElement.getMnemonicpath(), structureElement.getLevel(),
                    structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    "test who", structureElement.getComment());
            assertNotNull(createdStructureElement.getWhen());

            return createdStructureElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to update a structure element and assert result.
     *
     * @param structureElement structure element
     * @return updated structure element
     */
    public static StructureElement assertUpdate(StructureElement structureElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement[] updatedStructureElements  = null;
            StructureElement   updatedStructureElement   = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPutPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            updatedStructureElements = mapper.readValue(response[1], StructureElement[].class);
            updatedStructureElement = ITUtilNameStructureElement.assertContentLengthOne(updatedStructureElements);
            assertNotNull(updatedStructureElement.getUuid());

            uuid = updatedStructureElement.getUuid();
            structureElement.setUuid(uuid);

            ITUtilNameStructureElement.assertContent(updatedStructureElement,
                    structureElement.getType(), uuid, structureElement.getParent(),
                    structureElement.getName(), structureElement.getMnemonic(), structureElement.getMnemonicpath(), structureElement.getLevel(),
                    structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    "test who", structureElement.getComment());
            assertNotNull(updatedStructureElement.getWhen());

            return updatedStructureElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to delete a structure element and assert result.
     *
     * @param structureElement structure element
     * @return deleted structure element
     */
    public static StructureElement assertDelete(StructureElement structureElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement[] deletedStructureElements  = null;
            StructureElement   deletedStructureElement   = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlDeletePathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            deletedStructureElements = mapper.readValue(response[1], StructureElement[].class);
            deletedStructureElement = ITUtilNameStructureElement.assertContentLengthOne(deletedStructureElements);
            assertNotNull(deletedStructureElement.getUuid());

            uuid = deletedStructureElement.getUuid();
            structureElement.setUuid(uuid);

            ITUtilNameStructureElement.assertContent(deletedStructureElement,
                    structureElement.getType(), uuid, structureElement.getParent(),
                    structureElement.getName(), structureElement.getMnemonic(), structureElement.getMnemonicpath(), structureElement.getLevel(),
                    structureElement.getDescription(), Status.PENDING, Boolean.FALSE, Boolean.TRUE,
                    "test who", structureElement.getComment());
            assertNotNull(deletedStructureElement.getWhen());

            return deletedStructureElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to approve a structure element and assert result.
     *
     * @param structureElement structure element
     * @return approved structure element
     */
    public static StructureElement assertApprove(StructureElement structureElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement[] approvedStructureElements = null;
            StructureElement   approvedStructureElement  = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPatchPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/approve", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            approvedStructureElements = mapper.readValue(response[1], StructureElement[].class);
            approvedStructureElement = ITUtilNameStructureElement.assertContentLengthOne(approvedStructureElements);
            ITUtilNameStructureElement.assertContent(approvedStructureElement,
                    structureElement.getType(), structureElement.getUuid(), structureElement.getParent(),
                    structureElement.getName(), structureElement.getMnemonic(), structureElement.getMnemonicpath(), structureElement.getLevel(),
                    structureElement.getDescription(), Status.APPROVED, Boolean.TRUE, structureElement.isDeleted(),
                    "test who", "test comment");
            assertNotNull(approvedStructureElement.getWhen());

            return approvedStructureElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to cancel a structure element and assert result.
     *
     * @param structureElement structure element
     * @return cancelled structure element
     */
    public static StructureElement assertCancel(StructureElement structureElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement[] cancelledStructureElements = null;
            StructureElement   cancelledStructureElement  = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPatchPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/cancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            cancelledStructureElements = mapper.readValue(response[1], StructureElement[].class);
            cancelledStructureElement = ITUtilNameStructureElement.assertContentLengthOne(cancelledStructureElements);
            ITUtilNameStructureElement.assertContent(cancelledStructureElement,
                    structureElement.getType(), structureElement.getUuid(), structureElement.getParent(),
                    structureElement.getName(), structureElement.getMnemonic(), structureElement.getMnemonicpath(), structureElement.getLevel(),
                    structureElement.getDescription(), Status.CANCELLED, Boolean.FALSE, structureElement.isDeleted(),
                    "test who", "test comment");
            assertNotNull(cancelledStructureElement.getWhen());

            return cancelledStructureElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to reject a structure element and assert result.
     *
     * @param structureElement structure element
     * @return rejected structure element
     */
    public static StructureElement assertReject(StructureElement structureElement) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement[] rejectedStructureElements = null;
            StructureElement   rejectedStructureElement  = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlPatchPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/reject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            rejectedStructureElements = mapper.readValue(response[1], StructureElement[].class);
            rejectedStructureElement = ITUtilNameStructureElement.assertContentLengthOne(rejectedStructureElements);
            ITUtilNameStructureElement.assertContent(rejectedStructureElement,
                    structureElement.getType(), structureElement.getUuid(), structureElement.getParent(),
                    structureElement.getName(), structureElement.getMnemonic(), structureElement.getMnemonicpath(), structureElement.getLevel(),
                    structureElement.getDescription(), Status.REJECTED, Boolean.FALSE, structureElement.isDeleted(),
                    "test who", "test comment");
            assertNotNull(rejectedStructureElement.getWhen());

            return rejectedStructureElement;
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return null;
    }

    /**
     * Utility method to create, approve a structure element and assert result.
     *
     * @param structureElement structure element
     * @return approved structure element (after create)
     */
    public static StructureElement assertCreateApprove(StructureElement structureElement) {
        return assertApprove(assertCreate(structureElement));
    }

    /**
     * Utility method to create, cancel a structure element and assert result.
     *
     * @param structureElement structure element
     * @return cancelled structure element (after create)
     */
    public static StructureElement assertCreateCancel(StructureElement structureElement) {
        return assertCancel(assertCreate(structureElement));
    }

    /**
     * Utility method to create, reject a structure element and assert result.
     *
     * @param structureElement structure element
     * @return rejected structure element (after create)
     */
    public static StructureElement assertCreateReject(StructureElement structureElement) {
        return assertReject(assertCreate(structureElement));
    }

    /**
     * Utility method to update, approve a structure element and assert result.
     *
     * @param structureElement structure element
     * @return approved structure element (after update)
     */
    public static StructureElement assertUpdateApprove(StructureElement structureElement) {
        return assertApprove(assertUpdate(structureElement));
    }

    /**
     * Utility method to update, cancel a structure element and assert result.
     *
     * @param structureElement structure element
     * @return cancelled structure element (after update)
     */
    public static StructureElement assertUpdateCancel(StructureElement structureElement) {
        return assertCancel(assertUpdate(structureElement));
    }

    /**
     * Utility method to update, reject a structure element and assert result.
     *
     * @param structureElement structure element
     * @return rejected structure element (after update)
     */
    public static StructureElement assertUpdateReject(StructureElement structureElement) {
        return assertReject(assertUpdate(structureElement));
    }

    /**
     * Utility method to delete, approve a structure element and assert result.
     *
     * @param structureElement structure element
     * @return approved structure element (after delete)
     */
    public static StructureElement assertDeleteApprove(StructureElement structureElement) {
        return assertApprove(assertDelete(structureElement));
    }

    /**
     * Utility method to delete, cancel a structure element and assert result.
     *
     * @param structureElement structure element
     * @return cancelled structure element (after delete)
     */
    public static StructureElement assertDeleteCancel(StructureElement structureElement) {
        return assertCancel(assertDelete(structureElement));
    }

    /**
     * Utility method to delete, reject a structure element and assert result.
     *
     * @param structureElement structure element
     * @return rejected structure element (after delete)
     */
    public static StructureElement assertDeleteReject(StructureElement structureElement) {
        return assertReject(assertDelete(structureElement));
    }

}
