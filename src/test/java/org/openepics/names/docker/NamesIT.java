/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.docker.ITUtil.AuthorizationChoice;
import org.openepics.names.docker.ITUtil.EndpointChoice;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose.yml / Dockerfile.
 *
 * <p>
 * Focus of this class is names endpoint.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
public class NamesIT {

    @Container
    public static final DockerComposeContainer<?> ENVIRONMENT =
        new DockerComposeContainer<>(new File("docker-compose-it-db-schema-migration.yml"))
            .waitingFor(ITUtil.NAMING, Wait.forLogMessage(".*Started NamingApplication.*", 1));

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010PRL = null;
    private static UUID subsystem010    = null;
    private static UUID subsystemN1U1   = null;

    private static UUID disciplineEMR   = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceTypeFS    = null;
    private static UUID deviceTypeRFA   = null;
    private static UUID deviceTypeTT    = null;

    @BeforeAll
    public static void initAll() {
        // init system group, system, subsystem, discipline, device group, device type

        StructureElement structureElement         = null;
        StructureElement approvedStructureElement = null;

        structureElement = new StructureElement(
                Type.SYSTEMGROUP, null, null,
                "Accelerator", "Acc", "Acc", 1,
                "The ESS Linear Accelerator", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "approved by alfio");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        systemGroupAcc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SYSTEM, null, systemGroupAcc,
                "Radio Frequency Quadrupole", "RFQ", "Acc-RFQ", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        systemRFQ = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SUBSYSTEM, null, systemRFQ,
                "01 Phase Reference Line", "010PRL", "Acc-RFQ-010PRL", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approved by Daniel Piso");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        subsystem010PRL = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SUBSYSTEM, null, systemRFQ,
                "RFQ-010", "010", "Acc-RFQ-010", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approved by Daniel Piso");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        subsystem010 = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SUBSYSTEM, null, systemRFQ,
                "Power switch board 01", "N1U1", "Acc-RFQ-N1U1", 3,
                "Electrical power cabinets", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approved by Daniel Piso");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        subsystemN1U1 = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Electromagnetic Resonators", "EMR", "EMR", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineEMR = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineEMR,
                "Control", null, "EMR", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "These names are needed now, so I am approving, but please add a description to these later.");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupEMR = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "Flow Switch", "FS", "EMR-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approve names added from misc device group");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceTypeFS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "RF Antenna", "RFA", "EMR-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approve names added from misc device group");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceTypeRFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "Temperature Transmitter", "TT", "EMR-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approve names added from misc device group");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceTypeTT = approvedStructureElement.getUuid();
    }

    @Test
    public void checkCreate() {
        // purpose
        //     test conditions for create name
        //         not create itself
        //
        // what - combination of
        //     read        exists name
        //     read        is legacy name
        //     read        is valid to create
        //     read        validate create

        try {
            ObjectMapper mapper = new ObjectMapper();
            NameElement nameElement = new NameElement();
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/exists/RFQ-010PRL:EMR-RFA-051");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/islegacy/RFQ-010PRL:EMR-RFA-051");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/isvalidtocreate/RFQ-010PRL:EMR-RFA-051");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.TRUE);

            // validate create

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[{asdf]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[{asdf]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setSubsystem(subsystem010PRL);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setName("RFQ-010PRL");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // ----------

            nameElement.setName("RFQ-010PRL:EMR-RFA-051");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setIndex("051");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setDevicetype(deviceTypeRFA);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // ----------

            nameElement.setSubsystem(null);
            nameElement.setName(null);
            nameElement.setIndex(null);
            nameElement.setDevicetype(null);

            // ----------

            nameElement.setSystem(systemRFQ);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setName("RFQ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // ----------

            nameElement.setName("RFQ:EMR-RFA-051");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setIndex("051");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setDevicetype(deviceTypeRFA);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // ----------

            nameElement.setSystem(null);
            nameElement.setName(null);
            nameElement.setIndex(null);
            nameElement.setDevicetype(null);

            // ----------

            nameElement.setSystemgroup(systemGroupAcc);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setName("Acc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // ----------

            nameElement.setName("Acc:EMR-RFA-051");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setIndex("051");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            nameElement.setDevicetype(deviceTypeTT);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDevicetype(deviceTypeRFA);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void create() {
        // purpose
        //     test create name
        //
        // what - combination of
        //     create      create names
        //     read        exists name
        //     read        is legacy name
        //     read        is valid to create
        //     read        validate create
        //
        // note
        //     with and without index

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            NameElement nameElement        = null;
            NameElement nameElement2       = null;
            NameElement createdNameElement = null;

            nameElement = new NameElement(
                    null,
                    null, null, subsystem010PRL, deviceTypeRFA,
                    "RFQ-010PRL", "EMR-RFA",
                    "052", "RFQ-010PRL:EMR-RFA-052",
                    "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    null, "test who", "comment");

            nameElement2 = new NameElement(
                    null,
                    null, null, subsystem010PRL, deviceTypeRFA,
                    "RFQ-010PRL", "EMR-RFA",
                    "062", "RFQ-010PRL:EMR-RFA-061",
                    "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    null, "test who", "comment");

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/exists/RFQ-010PRL:EMR-RFA-052");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/islegacy/RFQ-010PRL:EMR-RFA-052");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/isvalidtocreate/RFQ-010PRL:EMR-RFA-052");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + ","+mapper.writeValueAsString(nameElement2) +"]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // create
            createdNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
            nameElement = createdNameElement;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/exists/RFQ-010PRL:EMR-RFA-052");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.TRUE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/islegacy/RFQ-010PRL:EMR-RFA-052");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/isvalidtocreate/RFQ-010PRL:EMR-RFA-052");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatecreate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkUpdate() {
        // purpose
        //     test conditions for update name
        //         not update
        //
        // what - combination of
        //     create      create names
        //     read        validate create
        //     read        validate update
        //
        // note
        //     create in order to update

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            NameElement nameElement        = null;
            NameElement createdNameElement = null;

            nameElement = new NameElement(
                    null,
                    null, null, subsystem010PRL, deviceTypeRFA,
                    "RFQ-010PRL", "EMR-RFA",
                    "053", "RFQ-010PRL:EMR-RFA-053",
                    "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    null, "test who", "comment");

            // create
            createdNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
            nameElement = createdNameElement;

            // validate update

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setDescription(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDescription("checkUpdate");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setComment(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setComment("checkUpdate");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setSubsystem(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSystemgroup(systemGroupAcc);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSystemgroup(null);
            nameElement.setSystem(systemRFQ);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSystem(null);
            nameElement.setSubsystem(subsystemN1U1);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSubsystem(subsystem010PRL);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setDevicetype(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDevicetype(deviceGroupEMR);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDevicetype(deviceTypeFS);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDevicetype(deviceTypeTT);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDevicetype(deviceTypeRFA);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setName(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setName("RFQ-010PRL:EMR-RFA-053");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setIndex(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setIndex("053");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // system structure, device structure not used for validation
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void update() {
        // purpose
        //     test update name
        //
        // what - combination of
        //     create      create names
        //     read        validate create
        //     read        validate update
        //     update      update names
        //
        // note
        //     create in order to update

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            NameElement nameElement        = null;
            NameElement createdNameElement = null;
            NameElement updatedNameElement = null;

            nameElement = new NameElement(
                    null,
                    null, null, subsystem010PRL, deviceTypeRFA,
                    "RFQ-010PRL", "EMR-RFA",
                    "054", "RFQ-010PRL:EMR-RFA-054",
                    "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
            nameElement = createdNameElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setDescription("updated description");
            nameElement.setComment("updated comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // update
            updatedNameElement = ITUtilNameStructureElement.assertUpdate(nameElement);
            nameElement = updatedNameElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validateupdate", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkDelete() {
        // purpose
        //     test conditions for delete name
        //         not delete
        //
        // what - combination of
        //     create      create names
        //     read        validate create
        //     read        validate delete
        //
        // note
        //     create in order to delete

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            NameElement nameElement        = null;
            NameElement createdNameElement = null;

            nameElement = new NameElement(
                    null,
                    null, null, subsystem010PRL, deviceTypeRFA,
                    "RFQ-010PRL", "EMR-RFA",
                    "055", "RFQ-010PRL:EMR-RFA-055",
                    "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    null, "test who", "comment");

            // create
            createdNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
            nameElement = createdNameElement;

            // validate delete

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setDescription(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setDescription("checkDelete");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setComment(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setComment("checkDelete");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setSystemgroup(systemGroupAcc);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSystemgroup(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setSystem(systemRFQ);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSystem(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setSubsystem(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            nameElement.setSubsystem(subsystem010PRL);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // device type not used for validation

            nameElement.setDevicetype(deviceTypeRFA);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // name, index not used for validation

            // system structure, device structure not used for validation
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void delete() {
        // purpose
        //     test delete name
        //
        // what - combination of
        //     create      create names
        //     read        validate create
        //     read        validate update
        //     delete      delete names
        //
        // note
        //     create in order to delete

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            NameElement nameElement        = null;
            NameElement createdNameElement = null;
            NameElement deletedNameElement = null;

            nameElement = new NameElement(
                    null,
                    null, null, subsystem010PRL, deviceTypeRFA,
                    "RFQ-010PRL", "EMR-RFA",
                    "056", "RFQ-010PRL:EMR-RFA-056",
                    "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                    null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
            nameElement = createdNameElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            nameElement.setDescription("deleted description");
            nameElement.setComment("deleted comment");
            nameElement.setDeleted(Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // delete
            deletedNameElement = ITUtilNameStructureElement.assertDelete(nameElement);
            nameElement = deletedNameElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.NAMES, "/validatedelete", "[" + mapper.writeValueAsString(nameElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryDeleted() {
        // purpose
        //     test read names in various ways for create
        //         search
        //       ( latest )
        //         history
        //         deleted
        //
        // note
        //     create (and more) to read (with content)

        NameElement nameElement         = null;
        NameElement responseNameElement = null;
        UUID uuid, uuid2 = null;

        NameElement nameElement1, nameElement7, nameElement8 = null;

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "001", "RFQ-010:EMR-FS-001",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");

        // create
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
        nameElement1 = responseNameElement;
        uuid = responseNameElement.getUuid();

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "002", "RFQ-010:EMR-FS-002",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
        uuid2 = responseNameElement.getUuid();

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "003", "RFQ-010:EMR-FS-003",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "004", "RFQ-010:EMR-FS-004",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "005", "RFQ-010:EMR-FS-005",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "006", "RFQ-010:EMR-FS-006",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "007", "RFQ-010:EMR-FS-007",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
        nameElement7 = responseNameElement;

        nameElement = new NameElement(
                null,
                null, null, subsystem010, deviceTypeFS,
                "RFQ-010", "EMR-FS",
                "008", "RFQ-010:EMR-FS-008",
                "description", Status.APPROVED, Boolean.TRUE, Boolean.FALSE,
                null, "test who", "comment");
        responseNameElement = ITUtilNameStructureElement.assertCreate(nameElement);
        nameElement8 = responseNameElement;

        // update element 1 twice
        nameElement = nameElement1;
        nameElement.setDescription("updated description");
        nameElement.setComment("updated comment");

        responseNameElement = ITUtilNameStructureElement.assertUpdate(nameElement);
        nameElement = responseNameElement;

        nameElement.setDescription("updated description again");
        nameElement.setComment("updated comment again");

        responseNameElement = ITUtilNameStructureElement.assertUpdate(nameElement);
        nameElement = responseNameElement;

        // delete element 7, 8
        responseNameElement = ITUtilNameStructureElement.assertDelete(nameElement7);
        responseNameElement = ITUtilNameStructureElement.assertDelete(nameElement8);

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            NameElement[] readNameElements = null;

            // read

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES);
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 8);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "?queryFields=UUID&queryValues=" + uuid.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "?queryFields=NAMEEQUIVALENCE&queryValues=RFQ-10%25");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 8);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "?deleted=false");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "?deleted=true");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 2);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/RFQ-010:EMR-FS-005");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/RFQ-010:EMR-FS-0");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/RFQ-010:EMR-FS-0__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/RFQ-010:EMR-FS-0%25");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/systemstructure/RFQ-010");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/systemstructure/RFQ-0");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/systemstructure/RFQ-0__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/systemstructure/RFQ-N1U1");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/devicestructure/EMR-FS");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/devicestructure/EMR-F");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/devicestructure/EMR-F_");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            assertTrue(readNameElements.length >= 6);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/devicestructure/EMR-TT");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 0);

            // history

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/history/" + systemRFQ.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/history/" + uuid.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 3);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/history/" + uuid2.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readNameElements = mapper.readValue(response[1], NameElement[].class);
            ITUtilNameStructureElement.assertContentLength(readNameElements, 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void equivalenceName() {
        try {
            String[] response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/equivalence/A2T-010PRL:RFS-PRLTap-054");
            ITUtil.assertResponseLength2CodeOKContent(response, "A2T-10PR1:RFS-PR1TAP-54");
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void existsName() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String[] response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/exists/A2T-010PRL:RFS-PRLTap-054");
            ITUtil.assertResponseLength2CodeOK(response);
            ResponseBoolean responseEntity = mapper.readValue(response[1], ResponseBoolean.class);
            assertNotNull(responseEntity);
            assertFalse(responseEntity.getResponse());
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void isLegacyName() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String[] response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/islegacy/A2T-010PRL:RFS-PRLTap-054");
            ITUtil.assertResponseLength2CodeOK(response);
            ResponseBoolean responseEntity = mapper.readValue(response[1], ResponseBoolean.class);
            assertNotNull(responseEntity);
            assertFalse(responseEntity.getResponse());
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void isValidToCreateName() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String[] response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/isvalidtocreate/A2T-010PRL:RFS-PRLTap-054");
            ITUtil.assertResponseLength2CodeOK(response);
            ResponseBoolean responseEntity = mapper.readValue(response[1], ResponseBoolean.class);
            assertNotNull(responseEntity);
            assertFalse(responseEntity.getResponse());
        } catch (IOException e) {
            fail();
        }
    }

}
