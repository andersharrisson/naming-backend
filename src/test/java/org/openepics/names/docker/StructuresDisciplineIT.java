/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.docker.ITUtil.AuthorizationChoice;
import org.openepics.names.docker.ITUtil.EndpointChoice;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose.yml / Dockerfile.
 *
 * <p>
 * Focus of this class is structures endpoint and discipline.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
public class StructuresDisciplineIT {

    // note
    //     StructureElement - when, who, comment - depend on requested or processed
    //         requested, requested by, requested comment
    //         processed, processed by, processed comment
    //     if less than a second between requested and processed, then considered one entry with processed
    //     history
    //         mnemonic path does not make same sense for history
    //         (very) tricky to find mnemonic path for uuid at proper time (history)
    //         therefore empty mnemonic path for history for structure
    //         one history entry if less than one second between requested and processed, otherwise two history entries
    //     attributes for entry for operations - create, update, delete, approve, cancel, reject
    //         some set client side, others set server side
    //         client side
    //             type, uuid, parent uuid, name, mnemonic, description, comment
    //         may be set client side for test purposes

    @Container
    public static final DockerComposeContainer<?> ENVIRONMENT =
        new DockerComposeContainer<>(new File("docker-compose-it-db-schema-migration.yml"))
            .waitingFor(ITUtil.NAMING, Wait.forLogMessage(".*Started NamingApplication.*", 1));

    @Test
    public void checkCreate() {
        // purpose
        //     test conditions for create discipline
        //         not create itself
        //
        // what - combination of
        //     read        exists in structure
        //     read        is valid to create
        //     read        validate create

        try {
            ObjectMapper mapper = new ObjectMapper();
            StructureElement structureElement = new StructureElement();
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/exists/DISCIPLINE/Cc");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.FALSE);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/isvalidtocreate/DISCIPLINE/Cc");
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[{asdf]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[{asdf]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setType(Type.DISCIPLINE);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setName("name");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setMnemonic("Cc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_BAD_REQUEST);
            ITUtil.assertMessageNotEmpty(mapper.readValue(response[1], Response.class));

            structureElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setType(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkCreateMnemonic() {
        // purpose
        //     test conditions for create discipline
        //         not create itself
        //
        // what - combination of
        //     read        validate create
        //
        // note
        //     mnemonic
        try {
            ObjectMapper mapper = new ObjectMapper();
            StructureElement structureElement = new StructureElement();
            String[] response = null;

            structureElement.setType(Type.DISCIPLINE);
            structureElement.setName("name");
            structureElement.setDescription("description");
            structureElement.setComment("comment");

            // mnemonic rules

            structureElement.setMnemonic(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("C");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Cc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Ccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Cccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Ccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Cccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Ccccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Cccccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Ccccccccc");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // mnemonic rules (2)

            structureElement.setMnemonic(" ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dis ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dis");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("000");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("Dis0");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic(":");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dis:");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Dis:   ");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("1");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("12");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("123");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("1234");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("12345");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("123456");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic("1234567");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("12345678");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("123456789");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecreate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
	    } catch (IOException e) {
	        fail();
	    } catch (InterruptedException e) {
	        fail();
	    } catch (Exception e) {
	        fail();
	    }
	}

    @Test
    public void createApprove() {
        // purpose
        //     test create and approve discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create in order to approve

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement createdStructureElement  = null;
            StructureElement approvedStructureElement = null;

            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Ca", "Ca", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(approvedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void createCancel() {
        // purpose
        //     test create and cancel discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate cancel
        //     patch       cancel structures
        //
        // note
        //     create in order to cancel

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement          = null;
            StructureElement createdStructureElement   = null;
            StructureElement cancelledStructureElement = null;

            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Cc", "Cc", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(cancelledStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void createReject() {
        // purpose
        //     test create and reject discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate reject
        //     patch       reject structures
        //
        // note
        //     create in order to reject

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement createdStructureElement  = null;
            StructureElement rejectedStructureElement = null;

            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Cr", "Cr", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(rejectedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkUpdate() {
        // purpose
        //     test conditions for update discipline
        //         not update
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create, approve in order to update

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement structureElement         = new StructureElement();
            StructureElement approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DISCIPLINE, null, null,
                    "name", "Cu", "Cu", 1,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;
            uuid = approvedStructureElement.getUuid();

            // validate update

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setType(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.SYSTEMGROUP);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.DISCIPLINE);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setUuid(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setUuid(uuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setName(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setName("name");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Cu");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setDescription(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setComment(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void updateApprove() {
        // purpose
        //     test update and approve discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     update      update structures
        //     patch       approve structures
        //
        // note
        //     create, approve in order to update, approve

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement updatedStructureElement  = null;
            StructureElement approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Ua", "Ua", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update approve check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // update
            updatedStructureElement = ITUtilNameStructureElement.assertUpdate(structureElement);
            structureElement = updatedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(approvedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void updateCancel() {
        // purpose
        //     test update and cancel discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     read        validate cancel
        //     update      update structures
        //     patch       approve structures
        //     patch       cancel structures
        //
        // note
        //     create, approve in order to update, cancel

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement          = null;
            StructureElement updatedStructureElement   = null;
            StructureElement approvedStructureElement  = null;
            StructureElement cancelledStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DISCIPLINE, null, null,
                    "name", "Uc", "Uc", 1,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update cancel check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // update
            updatedStructureElement = ITUtilNameStructureElement.assertUpdate(structureElement);
            structureElement = updatedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(cancelledStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void updateReject() {
        // purpose
        //     test update and reject discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate update
        //     read        validate approve
        //     read        validate reject
        //     update      update structures
        //     patch       approve structures
        //     patch       reject structures
        //
        // note
        //     create, approve in order to update, reject

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement updatedStructureElement  = null;
            StructureElement approvedStructureElement = null;
            StructureElement rejectedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DISCIPLINE, null, null,
                    "name", "Ur", "Ur", 1,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update reject check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // update
            updatedStructureElement = ITUtilNameStructureElement.assertUpdate(structureElement);
            structureElement = updatedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(rejectedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void checkDelete() {
        // purpose
        //     test conditions for delete discipline
        //         not delete
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create, approve in order to delete

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;
            UUID uuid = null;

            StructureElement   structureElement         = new StructureElement();
            StructureElement   approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DISCIPLINE, null, null,
                    "name", "Cd", "Cd", 1,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;
            uuid = approvedStructureElement.getUuid();

            // validate delete

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setType(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.SYSTEMGROUP);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setType(Type.DISCIPLINE);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setUuid(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setUuid(uuid);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setName(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setName("name");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setMnemonic(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setMnemonic("Cd");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setDescription(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setDescription("description");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            structureElement.setComment(null);
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBooleanMessageNotEmpty(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            structureElement.setComment("comment");
            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void deleteApprove() {
        // purpose
        //     test delete and approve discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     delete      delete structures
        //     patch       approve structures
        //
        // note
        //     create, approve in order to delete, approve

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement deletedStructureElement  = null;
            StructureElement approvedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Da", "Da", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment delete approve check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // delete
            structureElement.setDeleted(Boolean.TRUE);
            deletedStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);
            structureElement = deletedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateapprove", "[" + mapper.writeValueAsString(approvedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void deleteCancel() {
        // purpose
        //     test delete and cancel discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     read        validate cancel
        //     update      delete structures
        //     patch       approve structures
        //     patch       cancel structures
        //
        // note
        //     create, approve in order to delete, cancel

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement          = null;
            StructureElement deletedStructureElement   = null;
            StructureElement approvedStructureElement  = null;
            StructureElement cancelledStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DISCIPLINE, null, null,
                    "name", "Dc", "Dc", 1,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment update delete check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatedelete", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // delete
            deletedStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);
            structureElement = deletedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatecancel", "[" + mapper.writeValueAsString(cancelledStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void deleteReject() {
        // purpose
        //     test delete and reject discipline
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate delete
        //     read        validate approve
        //     read        validate reject
        //     update      delete structures
        //     patch       approve structures
        //     patch       reject structures
        //
        // note
        //     create, approve in order to delete, reject

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement structureElement         = null;
            StructureElement deletedStructureElement  = null;
            StructureElement approvedStructureElement = null;
            StructureElement rejectedStructureElement = null;

            // create, approve
            structureElement = new StructureElement(
                    Type.DISCIPLINE, null, null,
                    "name", "Dr", "Dr", 1,
                    "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                    null, "test who", "test comment");
            approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
            structureElement = approvedStructureElement;

            structureElement.setComment("comment delete reject check");

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validateupdate", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            // delete
            deletedStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);
            structureElement = deletedStructureElement;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.TRUE);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(structureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(AuthorizationChoice.NONE, EndpointChoice.STRUCTURES, "/validatereject", "[" + mapper.writeValueAsString(rejectedStructureElement) + "]"));
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), Boolean.FALSE);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryApprove() {
        // purpose
        //     test read discipline in various ways for create, approve
        //         search
        //         latest
        //         history
        //
        // note
        //     create (and more) to read (with content)

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement   structureElement         = null;
            StructureElement   createdStructureElement  = null;
            StructureElement[] readStructureElements    = null;
            StructureElement   approvedStructureElement = null;
            int length = 0;

            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Rsha", "Rsha", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            // read (1)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            length = readStructureElements.length;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=UUID&queryValues=" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            // approve
            approvedStructureElement = ITUtilNameStructureElement.assertApprove(structureElement);

            // read (2)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            assertEquals(length, readStructureElements.length);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=UUID&queryValues=" + approvedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rsha");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + approvedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryCancel() {
        // purpose
        //     test read discipline in various ways for create, cancel
        //         search
        //         latest
        //         history
        //
        // note
        //     create (and more) to read (with content)

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement   structureElement          = null;
            StructureElement   createdStructureElement   = null;
            StructureElement[] readStructureElements     = null;
            StructureElement   cancelledStructureElement = null;
            int length = 0;

            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Rshc", "Rshc", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            // read (1)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            length = readStructureElements.length;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=UUID&queryValues=" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            // cancel
            cancelledStructureElement = ITUtilNameStructureElement.assertCancel(structureElement);

            // read (2)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            assertEquals(length, readStructureElements.length);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=UUID&queryValues=" + cancelledStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshc");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + cancelledStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchHistoryReject() {
        // purpose
        //     test read discipline in various ways for create, reject
        //         search
        //         latest
        //         history
        //
        // note
        //     create (and more) to read (with content)

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement   structureElement         = null;
            StructureElement   createdStructureElement  = null;
            StructureElement[] readStructureElements    = null;
            StructureElement   rejectedStructureElement = null;
            int length = 0;

            structureElement = new StructureElement(
                  Type.DISCIPLINE, null, null,
                  "name", "Rshr", "Rshr", 1,
                  "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                  null, "test who", "comment");

            // create
            createdStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);
            structureElement = createdStructureElement;

            // read (1)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            length = readStructureElements.length;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=UUID&queryValues=" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLengthOne(readStructureElements);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + createdStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            // reject
            rejectedStructureElement = ITUtilNameStructureElement.assertReject(structureElement);

            // read (2)
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertNotNull(readStructureElements);
            assertTrue(readStructureElements.length >= 1);
            assertEquals(length, readStructureElements.length);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=UUID&queryValues=" + rejectedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 1);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonic/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/mnemonicpath/Rshr");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history/" + rejectedStructureElement.getUuid().toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            assertTrue(readStructureElements.length >= 1);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void readSearchStatusDeletedChildren() {
        // purpose
        //     test read system group in various ways
        //         status
        //       ( latest )
        //         deleted
        //         children
        //
        // what
        //     entries with different statuses
        //
        // note
        //     create (and more) to read (with content)
        //     querying for Status.APPROVED means latest approved

        StructureElement structureElement         = null;
        StructureElement responseStructureElement = null;
        UUID uuid = null;

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AA1", "AA1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        uuid = responseStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AA2", "AA2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AA3", "AA3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AA4", "AA4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AA5", "AA5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AB1", "AB1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AB2", "AB2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AB3", "AB3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AB4", "AB4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AB5", "AB5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateCancel(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AC1", "AC1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AC2", "AC2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AC3", "AC3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AC4", "AC4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AC5", "AC5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AD1", "AD1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AD2", "AD2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AD3", "AD3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AD4", "AD4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AD5", "AD5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreate(structureElement);

        String description2 = "some other description";
        String comment2 = "some other comment";
        String description3 = "more description";
        String comment3 = "more comment";

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AE1", "AE1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AE2", "AE2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AE3", "AE3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AE4", "AE4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AE5", "AE5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description2);
        structureElement.setComment(comment2);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDescription(description3);
        structureElement.setComment(comment3);
        responseStructureElement = ITUtilNameStructureElement.assertUpdateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteReject(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AF1", "AF1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AF2", "AF2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AF3", "AF3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AF4", "AF4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AF5", "AF5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDeleteApprove(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AG1", "AG1", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AG2", "AG2", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AG3", "AG3", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AG4", "AG4", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "name", "AG5", "AG5", 1,
                "description", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "test comment");
        responseStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        structureElement = responseStructureElement;
        structureElement.setDeleted(Boolean.FALSE);
        responseStructureElement = ITUtilNameStructureElement.assertDelete(structureElement);

        // 60 discipline entries

        try {
            ObjectMapper mapper = new ObjectMapper();
            String[] response = null;

            StructureElement[] readStructureElements = null;

            // from first structure element
            assertNotNull(uuid);

            // 45, not 60
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 45);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 10);

            // 20, not 35
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 20);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=CANCELLED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=REJECTED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 10);

            // 30, not 45
            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?statuses=PENDING&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 30);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=false&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 30);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=false&statuses=PENDING&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=false&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 15);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=false&statuses=CANCELLED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=false&statuses=REJECTED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=false&statuses=PENDING&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 20);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=true&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 15);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=true&statuses=PENDING&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=true&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=true&statuses=CANCELLED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=true&statuses=REJECTED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 5);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/DISCIPLINE?deleted=true&statuses=PENDING&statuses=APPROVED&queryFields=MNEMONIC&queryValues=A__");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 10);

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/children/DISCIPLINE/" + uuid.toString());
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            readStructureElements = mapper.readValue(response[1], StructureElement[].class);
            ITUtilNameStructureElement.assertContentLength(readStructureElements, 0);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void equivalenceMnemonic() {
        // purpose
        //     test mnemonic equivalence
        //
        // what
        //     read        equivalence mnemonic

        try {
            String[] response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/equivalence/Di");
            ITUtil.assertResponseLength2CodeOKContent(response, "D1");
        } catch (IOException e) {
            fail();
        }
    }

}
