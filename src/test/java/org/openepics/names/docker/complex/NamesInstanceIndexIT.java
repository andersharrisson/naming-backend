/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker.complex;

import java.io.File;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.docker.ITUtil;
import org.openepics.names.docker.ITUtilNameStructureElement;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose.yml / Dockerfile.
 *
 * <p>
 * Focus of this class is names endpoint and rules for disciplines P&ID and Scientific in particular.
 * In practice, this class contains tests for instance index, en masse.
 * </p>
 *
 * @author Lars Johansson
 *
 * @see NamingConventionUtil
 */
@SuppressWarnings("unused")
@Testcontainers
public class NamesInstanceIndexIT {

	// note
	//     disciplines
	//         P&ID
	//         Scientific
	//     mnemonic path P&ID numeric
	//     see NamingConventionUtil

    @Container
    public static final DockerComposeContainer<?> ENVIRONMENT =
        new DockerComposeContainer<>(new File("docker-compose-it-db-schema-migration.yml"))
            .waitingFor(ITUtil.NAMING, Wait.forLogMessage(".*Started NamingApplication.*", 1));

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010PRL = null;
	private static UUID subsystem010    = null;
    private static UUID subsystemN1U1   = null;

    private static UUID disciplineCryo  = null;
    private static UUID disciplineEMR   = null;
    private static UUID disciplineHVAC  = null;
    private static UUID disciplineProc  = null;
    private static UUID disciplineSC    = null;
    private static UUID disciplineVac   = null;
    private static UUID disciplineWtrC  = null;
    private static UUID disciplineBMD   = null;

    private static UUID deviceGroupCryo = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceGroupHVAC = null;
    private static UUID deviceGroupProc = null;
    private static UUID deviceGroupSC   = null;
    private static UUID deviceGroupVac  = null;
    private static UUID deviceGroupWtrC = null;
    private static UUID deviceGroupBMD  = null;

    private static UUID deviceType_Cryo_FS  = null;
    private static UUID deviceType_Cryo_IOC = null;
    private static UUID deviceType_Cryo_RFA = null;
    private static UUID deviceType_Cryo_TT  = null;

    private static UUID deviceType_EMR_FS   = null;
    private static UUID deviceType_EMR_IOC  = null;
    private static UUID deviceType_EMR_RFA  = null;
    private static UUID deviceType_EMR_TT   = null;

    private static UUID deviceType_HVAC_FS  = null;
    private static UUID deviceType_HVAC_IOC = null;
    private static UUID deviceType_HVAC_RFA = null;
    private static UUID deviceType_HVAC_TT  = null;

    private static UUID deviceType_Proc_FS  = null;
    private static UUID deviceType_Proc_IOC = null;
    private static UUID deviceType_Proc_RFA = null;
    private static UUID deviceType_Proc_TT  = null;

    private static UUID deviceType_SC_FS    = null;
    private static UUID deviceType_SC_IOC   = null;
    private static UUID deviceType_SC_RFA   = null;
    private static UUID deviceType_SC_TT    = null;

    private static UUID deviceType_Vac_FS   = null;
    private static UUID deviceType_Vac_IOC  = null;
    private static UUID deviceType_Vac_RFA  = null;
    private static UUID deviceType_Vac_TT   = null;

    private static UUID deviceType_WtrC_FS  = null;
    private static UUID deviceType_WtrC_IOC = null;
    private static UUID deviceType_WtrC_RFA = null;
    private static UUID deviceType_WtrC_TT  = null;

    private static UUID deviceType_BMD_FS   = null;
    private static UUID deviceType_BMD_IOC  = null;
    private static UUID deviceType_BMD_RFA  = null;
    private static UUID deviceType_BMD_TT   = null;

    @BeforeAll
    public static void initAll() {
        // init system group, system, subsystem, discipline, device group, device type

        StructureElement structureElement         = null;
        StructureElement approvedStructureElement = null;

        structureElement = new StructureElement(
                Type.SYSTEMGROUP, null, null,
                "Accelerator", "Acc", "Acc", 1,
                "The ESS Linear Accelerator", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "approved by alfio");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        systemGroupAcc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SYSTEM, null, systemGroupAcc,
                "Radio Frequency Quadrupole", "RFQ", "Acc-RFQ", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        systemRFQ = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SUBSYSTEM, null, systemRFQ,
                "01 Phase Reference Line", "010PRL", "Acc-RFQ-010PRL", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approved by Daniel Piso");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        subsystem010PRL = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SUBSYSTEM, null, systemRFQ,
                "RFQ-010", "010", "Acc-RFQ-010", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approved by Daniel Piso");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        subsystem010 = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SUBSYSTEM, null, systemRFQ,
                "Power switch board 01", "N1U1", "Acc-RFQ-N1U1", 3,
                "Electrical power cabinets", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "Approved by Daniel Piso");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        subsystemN1U1 = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Cryogenics", "Cryo", "Cryo", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineCryo = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Electromagnetic Resonators", "EMR", "EMR", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineEMR = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Heating, Cooling and Air Conditioning", "HVAC", "HVAC", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineHVAC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "General Process Control", "Proc", "Proc", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineProc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Software Controllers", "SC", "SC", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineSC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Vacuum", "Vac", "Vac", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineVac = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Water Cooling", "WtrC", "WtrC", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineWtrC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Beam Magnets and Deflectors", "BMD", "BMD", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineBMD = approvedStructureElement.getUuid();


        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineCryo,
                "empty", null, "Cryo", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupCryo = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineEMR,
                "empty", null, "EMR", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupEMR = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineHVAC,
                "empty", null, "HVAC", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupHVAC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineProc,
                "empty", null, "Proc", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupProc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineSC,
                "empty", null, "SC", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupSC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineVac,
                "empty", null, "Vac", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupVac = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineWtrC,
                "empty", null, "WtrC", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupWtrC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineBMD,
                "empty", null, "BMD", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupBMD = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupCryo,
                "Flow Switch", "FS", "Cryo-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Cryo_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupCryo,
                "Input Output Controller", "IOC", "Cryo-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Cryo_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupCryo,
                "RF Antenna", "RFA", "Cryo-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Cryo_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupCryo,
                "Temperature Transmitter", "TT", "Cryo-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Cryo_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "Input Output Controller", "IOC", "EMR-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_EMR_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "Flow Switch", "FS", "EMR-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_EMR_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "RF Antenna", "RFA", "EMR-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_EMR_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupEMR,
                "Temperature Transmitter", "TT", "EMR-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_EMR_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupHVAC,
                "Flow Switch", "FS", "HVAC-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_HVAC_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupHVAC,
                "Input Output Controller", "IOC", "HVAC-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_HVAC_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupHVAC,
                "RF Antenna", "RFA", "HVAC-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_HVAC_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupHVAC,
                "Temperature Transmitter", "TT", "HVAC-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_HVAC_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupProc,
                "Flow Switch", "FS", "Proc-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Proc_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupProc,
                "Input Output Controller", "IOC", "Proc-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Proc_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupProc,
                "RF Antenna", "RFA", "Proc-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Proc_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupProc,
                "Temperature Transmitter", "TT", "Proc-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Proc_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupSC,
                "Flow Switch", "FS", "SC-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_SC_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupSC,
                "Input Output Controller", "IOC", "SC-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_SC_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupSC,
                "RF Antenna", "RFA", "SC-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_SC_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupSC,
                "Temperature Transmitter", "TT", "SC-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_SC_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupVac,
                "Flow Switch", "FS", "Vac-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Vac_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupVac,
                "Input Output Controller", "IOC", "Vac-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Vac_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupVac,
                "RF Antenna", "RFA", "Vac-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Vac_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupVac,
                "Temperature Transmitter", "TT", "Vac-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_Vac_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupWtrC,
                "Flow Switch", "FS", "WtrC-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_WtrC_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupWtrC,
                "Input Output Controller", "IOC", "WtrC-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_WtrC_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupWtrC,
                "RF Antenna", "RFA", "WtrC-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_WtrC_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupWtrC,
                "Temperature Transmitter", "TT", "WtrC-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_WtrC_TT = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupBMD,
                "Flow Switch", "FS", "BMD-FS", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_BMD_FS = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupBMD,
                "Input Output Controller", "IOC", "BMD-IOC", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_BMD_IOC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupBMD,
                "RF Antenna", "RFA", "BMD-RFA", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_BMD_RFA = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICETYPE, null, deviceGroupBMD,
                "Temperature Transmitter", "TT", "BMD-TT", 3,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceType_BMD_TT = approvedStructureElement.getUuid();
    }

  @Test
  public void checkCreateInstanceIndex_Cryo() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline Cryo
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_Cryo_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_Cryo_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Cryo-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_EMR() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline EMR
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_EMR_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_EMR_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:EMR-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_HVAC() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline HVAC
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_HVAC_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_HVAC_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:HVAC-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_Proc() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline Proc
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_Proc_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_Proc_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Proc-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_SC() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline SC
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_SC_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000a",       "000a",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000ab",      "000ab",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000abc",     "000abc",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123a",       "123a",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123ab",      "123ab",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123abc",     "123abc",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_SC_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:SC-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_Vac() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline Vac
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_Vac_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_Vac_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:Vac-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_WtrC() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline WtrC
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_WtrC_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_WtrC_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-12",         "12",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-1234",       "1234",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000a",       "000a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000ab",      "000ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000abc",     "000abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123a",       "123a",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123ab",      "123ab",     Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123abc",     "123abc",    Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-1",          "1",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-01",         "01",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-0001",       "0001",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-0110",       "0110",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-10",         "10",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-1000",       "1000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:WtrC-RFA-a!",         "a!",        Boolean.FALSE);
  }

  @Test
  public void checkCreateInstanceIndex_BMD() {
      // purpose
      //     test conditions for create name
      //         not create itself
      //
      // what - combination of
      //     read        validate create
      //
      // note
      //     discipline BMD
      //     instance index

      NameElement nameElement = new NameElement();
      nameElement.setDescription("description");
      nameElement.setComment("comment");
      nameElement.setSubsystem(subsystem010PRL);

      // ----------------------------------------------------------------------------------------------------

      nameElement.setDevicetype(deviceType_BMD_IOC);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-1",          "1",         Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-12",         "12",        Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-1234",       "1234",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000a",       "000a",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000ab",      "000ab",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000abc",     "000abc",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123a",       "123a",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123ab",      "123ab",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123abc",     "123abc",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-1",          "1",         Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-01",         "01",        Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-0001",       "0001",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-0110",       "0110",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-10",         "10",        Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-1000",       "1000",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-IOC-a!",         "a!",        Boolean.FALSE);

      nameElement.setDevicetype(deviceType_BMD_RFA);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-051",        "051",       Boolean.TRUE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-0",          "0",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-00",         "00",        Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000",        "000",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-0000",       "0000",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-00000",      "00000",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000000",     "000000",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-0000000",    "0000000",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-00000000",   "00000000",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000000000",  "000000000", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-1",          "1",         Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-12",         "12",        Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123",        "123",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-1234",       "1234",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-12345",      "12345",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123456",     "123456",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-1234567",    "1234567",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-12345678",   "12345678",  Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123456789",  "123456789", Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000a",       "000a",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000ab",      "000ab",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000abc",     "000abc",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000abcd",    "000abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000A",       "000A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000AB",      "000AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000ABC",     "000ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-000ABCD",    "000ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123a",       "123a",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123ab",      "123ab",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123abc",     "123abc",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123abcd",    "123abcd",   Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123A",       "123A",      Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123AB",      "123AB",     Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123ABC",     "123ABC",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-123ABCD",    "123ABCD",   Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-1",          "1",         Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-01",         "01",        Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-001",        "001",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-0001",       "0001",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-0110",       "0110",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-10",         "10",        Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-100",        "100",       Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-1000",       "1000",      Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-10001",      "10001",     Boolean.FALSE);

      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA",            "",          Boolean.TRUE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA ",           " ",         Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-Idx",        "Idx",       Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-abcdef",     "abcdef",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-abc123",     "abc123",    Boolean.FALSE);
      ITUtilNameStructureElement.assertValidateCreate(nameElement, "RFQ-010PRL:BMD-RFA-a!",         "a!",        Boolean.FALSE);
  }

}
