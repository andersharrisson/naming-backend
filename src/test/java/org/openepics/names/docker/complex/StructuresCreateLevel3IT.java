/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker.complex;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.docker.ITUtil;
import org.openepics.names.docker.ITUtilNameStructureElement;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.StructureElement;
import org.openepics.names.rest.beans.Type;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose.yml / Dockerfile.
 *
 * <p>
 * Focus of this class is structures endpoints. In particular create level 3 entries
 * and create all entries before approve all entries.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
public class StructuresCreateLevel3IT {
    //     create in order to approve
	//     all create first, all approve separately after all create

    @Container
    public static final DockerComposeContainer<?> ENVIRONMENT =
        new DockerComposeContainer<>(new File("docker-compose-it-db-schema-migration.yml"))
            .waitingFor(ITUtil.NAMING, Wait.forLogMessage(".*Started NamingApplication.*", 1));

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010PRL = null;
	private static UUID subsystem010    = null;
    private static UUID subsystemN1U1   = null;

    private static UUID disciplineCryo  = null;
    private static UUID disciplineEMR   = null;
    private static UUID disciplineHVAC  = null;
    private static UUID disciplineProc  = null;
    private static UUID disciplineSC    = null;
    private static UUID disciplineVac   = null;
    private static UUID disciplineWtrC  = null;
    private static UUID disciplineBMD   = null;

    private static UUID deviceGroupCryo = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceGroupHVAC = null;
    private static UUID deviceGroupProc = null;
    private static UUID deviceGroupSC   = null;
    private static UUID deviceGroupVac  = null;
    private static UUID deviceGroupWtrC = null;
    private static UUID deviceGroupBMD  = null;

    private static UUID deviceType_Cryo_FS  = null;
    private static UUID deviceType_Cryo_IOC = null;
    private static UUID deviceType_Cryo_RFA = null;
    private static UUID deviceType_Cryo_TT  = null;

    private static UUID deviceType_EMR_FS   = null;
    private static UUID deviceType_EMR_IOC  = null;
    private static UUID deviceType_EMR_RFA  = null;
    private static UUID deviceType_EMR_TT   = null;

    private static UUID deviceType_HVAC_FS  = null;
    private static UUID deviceType_HVAC_IOC = null;
    private static UUID deviceType_HVAC_RFA = null;
    private static UUID deviceType_HVAC_TT  = null;

    private static UUID deviceType_Proc_FS  = null;
    private static UUID deviceType_Proc_IOC = null;
    private static UUID deviceType_Proc_RFA = null;
    private static UUID deviceType_Proc_TT  = null;

    private static UUID deviceType_SC_FS    = null;
    private static UUID deviceType_SC_IOC   = null;
    private static UUID deviceType_SC_RFA   = null;
    private static UUID deviceType_SC_TT    = null;

    private static UUID deviceType_Vac_FS   = null;
    private static UUID deviceType_Vac_IOC  = null;
    private static UUID deviceType_Vac_RFA  = null;
    private static UUID deviceType_Vac_TT   = null;

    private static UUID deviceType_WtrC_FS  = null;
    private static UUID deviceType_WtrC_IOC = null;
    private static UUID deviceType_WtrC_RFA = null;
    private static UUID deviceType_WtrC_TT  = null;

    private static UUID deviceType_BMD_FS   = null;
    private static UUID deviceType_BMD_IOC  = null;
    private static UUID deviceType_BMD_RFA  = null;
    private static UUID deviceType_BMD_TT   = null;

    @BeforeAll
    public static void initAll() {
        // init system group, system, discipline, device group

        StructureElement structureElement         = null;
        StructureElement approvedStructureElement = null;

        structureElement = new StructureElement(
                Type.SYSTEMGROUP, null, null,
                "Accelerator", "Acc", "Acc", 1,
                "The ESS Linear Accelerator", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "approved by alfio");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        systemGroupAcc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.SYSTEM, null, systemGroupAcc,
                "Radio Frequency Quadrupole", "RFQ", "Acc-RFQ", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        systemRFQ = approvedStructureElement.getUuid();

        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Cryogenics", "Cryo", "Cryo", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineCryo = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Electromagnetic Resonators", "EMR", "EMR", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineEMR = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Heating, Cooling and Air Conditioning", "HVAC", "HVAC", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineHVAC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "General Process Control", "Proc", "Proc", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineProc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Software Controllers", "SC", "SC", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineSC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Vacuum", "Vac", "Vac", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineVac = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Water Cooling", "WtrC", "WtrC", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineWtrC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DISCIPLINE, null, null,
                "Beam Magnets and Deflectors", "BMD", "BMD", 1,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        disciplineBMD = approvedStructureElement.getUuid();


        // ----------------------------------------------------------------------------------------------------

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineCryo,
                "empty", null, "Cryo", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupCryo = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineEMR,
                "empty", null, "EMR", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupEMR = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineHVAC,
                "empty", null, "HVAC", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupHVAC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineProc,
                "empty", null, "Proc", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupProc = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineSC,
                "empty", null, "SC", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupSC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineVac,
                "empty", null, "Vac", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupVac = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineWtrC,
                "empty", null, "WtrC", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupWtrC = approvedStructureElement.getUuid();

        structureElement = new StructureElement(
                Type.DEVICEGROUP, null, disciplineBMD,
                "empty", null, "BMD", 2,
                "empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
                null, "test who", "empty");
        approvedStructureElement = ITUtilNameStructureElement.assertCreateApprove(structureElement);
        deviceGroupBMD = approvedStructureElement.getUuid();
    }

    @Test
    public void createSubsystem() {
        // purpose
        //     test create and approve subsystem
    	//     all create, then all approve
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create in order to approve
    	//     all create first, all approve separately after all create

        try {
            StructureElement structureElement               = null;
            StructureElement createdStructureElement_010PRL = null;
            StructureElement createdStructureElement_010    = null;
            StructureElement createdStructureElement_N1U1   = null;
            StructureElement approvedStructureElement       = null;

        	structureElement = new StructureElement(
        			Type.SUBSYSTEM, null, systemRFQ,
        			"01 Phase Reference Line", "010PRL", "Acc-RFQ-010PRL", 3,
        			"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
        			null, "test who", "Approved by Daniel Piso");
        	createdStructureElement_010PRL = ITUtilNameStructureElement.assertCreate(structureElement);

        	structureElement = new StructureElement(
        			Type.SUBSYSTEM, null, systemRFQ,
        			"RFQ-010", "010", "Acc-RFQ-010", 3,
        			"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
        			null, "test who", "Approved by Daniel Piso");
        	createdStructureElement_010 = ITUtilNameStructureElement.assertCreate(structureElement);

        	structureElement = new StructureElement(
        			Type.SUBSYSTEM, null, systemRFQ,
        			"Power switch board 01", "N1U1", "Acc-RFQ-N1U1", 3,
        			"Electrical power cabinets", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
        			null, "test who", "Approved by Daniel Piso");
        	createdStructureElement_N1U1 = ITUtilNameStructureElement.assertCreate(structureElement);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_010PRL);
        	subsystem010PRL = approvedStructureElement.getUuid();

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_010);
        	subsystem010 = approvedStructureElement.getUuid();

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_N1U1);
        	subsystemN1U1 = approvedStructureElement.getUuid();

        	assertNotNull(subsystem010PRL);
        	assertNotNull(subsystem010);
        	assertNotNull(subsystemN1U1);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void createDeviceType() {
        // purpose
        //     test create and approve device type
    	//     all create, then all approve
        //
        // what - combination of
        //     create      create structures
        //     read        validate create
        //     read        validate approve
        //     patch       approve structures
        //
        // note
        //     create in order to approve
    	//     all create first, all approve separately after all create

        try {
            StructureElement structureElement                 = null;

            StructureElement createdStructureElement_Cryo_FS  = null;
            StructureElement createdStructureElement_Cryo_IOC = null;
            StructureElement createdStructureElement_Cryo_RFA = null;
            StructureElement createdStructureElement_Cryo_TT  = null;

            StructureElement createdStructureElement_EMR_FS   = null;
            StructureElement createdStructureElement_EMR_IOC  = null;
            StructureElement createdStructureElement_EMR_RFA  = null;
            StructureElement createdStructureElement_EMR_TT   = null;

            StructureElement createdStructureElement_HVAC_FS  = null;
            StructureElement createdStructureElement_HVAC_IOC = null;
            StructureElement createdStructureElement_HVAC_RFA = null;
            StructureElement createdStructureElement_HVAC_TT  = null;

            StructureElement createdStructureElement_Proc_FS  = null;
            StructureElement createdStructureElement_Proc_IOC = null;
            StructureElement createdStructureElement_Proc_RFA = null;
            StructureElement createdStructureElement_Proc_TT  = null;

            StructureElement createdStructureElement_SC_FS    = null;
            StructureElement createdStructureElement_SC_IOC   = null;
            StructureElement createdStructureElement_SC_RFA   = null;
            StructureElement createdStructureElement_SC_TT    = null;

            StructureElement createdStructureElement_Vac_FS   = null;
            StructureElement createdStructureElement_Vac_IOC  = null;
            StructureElement createdStructureElement_Vac_RFA  = null;
            StructureElement createdStructureElement_Vac_TT   = null;

            StructureElement createdStructureElement_WtrC_FS  = null;
            StructureElement createdStructureElement_WtrC_IOC = null;
            StructureElement createdStructureElement_WtrC_RFA = null;
            StructureElement createdStructureElement_WtrC_TT  = null;

            StructureElement createdStructureElement_BMD_FS   = null;
            StructureElement createdStructureElement_BMD_IOC  = null;
            StructureElement createdStructureElement_BMD_RFA  = null;
            StructureElement createdStructureElement_BMD_TT   = null;

            StructureElement approvedStructureElement         = null;

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupCryo,
            		"Flow Switch", "FS", "Cryo-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Cryo_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupCryo,
            		"Input Output Controller", "IOC", "Cryo-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Cryo_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupCryo,
            		"RF Antenna", "RFA", "Cryo-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Cryo_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupCryo,
            		"Temperature Transmitter", "TT", "Cryo-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Cryo_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupEMR,
            		"Input Output Controller", "IOC", "EMR-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_EMR_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupEMR,
            		"Flow Switch", "FS", "EMR-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_EMR_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupEMR,
            		"RF Antenna", "RFA", "EMR-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_EMR_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupEMR,
            		"Temperature Transmitter", "TT", "EMR-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_EMR_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupHVAC,
            		"Flow Switch", "FS", "HVAC-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_HVAC_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupHVAC,
            		"Input Output Controller", "IOC", "HVAC-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_HVAC_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupHVAC,
            		"RF Antenna", "RFA", "HVAC-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_HVAC_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupHVAC,
            		"Temperature Transmitter", "TT", "HVAC-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_HVAC_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupProc,
            		"Flow Switch", "FS", "Proc-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Proc_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupProc,
            		"Input Output Controller", "IOC", "Proc-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Proc_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupProc,
            		"RF Antenna", "RFA", "Proc-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Proc_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupProc,
            		"Temperature Transmitter", "TT", "Proc-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Proc_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupSC,
            		"Flow Switch", "FS", "SC-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_SC_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupSC,
            		"Input Output Controller", "IOC", "SC-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_SC_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupSC,
            		"RF Antenna", "RFA", "SC-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_SC_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupSC,
            		"Temperature Transmitter", "TT", "SC-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_SC_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupVac,
            		"Flow Switch", "FS", "Vac-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Vac_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupVac,
            		"Input Output Controller", "IOC", "Vac-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Vac_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupVac,
            		"RF Antenna", "RFA", "Vac-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Vac_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupVac,
            		"Temperature Transmitter", "TT", "Vac-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_Vac_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupWtrC,
            		"Flow Switch", "FS", "WtrC-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_WtrC_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupWtrC,
            		"Input Output Controller", "IOC", "WtrC-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_WtrC_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupWtrC,
            		"RF Antenna", "RFA", "WtrC-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_WtrC_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupWtrC,
            		"Temperature Transmitter", "TT", "WtrC-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_WtrC_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupBMD,
            		"Flow Switch", "FS", "BMD-FS", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_BMD_FS = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupBMD,
            		"Input Output Controller", "IOC", "BMD-IOC", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_BMD_IOC = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupBMD,
            		"RF Antenna", "RFA", "BMD-RFA", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_BMD_RFA = ITUtilNameStructureElement.assertCreate(structureElement);

            structureElement = new StructureElement(
            		Type.DEVICETYPE, null, deviceGroupBMD,
            		"Temperature Transmitter", "TT", "BMD-TT", 3,
            		"empty", Status.PENDING, Boolean.FALSE, Boolean.FALSE,
            		null, "test who", "empty");
            createdStructureElement_BMD_TT = ITUtilNameStructureElement.assertCreate(structureElement);

            // ----------------------------------------------------------------------------------------------------

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Cryo_FS);
        	deviceType_Cryo_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Cryo_IOC);
        	deviceType_Cryo_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Cryo_RFA);
        	deviceType_Cryo_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Cryo_TT);
        	deviceType_Cryo_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_Cryo_FS);
        	assertNotNull(deviceType_Cryo_IOC);
        	assertNotNull(deviceType_Cryo_RFA);
        	assertNotNull(deviceType_Cryo_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_EMR_FS);
        	deviceType_EMR_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_EMR_IOC);
        	deviceType_EMR_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_EMR_RFA);
        	deviceType_EMR_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_EMR_TT);
        	deviceType_EMR_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_EMR_FS);
        	assertNotNull(deviceType_EMR_IOC);
        	assertNotNull(deviceType_EMR_RFA);
        	assertNotNull(deviceType_EMR_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_HVAC_FS);
        	deviceType_HVAC_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_HVAC_IOC);
        	deviceType_HVAC_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_HVAC_RFA);
        	deviceType_HVAC_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_HVAC_TT);
        	deviceType_HVAC_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_HVAC_FS);
        	assertNotNull(deviceType_HVAC_IOC);
        	assertNotNull(deviceType_HVAC_RFA);
        	assertNotNull(deviceType_HVAC_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Proc_FS);
        	deviceType_Proc_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Proc_IOC);
        	deviceType_Proc_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Proc_RFA);
        	deviceType_Proc_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Proc_TT);
        	deviceType_Proc_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_Proc_FS);
        	assertNotNull(deviceType_Proc_IOC);
        	assertNotNull(deviceType_Proc_RFA);
        	assertNotNull(deviceType_Proc_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_SC_FS);
        	deviceType_SC_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_SC_IOC);
        	deviceType_SC_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_SC_RFA);
        	deviceType_SC_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_SC_TT);
        	deviceType_SC_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_SC_FS);
        	assertNotNull(deviceType_SC_IOC);
        	assertNotNull(deviceType_SC_RFA);
        	assertNotNull(deviceType_SC_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Vac_FS);
        	deviceType_Vac_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Vac_IOC);
        	deviceType_Vac_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Vac_RFA);
        	deviceType_Vac_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_Vac_TT);
        	deviceType_Vac_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_Vac_FS);
        	assertNotNull(deviceType_Vac_IOC);
        	assertNotNull(deviceType_Vac_RFA);
        	assertNotNull(deviceType_Vac_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_WtrC_FS);
        	deviceType_WtrC_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_WtrC_IOC);
        	deviceType_WtrC_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_WtrC_RFA);
        	deviceType_WtrC_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_WtrC_TT);
        	deviceType_WtrC_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_WtrC_FS);
        	assertNotNull(deviceType_WtrC_IOC);
        	assertNotNull(deviceType_WtrC_RFA);
        	assertNotNull(deviceType_WtrC_TT);

        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_BMD_FS);
        	deviceType_BMD_FS = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_BMD_IOC);
        	deviceType_BMD_IOC = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_BMD_RFA);
        	deviceType_BMD_RFA = approvedStructureElement.getUuid();
        	approvedStructureElement = ITUtilNameStructureElement.assertApprove(createdStructureElement_BMD_TT);
        	deviceType_BMD_TT = approvedStructureElement.getUuid();

        	assertNotNull(deviceType_BMD_FS);
        	assertNotNull(deviceType_BMD_IOC);
        	assertNotNull(deviceType_BMD_RFA);
        	assertNotNull(deviceType_BMD_TT);

            // possibly combine with readStructures
        } catch (Exception e) {
            fail();
        }
    }

}
