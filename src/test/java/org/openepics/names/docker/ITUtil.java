/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.util.response.Response;
import org.openepics.names.util.response.ResponseBoolean;
import org.openepics.names.util.response.ResponseBooleanList;

/**
 * Utility class to help (Docker) integration tests for Naming and PostgreSQL.
 *
 * @author Lars Johansson
 */
public class ITUtil {

    public static final String NAMING = "naming";

    static final String AUTH_USER     = "user:userPass";
    static final String AUTH_ADMIN    = "admin:adminPass";
    static final String EMPTY_JSON    = "[]";
    static final String HTTP          = "http://";
    static final String HEADER_JSON   = "'Content-Type: application/json'";

    static final String IP_PORT_NAMING = "127.0.0.1:8080";

    static final String API_V1_NAMES      = "/api/v1/names";
    static final String API_V1_STRUCTURES = "/api/v1/structures";

    static final String HTTP_IP_PORT_NAMING                              = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING;

    static final String HTTP_IP_PORT_NAMING_API_V1_NAMES                 = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING + API_V1_NAMES;
    static final String HTTP_AUTH_USER_IP_PORT_NAMING_API_V1_NAMES       = ITUtil.HTTP + ITUtil.AUTH_USER  + "@" + ITUtil.IP_PORT_NAMING + API_V1_NAMES;
    static final String HTTP_AUTH_ADMIN_IP_PORT_NAMING_API_V1_NAMES      = ITUtil.HTTP + ITUtil.AUTH_ADMIN + "@" + ITUtil.IP_PORT_NAMING + API_V1_NAMES;

    static final String HTTP_IP_PORT_NAMING_API_V1_STRUCTURES            = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING + API_V1_STRUCTURES;
    static final String HTTP_AUTH_USER_IP_PORT_NAMING_API_V1_STRUCTURES  = ITUtil.HTTP + ITUtil.AUTH_USER  + "@" + ITUtil.IP_PORT_NAMING + API_V1_STRUCTURES;
    static final String HTTP_AUTH_ADMIN_IP_PORT_NAMING_API_V1_STRUCTURES = ITUtil.HTTP + ITUtil.AUTH_ADMIN + "@" + ITUtil.IP_PORT_NAMING + API_V1_STRUCTURES;

    private static final String BRACKET_BEGIN     = "[";
    private static final String BRACKET_END       = "]";
    private static final String CURLY_BRACE_BEGIN = "{";
    private static final String CURLY_BRACE_END   = "}";
    private static final String HTTP_REPLY        = "HTTP";

    /**
     * This class is not to be instantiated.
     */
    private ITUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Do GET request with given string as URL and return response code.
     *
     * @param spec string to parse as URL
     * @return response code
     *
     * @throws IOException
     */
    static int doGet(String spec) throws IOException {
        URL url = new URL(spec);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        return con.getResponseCode();
    }

    /**
     * Do GET request with given string as URL and return response with string array with response code and response string.
     *
     * @param spec string to parse as URL
     * @return string array with response code and response string
     *
     * @throws IOException
     */
    static String[] doGetJson(String spec) throws IOException {
        URL url = new URL(spec);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        int responseCode = con.getResponseCode();

        String line;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = responseCode == HttpURLConnection.HTTP_OK
                ? new BufferedReader(new InputStreamReader(con.getInputStream()))
                : new BufferedReader(new InputStreamReader(con.getErrorStream()))) {
            while((line = br.readLine()) != null) {
                sb.append(line);
            }
        }

        return new String[] {String.valueOf(responseCode), sb.toString().trim()};
    }

    /**
     * Run a shell command and return response with string array with response code and response string.
     *
     * @param command shell command
     * @return string array with response code and response string
     *
     * @throws IOException
     * @throws InterruptedException
     * @throws Exception
     */
    static String[] runShellCommand(String command) throws IOException, InterruptedException, Exception {
        // run shell command & return http response code if available

        final ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);

        String responseCode = null;
        String responseContent = null;
        try {
            final Process process = processBuilder.start();
            final BufferedReader errorStream = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            final BufferedReader inputStream = new BufferedReader(new InputStreamReader(process.getInputStream()));
            final boolean processFinished = process.waitFor(30, TimeUnit.SECONDS);

            String line = null;
            while ((line = inputStream.readLine()) != null) {
                if (line.startsWith(HTTP_REPLY)) {
                    // response code, e.g. "HTTP/1.1 200", "HTTP/1.1 401", "HTTP/1.1 500"
                    String[] s = line.trim().split(" ");
                    if (s != null && s.length == 2) {
                        responseCode = s[1];
                    }
                } else if ((line.startsWith(BRACKET_BEGIN) && line.endsWith(BRACKET_END))
                        || (line.startsWith(CURLY_BRACE_BEGIN) && line.endsWith(CURLY_BRACE_END))) {
                    // response string, json
                    responseContent = line;
                }
            }

            if (!processFinished) {
                throw new Exception("Timed out waiting to execute command: " + command);
            }
            if (process.exitValue() != 0) {
                throw new Exception(
                        String.format("Shell command finished with status %d error: %s",
                                process.exitValue(),
                                errorStream.lines().collect(Collectors.joining())));
            }
        } catch (IOException | InterruptedException e) {
            throw e;
        }
        return new String[] {responseCode, responseContent};
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Assert that response object is as expected, an array with 2 elements
     * of which first contains response code OK (200).
     *
     * @param response string array with response of http request, response code and content
     *
     * @see HttpURLConnection#HTTP_OK
     */
    static void assertResponseLength2CodeOK(String[] response) {
        assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
    }

    /**
     * Assert that response object is as expected, an array with 2 elements
     * of which first element contains given response code.
     *
     * @param response string array with response of http request, response code and content
     * @param responseCode expected response code
     *
     * @see HttpURLConnection for available response codes
     */
    static void assertResponseLength2Code(String[] response, int responseCode) {
        assertNotNull(response);
        assertEquals(2, response.length);
        assertEquals(responseCode, Integer.parseInt(response[0]));
    }

    /**
     * Assert that response object is as expected, an array with 2 elements
     * of which first element contains response code OK (200) and second element contains given response content.
     *
     * @param response string array with response of http request, response code and content
     * @param responseContent expected response content
     *
     * @see HttpURLConnection#HTTP_OK
     */
    static void assertResponseLength2CodeOKContent(String[] response, String responseContent) {
        assertResponseLength2CodeContent(response, HttpURLConnection.HTTP_OK, responseContent);
    }

    /**
     * Assert that response object is as expected, an array with 2 elements
     * of which first element contains given response code and second element contains given response content.
     *
     * @param response string array with response of http request, response code and content
     * @param responseCode expected response code
     * @param responseContent expected response content
     *
     * @see HttpURLConnection for available response codes
     */
    static void assertResponseLength2CodeContent(String[] response, int responseCode, String responseContent) {
        assertResponseLength2Code(response, responseCode);
        assertEquals(responseContent, response[1]);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Assert response available with empty message.
     *
     * @param actual response object
     */
    static void assertMessageEmpty(Response actual) {
        assertMessageEmpty(actual, Boolean.TRUE);
    }
    /**
     * Assert response available with non-empty message.
     *
     * @param actual response object
     */
    static void assertMessageNotEmpty(Response actual) {
        assertMessageEmpty(actual, Boolean.FALSE);
    }
    /**
     * Assert response available with empty message.
     *
     * @param actual response object
     * @param expectedMessageEmpty expected message behavior
     */
    private static void assertMessageEmpty(Response actual, Boolean expectedMessageEmpty) {
        assertNotNull(actual);
        assertEquals(StringUtils.isEmpty(actual.getMessage()), expectedMessageEmpty);
    }

    /**
     * Assert response boolean available with expected response.
     *
     * @param actual response boolean object
     * @param expectedResponse expected response
     */
    static void assertEqualsResponseBoolean(ResponseBoolean actual, Boolean expectedResponse) {
        assertNotNull(actual);
        assertEquals(expectedResponse, actual.getResponse());
    }
    /**
     * Assert response boolean available with empty message.
     *
     * @param actual response boolean object
     * @param expectedResponse expected message behavior
     */
    static void assertEqualsResponseBooleanMessageEmpty(ResponseBoolean actual, Boolean expectedResponse) {
        assertEqualsResponseBooleanMessageEmpty(actual, expectedResponse, Boolean.TRUE);
    }
    /**
     * Assert response boolean available with non-empty message.
     *
     * @param actual response boolean object
     * @param expectedResponse expected message behavior
     */
    static void assertEqualsResponseBooleanMessageNotEmpty(ResponseBoolean actual, Boolean expectedResponse) {
        assertEqualsResponseBooleanMessageEmpty(actual, expectedResponse, Boolean.FALSE);
    }
    /**
     * Assert response boolean available with expected response and expected message behavior.
     *
     * @param actual response boolean object
     * @param expectedResponse expected response
     * @param expectedMessageEmpty expected message behavior
     */
    private static void assertEqualsResponseBooleanMessageEmpty(ResponseBoolean actual, Boolean expectedResponse, Boolean expectedMessageEmpty) {
        assertEqualsResponseBoolean(actual, expectedResponse);
        assertEquals(StringUtils.isEmpty(actual.getMessage()), expectedMessageEmpty);
    }

    /**
     * Assert response boolean list available with expected response.
     *
     * @param actual response boolean list object
     * @param expectedResponse expected response
     */
    static void assertEqualsResponseBoolean(ResponseBooleanList actual, Boolean expectedResponse) {
        assertNotNull(actual);
        assertEquals(expectedResponse, actual.getResponse());
    }
    /**
     * Assert response boolean list available with empty message.
     *
     * @param actual response boolean list object
     * @param expectedResponse expected message behavior
     */
    static void assertEqualsResponseBooleanMessageEmpty(ResponseBooleanList actual, Boolean expectedResponse) {
        assertEqualsResponseBooleanMessageEmpty(actual, expectedResponse, Boolean.TRUE);
    }
    /**
     * Assert response boolean list available with non-empty message.
     *
     * @param actual response boolean list object
     * @param expectedResponse expected message behavior
     */
    static void assertEqualsResponseBooleanMessageNotEmpty(ResponseBooleanList actual, Boolean expectedResponse) {
        assertEqualsResponseBooleanMessageEmpty(actual, expectedResponse, Boolean.FALSE);
    }
    /**
     * Assert response boolean list available with expected response and expected message behavior.
     *
     * @param actual response boolean list object
     * @param expectedResponse expected response
     * @param expectedMessageEmpty expected message behavior
     */
    private static void assertEqualsResponseBooleanMessageEmpty(ResponseBooleanList actual, Boolean expectedResponse, Boolean expectedMessageEmpty) {
        assertEqualsResponseBoolean(actual, expectedResponse);
        assertEquals(StringUtils.isEmpty(actual.getMessage()), expectedMessageEmpty);
    }

    // ----------------------------------------------------------------------------------------------------

    // enum for http methods
    static enum MethodChoice        {POST, GET, PUT, DELETE, PATCH};

    // enum for different authorizations
    static enum AuthorizationChoice {NONE, USER, ADMIN};

    // enum for different endpoints
    static enum EndpointChoice      {NAMES, STRUCTURES};

    /**
     * Utility method to return curl for POST (create information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for POST property (create information) with path and json
     */
    static String curlPostPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.POST, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for GET (get information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for GET property (get information) with path and json
     */
    static String curlGetPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.GET, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for PUT (update information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for PUT property (update information) with path and json
     */
    static String curlPutPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.PUT, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for DELETE (delete information) with path and json
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for DELETE property (delete information) with path
     */
    static String curlDeletePathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.DELETE, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for PATCH (patch information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return
     */
    static String curlPatchPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.PATCH, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Prepare curl command for test to run for contacting server.
     *
     * @param methodChoice method choice
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl command to run
     */
    private static String curlMethodAuthEndpointPathJson(MethodChoice methodChoice, AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        String pathstr = !StringUtils.isEmpty(path)
                ? path
                : "";

        String data = !StringUtils.isEmpty(json)
                ? " -d '" + json + "'"
                : "";

        return "curl"
            + " -H " + ITUtil.HEADER_JSON
            + " -X"  + ITUtil.getMethodString(methodChoice)
            + " -i "
            + ITUtil.HTTP
            + ITUtil.getAuthorizationString(authorizationChoice)
            + ITUtil.IP_PORT_NAMING
            + ITUtil.getEndpointString(endpointChoice)
            + pathstr
            + data;
    }

    /**
     * Utility method to return string for http method. To be used when constructing url to send query to server.
     *
     * @param methodChoice method choice, i.e. POST, GET, PUT, DELETE, PATCH
     * @return string for http method
     */
    private static String getMethodString(MethodChoice methodChoice) {
        switch (methodChoice) {
        case POST:
            return "POST";
        case GET:
            return "GET";
        case PUT:
            return "PUT";
        case PATCH:
            return "PATCH";
        case DELETE:
            return "DELETE";
        default:
            return "GET";
        }
    }

    /**
     * Utility method to return string for authorization. To be used when constructing url to send query to server.
     *
     * @param authorizationChoice authorization choice
     * @return string for authorization
     */
    private static String getAuthorizationString(AuthorizationChoice authorizationChoice) {
        switch (authorizationChoice) {
        case ADMIN:
            return ITUtil.AUTH_ADMIN  + "@";
        case USER:
            return ITUtil.AUTH_USER  + "@";
        case NONE:
            return StringUtils.EMPTY;
        default:
            return StringUtils.EMPTY;
        }
    }

    /**
     * Utility method to return string for endpoint. To be used when constructing url to send query to server.
     *
     * @param endpointChoice endpoint choice
     * @return string for endpoint
     */
    private static String getEndpointString(EndpointChoice endpointChoice) {
        switch (endpointChoice) {
        case NAMES:
            return ITUtil.API_V1_NAMES;
        case STRUCTURES:
            return ITUtil.API_V1_STRUCTURES;
        default:
            return StringUtils.EMPTY;
        }
    }

}
