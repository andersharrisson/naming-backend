/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.StructureElement;

/**
 * Unit tests for StructureElementUtil class.
 *
 * @author Lars Johansson
 *
 * @see StructureElementUtil
 */
public class StructureElementUtilTest {

    /**
     * Test of get structure element for content.
     */
    @Test
    public void getStructureElementContent() {
        StructureElement structureElement = StructureElementUtil.getStructureElement(
                null,
                null,
                null,
                null, null, null, null,
                null, null, null, null,
                null, null, null);

        assertNotNull(structureElement);
        assertEquals(null, structureElement.getType());
        assertEquals(null, structureElement.getUuid());
        assertEquals(null, structureElement.getParent());
        assertEquals(null, structureElement.getName());
        assertEquals(null, structureElement.getMnemonic());
        assertEquals(null, structureElement.getMnemonicpath());
        assertEquals(null, structureElement.getLevel());
        assertEquals(null, structureElement.getDescription());
        assertEquals(null, structureElement.getStatus());
        assertEquals(null, structureElement.isLatest());
        assertEquals(null, structureElement.isDeleted());
        assertEquals(null, structureElement.getWhen());
        assertEquals(null, structureElement.getWho());
        assertEquals(null, structureElement.getComment());
    }

    @Test
    public void equals() {
        StructureElement se1 = new StructureElement();
        StructureElement se2 = new StructureElement();

        //        Field[] fields = se1.getClass().getFields();
        //        Field[] declaredFields = se1.getClass().getDeclaredFields();
        //
        //        Field[] superclass_fields = se1.getClass().getSuperclass().getFields();
        //        Field[] superclass_declaredFields = se1.getClass().getSuperclass().getDeclaredFields();
        //
        //        for (Field field : fields) {
        //            System.out.println("fields.field.getName:                    " + field.getName());
        //        }
        //        System.out.println("-----------------------------------------");
        //        for (Field field : declaredFields) {
        //            System.out.println("declaredFields.field.getName:            " + field.getName());
        //        }
        //        System.out.println("-----------------------------------------");
        //        for (Field field : superclass_fields) {
        //            System.out.println("superclass_fields.field.getName:         " + field.getName());
        //        }
        //        // LOGGER.log(Level.INFO, "-----------------------------------------");
        //        System.out.println("-----------------------------------------");
        //        for (Field field : superclass_declaredFields) {
        //            System.out.println("superclass_declaredFields.field.getName: " + field.getName());
        //        }

        assertTrue(se1.equals(se2));
    }

}
