/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.repository.model.Name;

/**
 * Unit tests for NamingUtil class.
 *
 * @author Lars Johansson
 *
 * @see NameUtil
 */
public class NameUtilTest {

    /**
     * Test of get level of system structure for name.
     */
    @Test
    public void getLevelSystemStructure() {
        Name name = new Name();
        assertEquals(-1, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(UUID.randomUUID());
        name.setSystemUuid(null);
        name.setSystemgroupUuid(null);
        assertEquals(3, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(null);
        name.setSystemUuid(UUID.randomUUID());
        name.setSystemgroupUuid(null);
        assertEquals(2, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(UUID.randomUUID());
        name.setSystemUuid(UUID.randomUUID());
        name.setSystemgroupUuid(null);
        assertEquals(-1, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(null);
        name.setSystemUuid(null);
        name.setSystemgroupUuid(UUID.randomUUID());
        assertEquals(1, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(UUID.randomUUID());
        name.setSystemUuid(null);
        name.setSystemgroupUuid(UUID.randomUUID());
        assertEquals(-1, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(null);
        name.setSystemUuid(UUID.randomUUID());
        name.setSystemgroupUuid(UUID.randomUUID());
        assertEquals(-1, NameUtil.getLevelSystemStructure(name));

        name.setSubsystemUuid(UUID.randomUUID());
        name.setSystemUuid(UUID.randomUUID());
        name.setSystemgroupUuid(UUID.randomUUID());
        assertEquals(-1, NameUtil.getLevelSystemStructure(name));
    }

    /**
     * Test of get level of device structure for name.
     */
    @Test
    public void getLevelDeviceStructure() {
        Name name = new Name();
        assertEquals(-1, NameUtil.getLevelDeviceStructure(name));

        name.setDevicetypeUuid(UUID.randomUUID());
        assertEquals(3, NameUtil.getLevelDeviceStructure(name));
    }

}
