/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.rest.beans.Type;
import org.springframework.http.HttpStatus;

import com.google.common.collect.Lists;

/**
 * Unit tests for ValidateUtil class.
 *
 * @author Lars Johansson
 *
 * @see ValidateUtil
 */
public class ValidateUtilTest {

    /**
     * Test of validate input comment.
     */
    @Test
    public void validateInputCommentNull() {
        try {
            ValidateUtil.validateInputComment(null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("comment is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input comment.
     */
    @Test
    public void validateInputCommentEmpty() {
        try {
            ValidateUtil.validateInputComment("");
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("comment is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input comment.
     */
    @Test
    public void validateInputComment() {
        ValidateUtil.validateInputComment("asdf");
    }

    /**
     * Test of validate input description.
     */
    @Test
    public void validateInputDescriptionNull() {
        try {
            ValidateUtil.validateInputDescription(null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("description is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input description.
     */
    @Test
    public void validateInputDescriptionEmpty() {
        try {
            ValidateUtil.validateInputDescription("");
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("description is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input description.
     */
    @Test
    public void validateInputDescription() {
        ValidateUtil.validateInputDescription("asdf");
    }

    /**
     * Test of validate input mnemonic.
     */
    @Test
    public void validateInputMnemonicNull() {
        try {
            ValidateUtil.validateInputMnemonic(null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("mnemonic is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input mnemonic.
     */
    @Test
    public void validateInputMnemonicEmpty() {
        try {
            ValidateUtil.validateInputMnemonic("");
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("mnemonic is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input mnemonic.
     */
    @Test
    public void validateInputMnemonic() {
        ValidateUtil.validateInputMnemonic("asdf");
    }

    /**
     * Test of validate input name.
     */
    @Test
    public void validateInputNameNull() {
        try {
            ValidateUtil.validateInputName(null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("name is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input name.
     */
    @Test
    public void validateInputNameEmpty() {
        try {
            ValidateUtil.validateInputName("");
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("name is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input name.
     */
    @Test
    public void validateInputName() {
        ValidateUtil.validateInputName("asdf");
    }

    /**
     * Test of validate input type.
     */
    @Test
    public void validateInputTypeNull() {
        try {
            ValidateUtil.validateInputType(null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("type is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input type.
     */
    @Test
    public void validateInputType() {
        ValidateUtil.validateInputType(Type.SYSTEMGROUP);
        ValidateUtil.validateInputType(Type.SYSTEM);
        ValidateUtil.validateInputType(Type.SUBSYSTEM);
        ValidateUtil.validateInputType(Type.DISCIPLINE);
        ValidateUtil.validateInputType(Type.DEVICEGROUP);
        ValidateUtil.validateInputType(Type.DEVICETYPE);
    }

    /**
     * Test of validate input uuid.
     */
    @Test
    public void validateInputUuidNull() {
        try {
            ValidateUtil.validateInputUuid(null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("uuid is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input uuid.
     */
    @Test
    public void validateInputUuidEmpty() {
        try {
            ValidateUtil.validateInputUuid("");
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("uuid is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input uuid.
     */
    @Test
    public void validateInputUuid() {
        ValidateUtil.validateInputUuid(UUID.randomUUID().toString());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of validate input read names.
     */
    @Test
    public void validateInputReadNames() {
        ValidateUtil.validateNamesInputRead(
                null, null, null,
                null,
                null, null,
                null, null);
    }

    /**
     * Test of validate input create name element.
     */
    @Test
    public void validateInputCreateNameElement() {
        ValidateUtil.validateNameElementInputCreate(null);
    }

    /**
     * Test of validate data create name element.
     */
    @Test
    public void validateDataCreateNameElement() {
        ValidateUtil.validateNameElementDataCreate(null, null, null, null, null);
    }

    /**
     * Test of validate data create name.
     */
    @Test
    public void validateDataCreateName() {
        ValidateUtil.validateNameDataCreate(null, null, null, null, null);
    }
    /**
     * Test of validate input update name element.
     */
    @Test
    public void validateInputUpdateNameElement() {
        ValidateUtil.validateNameElementInputUpdate(null);
    }

    /**
     * Test of validate data update name element.
     */
    @Test
    public void validateDataUpdateNameElement() {
        ValidateUtil.validateNameElementDataUpdate(null, null, null, null, null);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of validate input read structures.
     */
    @Test
    public void validateInputReadStructuresTypeNull() {
        try {
            ValidateUtil.validateStructuresInputRead(
                    null, null, null, null, null,
                    null,
                    null, null,
                    null, null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("type is not available", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }

    /**
     * Test of validate input read structures.
     */
    @Test
    public void validateInputReadStructures() {
        ValidateUtil.validateStructuresInputRead(
                Type.SYSTEMGROUP, null, null, null, null,
                null,
                null, null,
                null, null);
        ValidateUtil.validateStructuresInputRead(
                Type.SYSTEM, null, null, null, null,
                null,
                null, null,
                null, null);
        ValidateUtil.validateStructuresInputRead(
                Type.SUBSYSTEM, null, null, null, null,
                null,
                null, null,
                null, null);
        ValidateUtil.validateStructuresInputRead(
                Type.DISCIPLINE, null, null, null, null,
                null,
                null, null,
                null, null);
        ValidateUtil.validateStructuresInputRead(
                Type.DEVICEGROUP, null, null, null, null,
                null,
                null, null,
                null, null);
        ValidateUtil.validateStructuresInputRead(
                Type.DEVICETYPE, null, null, null, null,
                null,
                null, null,
                null, null);
    }

    /**
     * Test of validate data create structure element.
     */
    @Test
    public void validateDataCreateStructureElement() {
        ValidateUtil.validateStructureElementDataCreate(null, null, null, null);
    }

    /**
     * Test of validate data create structure.
     */
    @Test
    public void validateDataCreateStructuret() {
        ValidateUtil.validateStructureDataCreate(null, null, null, null, null);
    }

    /**
     * Test of validate input update structure element.
     */
    @Test
    public void validateInputUpdateStructureElement() {
        ValidateUtil.validateStructureElementInputUpdate(null, null);
    }

    /**
     * Test of validate data update structure element.
     */
    @Test
    public void validateDataUpdateStructureElement() {
        ValidateUtil.validateStructureElementDataUpdate(null, null, null, null);
    }

    @Test
    public void validateDummy() {
        final List<Subsystem> subsystems = Lists.newArrayList();
        try {
            ValidateUtil.validateCondition(subsystems != null && subsystems.size() == 1, HttpStatus.INTERNAL_SERVER_ERROR,
                    ValidateUtil.SUBSYSTEM + ValidateUtil.SPACE + ValidateUtil.IS_NOT_CORRECT, null);
            fail();
        } catch (ServiceHttpStatusException e) {
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getHttpStatus());
            assertEquals("subsystem is not correct", e.getMessage());
            assertEquals(null, e.getCause());
        }
    }

}
