/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.openepics.names.repository.model.Discipline;

/**
 * Unit tests for NamingConventionUtil class.
 *
 * @author Lars Johansson
 *
 * @see NamingConventionUtil
 */
public class NamingConventionUtilTest {

    // conventionname_abc_def
    //     a - 1/0 - system group   or not
    //     b - 1/0 - system         or not
    //     c - 1/0 - subsystem      or not
    //     d - 1/0 - discipline     or not
    //     e - 1/0 - device type    or not
    //     f - 1/0 - instance index or not

    private static final String NULL  = null;
    private static final String EMPTY = "";
    private static final String DUMMY = "DUMMY";

    private static final String ACC           = "Acc";
    private static final String LEBT          = "LEBT";
    private static final String ZERO_ONE_ZERO = "010";

    private static final String CONVENTIONNAME_010             = "LEBT";
    private static final String CONVENTIONNAME_011             = "LEBT-010";
    private static final String CONVENTIONNAME_111             = "Acc-LEBT-010";

    private static final String CONVENTIONNAME_010_111         = "CWL:WtrC-EC-003";
    private static final String CONVENTIONNAME_011_110         = "LEBT-010:PwrC-PSChop";
    private static final String CONVENTIONNAME_011_111         = "CWL-CWSE05:WtrC-EC-003";
    private static final String CONVENTIONNAME_111_110         = "Acc-LEBT-010:PwrC-PSChop";
    private static final String CONVENTIONNAME_111_111         = "DUMMY-CWL-CWSE05:WtrC-EC-003";

    private static final String CONVENTIONNAME_011_111_SC1     = "CWL-CWSE05:SC-EC-003";
    private static final String CONVENTIONNAME_011_111_SC2     = "CWL-CWSE05:SC-IOC-003";

    private static final String CONVENTIONNAME_011_111_EQCLASS = "CW1-CWSE5:WTRC-EC-3";
    private static final String CONVENTIONNAME_111_111_EQCLASS = "DUMMY-CW1-CWSE5:WTRC-EC-3";

    /**
     * Test of extraction of system group from convention name.
     */
    @Test
    public void extractSystemGroup() {
        assertNull(NamingConventionUtil.extractSystemGroup(NULL));
        assertNull(NamingConventionUtil.extractSystemGroup(EMPTY));
        assertNull(NamingConventionUtil.extractSystemGroup(DUMMY));

        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011));

        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_110));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111));
        assertEquals(ACC,       NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_111_110));
        assertEquals(DUMMY,     NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_111_111));

        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111_SC1));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111_SC2));

        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals(DUMMY,     NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of system from convention name.
     */
    @Test
    public void extractSystem() {
        assertNull(NamingConventionUtil.extractSystem(NULL));
        assertNull(NamingConventionUtil.extractSystem(EMPTY));
        assertEquals(DUMMY,     NamingConventionUtil.extractSystem(DUMMY));

        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_010));
        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_011));

        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_010_111));
        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_011_110));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111));
        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_111_110));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_111_111));

        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111_SC1));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111_SC2));

        assertEquals("CW1",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("CW1",     NamingConventionUtil.extractSystem(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of subsystem group from convention name.
     */
    @Test
    public void extractSubsystem() {
        assertNull(NamingConventionUtil.extractSubsystem(NULL));
        assertNull(NamingConventionUtil.extractSubsystem(EMPTY));
        assertNull(NamingConventionUtil.extractSubsystem(DUMMY));

        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(CONVENTIONNAME_010));
        assertEquals("010",     NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011));

        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(CONVENTIONNAME_010_111));
        assertEquals("010",     NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_110));
        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111));
        assertEquals("010",     NamingConventionUtil.extractSubsystem(CONVENTIONNAME_111_110));
        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_111_111));

        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111_SC1));
        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111_SC2));

        assertEquals("CWSE5",   NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("CWSE5",   NamingConventionUtil.extractSubsystem(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of discipline from convention name.
     */
    @Test
    public void extractDiscipline() {
        assertNull(NamingConventionUtil.extractDiscipline(NULL));
        assertNull(NamingConventionUtil.extractDiscipline(EMPTY));
        assertNull(NamingConventionUtil.extractDiscipline(DUMMY));

        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011));

        assertEquals("WtrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010_111));
        assertEquals("PwrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_110));
        assertEquals("WtrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111));
        assertEquals("PwrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_110));
        assertEquals("WtrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_111));

        assertEquals("SC",      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC1));
        assertEquals("SC",      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC2));

        assertEquals("WTRC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("WTRC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of device type from convention name.
     */
    @Test
    public void extractDeviceType() {
        assertNull(NamingConventionUtil.extractDeviceType(NULL));
        assertNull(NamingConventionUtil.extractDeviceType(EMPTY));
        assertNull(NamingConventionUtil.extractDeviceType(DUMMY));

        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011));

        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_010_111));
        assertEquals("PSChop",  NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_110));
        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111));
        assertEquals("PSChop",  NamingConventionUtil.extractDeviceType(CONVENTIONNAME_111_110));
        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_111_111));

        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111_SC1));
        assertEquals("IOC",     NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111_SC2));

        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of instance index from convention name.
     */
    @Test
    public void extractInstanceIndex() {
        assertNull(NamingConventionUtil.extractInstanceIndex(NULL));
        assertNull(NamingConventionUtil.extractInstanceIndex(EMPTY));
        assertNull(NamingConventionUtil.extractInstanceIndex(DUMMY));

        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011));

        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_110));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_111_110));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_111_111));

        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111_SC1));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111_SC2));

        assertEquals("3",       NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("3",       NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of mnemonic path for system structure from convention name.
     */
    @Test
    public void extractMnemonicPathSystemStructure() {
        assertEquals(NULL,               NamingConventionUtil.extractMnemonicPathSystemStructure(NULL));
        assertEquals(NULL,               NamingConventionUtil.extractMnemonicPathSystemStructure(EMPTY));
        assertEquals(DUMMY,              NamingConventionUtil.extractMnemonicPathSystemStructure(DUMMY));

        assertEquals(CONVENTIONNAME_010, NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_010));
        assertEquals(CONVENTIONNAME_011, NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011));
        assertEquals(CONVENTIONNAME_111, NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_111));

        assertEquals("CWL",              NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_010_111));
        assertEquals("LEBT-010",         NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_110));
        assertEquals("CWL-CWSE05",       NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111));
        assertEquals("Acc-LEBT-010",     NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_111_110));
        assertEquals("DUMMY-CWL-CWSE05", NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_111_111));

        assertEquals("CWL-CWSE05",       NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111_SC1));
        assertEquals("CWL-CWSE05",       NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111_SC2));

        assertEquals("CW1-CWSE5",        NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("DUMMY-CW1-CWSE5",  NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test of extraction of mnemonic path for device structure from convention name.
     */
    @Test
    public void extractMnemonicPathDeviceStructure() {
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(NULL));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(EMPTY));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(DUMMY));

        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111));

        assertEquals("WtrC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010_111));
        assertEquals("PwrC-PSChop", NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_110));
        assertEquals("WtrC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111));
        assertEquals("PwrC-PSChop", NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_110));
        assertEquals("WtrC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_111));

        assertEquals("SC-EC",       NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC1));
        assertEquals("SC-IOC",      NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC2));

        assertEquals("WTRC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals("WTRC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test to get index style from convention name.
     */
    @Test
    public void getInstanceIndexStyleConventionName() {
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle("Mech-TDS"));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle("Cryo-TDS"));

        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(NULL));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(EMPTY));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(DUMMY));

        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_010));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_111));

        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_010_111));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_110));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_111_110));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_111_111));

        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111_SC1));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111_SC2));

        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111_EQCLASS));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test to get index style from discipline.
     */
    @Test
    public void getInstanceIndexStyleDiscipline() {
        Discipline discipline = new Discipline();

        discipline.setMnemonic(NULL);
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic(EMPTY);
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic(DUMMY);
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));

        discipline.setMnemonic(ACC);
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic(LEBT);
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic(ZERO_ONE_ZERO);
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));

        discipline.setMnemonic("Mech");
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic("Cryo");
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic("SC");
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID, NamingConventionUtil.getInstanceIndexStyle(discipline));
    }

    /**
     * Test to check if discipline is PID.
     */
    @Test
    public void isDisciplinePID() {
        assertFalse(NamingConventionUtil.isDisciplinePID(NULL));
        assertFalse(NamingConventionUtil.isDisciplinePID(EMPTY));
        assertFalse(NamingConventionUtil.isDisciplinePID(DUMMY));

        assertFalse(NamingConventionUtil.isDisciplinePID("Mech"));
        assertTrue (NamingConventionUtil.isDisciplinePID("Cryo"));

        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111)));

        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010_111)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_110)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_110)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_111)));

        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC1)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC2)));

        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_EQCLASS)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_111_EQCLASS)));
    }

    /**
     * Test to check if mnemonic path (device structure) is PID numeric.
     */
    @Test
    public void isMnemonicPathPIDNumeric() {
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NULL));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(EMPTY));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(DUMMY));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric("Mech"));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric("Cryo"));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111)));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010_111)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_110)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_110)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_111)));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC1)));
        assertTrue (NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC2)));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_EQCLASS)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_111_EQCLASS)));
    }

    /**
     * Test to check if convention name is offsite.
     */
    @Test
    public void isOffsite() {
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_010));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011));

        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_010_111));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_110));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111));
        assertTrue (NamingConventionUtil.isOffsite(CONVENTIONNAME_111_110));
        assertTrue (NamingConventionUtil.isOffsite(CONVENTIONNAME_111_111));

        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111_SC1));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111_SC2));

        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111_EQCLASS));
        assertTrue (NamingConventionUtil.isOffsite(CONVENTIONNAME_111_111_EQCLASS));
    }

    /**
     * Test to check conversion of string (e.g. name) to mnemonic path.
     */
    @Test
    public void string2MnemonicPath() {
        String[] array = null;

        array = NamingConventionUtil.string2MnemonicPath(null);
        assertNull(array);

        array = NamingConventionUtil.string2MnemonicPath("");
        assertNotNull(array);
        assertEquals(0, array.length);

        array = NamingConventionUtil.string2MnemonicPath("A");
        assertNotNull(array);
        assertEquals(1, array.length);
        assertEquals("A", array[0]);

        array = NamingConventionUtil.string2MnemonicPath("A-B");
        assertNotNull(array);
        assertEquals(2, array.length);
        assertEquals("A", array[0]);
        assertEquals("B", array[1]);

        array = NamingConventionUtil.string2MnemonicPath("A-B-C");
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals("A", array[0]);
        assertEquals("B", array[1]);
        assertEquals("C", array[2]);
    }
}
