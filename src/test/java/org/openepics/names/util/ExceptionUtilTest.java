/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Unit tests for ExceptionUtil class.
 *
 * @author Lars Johansson
 *
 * @see ExceptionUtil
 */
public class ExceptionUtilTest {

    /**
     * Test of create response status exception.
     */
    @Test
    public void createResponseStatusException() {
        ResponseStatusException exception = ExceptionUtil.createResponseStatusException(HttpStatus.I_AM_A_TEAPOT, null, null);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, exception.getStatus());
        assertNull  (exception.getReason());
        assertNull  (exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionBadRequest();
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionUnauthorized();
        assertEquals(HttpStatus.UNAUTHORIZED, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionForbidden();
        assertEquals(HttpStatus.FORBIDDEN, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionNotFound();
        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionConflict();
        assertEquals(HttpStatus.CONFLICT, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionInternalServerError();
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createResponseStatusExceptionNotImplemented();
        assertEquals(HttpStatus.NOT_IMPLEMENTED, exception.getStatus());
        assertEquals(ExceptionUtil.OPERATION_COULD_NOT_BE_PERFORMED, exception.getReason());
        assertEquals(null, exception.getCause());
    }

    /**
     * Test of create service http status exception.
     */
    @Test
    public void createServiceHttpStatusException() {
        ServiceHttpStatusException exception = ExceptionUtil.createServiceHttpStatusException(null, null, null, null, null);
        assertNull(exception.getHttpStatus());
        assertNull(exception.getMessage());
        assertNull(exception.getDetails());
        assertNull(exception.getUserFriendly());
        assertNull(exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionBadRequest(null, null, null);
        assertEquals(HttpStatus.BAD_REQUEST, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionUnauthorized(null, null, null);
        assertEquals(HttpStatus.UNAUTHORIZED, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionForbidden(null, null, null);
        assertEquals(HttpStatus.FORBIDDEN, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionNotFound(null, null, null);
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionConflict(null, null, null);
        assertEquals(HttpStatus.CONFLICT, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionInternalServerError(null, null, null);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());

        exception = ExceptionUtil.createServiceHttpStatusExceptionNotImplemented(null, null, null);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, exception.getHttpStatus());
        assertEquals(null, exception.getMessage());
        assertEquals(null, exception.getDetails());
        assertEquals(null, exception.getUserFriendly());
        assertEquals(null, exception.getCause());
    }

}
