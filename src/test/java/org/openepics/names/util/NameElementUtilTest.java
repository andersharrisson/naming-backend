/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.openepics.names.repository.model.Name;
import org.openepics.names.rest.beans.NameElement;
import org.openepics.names.rest.beans.Status;

/**
 * Unit tests for NameElementUtil class.
 *
 * @author Lars Johansson
 *
 * @see NameElementUtil
 */
public class NameElementUtilTest {

    /**
     * Test of get name element for name.
     */
    @Test
    public void getNameElement() {
        NameElement nameElement = NameElementUtil.getNameElement(null);
        assertNull(nameElement);

        Name name = new Name();
        nameElement = NameElementUtil.getNameElement(name);

        assertNotNull(nameElement);
        assertEquals(null, nameElement.getUuid());
        assertEquals(null, nameElement.getSystemgroup());
        assertEquals(null, nameElement.getSystem());
        assertEquals(null, nameElement.getSubsystem());
        assertEquals(null, nameElement.getDevicetype());
        assertEquals(null, nameElement.getSystemstructure());
        assertEquals(null, nameElement.getDevicestructure());
        assertEquals(null, nameElement.getIndex());
        assertEquals(null, nameElement.getName());
        assertEquals(null, nameElement.getDescription());
        assertEquals(Status.APPROVED, nameElement.getStatus());
        assertEquals(null, nameElement.isLatest());
        assertEquals(null, nameElement.isDeleted());
        assertEquals(null, nameElement.getWhen());
        assertEquals(null, nameElement.getWho());
        assertEquals(null, nameElement.getComment());
    }

    /**
     * Test of get name element for content.
     */
    @Test
    public void getNameElementContent() {
        NameElement nameElement = NameElementUtil.getNameElement(
                null,
                null, null, null, null,
                null, null,
                null, null,
                null, null, null, null,
                null, null, null);

        assertNotNull(nameElement);
        assertEquals(null, nameElement.getUuid());
        assertEquals(null, nameElement.getSystemgroup());
        assertEquals(null, nameElement.getSystem());
        assertEquals(null, nameElement.getSubsystem());
        assertEquals(null, nameElement.getDevicetype());
        assertEquals(null, nameElement.getSystemstructure());
        assertEquals(null, nameElement.getDevicestructure());
        assertEquals(null, nameElement.getIndex());
        assertEquals(null, nameElement.getName());
        assertEquals(null, nameElement.getDescription());
        assertEquals(null, nameElement.getStatus());
        assertEquals(null, nameElement.isLatest());
        assertEquals(null, nameElement.isDeleted());
        assertEquals(null, nameElement.getWhen());
        assertEquals(null, nameElement.getWho());
        assertEquals(null, nameElement.getComment());
    }

}
