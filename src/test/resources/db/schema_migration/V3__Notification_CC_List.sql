CREATE TABLE user_notification (
	id bigserial NOT NULL,
	notification_type varchar(32) NOT NULL,
	user_login_name varchar(32) NOT NULL,
	CONSTRAINT user_notification_pk PRIMARY KEY (id)
);