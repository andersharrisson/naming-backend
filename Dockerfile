# ------------------------------------------------------------------------------
# Copyright (C) 2021 European Spallation Source ERIC.
# ------------------------------------------------------------------------------

FROM openjdk:17

# deployment unit
COPY target/naming-backend-*.jar /naming/naming-backend.jar

CMD ["java", "-jar", "/naming/naming-backend.jar", "--spring.config.name=application-docker"]
